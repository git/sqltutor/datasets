/*
   This file is part of GNU Sqltutor Datasets
   Copyright (C) 2018  Free Software Foundation, Inc.
   Contributed by Ales Cepek <cepek@gnu.org>

   GNU Sqltutor is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   GNU Sqltutor is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GNU Sqltutor.  If not, see <http://www.gnu.org/licenses/>.
 */


CREATE OR REPLACE FUNCTION sqltutor.datasets_version()
RETURNS text
AS $$
DECLARE
BEGIN
   RETURN 'VERSION';
END
$$ LANGUAGE plpgsql;
