#!/bin/bash

FROM=sqltutor_data
COPY=sqltutor_data_copy
DB=sqltutor

function copytable {
cat <<EOF > copytable.tmp
CREATE TABLE IF NOT EXISTS $COPY.$1
AS TABLE $FROM.$1
EOF
psql $DB < copytable.tmp
}

psql $DB -c "CREATE SCHEMA IF NOT EXISTS $COPY;";

copytable laureati
copytable premyslovci
copytable staty
copytable twitter
copytable unesco
copytable manufacturers
copytable products
copytable zastavky
copytable linky
copytable ttmd
copytable team
copytable ttms
copytable country
copytable ttws
copytable games
copytable dopravni_letadla
copytable letecke_spolecnosti
copytable letecke_flotily
copytable pracoviste
copytable zamestnanci
copytable mzdy
copytable filmy
copytable umelci
copytable obsazeni
copytable rezie
copytable olympics
copytable athletes
copytable sports
copytable medals
copytable ioc_codes
copytable toky
copytable stanice
copytable vodocty
copytable limity_cm
copytable cleneni

copytable bbc
copytable nobel
copytable stops
copytable route
copytable actor
copytable movie
copytable casting
copytable suppliers
copytable parts
copytable shipments

copytable autor
copytable buch
copytable ba
copytable sachverz
copytable bs
copytable artikel
copytable lieferant
copytable lieferbez
copytable kunde
copytable auftrag
copytable auftrpos


rm -rf copytable.tmp
