#!/bin/bash

FROM=sqltutor_data
COPY=sqltutor_data_copy
DB=sqltutor

function comparetable {
cat <<EOF > comparetable.tmp
SELECT *
FROM $FROM.$1
EXCEPT
SELECT *
FROM $COPY.$1
UNION
SELECT *
FROM $COPY.$1
EXCEPT
SELECT *
FROM $FROM.$1;
EOF
echo "****** compare table $1"
psql $DB < comparetable.tmp
}

psql $DB -c "CREATE SCHEMA IF NOT EXISTS $COPY;";

comparetable laureati
comparetable premyslovci
comparetable staty
comparetable twitter
comparetable unesco
comparetable manufacturers
comparetable products
comparetable zastavky
comparetable linky
comparetable ttmd
comparetable team
comparetable ttms
comparetable country
comparetable ttws
comparetable games
comparetable dopravni_letadla
comparetable letecke_spolecnosti
comparetable letecke_flotily
comparetable pracoviste
comparetable zamestnanci
comparetable mzdy
comparetable filmy
comparetable umelci
comparetable obsazeni
comparetable rezie
comparetable olympics
comparetable athletes
comparetable sports
comparetable medals
comparetable ioc_codes
comparetable toky
comparetable stanice
comparetable vodocty
comparetable limity_cm
comparetable cleneni

comparetable bbc
comparetable nobel
comparetable stops
comparetable route
comparetable actor
comparetable movie
comparetable casting
comparetable suppliers
comparetable parts
comparetable shipments

comparetable autor
comparetable buch
comparetable ba
comparetable sachverz
comparetable bs
comparetable artikel
comparetable lieferant
comparetable lieferbez
comparetable kunde
comparetable auftrag
comparetable auftrpos

rm -rf comparetable.tmp

