#include <iostream>
#include <string>
#include <queue>
#include <cctype>
#include <pqxx/pqxx>

#ifdef HAVE_CONFIG_H
#include <config.h>

#define xstr(s) str(s)
#define str(s) #s

#endif

using std::cout;
using std::endl;
using std::string;

struct A {
  string dataset, answer;
  int    dataset_id {0};
  int    problem_id {0};
  int    priority   {0};

  bool operator==(const A& a) const
  {
    return dataset_id==a.dataset_id && problem_id==a.problem_id;
  }
};

std::queue<A> queue;

int main()
{
  cout << "Test table sqltutor.answers" << endl;


#ifdef HAVE_CONFIG_H
  std::string db_connection =
    " dbname="   xstr(SQLTUTOR_DATABASE)
    " host=localhost"
    " user="     xstr(SQLTUTOR_WWW_USER)
    " password=" xstr(SQLTUTOR_PASSWORD)
    ;
#else
  std::string db_connection =
    " dbname=sqltutor"
    " host=localhost"
    " user=sqlquiz"
    " password=sqlkrok"
    ;
#endif
  
  pqxx::connection connection( db_connection );
  if (!connection.is_open())
    {
      cout << "\nConnection to database failed\n\n";
      return 1;
    }

  {
    pqxx::work t(connection,  "Main  transactionm");
    t.exec("SET search_path TO sqltutor");

    pqxx::result results(t.exec(
       "SELECT dataset, ""dataset_id, problem_id, priority, answer "
       "FROM answers "
       "NATURAL JOIN datasets "
       "ORDER BY dataset_id ASC, problem_id ASC, priority ASC"));

    A d;
    for (auto a=results.begin(); a!=results.end(); ++a)
      {
        d.dataset    = a[0].as(string());
        d.dataset_id = a[1].as(int());
        d.problem_id = a[2].as(int());
        d.priority   = a[3].as(int());
        d.answer     = a[4].as(string());

        // replace closing semicolon in SQL by new-line
        while (!d.answer.empty() and
               (std::isspace(d.answer.back()) || d.answer.back()==';'))
          {
            d.answer.pop_back();
          }
        d.answer.push_back('\n');

        queue.push(d);
      }
  }
  int N = queue.size(), errors=0;

  A p, a;
  while (!queue.empty())
    {
      a = queue.front();
      queue.pop();
      cout << a.dataset    << " "
           << a.dataset_id << " "
           << a.problem_id << " "
           << a.priority   << " ";

      pqxx::work t(connection);
      t.exec("SET search_path TO sqltutor_data");

      // test answers
      try
        {
          t.exec(a.answer);
        }
      catch(pqxx::pqxx_exception& e)
        {
          errors++;
          cout << endl
               << e.base().what() << endl
               << a.answer << endl;
        }

      // test equvalents
      if (a == p)
        {
          cout << "equivalent solution ";
          // string test_equivalent_select =
          //   "SELECT * FROM (\n" + a.answer + ") AS test_select_part_1\n"
          //   " except " +
          //   "SELECT * FROM (\n" + p.answer + ") AS test_select_part2\n"
          //   " UNION ALL \n" +
          //   "SELECT * FROM (\n" + p.answer + ") AS test_select_part3\n"
          //   " EXCEPT \n"
          //   "SELECT * FROM (\n" + a.answer + ") AS test_select_part4\n"
          // ;
          // Single SQL select would fail sometimes, thus we use temp tables
          try
            {
              pqxx::result ra(t.exec("CREATE TEMP TABLE tmpa AS " + a.answer));
              pqxx::result rp(t.exec("CREATE TEMP TABLE tmpp AS " + p.answer));
              pqxx::result r1(t.exec("CREATE TEMP TABLE tmp1 AS "
                                     " SELECT * FROM tmpa "
                                     " EXCEPT "
                                     " SELECT * FROM tmpp "
                                     ));
              pqxx::result r2(t.exec("CREATE TEMP TABLE tmp2 AS "
                                     " SELECT * FROM tmpp "
                                     " EXCEPT "
                                     " SELECT * FROM tmpa "
                                     ));
              pqxx::result rr (t.exec(" SELECT * FROM tmp1 "
                                     " UNION "
                                     " SELECT * FROM tmp2 "
                                     ));
              if (rr.empty())
                {
                  cout << "OK";
                }
              else
                {
                  errors++;
                  cout << "test failed"
                       << "\n--------------------------------\n"
                       << a.answer
                       << "\n--------------------------------\n"
                       << p.answer
                       << endl;
                }
            }
          catch(pqxx::pqxx_exception& e)
            {
              errors++;
              cout << e.base().what() << endl;
              cout << "test failed"
                   << "\n--------------------------------\n"
                   << a.answer
                   << "\n--------------------------------\n"
                   << p.answer
                   << endl;
            }
        }
      cout << endl;

      p = a;
    }

  cout << "\nNumber of answers = " << N
       << "      errors = " << errors << endl;
}
