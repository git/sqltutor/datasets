cmake_minimum_required(VERSION 2.8)

project(answers)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -lpqxx -lpq")

#find_library(PQXX_LIB pqxx)
#find_library(PQ_LIB pq)

add_executable(${PROJECT_NAME} "answers.cpp")


