﻿/* This work is licensed under a Creative Commons Attribution 4.0
   International License.

   ---------------------------------------------------------------------------
   Copyright (c) 2017 - 2019 by Burkhardt Renz. All rights reserved.
   Database Wein with SQLTutor
   ---------------------------------------------------------------------------
*/

BEGIN;

CREATE SCHEMA IF NOT EXISTS sqltutor;
SET search_path TO sqltutor;

select init_dataset ('wein');
select add_ds_source('wein',2017, 'Burkhardt Renz');
select add_ds_table ('wein',1, 'Artikel', 'ArtNr, Bez, Weingut, Jahrgang, Farbe, Preis');
select add_ds_table ('wein',2, 'Lieferant','LftNr, Firma, Postfach, PLZ, Ort');
select add_ds_table ('wein',3, 'LieferBez', 'LftNr, ArtNr');
select add_ds_table ('wein',4, 'Kunde', 'KndNr, Name, Vorname, Str, PLZ, Ort');
select add_ds_table ('wein',5, 'Auftrag', 'AuftrNr, Datum, KndNr');
select add_ds_table ('wein',6, 'AuftrPos', 'AuftrNr, Anzahl, ArtNr');


/* function parameters
   -------------------
   init_dataset           (dsname text)
   sqltutor.add_ds_source (dataset text, year int, source text)
   add_ds_table           (dataset text, ord int, ds_table text, columns text)
   insert_problem         (dataset text, problem_id int, points int,
                           category text)
   insert_question        (dataset text, problem_id int, q_ord int,
                           lang char(2), question text)
   insert_answer          (dataset text, problem_id int, priority int,
                           answer text)
*/

/* The questions and answers
*/

select insert_problem ('wein', 1, 1, 'select');

select insert_question('wein', 1, 1, 'de',
$$
Verwenden Sie den SQL-Prozessor, um 39 + 3 zu berechnen.
$$);

select insert_answer('wein', 1, 1,
$$
SELECT 39 + 3
$$);

select insert_problem ('wein', 2, 1, 'select');

select insert_question('wein', 2, 1, 'de',
$$
Verwenden Sie den SQL-Prozessor, um das Datum zu ermitteln,
das 36 Tage nach dem 1. April 2017 liegt.
$$);

select insert_answer('wein', 2, 1,
$$
SELECT date '2017-04-01' + 36
$$);

select insert_problem ('wein', 3, 1, 'select');

select insert_question('wein', 3, 1, 'de',
$$
Ermitteln Sie alle Angaben zu allen Artikeln in der Tabelle Artikel.
$$);

select insert_answer('wein', 3, 1,
$$
SELECT * 
FROM Artikel
$$);

select insert_problem ('wein', 4, 2, 'select');

select insert_question('wein', 4, 1, 'de',
$$
Von welchen Weingütern sind die Artikel?
$$);

select insert_answer('wein', 4, 1,
$$
SELECT DISTINCT Weingut
FROM Artikel
$$);

select insert_problem ('wein', 5, 2, 'select');

select insert_question('wein', 5, 1, 'de',
$$
Ermitteln Sie alle Angaben zu den Rotweinen in der Tabelle Artikel.
$$);

select insert_answer('wein', 5, 1,
$$
SELECT *
FROM Artikel
WHERE Farbe = 'rot'
$$);

select insert_problem ('wein', 6, 2, 'select');

select insert_question('wein', 6, 1, 'de',
$$
Ermitteln Sie alle Angaben zu allen Weinen, die nicht rot sind.
$$);

select insert_answer('wein', 6, 1,
$$
SELECT *
FROM Artikel
WHERE Farbe <> 'rot'
$$);

select insert_problem ('wein', 7, 2, 'select');

select insert_question('wein', 7, 1, 'de',
$$
Ermitteln Sie alle Angaben zu allen Weinen, die weniger als 15 Euro kosten.
$$);

select insert_answer('wein', 7, 1,
$$
SELECT *
FROM Artikel
WHERE Preis < 15.00
$$);

select insert_problem ('wein', 8, 3, 'select');

select insert_question('wein', 8, 1, 'de',
$$
Ermitteln Sie alle Angaben zu allen Rotweinen, die weniger als 15 Euro kosten.
$$);

select insert_answer('wein', 8, 1,
$$
SELECT *
FROM Artikel
WHERE Farbe = 'rot' AND Preis < 15.00
$$);

select insert_problem ('wein', 9, 3, 'select');

select insert_question('wein', 9, 1, 'de',
$$
Ermitteln Sie alle Angaben zu Weinen, die rot sind
oder weniger als 15 Euro kosten.
$$);

select insert_answer('wein', 9, 1,
$$
SELECT *
FROM Artikel
WHERE Farbe = 'rot' OR Preis < 15.00
$$);

select insert_problem ('wein', 10, 4, 'select');

select insert_question('wein', 10, 1, 'de',
$$
Ermitteln Sie alle Angaben zu beliebigen Weißweinen oder Rotweinen,
die weniger als 15 Euro kosten.
$$);

select insert_answer('wein', 10, 1,
$$
SELECT *
FROM Artikel
WHERE Farbe = 'weiß' OR (Farbe = 'rot' AND Preis < 15.00)
$$);

select insert_problem ('wein', 11, 4, 'select');

select insert_question('wein', 11, 1, 'de',
$$
Ermitteln Sie alle Angaben zu den Rotweinen des Weinguts 'Louis Max'.
$$);

select insert_answer('wein', 11, 1,
$$
SELECT *
FROM Artikel
WHERE (Weingut, Farbe) = ('Louis Max', 'rot')
$$);

select insert_problem ('wein', 12, 4, 'select');

select insert_question('wein', 12, 1, 'de',
$$
Ermitteln Sie alle Angaben zu den Weinen aus den
Jahrgängen 2004 - 2006.
$$);

select insert_answer('wein', 12, 1,
$$
SELECT *
FROM Artikel
WHERE Jahrgang BETWEEN 2004 AND 2006
$$);

select insert_problem ('wein', 13, 4, 'select');

select insert_question('wein', 13, 1, 'de',
$$
Ermitteln Sie alle Angaben zu den Weinen der Weingüter 
'Louis Max' und 'Domaine Cazes'.
$$);

select insert_answer('wein', 13, 1,
$$
SELECT *
FROM Artikel
WHERE Weingut IN ('Louis Max', 'Domaine Cazes')
$$);

select insert_problem ('wein', 14, 4, 'select');

select insert_question('wein', 14, 1, 'de',
$$
Ermitteln Sie alle Angaben der Weine, deren Bezeichnung
die Zeichenfolge 'Château' enthält.
$$);

select insert_answer('wein', 14, 1,
$$
SELECT *
FROM Artikel
WHERE BEZ LIKE '%Château%'
$$);

select insert_problem ('wein', 15, 4, 'select');

select insert_question('wein', 15, 1, 'de',
$$
Ermitteln Sie alle Angaben der Weine, deren Bezeichnung
mit 'L' oder 'P' beginnt.
$$);

select insert_answer('wein', 15, 1,
$$
SELECT *
FROM Artikel
WHERE BEZ SIMILAR TO '(L|P)%'
$$);

select insert_problem ('wein', 16, 1, 'select');

select insert_question('wein', 16, 1, 'de',
$$
Wieviele Artikel sind im Angebot?
$$);

select insert_answer('wein', 16, 1,
$$
SELECT count(*)
FROM Artikel
$$);

select insert_problem ('wein', 17, 2, 'select');

select insert_question('wein', 17, 1, 'de',
$$
Von wieviel verschiedenen Weingütern sind die Artikel?
$$);

select insert_answer('wein', 17, 1,
$$
SELECT count(DISTINCT Weingut)
FROM Artikel
$$);

select insert_problem ('wein', 18, 4, 'select');

select insert_question('wein', 18, 1, 'de',
$$
Was kostet ein Paket mit je einem der drei Weine von 'Louis Max'?
$$);

select insert_answer('wein', 18, 1,
$$
SELECT sum(Preis)
FROM Artikel
WHERE Weingut = 'Louis Max'
$$);

select insert_problem ('wein', 19, 4, 'select');

select insert_question('wein', 19, 1, 'de',
$$
Was ist durchschnittliche Preis der Artikel im Angebot?
$$);

select insert_answer('wein', 19, 1,
$$
SELECT avg(Preis)
FROM Artikel
$$);

select insert_answer('wein', 19, 2,
$$
SELECT round (avg(Preis), 2)
FROM Artikel
$$);
select insert_problem ('wein', 20, 6, 'select');

select insert_question('wein', 20, 1, 'de',
$$
Ermitteln Sie Bezeichnung und Preis des teuersten Artikels.
$$);

select insert_answer('wein', 20, 1,
$$
SELECT Bez, Preis
FROM Artikel
WHERE Preis = (select max(Preis) from Artikel)
$$);

select insert_problem ('wein', 21, 8, 'select');

select insert_question('wein', 21, 1, 'de',
$$
Wieviele Weine pro Farbe sind im Angebot? Erstellen Sie ein Ergebnis mit
Farbe und Anzahl.
$$);

select insert_answer('wein', 21, 1,
$$
SELECT Farbe, count(ArtNr)
FROM Artikel
GROUP BY Farbe
$$);

select insert_problem ('wein', 22, 8, 'select');

select insert_question('wein', 22, 1, 'de',
$$
Wieviele Weine pro Jahrgang sind im Angebot? Erstellen Sie ein Ergebnis mit
Jahrgang und Anzahl.
$$);

select insert_answer('wein', 22, 1,
$$
SELECT Jahrgang, count(ArtNr)
FROM Artikel
GROUP BY Jahrgang
$$);

select insert_problem ('wein', 23, 10, 'select');

select insert_question('wein', 23, 1, 'de',
$$
Ermitteln Sie alle Farben von Weinen, wo in der Gruppe der jeweiligen
Farbe der Durchschnittspreis größer als 12 Euro ist.
$$);

select insert_answer('wein', 23, 1,
$$
SELECT Farbe
FROM Artikel
GROUP BY Farbe
HAVING avg(Preis) > 12.00
$$);


select insert_problem ('wein', 24, 2, 'join');

select insert_question('wein', 24, 1, 'de',
$$
Ermitteln Sie KndNr, Name von Kunde und AuftrNr, Datum und KndNr von Auftrag
von allen Kombinationen von Kunde und Auftrag, unabhängig davon, ob der
Kunde den Auftrag tatsächlich gestellt hat.
$$);

select insert_answer('wein', 24, 1,
$$
SELECT Kunde.KndNr, Kunde.Name, Auftrag.AuftrNr, Auftrag.Datum, Auftrag.KndNr a_KndNr
FROM Kunde CROSS JOIN Auftrag
$$);

select insert_answer('wein', 24, 2,
$$
SELECT Kunde.KndNr, Kunde.Name, Auftrag.AuftrNr, Auftrag.Datum, Auftrag.KndNr a_KndNr
FROM Kunde, Auftrag
$$);

select insert_problem ('wein', 25, 2, 'join');

select insert_question('wein', 25, 1, 'de',
$$
Ermitteln Sie KndNr, Name von Kunde und AuftrNr, Datum von Auftrag
von den Kombinationen von Kunde und Auftrag, bei denen der Kunde den
Auftrag tatsächlich gestellt hat.
$$);

select insert_answer('wein', 25, 1,
$$
SELECT KndNr, Name, AuftrNr, Datum
FROM Kunde JOIN Auftrag USING (KndNr)
$$);

select insert_answer('wein', 25, 2,
$$
SELECT KndNr, Name, AuftrNr, Datum
FROM Kunde NATURAL JOIN Auftrag
$$);

select insert_answer('wein', 25, 3,
$$
SELECT Kunde.KndNr, Kunde.Name, Auftrag.AuftrNr, Auftrag.Datum
FROM Kunde, Auftrag
WHERE Kunde.KndNr = Auftrag.KndNr
$$);

select insert_problem ('wein', 26, 3, 'join');

select insert_question('wein', 26, 1, 'de',
$$
Ermitteln Sie ArtNr, Bez, Weingut und Jahrgang aller Weine des Auftrags mit
der AuftrNr 1003.
$$);

select insert_answer('wein', 26, 1,
$$
SELECT ArtNr, Bez, Weingut, Jahrgang
FROM AuftrPos JOIN Artikel USING (ArtNr)
WHERE AuftrNr = 1003
$$);

select insert_problem ('wein', 27, 4, 'join');

select insert_question('wein', 27, 1, 'de',
$$
Ermitteln Sie KndNr, AuftrNr, Bez, Weingut und Jahrgang aller Weine,
die beim Auftrag 1001 bestellt wurden.
$$);

select insert_answer('wein', 27, 1,
$$
SELECT KndNr, AuftrNr, Bez, Weingut, Jahrgang
FROM Auftrag JOIN AuftrPos USING (AuftrNr)
             JOIN Artikel USING (ArtNr)
WHERE AuftrNr = 1001
$$);

select insert_problem ('wein', 28, 6, 'join');

select insert_question('wein', 28, 1, 'de',
$$
Ermitteln Sie Name und Vorname des Kunden, sowie Anzahl und Bez der Weine
des Auftrags mit der AuftrNr 1001.
$$);

select insert_answer('wein', 28, 1,
$$
SELECT Name, Vorname, Anzahl, Bez
FROM Kunde JOIN Auftrag USING (KndNr)
           JOIN AuftrPos USING (AuftrNr)
           JOIN Artikel USING (ArtNr)
WHERE AuftrNr = 1001
$$);

select insert_problem ('wein', 29, 6, 'join');

select insert_question('wein', 29, 1, 'de',
$$
Ermitteln Sie Kundennummer, Name und Vorname der Kunden, die einen
Weißwein bestellt haben.
$$);

select insert_answer('wein', 29, 1,
$$
SELECT DISTINCT KndNr, Name, Vorname
FROM Kunde JOIN Auftrag USING (KndNr)
           JOIN AuftrPos USING (AuftrNr)
           JOIN Artikel USING (ArtNr)
WHERE Farbe = 'weiß'
$$);

select insert_problem ('wein', 30, 10, 'join');

select insert_question('wein', 30, 1, 'de',
$$
Ermitteln Sie Name, Vorname und Adresse aller Kunden, die denselben Namen
und Vornamen haben, aber verschiedene Adressen.
$$);

select insert_answer('wein', 30, 1,
$$
SELECT A.Name, A.Vorname, A.Str, A.PLZ, A.Ort
FROM Kunde A CROSS JOIN Kunde B
WHERE (A.Name, A.Vorname) = (B.Name, B.Vorname)
AND (A.Str, A.PLZ, A.Ort) <> (B.Str, B.PLZ, B.Ort)
$$);

select insert_answer('wein', 30, 2,
$$
SELECT A.Name, A.Vorname, A.Str, A.PLZ, A.Ort
FROM Kunde A, Kunde B
WHERE A.Name = B.Name AND A.Vorname = B.Vorname
AND NOT (A.Str = B.Str AND A.PLZ = B.PLZ AND A.Ort = B.Ort)
$$);

select insert_problem ('wein', 31, 6, 'join');

select insert_question('wein', 31, 1, 'de',
$$
Ermitteln Sie eine Statistik der Zahl der verkauften Flaschen pro Farbe der
Weine.
$$);

select insert_answer('wein', 31, 1,
$$
SELECT Farbe, sum(Anzahl)
FROM AuftrPos JOIN Artikel USING (ArtNr)
GROUP BY Farbe
$$);

select insert_problem ('wein', 32, 10, 'join');

select insert_question('wein', 32, 1, 'de',
$$
Ermitteln Sie eine Liste der Auftragsnummern mit der Anzahl der Rotweine
im Auftrag, sofern diese größer als 10 ist.
$$);

select insert_answer('wein', 32, 1,
$$
SELECT AuftrNr, sum(Anzahl)
FROM AuftrPos JOIN Artikel USING (ArtNr)
WHERE Farbe = 'rot'
GROUP BY AuftrNr
HAVING sum(Anzahl) > 10
$$);

select insert_problem ('wein', 33, 8, 'join');

select insert_question('wein', 33, 1, 'de',
$$
Ermitteln Sie eine Liste der Auftragsnummern mit dem Warenwert des
jeweiligen Auftrags.
$$);

select insert_answer('wein', 33, 1,
$$
SELECT AuftrNr, sum(Anzahl * Preis) as "Gesamtpreis"
FROM AuftrPos JOIN Artikel USING (ArtNr)
GROUP BY AuftrNr
$$);

select insert_problem ('wein', 34, 10, 'join');

select insert_question('wein', 34, 1, 'de',
$$
Ermitteln Sie eine Liste der Kunden (KndNr, Name, Vorname) mit dem
gesamten Umsatz des Kunden aus allen seinen Aufträgen.
$$);

select insert_answer('wein', 34, 1,
$$
SELECT KndNr, Name, Vorname, sum(Anzahl * Preis) as "Gesamtumsatz"
FROM Kunde JOIN Auftrag USING (KndNr)
           JOIN AuftrPos USING (AuftrNr)
           JOIN Artikel USING (ArtNr)
GROUP BY KndNr, Name, Vorname
$$);

select insert_problem ('wein', 35, 1, 'outer join');

select insert_question('wein', 35, 1, 'de',
$$
Wieviele Datensätze hat das kartesische Produkt der Tabellen Kunde und
Auftrag?
$$);

select insert_answer('wein', 35, 1,
$$
SELECT count(*)
FROM Kunde CROSS JOIN Auftrag
$$);

select insert_problem ('wein', 36, 1, 'outer join');

select insert_question('wein', 36, 1, 'de',
$$
Wieviele Datensätze hat die Kombination der Kunden mit den Aufträgen, die sie
tatsächlich erteilt haben?
$$);

select insert_answer('wein', 36, 1,
$$
SELECT count(*)
FROM Kunde JOIN Auftrag USING (KndNr)
$$);

select insert_problem ('wein', 37, 8, 'outer join');

select insert_question('wein', 37, 1, 'de',
$$
Ermitteln Sie KndNr, Name, Vorname, Ort der Kunden sowie AuftrNr und Datum
ihrer Aufträge. Geben Sie dabei auch Kunden an, die noch keinen Auftrag
erteilt haben.
$$);

select insert_answer('wein', 37, 1,
$$
SELECT KndNr, Name, Vorname, Ort, AuftrNr, Datum
FROM Kunde LEFT OUTER JOIN Auftrag USING (KndNr)
$$);

select insert_answer('wein', 37, 2,
$$
SELECT KndNr, Name, Vorname, Ort, AuftrNr, Datum
FROM Auftrag RIGHT OUTER JOIN Kunde USING (KndNr)
$$);

select insert_problem ('wein', 38, 8, 'outer join');

select insert_question('wein', 38, 1, 'de',
$$
Ermitteln Sie KndNr, Name, Vorname, Ort der Kunden sowie AuftrNr und Datum
ihrer Aufträge. Geben Sie dabei auch Aufträge an, denen noch kein Kunde
zugeordnet ist.
$$);

select insert_answer('wein', 38, 1,
$$
SELECT KndNr, Name, Vorname, Ort, AuftrNr, Datum
FROM Kunde RIGHT OUTER JOIN Auftrag USING (KndNr)
$$);

select insert_answer('wein', 38, 2,
$$
SELECT KndNr, Name, Vorname, Ort, AuftrNr, Datum
FROM Auftrag LEFT OUTER JOIN Kunde USING (KndNr)
$$);

select insert_problem ('wein', 39, 8, 'outer join');

select insert_question('wein', 39, 1, 'de',
$$
Ermitteln Sie KndNr, Name, Vorname, Ort der Kunden sowie AuftrNr und Datum
ihrer Aufträge. Geben Sie dabei alle Kunden und alle Aufträge an, 
gegebenenfalls ohne Auftrag bzw. ohne Kunde.
$$);

select insert_answer('wein', 39, 1,
$$
SELECT KndNr, Name, Vorname, Ort, AuftrNr, Datum
FROM Kunde FULL OUTER JOIN Auftrag USING (KndNr)
$$);

select insert_problem ('wein', 40, 4, 'set operators');

select insert_question('wein', 40, 1, 'de',
$$
Ermitteln Sie eine Liste aller Namen von Kunden sowie Lieferanten (Firma).
$$);

select insert_answer('wein', 40, 1,
$$
SELECT Name FROM Kunde 
UNION
SELECT Firma as Name FROM Lieferant
$$);

select insert_problem ('wein', 41, 4, 'set operators');

select insert_question('wein', 41, 1, 'de',
$$
Ermitteln Sie alle Orte, an denen sowohl ein Kunde als auch ein
Lieferant wohnt.
$$);

select insert_answer('wein', 41, 1,
$$
SELECT Ort FROM Kunde 
INTERSECT
SELECT Ort FROM Lieferant
$$);

select insert_problem ('wein', 42, 4, 'set operators');

select insert_question('wein', 42, 1, 'de',
$$
Ermitteln Sie alle Orte, an denen ein Kunde wohnt, jedoch kein
Lieferant.
$$);

select insert_answer('wein', 42, 1,
$$
SELECT Ort FROM Kunde 
EXCEPT
SELECT Ort FROM Lieferant
$$);

select insert_problem ('wein', 43, 4, 'set operators');

select insert_question('wein', 43, 1, 'de',
$$
Ermitteln Sie alle Orte, an denen ein Lieferant wohnt, jedoch kein
Kunde.
$$);

select insert_answer('wein', 43, 1,
$$
SELECT Ort FROM Lieferant
EXCEPT
SELECT Ort FROM Kunde 
$$);

select insert_problem ('wein', 44, 4, 'nested statements');

select insert_question('wein', 44, 1, 'de',
$$
Ermitteln Sie KndNr, Name, Vorname und Ort der Kunden, die noch keinen
Auftrag erteilt haben.
$$);

select insert_answer('wein', 44, 1,
$$
SELECT KndNr, Name, Vorname, Ort
FROM Kunde
WHERE KndNr NOT IN
    (SELECT KndNr FROM Auftrag WHERE KndNr IS NOT NULL)
$$);

select insert_answer('wein', 44, 2,
$$
SELECT KndNr, Name, Vorname, Ort
FROM Kunde LEFT OUTER JOIN Auftrag USING (KndNr)
WHERE AuftrNr IS NULL
$$);

select insert_answer('wein', 44, 3,
$$
SELECT KndNr, Name, Vorname, Ort
FROM Kunde 
WHERE NOT EXISTS (SELECT * FROM Auftrag WHERE Auftrag.KndNr = Kunde.KndNr)
$$);

select insert_problem ('wein', 45, 10, 'nested statements');

select insert_question('wein', 45, 1, 'de',
$$
Ermitteln Sie Bez, Weingut, Jahrgang und Farbe des ältesten Jahrgangs jeder
Farbe.
$$);

select insert_answer('wein', 45, 1,
$$
SELECT A.Bez, A.Weingut, A.Jahrgang, A.Farbe
FROM Artikel A
WHERE A.Jahrgang =
   (SELECT min(B.Jahrgang) FROM Artikel B WHERE A.Farbe = B.Farbe)
$$);


-- /* tutorial
--  */
-- 
-- select init_tutorial('de', 'sqltutor');
-- select insert_dataset('sqltutor', 'de', 'wein');
-- 
-- moved to datasets/tutorials/de.sqltutor.sql

COMMIT;
