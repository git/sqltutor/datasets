/* This work is licensed under a Creative Commons Attribution 4.0
   International License.

   ---------------------------------------------------------------------------
   Copyright (c) 2004 - 2017 by Burkhardt Renz. All rights reserved.
   Database Literatur with SQLTutor
   $Id: lit-tutor.sql 4269 2017-12-28 07:11:52Z br $
   ---------------------------------------------------------------------------
*/

BEGIN;

CREATE SCHEMA IF NOT EXISTS sqltutor;
SET search_path TO sqltutor;

select init_dataset ('literatur');
select add_ds_source('literatur',2017, 'Burkhardt Renz');
select add_ds_table ('literatur',1, 'Autor', 'AId, Name, Vorname');
select add_ds_table ('literatur',2, 'Buch','BId, Titel, Verlag, Ort, Jahr, ISBN');
select add_ds_table ('literatur',3, 'BA', 'BId, AId');
select add_ds_table ('literatur',4, 'Sachverz', 'SId, Schlagwort');
select add_ds_table ('literatur',5, 'BS', 'BId, SId');


/* function parameters
   -------------------
   init_dataset           (dsname text)
   sqltutor.add_ds_source (dataset text, year int, source text)
   add_ds_table           (dataset text, ord int, ds_table text, columns text)
   insert_problem         (dataset text, problem_id int, points int,
                           category text)
   insert_question        (dataset text, problem_id int, q_ord int,
                           lang char(2), question text)
   insert_answer          (dataset text, problem_id int, priority int,
                           answer text)
*/

/* The questions and answers
*/

select insert_problem ('literatur', 1, 2, 'select');

select insert_question('literatur', 1, 1, 'de',
$$
Erstellen Sie eine Liste aller Bücher mit dem Schlagwort ’Datenbanken’, 
welche 2000 oder später erschienen sind. 
Die Liste soll folgende Angaben enthalten: BId, Titel, Verlag, Ort, Jahr. 
$$);

select insert_answer('literatur', 1, 1,
$$
SELECT BId, Titel, Verlag, Ort, Jahr
FROM Buch JOIN BS USING (Bid) jOIN Sachverz USING (SId)
WHERE Jahr >= '2000' AND Schlagwort = 'Datenbanken'
$$);

select insert_problem ('literatur', 2, 4, 'select');

select insert_question('literatur', 2, 1, 'de',
$$
Erstellen Sie eine Liste aller Bücher mit dem Schlagwort ’SQL’. 
Die Liste soll folgende Angaben enthalten: 
BId, Titel, Jahr, Name und Vorname der Autor(en). 
$$);

select insert_answer('literatur', 2, 1,
$$
SELECT BId, Titel, Jahr, Name, Vorname
FROM Autor JOIN BA USING (AId)
           JOIN Buch USING (BId)
           JOIN BS USING (BId)
           JOIN Sachverz USING (SID)
WHERE Schlagwort = 'SQL'
$$);

select insert_problem ('literatur', 3, 8, 'select');

select insert_question('literatur', 3, 1, 'de',
$$
Gibt es einen Autor oder eine Autorin, der oder die in der Tabelle
Autor doppelt gespeichert ist? Wenn ja, geben Sie Name und Vorname 
zusammen mit der zugehörigen AId aus!
$$);

select insert_answer('literatur', 3, 1,
$$
SELECT A.AId, A.Name, A.Vorname
FROM Autor A CROSS JOIN Autor B
WHERE (A.Name, A.Vorname) = (B.Name, B.Vorname)
  AND A.AId <> B.AId
$$);

select insert_answer('literatur', 3, 2,
$$
SELECT AId, Name, Vorname FROM autor
WHERE (Name, Vorname) IN
  (SELECT name, Vorname
   FROM Autor
   GROUP BY Name, Vorname
   HAVING count(*) > 1)
$$);

select insert_problem ('literatur', 4, 8, 'select');

select insert_question('literatur', 4, 1, 'de',
$$
Wieviele verschiedene Bücher, Autoren und Schlagworte gibt es? 
Geben Sie die Antwort als eine Tabelle (mit einer Zeile) mit 
den Spaltennamen ’Anz Buecher’, ’Anz Autoren’, ’Anz Schlagworte’ aus.
$$);

select insert_answer('literatur', 4, 1,
$$
SELECT (SELECT count (*) from Buch) as "Anz Buecher",
       (SELECT count (*) from Autor) as "Anz Autoren",
       (SELECT Count (*) from Sachverz) as "Anz Schlagworte"
$$);

select insert_problem ('literatur', 5, 4, 'select');

select insert_question('literatur', 5, 1, 'de',
$$
Von welchem Verlag sind wieviele Bücher in den gespeicherten 
Literaturdaten enthalten? Erstellen Sie eine Liste mit der Angabe des
Verlags und der Anzahl seiner Bücher. 
$$);

select insert_answer('literatur', 5, 1,
$$
SELECT Verlag, count (*) as "Anzahl Buecher" 
FROM Buch
GROUP BY Verlag
ORDER BY Verlag
$$);

select insert_problem ('literatur', 6, 4, 'select');

select insert_question('literatur', 6, 1, 'de',
$$
Erstellen Sie eine statistische Verteilung der Bücher nach Erscheinungsjahr. 
Also eine Liste mit "Jahr" und "Anz Buecher". 
$$);

select insert_answer('literatur', 6, 1,
$$
SELECT Jahr, count (*) as Anzahl
FROM Buch
GROUP BY Jahr
ORDER BY Jahr
$$);

select insert_problem ('literatur', 7, 8, 'select');

select insert_question('literatur', 7, 1, 'de',
$$
Erstellen Sie eine statistische Verteilung der Bücher nach Erscheinungsjahr. 
Geben Sie das Jahr an und den prozentualen Anteil an der Gesamtzahl der Bücher 
(gerundet auf 2 Stellen nach dem Komma).
$$);

select insert_answer('literatur', 7, 1,
$$
SELECT Jahr, round(count (*)*100.0/(SELECT count(*) FROM Buch),2)
  AS "Anteil in Proz"
FROM Buch
GROUP BY Jahr
ORDER BY Jahr
$$);


select insert_problem ('literatur', 8, 4, 'select');

select insert_question('literatur', 8, 1, 'de',
$$
Erstellen Sie eine Liste, aus welcher hervorgeht, welcher 
Autor (Name und Vorname) wieviele Bücher geschrieben hat 
(auch Mit-Autorenschaft zählt).
$$);

select insert_answer('literatur', 8, 1,
$$
SELECT Name, Vorname, count(*) AS anzahl
FROM Autor JOIN BA USING (AId)
GROUP BY Name, Vorname
ORDER BY Name, Vorname
$$);

select insert_problem ('literatur', 9, 8, 'select');

select insert_question('literatur', 9, 1, 'de',
$$
Erstellen Sie eine Liste, aus welcher hervorgeht, welcher 
Autor (AId, Name und Vorname) wieviele Bücher geschrieben hat.
Geben Sie auch die Autoren aus, die keine Bücher geschrieben
haben.
$$);

select insert_answer('literatur', 9, 1,
$$
SELECT Aid, Name, Vorname, count(BId) AS anzahl
FROM Autor LEFT OUTER JOIN BA USING (AId)
GROUP BY AId, Name, Vorname
ORDER BY Name, Vorname
$$);

select insert_problem ('literatur', 10, 4, 'select');

select insert_question('literatur', 10, 1, 'de',
$$
Erstellen Sie eine Liste aller Bücher, bei denen der Titel mit der 
Zeichenkette ’Datenbank’ beginnt. 
Die Liste soll folgende Spalten enthalten: Titel, Verlag, Ort, Jahr.
$$);

select insert_answer('literatur', 10, 1,
$$
SELECT Titel, Verlag, Ort, Jahr
FROM Buch
WHERE Titel like 'Datenbank%'
$$);

select insert_problem ('literatur', 11, 6, 'select');

select insert_question('literatur', 11, 1, 'de',
$$
Erstellen Sie eine Liste aller Bücher, bei denen der 
Name des Autors mit der Zeichenkette ’Da’ beginnt. 
Die Liste soll folgende Spalten enthalten: Titel, Jahr, 
Name und Vorname des Autors. 
$$);

select insert_answer('literatur', 11, 1,
$$
SELECT Titel, Jahr, Name, Vorname
FROM Buch JOIN BA USING (BId) JOIN Autor USING (AId)
WHERE Name LIKE 'Da%'
$$);

select insert_problem ('literatur', 12, 8, 'select');

select insert_question('literatur', 12, 1, 'de',
$$
Erstellen Sie eine Liste aller Autoren, welche mehr als 
2 Bücher geschrieben haben. Die Liste soll Name, Vorname und 
die Anzahl der Bücher enthalten.
$$);

select insert_answer('literatur', 12, 1,
$$
SELECT Name, Vorname, count(*) AS "Anz Bücher"
FROM Autor JOIN BA USING (AId)
GROUP BY Name, Vorname
HAVING count(*) > 2
$$);

select insert_problem ('literatur', 13, 8, 'select');

select insert_question('literatur', 13, 1, 'de',
$$
Erstellen Sie eine Liste aller Bücher, welche mehr als einen Autor haben. 
Die Liste soll die BId, den Titel und die ISBN und die Zahl der Autoren 
enthalten.
$$);

select insert_answer('literatur', 13, 1,
$$
SELECT BId, Titel, ISBN, count(*) AS "Anz Autoren"
FROM Buch JOIN BA USING (BId)
GROUP BY BId, Titel, ISBN
HAVING count(*) > 1
$$);

select insert_problem ('literatur', 14, 4, 'select');

select insert_question('literatur', 14, 1, 'de',
$$
Erstellen Sie eine Liste aller Bücher, welche bei einem der 
Verlage ’Springer’, ’Oldenbourg’, ’Vieweg’, ’Teubner’ erschienen sind. 
Die Liste soll folgende Angaben enthalten: 
Verlag, Jahr, Titel, BId, ISBN, Name des Autors.
$$);

select insert_answer('literatur', 14, 1,
$$
SELECT Verlag, Jahr, Titel, BId, ISBN, Name
FROM Buch JOIN BA USING (BId) JOIN Autor USING (AId)
WHERE Verlag IN ('Springer', 'Oldenbourg', 'Vieweg', 'Teubner')
ORDER BY Verlag
$$);

select insert_problem ('literatur', 15, 2, 'select');

select insert_question('literatur', 15, 1, 'de',
$$
Erstellen Sie eine Liste aller Verlage mit Sitz (Erscheinungsort) 
in München. 
$$);

select insert_answer('literatur', 15, 1,
$$
SELECT DISTINCT Verlag
FROM Buch
WHERE Ort = 'München'
$$);

select insert_problem ('literatur', 16, 12, 'select');

select insert_question('literatur', 16, 1, 'de',
$$
Erstellen Sie eine Liste aller Bücher mit Erscheinungsjahr zwischen 1995 
und 2010, welche sowohl mit den Schlagworten ’Datenbanken’ als auch 
Datenbanksystem’ beschrieben sind. 
Die Liste soll folgende Angaben enthalten: 
Name und Vorname des Autors, Titel, Verlag, Jahr.
$$);

select insert_answer('literatur', 16, 1,
$$
SELECT Name, Vorname, Titel, Verlag, Jahr
FROM Autor JOIN BA USING (AId) JOIN Buch USING (BId)
           JOIN BS USING (BId) JOIN Sachverz USING (SId)
WHERE Jahr BETWEEN '1995' AND '2010'
  AND Schlagwort = 'Datenbanksystem'
INTERSECT
SELECT Name, Vorname, Titel, Verlag, Jahr
FROM Autor JOIN BA USING (AId) JOIN Buch USING (BId)
           JOIN BS USING (BId) JOIN Sachverz USING (SId)
WHERE Jahr BETWEEN '1995' AND '2010'
  AND Schlagwort = 'Datenbanken'
$$);

select insert_problem ('literatur', 17, 4, 'select');

select insert_question('literatur', 17, 1, 'de',
$$
Wieviele Bücher gibt es, die durch mindestens ein Schlagwort beschrieben sind?
$$);

select insert_answer('literatur', 17, 1,
$$
SELECT count(DISTINCT BId) as Anzahl
FROM BS
$$);

select insert_problem ('literatur', 18, 4, 'select');

select insert_question('literatur', 18, 1, 'de',
$$
Wieviele Bücher gibt es, die durch kein Schlagwort beschrieben sind?
$$);

select insert_answer('literatur', 18, 1,
$$
SELECT count(BId) AS Anzahl
FROM Buch
WHERE BId NOT IN (SELECT BId from BS)
$$);

select insert_answer('literatur', 18, 2,
$$
SELECT 
  (SELECT count(BId) FROM Buch) - (SELECT count(DISTINCT BId) FROM BS)
  AS 	Anzahl
$$);

select insert_answer('literatur', 18, 3,
$$
SELECT count(BId) AS Anzahl
FROM Buch
WHERE NOT EXISTS (SELECT * FROM BS WHERE BS.BId = Buch.BId)
$$);

select insert_answer('literatur', 18, 4,
$$
SELECT count(Buch.BId) AS Anzahl
FROM Buch LEFT OUTER JOIN BS USING (BId)
WHERE SId IS NULl
$$);

-- /* tutorial
--  */
-- 
-- select init_tutorial('de', 'sqltutor');
-- select insert_dataset('sqltutor', 'de', 'literatur');
-- 
-- moved to datasets/tutorials/de.sqltutor.sql

COMMIT;
