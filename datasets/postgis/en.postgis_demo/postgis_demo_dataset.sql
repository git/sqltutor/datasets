SELECT sqltutor.init_dataset('postgis_demo');
SELECT sqltutor.add_ds_source('postgis_demo', 2010, 'Martin.Landa@opengeolabs.cz');
SELECT sqltutor.add_ds_table('postgis_demo', 1,  'cities',            'gid, geom, id, name, orp, region, nuts3, nuts4, popul91, popul01, popul02');
SELECT sqltutor.add_ds_table('postgis_demo', 2,  'cities_p',          'gid, geom, id, name, popul91');
SELECT sqltutor.add_ds_table('postgis_demo', 3,  'counties',          'gid, geom, name');
SELECT sqltutor.add_ds_table('postgis_demo', 4,  'cz',                'gid, geom');
SELECT sqltutor.add_ds_table('postgis_demo', 5,  'cz_boundary',       'gid, geom');
SELECT sqltutor.add_ds_table('postgis_demo', 6,  'forests',           'gid, geom');
SELECT sqltutor.add_ds_table('postgis_demo', 7,  'mstm50',            'gid, geom, name');
SELECT sqltutor.add_ds_table('postgis_demo', 8,  'railways',          'gid, geom');
SELECT sqltutor.add_ds_table('postgis_demo', 9,  'railway_stations',  'gid, geom');
SELECT sqltutor.add_ds_table('postgis_demo', 10, 'regions',           'gid, geom, name');

