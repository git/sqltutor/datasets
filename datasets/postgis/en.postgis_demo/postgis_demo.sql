BEGIN;

SET search_path TO sqltutor;

SELECT insert_problem ('postgis_demo', 1, 2, 'select');
SELECT insert_question('postgis_demo', 1, 1, 'en',
'Which city in the Czech Republic has the smallest area.
');
SELECT insert_answer  ('postgis_demo', 1, 1,
'SELECT   name
FROM     cities
ORDER BY ST_Area(geom)
LIMIT    1;
');

SELECT insert_problem ('postgis_demo', 2, 1, 'select');
SELECT insert_question('postgis_demo', 2, 1, 'en',
'Which city in the Czech Republic had in 2002 the smallest number of inhabitants.
');
SELECT insert_answer  ('postgis_demo', 2, 1,
'SELECT   name
FROM     cities
ORDER BY popul02
LIMIT    1;
');

SELECT insert_problem ('postgis_demo', 3, 1, 'select');
SELECT insert_question('postgis_demo', 3, 1, 'en',
'Which ORP in the Czech Republic had in 2002 the smallest number of inhabitants.
');
SELECT insert_answer  ('postgis_demo', 3, 1,
'SELECT   orp
FROM     cities
ORDER BY popul02
LIMIT    1;
');

SELECT insert_problem ('postgis_demo', 4, 4, 'select');
SELECT insert_question('postgis_demo', 4, 1, 'en',
'Show area (in square kilometres) of the city in the Czech Republic which had in 2002 the smallest number of inhabitants.
');
SELECT insert_answer  ('postgis_demo', 4, 1,
'SELECT   ROUND(ST_Area(geom)/1e6)
FROM     cities
ORDER BY popul02
LIMIT    1;
');

SELECT insert_problem ('postgis_demo', 5, 1, 'select');
SELECT insert_question('postgis_demo', 5, 1, 'en',
'How many cities are located in the Czech Republic?
');
SELECT insert_answer  ('postgis_demo', 5, 1,
'SELECT COUNT(*)
FROM   cities;
');

SELECT insert_problem ('postgis_demo', 6, 3, 'select');
SELECT insert_question('postgis_demo', 6, 1, 'en',
'What is the area of the Czech Republic in square kilometres?
');
SELECT insert_answer  ('postgis_demo', 6, 1,
'SELECT ROUND(SUM(ST_Area(geom))/1e6)
FROM   cities;
');

SELECT insert_problem ('postgis_demo', 7, 5, 'select');
SELECT insert_question('postgis_demo', 7, 1, 'en',
'Which cities in the Czech Republic have their area larger then city ''Brno''?
');
SELECT insert_answer  ('postgis_demo', 7, 1,
'SELECT name
FROM   cities
WHERE  ST_Area(geom) >
(
SELECT ST_Area(geom)
FROM   cities
WHERE  name = ''Brno''
);
');

SELECT insert_problem ('postgis_demo', 8, 3, 'select');
SELECT insert_question('postgis_demo', 8, 1, 'en',
'What is the average area (in square kilometres) of the cities in the Czech Republic?
');
SELECT insert_answer  ('postgis_demo', 8, 1,
'SELECT ROUND(AVG(ST_Area(geom))/1e6)
FROM  cities;
');

SELECT insert_problem ('postgis_demo', 9, 2, 'select');
SELECT insert_question('postgis_demo', 9, 1, 'en',
'Which is the smallest city in ORP ''Tabor''.
');
SELECT insert_answer  ('postgis_demo', 9, 1,
'SELECT   name
FROM     cities
WHERE    orp = ''Tabor''
ORDER BY ST_Area(geom)
LIMIT    1;
');

SELECT insert_problem ('postgis_demo', 10, 3, 'select');
SELECT insert_question('postgis_demo', 10, 1, 'en',
'Calculate the average area of the cities in ORP ''Tabor''?
');
SELECT insert_answer  ('postgis_demo', 10, 1,
'SELECT ROUND(AVG(ST_Area(geom))/1e6)
FROM   cities
WHERE  orp = ''Tabor'';
');

SELECT insert_problem ('postgis_demo', 11, 2, 'select');
SELECT insert_question('postgis_demo', 11, 1, 'en',
'How many cities in the Czech Republic starts with letter ''A''?
');
SELECT insert_answer  ('postgis_demo', 11, 1,
'SELECT COUNT(*)
FROM   cities
WHERE  name LIKE ''A%'';
');

SELECT insert_problem ('postgis_demo', 12, 2, 'select');
SELECT insert_question('postgis_demo', 12, 1, 'en',
'How many cities in the Czech Republic contains string ''Lhota'' in their names?
');
SELECT insert_answer  ('postgis_demo', 12, 1,
'SELECT COUNT(*)
FROM   cities
WHERE  name LIKE ''%Lhota%'';
');

SELECT insert_problem ('postgis_demo', 13, 2, 'select');
SELECT insert_question('postgis_demo', 13, 1, 'en',
'How many cities in the Czech Republic have exactly 10 letters (including spaces) in their names?
');
SELECT insert_answer  ('postgis_demo', 13, 1,
'SELECT COUNT(*)
FROM   cities
WHERE  name LIKE ''__________'';
');

SELECT insert_problem ('postgis_demo', 14, 2, 'select');
SELECT insert_question('postgis_demo', 14, 1, 'en',
'How many cities in ORP ''Tabor'' ends with string ''ice''?
');
SELECT insert_answer  ('postgis_demo', 14, 1,
'SELECT COUNT(*)
FROM   cities
WHERE  orp = ''Tabor''
AND    name LIKE ''%ice'';
');

SELECT insert_problem ('postgis_demo', 15, 2, 'select');
SELECT insert_question('postgis_demo', 15, 1, 'en',
'Which cities in the Czech Republic had population in 2002 between 4500 and 5000 inhabitants, and are located in ''Moravskoslezsky'' region (''MS'')?
');
SELECT insert_answer  ('postgis_demo', 15, 1,
'SELECT name
FROM   cities
WHERE  region = ''MS''
AND    popul02 BETWEEN 4500 AND 5000;
');

SELECT insert_problem ('postgis_demo', 16, 3, 'select');
SELECT insert_question('postgis_demo', 16, 1, 'en',
'Which cities in the Czech Republic have population density greater then 2000 inhabitants / square kilometres?
');
SELECT insert_answer  ('postgis_demo', 16, 1,
'SELECT name
FROM   cities
WHERE  popul02 / (ST_Area(geom)/1e6) > 2000;
');

SELECT insert_problem ('postgis_demo', 17, 3, 'select');
SELECT insert_question('postgis_demo', 17, 1, 'en',
'How many cities are in the Czech Republic where the population increased between 1991 and 2001 at least on 100%?
');
SELECT insert_answer  ('postgis_demo', 17, 1,
'SELECT COUNT(*)
FROM   cities
WHERE  (popul01 - popul91) / popul91 > 1.0;
');

SELECT insert_problem ('postgis_demo', 18, 4, 'select');
SELECT insert_question('postgis_demo', 18, 1, 'en',
'What is the area (in square kilometres) of all cities which starts with letter ''K'' in all ORPs which starts also with letter ''K''?
');
SELECT insert_answer  ('postgis_demo', 18, 1,
'SELECT ROUND(SUM(ST_Area(geom))/1e6)
FROM   cities
WHERE  orp LIKE ''K%''
AND    name LIKE ''K%'';
');

SELECT insert_problem ('postgis_demo', 19, 3, 'select');
SELECT insert_question('postgis_demo', 19, 1, 'en',
'How many cities in the Czech Republic which are not located in Stredocesky region (''ST'') had in 2002 more then 1000 inhabitants?
');
SELECT insert_answer  ('postgis_demo', 19, 1,
'SELECT COUNT(*)
FROM   cities
WHERE  region != ''ST''
AND    popul02 > 1000;
');

SELECT insert_problem ('postgis_demo', 20, 5, 'select');
SELECT insert_question('postgis_demo', 20, 1, 'en',
'Which city in the Czech Republic has the smallest ratio between length of its boundary and its area?
');
SELECT insert_answer  ('postgis_demo', 20, 1,
'SELECT   name
FROM     cities
ORDER BY ST_Perimeter(geom) / ST_Area(geom)
LIMIT    1;
');

SELECT insert_problem ('postgis_demo', 21, 5, 'select');
SELECT insert_question('postgis_demo', 21, 1, 'en',
'How many cities in the Czech Republic are crossed by the railway?
');
SELECT insert_answer  ('postgis_demo', 21, 1,
'SELECT COUNT(DISTINCT id)
FROM   cities
JOIN   railways
ON     ST_Intersects(railways.geom, cities.geom);
');

SELECT insert_problem ('postgis_demo', 22, 5, 'select');
SELECT insert_question('postgis_demo', 22, 1, 'en',
'How many railway stations in the Czech Republic are located in the forest?
');
SELECT insert_answer  ('postgis_demo', 22, 1,
'SELECT COUNT(*)
FROM   railway_stations as rs
JOIN   forests
ON     ST_Within(rs.geom, forests.geom);
');

SELECT insert_problem ('postgis_demo', 23, 7, 'select');
SELECT insert_question('postgis_demo', 23, 1, 'en',
'How many cities in the Czech Republic are located near the state border (i.e., share with the state border at least one linear segment)?
');
SELECT insert_answer  ('postgis_demo', 23, 1,
'SELECT   COUNT(id)
FROM     cities
JOIN     cz_boundary
ON       ST_Touches(cities.geom, cz_boundary.geom)
AND      ST_Dimension(ST_Intersection(cities.geom, cz_boundary.geom)) > 0;
');

SELECT insert_problem ('postgis_demo', 24, 10, 'select');
SELECT insert_question('postgis_demo', 24, 1, 'en',
'How many cities (point layer) in the Czech Republic are located more then 10km from the nearest railway?
');
SELECT insert_answer  ('postgis_demo', 24, 1,
'SELECT    COUNT(DISTINCT cities_p.gid)
FROM      cities_p
LEFT JOIN railways
ON        ST_DWithin(cities_p.geom, railways.geom, 10e3)
WHERE     railways.gid IS NULL;
');

SELECT insert_problem ('postgis_demo', 25, 7, 'select');
SELECT insert_question('postgis_demo', 25, 1, 'en',
'How many cities (point layer) are located within the map sheet TM50 ''M-33-63-A''?
');
SELECT insert_answer  ('postgis_demo', 25, 1,
'SELECT COUNT(cities_p.id)
FROM   cities_p
JOIN   mstm50
ON     mstm50.name = ''M-33-63-A''
AND    ST_Within(cities_p.geom, mstm50.geom);
');

SELECT insert_problem ('postgis_demo', 26, 6, 'select');
SELECT insert_question('postgis_demo', 26, 1, 'en',
'How many counties in the Czech Republic has at least one city (point layer) with the population greater then 50,000 inhabitants?
');
SELECT insert_answer  ('postgis_demo', 26, 1,
'SELECT   COUNT(DISTINCT counties.gid)
FROM     cities_p
JOIN     counties
ON       popul91 > 5e4
AND      ST_Within(cities_p.geom, counties.geom);
');

SELECT insert_problem ('postgis_demo', 27, 8, 'select');
SELECT insert_question('postgis_demo', 27, 1, 'en',
'How many cities in the Czech Republic are located on the border of Stredocesky (''CZ031'') and Jihocesky (''CZ021'') region (''nuts3'')?
');
SELECT insert_answer  ('postgis_demo', 27, 1,
'SELECT   COUNT(DISTINCT cities.gid)
FROM     cities
JOIN     regions
ON       regions.name IN (''CZ031'', ''CZ021'')
AND      cities.nuts3  IN (''CZ031'', ''CZ021'')
AND      ST_Touches(cities.geom, regions.geom);
');

SELECT insert_problem ('postgis_demo', 28, 6, 'select');
SELECT insert_question('postgis_demo', 28, 1, 'en',
'How many map sheets of TM50 crosses the state border of the Czech Republic?
');
SELECT insert_answer  ('postgis_demo', 28, 1,
'SELECT   COUNT(DISTINCT mstm50.gid)
FROM     mstm50
JOIN     cz
ON       ST_Overlaps(mstm50.geom, cz.geom);
');

SELECT insert_problem ('postgis_demo', 29, 7, 'select');
SELECT insert_question('postgis_demo', 29, 1, 'en',
'Which map sheets of TM50 intersects city ''Brno'' (polygon layer)?
');
SELECT insert_answer  ('postgis_demo', 29, 1,
'SELECT mstm50.name
FROM   mstm50
JOIN   cities
ON     cities.name = ''Brno''
AND    ST_Overlaps(cities.geom, mstm50.geom);
');

-- TODO
-- SELECT insert_problem ('postgis_demo', 30, 12, 'select');
-- SELECT insert_question('postgis_demo', 30, 1, 'en',
-- 'How many map sheets of TM50 overlaps territory of the Czech Republic and don''t contain any railway?
-- ');
-- SELECT insert_answer  ('postgis_demo', 30, 1,
-- 'SELECT COUNT(DISTINCT mstm50_cr.gid)
-- FROM
-- (
-- SELECT mstm50.gid AS gid, mstm50.geom AS geom
-- FROM   mstm50
-- JOIN   cities
-- ON     ST_Overlaps(mstm50.geom, cities.geom)
-- )
-- AS     mstm50_cr
-- JOIN   railways
-- ON     ST_Disjoint(railways.geom, mstm50_cr.geom);
-- ');

SELECT insert_problem ('postgis_demo', 31, 14, 'select');
SELECT insert_question('postgis_demo', 31, 1, 'en',
'How many cities in the Czech Republic are located on more then one map sheet TM50?
');
SELECT insert_answer  ('postgis_demo', 31, 1,
'SELECT   COUNT(*)
FROM
(
SELECT   COUNT(*)
FROM     cities
JOIN     mstm50
ON       ST_Overlaps(cities.geom, mstm50.geom)
GROUP BY cities.gid
HAVING   COUNT(mstm50.gid) > 1
)
AS       cities_tm50;
');

SELECT insert_problem ('postgis_demo', 32, 15, 'select');
SELECT insert_question('postgis_demo', 32, 1, 'en',
'How many cities border with (i.e., share at least one linear segment) city ''Praha''?
');
SELECT insert_answer  ('postgis_demo', 32, 1,
'SELECT COUNT(*)
FROM   cities
JOIN
(
SELECT geom
FROM   cities
WHERE  name = ''Praha''
)
AS     prague
ON     ST_Relate(cities.geom, prague.geom, ''F***1****'');
');

SELECT insert_problem ('postgis_demo', 33, 10, 'select');
SELECT insert_question('postgis_demo', 33, 1, 'en',
'How many cities share a border with city ''Praha'' (at least with one node)?
');
SELECT insert_answer  ('postgis_demo', 33, 1,
'SELECT COUNT(*)
FROM   cities
WHERE  ST_Touches(geom,
       (
SELECT geom
FROM   cities
WHERE  name = ''Praha''
       )
);
');

SELECT insert_problem ('postgis_demo', 34, 7, 'select');
SELECT insert_question('postgis_demo', 34, 1, 'en',
'Which ORP has the largest area and how much it is (in square kilometers)?
');
SELECT insert_answer  ('postgis_demo', 34, 1,
'SELECT   orp, ROUND(SUM(ST_Area(geom)/1e6)) AS area
FROM     cities
GROUP BY orp
ORDER BY area DESC
LIMIT    1;
');

SELECT insert_problem ('postgis_demo', 35, 6, 'select');
SELECT insert_question('postgis_demo', 35, 1, 'en',
'Which ORP in the Czech Republic contains the largest number of cities and how many it is?
');
SELECT insert_answer  ('postgis_demo', 35, 1,
'SELECT   orp, COUNT(*) AS ccount
FROM     cities
GROUP BY orp
ORDER BY ccount DESC
LIMIT    1;
');

SELECT insert_problem ('postgis_demo', 36, 7, 'select');
SELECT insert_question('postgis_demo', 36, 1, 'en',
'What is the area of the forest on the map sheet TM50 ''M-33-63-A'' in square kilometers?
');
SELECT insert_answer  ('postgis_demo', 36, 1,
'SELECT ROUND(SUM(ST_Area(ST_Intersection(mstm50.geom, forests.geom)))/1e6)
FROM   mstm50
JOIN   forests
ON     mstm50.name = ''M-33-63-A''
AND    ST_Intersects(mstm50.geom, forests.geom);
');

SELECT insert_problem ('postgis_demo', 37, 10, 'select');
SELECT insert_question('postgis_demo', 37, 1, 'en',
'How many percent of the area of city Praha covers the forest?
');
SELECT insert_answer  ('postgis_demo', 37, 1,
'SELECT   ROUND(((forests_area/area) * 100))
FROM
(
SELECT   SUM(ST_Area(ST_Intersection(cities.geom, forests.geom))) AS forests_area, ST_Area(cities.geom) AS area
FROM     cities
JOIN     forests
ON       name = ''Praha''
AND      ST_Intersects(cities.geom, forests.geom)
GROUP BY area
)
AS       forests_prague;
');

SELECT insert_problem ('postgis_demo', 38, 7, 'select');
SELECT insert_question('postgis_demo', 38, 1, 'en',
'What is the area of the territory (in square kilometers) within 10km from city ''Brno'' (including this city)?
');
SELECT insert_answer  ('postgis_demo', 38, 1,
'SELECT ROUND(ST_Area(ST_Buffer(geom, 1e4))/1e6)
FROM   cities
WHERE  name = ''Brno'';
');

-- TODO
-- SELECT insert_problem ('postgis_demo', 39, 8, 'select');
-- SELECT insert_question('postgis_demo', 39, 1, 'en',
-- 'How many map sheets TM50 overlaps the territory within 10km from city ''Brno'' (including this city)?
-- ');
-- SELECT insert_answer  ('postgis_demo', 39, 1,
-- 'SELECT COUNT(*)
-- FROM   (
-- SELECT ST_Buffer(geom, 1e4) as geom
-- FROM   cities
-- WHERE  name = ''Brno''
--        )
-- AS     brno
-- JOIN   mstm50
-- ON     ST_Intersects(brno.geom, mstm50.geom);
-- ');

-- TODO
-- SELECT insert_problem ('postgis_demo', 40, 9, 'select');
-- SELECT insert_question('postgis_demo', 40, 1, 'en',
-- 'How many cities (polygon layer) are completely within the territory 10km from the border of cities ''Praha''?
-- ');
-- SELECT insert_answer  ('postgis_demo', 40, 1,
-- 'SELECT COUNT(*)
-- FROM   cities
-- JOIN
-- (
-- SELECT ST_Buffer(geom, 1e4) AS geom
-- FROM   cities
-- WHERE  name = 'Praha'
-- )
-- AS     praha_buffer
-- ON     ST_Within(cities.geom, praha_buffer.geom);
-- ');

-- TODO
-- SELECT insert_problem ('postgis_demo', 41, 15, 'select');
-- SELECT insert_question('postgis_demo', 41, 1, 'en',
-- 'In which cities covers the forests more then 96 percent of the area and how many percent it is?
-- ');
-- SELECT insert_answer  ('postgis_demo', 41, 1,
-- 'SELECT   name, ROUND(100 * ratio::NUMERIC, 1) AS ratio
-- FROM
-- (
-- SELECT   cities.id, cities.name,
--          SUM(ST_Area(ST_Intersection(cities.geom, forests.geom))) /
--          AVG(ST_Area(cities.geom)) AS ratio
-- FROM     cities
-- JOIN     forests
-- ON       ST_Intersects(cities.geom, forests.geom)
-- GROUP BY cities.id, cities.name
-- )
-- AS       cities_forests
-- WHERE    ratio > 0.96
-- ORDER BY ratio DESC;
-- ');

COMMIT;
