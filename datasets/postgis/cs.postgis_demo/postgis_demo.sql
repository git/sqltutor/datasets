BEGIN;

SET search_path TO sqltutor;

SELECT insert_question('postgis_demo', 1, 1, 'cs',
'Vypište název nejmenší obce v ČR podle rozlohy.
');

SELECT insert_question('postgis_demo', 2, 1, 'cs',
'Vypište název obce s nejmenším počtem obyvatel v ČR za rok 2002.
');

SELECT insert_question('postgis_demo', 3, 1, 'cs',
'Vypište název ORP (obec s rozšířenou působností) obce s nejmenším počtem obyvatel v ČR za rok 2002.
');

SELECT insert_question('postgis_demo', 4, 1, 'cs',
'Vypište rozlohu (v km2) obce s nejmenším počtem obyvatel v ČR za rok 2002.
');

SELECT insert_question('postgis_demo', 5, 1, 'cs',
'Kolik je v ČR obcí?
');

SELECT insert_question('postgis_demo', 6, 1, 'cs',
'Jaká je rozloha ČR v km2?
');

SELECT insert_question('postgis_demo', 7, 1, 'cs',
'Které obce v ČR mají větší rozlohu než ''Brno''?
');


SELECT insert_question('postgis_demo', 8, 1, 'cs',
'Jaká je průměrná rozloha (v km2) jedné obce v ČR?
');

SELECT insert_question('postgis_demo', 9, 1, 'cs',
'Vypište název nejmenší obce v ORP (obec s rozšířenou působností) ''Tabor''.
');

SELECT insert_question('postgis_demo', 10, 1, 'cs',
'Jaká je průměrná rozloha jedné obce v ORP (obec s rozšířenou působností) ''Tabor''?
');

SELECT insert_question('postgis_demo', 11, 1, 'cs',
'Kolik obcí v ČR začíná na písmeno ''A''?
');

SELECT insert_question('postgis_demo', 12, 1, 'cs',
'Kolik obcí v ČR má v názvu ''Lhota''?
');

SELECT insert_question('postgis_demo', 13, 1, 'cs',
'Kolik obcí v ČR má název přesně o 10 písmenech (včetně mezer)?
');

SELECT insert_question('postgis_demo', 14, 1, 'cs',
'Kolik obcí v ORP (obec s rozšířenou působností) ''Tabor'' končí na ''ice''?
');

SELECT insert_question('postgis_demo', 15, 1, 'cs',
'Které obce (vypište jejich název) měli v roce 2002 mezi 4500 a 5000 obyvateli a zároveň leží v Moravskoslezském kraji?
');

SELECT insert_question('postgis_demo', 16, 1, 'cs',
'Které obce (vypište jejich název) mají hustotu obyvatel větší než 2000 obyv. / km2?
');

SELECT insert_question('postgis_demo', 17, 1, 'cs',
'Kolik je obcí v ČR, kde mezi lety 1991 a 2001 vzrostl počet obyvatel alespoň o 100%?
');

SELECT insert_question('postgis_demo', 18, 1, 'cs',
'Jaká je rozloha všech obcí (v km2) začínajících písmenem ''K'' ve všech ORP (obec s rozšířenou působností), které začínají písmenem ''K''?
');

SELECT insert_question('postgis_demo', 19, 1, 'cs',
'Kolik obcí v ČR mimo Středočeský kraj (''ST'') mělo v roce 2002 více než 1000 obyvatel?
');

SELECT insert_question('postgis_demo', 20, 1, 'cs',
'Která obec (vypište její název) v ČR má nejmenší poměr délky hranic ku ploše?
');

SELECT insert_question('postgis_demo', 21, 1, 'cs',
'Kolik obcí v ČR protíná železnice?
');

SELECT insert_question('postgis_demo', 22, 1, 'cs',
'Kolik železničních stanic v ČR se nachází v lese?
');

SELECT insert_question('postgis_demo', 23, 1, 'cs',
'Kolik obcí v ČR leží v blízkosti státní hranice (sdílí se státní hranicí aspoň jeden liniový segment)?
');

SELECT insert_question('postgis_demo', 24, 1, 'cs',
'Kolik obcí (bodová vrstva) v ČR leží dál než 10 km od nejbližší železnice?
');

SELECT insert_question('postgis_demo', 25, 1, 'cs',
'Kolik obcí (bodová vrstva) v ČR leží na mapovém listu ''M-33-63-A''?
');

SELECT insert_question('postgis_demo', 26, 1, 'cs',
'Kolik je v ČR okresů (nuts4), ve kterých je alespoň jedno město (bodová vrstva) nad 50 000 obyvatel?
');

SELECT insert_question('postgis_demo', 27, 1, 'cs',
'Kolik obcí v ČR leží na hranici Středočeského a Jihočeského kraje (nuts3)?
');

SELECT insert_question('postgis_demo', 28, 1, 'cs',
'Na kolika různých mapových listech TM50 je zobrazena státní hranice ČR?
');

SELECT insert_question('postgis_demo', 29, 1, 'cs',
'Na kterých (vypište jejich označení) mapových listech TM50 je zobrazen okres ''Brno-město'' (nuts4)?
');

-- SELECT insert_question('postgis_demo', 30, 1, 'cs',
-- 'Kolik mapových listů TM50, na kterých leží alespoň kousek území ČR, neobsahuje žadnou železnici?
-- ');

SELECT insert_question('postgis_demo', 31, 1, 'cs',
'Kolik obcí v ČR je zobrazeno na více než jednom mapovém listu TM50?
');

SELECT insert_question('postgis_demo', 32, 1, 'cs',
'S kolika obcemi sousedí (tj. sdílí aspoň jeden liniový segment) Praha svojí hranicí?
');

SELECT insert_question('postgis_demo', 33, 1, 'cs',
'S kolika obcemi sousedí Praha alespoň 1 bodem?
');

SELECT insert_question('postgis_demo', 34, 1, 'cs',
'Která ORP (obec s rozšířenou působností) má největší rozlohu a kolik to je (v km2)?
');

SELECT insert_question('postgis_demo', 35, 1, 'cs',
'Která ORP (obec s rozšířenou působností) v ČR je složena z nejvíce obcí a kolik to je?
');

SELECT insert_question('postgis_demo', 36, 1, 'cs',
'Jaká je výměra lesa na mapovém listu ''M-33-63-A'' v km2?
');

SELECT insert_question('postgis_demo', 37, 1, 'cs',
'Kolik procent Prahy pokrývá les?
');

SELECT insert_question('postgis_demo', 38, 1, 'cs',
'Jakou výměru má území (v km2) ve vzdálenosti do 10 km od Brna (včetně Brna)?
');

-- SELECT insert_question('postgis_demo', 39, 1, 'cs',
-- 'Na kolika mapových listech TM50 leží území ve vzdálenosti do 10 km od Brna (včetně Brna)?
-- ');

-- SELECT insert_question('postgis_demo', 40, 1, 'cs',
-- 'Kolik obcí (polygonová vrstva) má celou svou plochu do 10 km od hranic Prahy?
-- ');

-- SELECT insert_question('postgis_demo', 41, 1, 'cs',
-- 'Ve kterých obcích zaujímá les více než 96 procent výměry a kolik procent v těchto obcích to je?
-- ');

COMMIT;
