/* 
   This file is public domain.

   The data is a matter of public record, and comes from
   Pavel Stehule  http://www.postgres.cz/index.php/Pavel_Stehule
 */


SET search_path TO sqltutor_data;

BEGIN;

DROP TABLE IF EXISTS Pracoviste CASCADE;
DROP TABLE IF EXISTS Zamestnanci CASCADE;
DROP TABLE IF EXISTS Mzdy CASCADE;

CREATE TABLE Pracoviste(
  kod char(2) PRIMARY KEY,
  popis varchar(20) UNIQUE NOT NULL CHECK (popis <> '')
);

CREATE TABLE Zamestnanci(
  id SERIAL PRIMARY KEY,
  jmeno varchar(20) NOT NULL CHECK (jmeno <> ''),
  prijmeni varchar(20) NOT NULL CHECK (prijmeni <> ''),
  pracoviste_kod char(2) NOT NULL REFERENCES pracoviste(kod),
  vek integer NOT NULL CHECK(vek > 0)
);

CREATE TABLE Mzdy(
  id SERIAL PRIMARY KEY,
  vlozeno date NOT NULL DEFAULT(CURRENT_DATE),
  zamestnanec_id integer NOT NULL REFERENCES Zamestnanci(id),
  castka NUMERIC(8,2) NOT NULL CHECK (castka > 0.0)
);

INSERT INTO Pracoviste VALUES
('kc','Konstrukce'),
('pr','Provoz'),
('sk','Sekretariat'),
('vy','Vyroba'),
('it','Informatika');


INSERT INTO Zamestnanci(jmeno, prijmeni, pracoviste_kod, vek) VALUES
('Pavel', 'Stehule', 'it',33),
('Radek', 'Hirjak', 'kc', 32),
('Jan', 'Pytel', 'it', 31),
('Zdenek', 'Stehule', 'vy', 28),
('Lucie', 'Kubikova', 'sk', 25),
('Tomas', 'Zezula', 'sk', 45);


INSERT INTO Mzdy (id, vlozeno, zamestnanec_id, castka) VALUES
(DEFAULT, '2007-01-01', 1, 30000),
(DEFAULT, '2007-01-01', 2, 25000),
(DEFAULT, '2007-01-01', 3, 28000),
(DEFAULT, '2007-01-01', 4, 20000),
(DEFAULT, '2007-01-01', 5, 18000),
(DEFAULT, '2007-01-01', 6, 50000),

(DEFAULT, '2007-02-01', 1, 30000),
(DEFAULT, '2007-02-01', 2, 25000),
(DEFAULT, '2007-02-01', 3, 30000),
(DEFAULT, '2007-02-01', 4, 20000),
(DEFAULT, '2007-02-01', 5, 18000),
(DEFAULT, '2007-02-01', 6, 50000),

(DEFAULT, '2007-03-01', 1, 31000),
(DEFAULT, '2007-03-01', 2, 26000),
(DEFAULT, '2007-03-01', 3, 31000),
(DEFAULT, '2007-03-01', 4, 20000),
(DEFAULT, '2007-03-01', 5, 19000),
(DEFAULT, '2007-03-01', 6, 60000),

(DEFAULT, '2007-04-01', 1, 30500),
(DEFAULT, '2007-04-01', 2, 25000),
(DEFAULT, '2007-04-01', 3, 28000),
(DEFAULT, '2007-04-01', 4, 20000),
(DEFAULT, '2007-04-01', 6, 51000);

COMMIT;
