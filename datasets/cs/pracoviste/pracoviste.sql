BEGIN;

SET search_path TO sqltutor;

SELECT insert_problem ('pracoviste', 101, 1, 'select');
SELECT insert_question('pracoviste', 101, 1, 'cs', 
'Vypište jména a příjmení všech zaměstnanců
');
SELECT insert_answer  ('pracoviste', 101, 1,
'SELECT jmeno, prijmeni FROM zamestnanci;
');

SELECT insert_problem ('pracoviste', 102, 2, 'select');
SELECT insert_question('pracoviste', 102, 1, 'cs', 
'Vypište jméno, příjmení a pracoviště všech zaměstnanců
');
SELECT insert_answer  ('pracoviste', 102, 1,
'SELECT  jmeno, prijmeni, popis 
FROM    zamestnanci JOIN pracoviste
        ON kod=pracoviste_kod;
');

SELECT insert_problem ('pracoviste', 103, 2, 'join');
SELECT insert_question('pracoviste', 103, 1, 'cs', 
'Vypište jméno, příjmení, částku, datum a popis všech zaměstnanců
');
SELECT insert_answer  ('pracoviste', 103, 1,
'SELECT jmeno, prijmeni, castka, vlozeno, popis
FROM   zamestnanci JOIN pracoviste
       ON kod=pracoviste_kod
       JOIN mzdy
       ON zamestnanci.id = mzdy.zamestnanec_id;
');

SELECT insert_problem ('pracoviste', 104, 2, 'select');
SELECT insert_question('pracoviste', 104, 1, 'cs', 
'Vypište jméno, příjmení a id všech zaměstnanců, jejichž pracoviště
je ''it''.
');
SELECT insert_answer  ('pracoviste', 104, 1,
'SELECT jmeno, prijmeni, zamestnanci.id
  FROM zamestnanci
 WHERE pracoviste_kod=''it'';
');

SELECT insert_problem ('pracoviste', 105, 2, 'join');
SELECT insert_question('pracoviste', 105, 1, 'cs', 
'Vypište jméno, příjmení a částku všech zaměstnanců, jejichž měsíční
příjem je nižší než 30000
');
SELECT insert_answer  ('pracoviste', 105, 1,
'SELECT jmeno, prijmeni, castka
  FROM zamestnanci JOIN 
       mzdy
       ON zamestnanci.id=zamestnanec_id
 WHERE castka < 30000;
');

SELECT insert_problem ('pracoviste', 106, 4, 'join');
SELECT insert_question('pracoviste', 106, 1, 'cs', 
'Vypište jméno, příjmení, datum a částku všech zaměstnanců, jejichž příjem
je vyšší než 20000 a jejich příjmení je Stehule.
');
SELECT insert_answer  ('pracoviste', 106, 1,
'SELECT  jmeno, prijmeni, vlozeno, castka
FROM    zamestnanci JOIN mzdy
        ON zamestnanci.id=zamestnanec_id
WHERE   castka > 20000
        AND prijmeni=''Stehule''
ORDER BY castka;
');

SELECT insert_problem ('pracoviste', 107, 5, 'subselect');
SELECT insert_question('pracoviste', 107, 1, 'cs', 
'Vypište jméno, příjmení a částku všech zaměstnanců, kteří mají nižší
mzdu než je průměrná.
');
SELECT insert_answer  ('pracoviste', 107, 1,
'SELECT  jmeno, prijmeni, castka
FROM    zamestnanci JOIN mzdy
        ON zamestnanci.id = zamestnanec_id
WHERE   castka < ( SELECT AVG(castka) FROM mzdy );
');

SELECT insert_problem ('pracoviste', 108, 5, 'aggregate');
SELECT insert_question('pracoviste', 108, 1, 'cs', 
'Vypište, kolik zaměstnanců je na každém pracovišti
');
SELECT insert_answer  ('pracoviste', 108, 1,
'SELECT  count(id) AS zamestnanci, popis 
FROM    pracoviste LEFT JOIN zamestnanci
        ON kod=pracoviste_kod
GROUP BY popis;
');

SELECT insert_problem ('pracoviste', 109, 8, 'subselect');
SELECT insert_question('pracoviste', 109, 1, 'cs', 
'Vypište zaměstnance, kteří v dubnu nedostali výplatu
');
SELECT insert_answer  ('pracoviste', 109, 1,
'SELECT jmeno, prijmeni FROM zamestnanci
        WHERE id NOT IN
          (SELECT zamestnanec_id FROM mzdy
                   WHERE EXTRACT(month FROM vlozeno) = ''4'');
');
SELECT insert_answer  ('pracoviste', 109, 2,
'SELECT jmeno, prijmeni FROM zamestnanci
   LEFT JOIN ( SELECT * FROM mzdy
               WHERE EXTRACT( month FROM vlozeno) = ''4''
             ) AS mzdy_duben
   ON zamestnanec_id = zamestnanci.id
   WHERE castka IS NULL;
');

COMMIT;

