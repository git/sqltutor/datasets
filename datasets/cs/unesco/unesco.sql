BEGIN;

SET search_path TO sqltutor;

SELECT insert_problem ('unesco', 401, 2, 'subselect');
SELECT insert_question('unesco', 401, 1, 'cs', 
'Kolik památek je v databázi? Poznámka: některé památky jsou v databázi 
uvedeny vícekrát, protože jsou sdíleny více zeměmi.
');
SELECT insert_answer  ('unesco', 401, 1,
'SELECT count(distinct pamatka) FROM unesco;
');

SELECT insert_problem ('unesco', 402, 1, 'select');
SELECT insert_question('unesco', 402, 1, 'cs', 
'Vypište všechny památky a rok jejich zápisu do seznamu světového
dědictví UNESCO.
');
SELECT insert_answer  ('unesco', 402, 1,
'SELECT pamatka, zapis FROM unesco;
');

SELECT insert_problem ('unesco', 403, 1, 'select');
SELECT insert_question('unesco', 403, 1, 'cs', 
'Vypište památky zapsané do seznamu světového dědictví UNESCO, které
byly zapsány v roce 2007.
');
SELECT insert_answer  ('unesco', 403, 1,
'SELECT pamatka FROM unesco WHERE zapis=2007;
');

SELECT insert_problem ('unesco', 404, 1, 'select');
SELECT insert_question('unesco', 404, 1, 'cs', 
'Vypište rakouské (''Austria'') památky zapsané do seznamu
světového dědictví UNESCO.
');
SELECT insert_answer  ('unesco', 404, 1,
'SELECT pamatka FROM unesco WHERE zeme=''Austria'';
');

SELECT insert_problem ('unesco', 405, 2, 'select');
SELECT insert_question('unesco', 405, 1, 'cs', 
'Vypište německé (''Germany'') památky zapsané do seznamu
světového dědictví UNESCO v roce 2006.
');
SELECT insert_answer  ('unesco', 405, 1,
'SELECT pamatka FROM unesco WHERE zeme=''Germany'' AND zapis=2006;
');

SELECT insert_problem ('unesco', 406, 2, 'select');
SELECT insert_question('unesco', 406, 1, 'cs', 
'Vypište rok zápisu a památky zapsané do seznamu světového dědictví
UNESCO pro země ''Czech Republic'', ''Poland'' a ''Slovakia''.
');
SELECT insert_answer  ('unesco', 406, 1,
'SELECT zapis, pamatka
FROM   unesco
WHERE  zeme IN (''Czech Republic'', ''Poland'', ''Slovakia'');
');

SELECT insert_problem ('unesco', 407, 2, 'select');
SELECT insert_question('unesco', 407, 1, 'cs', 
'Vypište ze seznamy světového dědictví UNESCO státy, které mají v
názvu slovo ''United''. Každou zemi uveďte jen jednou.
');
SELECT insert_answer  ('unesco', 407, 1,
'SELECT DISTINCT zeme
FROM   unesco
WHERE  zeme LIKE ''%United%'';
');

SELECT insert_problem ('unesco', 408, 2, 'select');
SELECT insert_question('unesco', 408, 1, 'cs', 
'Vypište země a památky, které se týkají geodézie (''Geodetic'')
ze seznamu světového dědictví UNESCO.
');
SELECT insert_answer  ('unesco', 408, 1,
'SELECT zeme, pamatka
FROM   unesco
WHERE  pamatka LIKE ''%Geodetic%'';
');

SELECT insert_problem ('unesco', 409, 1, 'select');
SELECT insert_question('unesco', 409, 1, 'cs', 
'Vypište všechny kategorie památek používané v seznamu světového
dědictví UNESCO. Každou kategerorii vypište jen jednou.
');
SELECT insert_answer  ('unesco', 409, 1,
'SELECT DISTINCT kategorie FROM unesco;
');

SELECT insert_problem ('unesco', 410, 3, 'aggregate');
SELECT insert_question('unesco', 410, 1, 'cs', 
'Kolik památek v seznamu světového dědictví je zapsáno v jednotlivých
kategoriích. Pro každou kategorii uveďte počet.
');
SELECT insert_answer  ('unesco', 410, 1,
'SELECT kategorie, COUNT(distinct pamatka)
  FROM unesco
 GROUP BY kategorie;
');

SELECT insert_problem ('unesco', 411, 2, 'aggregate');
SELECT insert_question('unesco', 411, 1, 'cs', 
'Ve kterém roce byly zapsány první památky do seznamu světového dědictví 
UNESCO.
');
SELECT insert_answer  ('unesco', 411, 1,
'SELECT MIN(zapis) FROM   unesco;
');

SELECT insert_problem ('unesco', 412, 4, 'aggregate');
SELECT insert_question('unesco', 412, 1, 'cs', 
'Které země mají v seznamu světového dědictví UNESCO zapsáno deset a více 
památek? Uveďte zemi a počet památek.
');
SELECT insert_answer  ('unesco', 412, 1,
'SELECT zeme, COUNT(pamatka)
  FROM unesco
 GROUP BY zeme
HAVING COUNT(pamatka) >= 10;
');

SELECT insert_problem ('unesco', 413, 5, 'aggregate');
SELECT insert_question('unesco', 413, 1, 'cs', 
'Které země Latinské Ameriky (''Latin America'') mají v seznamu
světového dědictví UNESCO zapsáno alespoň pět památek?  Uveďte vždy
zemi a počet.
');
SELECT insert_answer  ('unesco', 413, 1,
'SELECT zeme, COUNT(pamatka)
  FROM unesco
 WHERE region=''Latin America''
 GROUP BY zeme
HAVING COUNT(pamatka) >= 5
 ORDER BY COUNT(pamatka) DESC; 
');

SELECT insert_problem ('unesco', 414, 6, 'aggregate');
SELECT insert_question('unesco', 414, 1, 'cs', 
'Která země má v seznamu světového dědictví UNESCO zapsáno nejvíce
památek a kolik?
');
SELECT insert_answer  ('unesco', 414, 1,
'SELECT zeme, COUNT(pamatka)
FROM   unesco
GROUP BY zeme
HAVING COUNT(pamatka) >= ALL (SELECT COUNT(pamatka) FROM unesco
                              GROUP BY zeme);
');

SELECT insert_problem ('unesco', 415, 7, 'aggregate');
SELECT insert_question('unesco', 415, 1, 'cs', 
'Které památky ze seznamu světového dědictví UNESCO překračují
hranice jedné země? Tj. které památky se týkají více zemí a kolika?
');
SELECT insert_answer  ('unesco', 415, 1,
'SELECT pamatka, COUNT(zeme)
FROM   unesco
GROUP BY pamatka
HAVING COUNT(zeme) > 1;
');

COMMIT;

