SELECT sqltutor.init_dataset ('letadla');
SELECT sqltutor.add_ds_source('letadla', 2007, 'http://www.letadla.info/');
SELECT sqltutor.add_ds_table ('letadla', 1, 'dopravni_letadla',    
                  'id, vyrobce, letadlo, dolet_km, kapacita, v_provozu_od');
SELECT sqltutor.add_ds_table ('letadla', 2, 'letecke_spolecnosti', 
                  'id, spolecnost, zeme, svetadil, aliance, zalozeno');
SELECT sqltutor.add_ds_table ('letadla', 3, 'letecke_flotily',     
                  'spolecnost_id, letadlo_id, pocet_letadel');
