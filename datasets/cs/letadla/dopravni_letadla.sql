/* 
   This file is public domain.

   The data is a matter of public record, and comes from
   http://www.letadla.info/   
 */


SET search_path TO sqltutor_data;

BEGIN;

UPDATE dopravni_letadla SET dolet_km=7700, kapacita=266, v_provozu_od=1974 WHERE id=1;
UPDATE dopravni_letadla SET dolet_km=9600, kapacita=280, v_provozu_od=1983 WHERE id=2;
UPDATE dopravni_letadla SET dolet_km=5950, kapacita=117, v_provozu_od=2003 WHERE id=3;
UPDATE dopravni_letadla SET dolet_km=6800, kapacita=142, v_provozu_od=1996 WHERE id=4;
UPDATE dopravni_letadla SET dolet_km=5700, kapacita=179, v_provozu_od=1988 WHERE id=5;
UPDATE dopravni_letadla SET dolet_km=5600, kapacita=220, v_provozu_od=1994 WHERE id=6;
UPDATE dopravni_letadla SET dolet_km=12500, kapacita=335, v_provozu_od=1993 WHERE id=7;
UPDATE dopravni_letadla SET dolet_km=16700, kapacita=372, v_provozu_od=1993 WHERE id=8;
UPDATE dopravni_letadla SET dolet_km=15000, kapacita=555, v_provozu_od=2007 WHERE id=9;
UPDATE dopravni_letadla SET dolet_km=2000, kapacita=132, v_provozu_od=1959 WHERE id=10;
UPDATE dopravni_letadla SET dolet_km=3600, kapacita=42, v_provozu_od=1985 WHERE id=15;
UPDATE dopravni_letadla SET dolet_km=2600, kapacita=64, v_provozu_od=1989 WHERE id=16;
UPDATE dopravni_letadla SET dolet_km=5600, kapacita=219, v_provozu_od=1957 WHERE id=26;
UPDATE dopravni_letadla SET dolet_km=3800, kapacita=117, v_provozu_od=1999 WHERE id=27;
UPDATE dopravni_letadla SET dolet_km=9800, kapacita=165, v_provozu_od=1960 WHERE id=28;
UPDATE dopravni_letadla SET dolet_km=4000, kapacita=189, v_provozu_od=1964 WHERE id=29;
UPDATE dopravni_letadla SET dolet_km=6200, kapacita=100, v_provozu_od=1968 WHERE id=30;
UPDATE dopravni_letadla SET dolet_km=14200, kapacita=568, v_provozu_od=1970 WHERE id=31;
UPDATE dopravni_letadla SET dolet_km=7200, kapacita=289, v_provozu_od=1983 WHERE id=32;
UPDATE dopravni_letadla SET dolet_km=12200, kapacita=350, v_provozu_od=1982 WHERE id=33;
UPDATE dopravni_letadla SET dolet_km=17400, kapacita=550, v_provozu_od=1995 WHERE id=34;
UPDATE dopravni_letadla SET dolet_km=16300, kapacita=330, v_provozu_od=2008 WHERE id=35;
UPDATE dopravni_letadla SET dolet_km=4000, kapacita=36, v_provozu_od=1952 WHERE id=41;
UPDATE dopravni_letadla SET dolet_km=2000, kapacita=30, v_provozu_od=1973 WHERE id=47;
UPDATE dopravni_letadla SET dolet_km=2000, kapacita=30, v_provozu_od=1973 WHERE id=48;
UPDATE dopravni_letadla SET dolet_km=3100, kapacita=50, v_provozu_od=1996 WHERE id=49;
UPDATE dopravni_letadla SET dolet_km=3100, kapacita=50, v_provozu_od=1996 WHERE id=50;
UPDATE dopravni_letadla SET dolet_km=1300, kapacita=44, v_provozu_od=1957 WHERE id=56;
UPDATE dopravni_letadla SET dolet_km=2700, kapacita=65, v_provozu_od=1969 WHERE id=57;
UPDATE dopravni_letadla SET dolet_km=2800, kapacita=50, v_provozu_od=1987 WHERE id=58;
UPDATE dopravni_letadla SET dolet_km=3400, kapacita=70, v_provozu_od=1994 WHERE id=59;
UPDATE dopravni_letadla SET dolet_km=3100, kapacita=107, v_provozu_od=1988 WHERE id=60;
UPDATE dopravni_letadla SET dolet_km=1600, kapacita=24, v_provozu_od=1953 WHERE id=61;
UPDATE dopravni_letadla SET dolet_km=6500, kapacita=122, v_provozu_od=1960 WHERE id=62;
UPDATE dopravni_letadla SET dolet_km=5600, kapacita=98, v_provozu_od=1959 WHERE id=64;
UPDATE dopravni_letadla SET dolet_km=11200, kapacita=259, v_provozu_od=1959 WHERE id=67;
UPDATE dopravni_letadla SET dolet_km=3300, kapacita=139, v_provozu_od=1965 WHERE id=68;
UPDATE dopravni_letadla SET dolet_km=10000, kapacita=380, v_provozu_od=1971 WHERE id=66;
UPDATE dopravni_letadla SET dolet_km=12600, kapacita=410, v_provozu_od=1990 WHERE id=72;
UPDATE dopravni_letadla SET dolet_km=3800, kapacita=106, v_provozu_od=1959 WHERE id=82;
UPDATE dopravni_letadla SET dolet_km=2700, kapacita=100, v_provozu_od=1956 WHERE id=83;
UPDATE dopravni_letadla SET dolet_km=2100, kapacita=44, v_provozu_od=1962 WHERE id=84;
UPDATE dopravni_letadla SET dolet_km=3500, kapacita=80, v_provozu_od=1967 WHERE id=85;
UPDATE dopravni_letadla SET dolet_km=3000, kapacita=65, v_provozu_od=1953 WHERE id=88;
UPDATE dopravni_letadla SET dolet_km=10500, kapacita=174, v_provozu_od=1964 WHERE id=87;

COMMIT;
