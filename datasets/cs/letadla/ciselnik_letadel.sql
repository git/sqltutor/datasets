/* 
   This file is public domain.

   The data is a matter of public record, and comes from
   http://www.letadla.info/   
 */


SET search_path TO sqltutor_data;

BEGIN;

DROP TABLE IF EXISTS dopravni_letadla CASCADE;

CREATE TABLE dopravni_letadla (
    id           integer  PRIMARY KEY,
    vyrobce      varchar(20)  NOT NULL,
    letadlo      varchar(20)  NOT NULL,
    dolet_km     integer,
    kapacita     integer,
    v_provozu_od integer
);


INSERT INTO dopravni_letadla (id, vyrobce, letadlo) VALUES
( 1, 'Airbus', 'A300' ),
( 2, 'Airbus', 'A310' ),
( 3, 'Airbus', 'A318' ),
( 4, 'Airbus', 'A319' ),
( 5, 'Airbus', 'A320' ),
( 6, 'Airbus', 'A321' ),
( 7, 'Airbus', 'A330' ),
( 8, 'Airbus', 'A340' ),
( 9, 'Airbus', 'A380' ),
( 10, 'Antonov', 'An-10' ),
( 11, 'Antonov', 'An-12' ),
( 12, 'Antonov', 'An-26' ),
( 13, 'Antonov', 'An-72' ),
( 14, 'Antonov', 'An-74' ),
( 15, 'ATR', 'ATR-42' ),
( 16, 'ATR', 'ATR-72' ),
( 17, 'BAe', '146-100' ),
( 18, 'BAe', '146-200' ),
( 19, 'BAe', '146-300' ),
( 20, 'BAe', '748' ),
( 21, 'BAe', 'ATP' ),
( 22, 'BAe', 'Jetstream 31' ),
( 23, 'BAe', 'RJ100' ),
( 24, 'BAe', 'RJ70' ),
( 25, 'BAe', 'RJ85' ),
( 26, 'Boeing', '707' ),
( 27, 'Boeing', '717' ),
( 28, 'Boeing', '720' ),
( 29, 'Boeing', '727' ),
( 30, 'Boeing', '737' ),
( 31, 'Boeing', '747' ),
( 32, 'Boeing', '757' ),
( 33, 'Boeing', '767' ),
( 34, 'Boeing', '777' ),
( 35, 'Boeing', '787' ),
( 36, 'Bombardier', 'CRJ-100' ),
( 37, 'Bombardier', 'CRJ-200' ),
( 38, 'Bombardier', 'CRJ-700' ),
( 39, 'Bombardier', 'CRJ-900' ),
( 40, 'Convair', '580' ),
( 41, 'De Havilland', 'Comet' ),
( 42, 'De Havilland', 'DHC-7' ),
( 43, 'De Havilland', 'DHC-8' ),
( 44, 'Dornier', '328' ),
( 45, 'Dornier', '328JET' ),
( 46, 'Dornier', 'Do-228' ),
( 47, 'Embraer', 'EMB-110' ),
( 48, 'Embraer', 'EMB-120' ),
( 49, 'Embraer', 'ERJ-135' ),
( 50, 'Embraer', 'ERJ-145' ),
( 51, 'Embraer', 'ERJ-170' ),
( 52, 'Embraer', 'ERJ-190' ),
( 53, 'Fairchild', 'Metro 23' ),
( 54, 'Fairchild', 'Metro I' ),
( 55, 'Fairchild', 'Metro III' ),
( 56, 'Fokker', 'F-27' ),
( 57, 'Fokker', 'F-28' ),
( 58, 'Fokker', 'F-50' ),
( 59, 'Fokker', 'F-70' ),
( 60, 'Fokker', 'F-100' ),
( 61, 'Ilyushin', 'Il-14' ),
( 62, 'Ilyushin', 'Il-18' ),
( 63, 'Ilyushin', 'Il-76T' ),
( 64, 'Lockheed', 'Electra' ),
( 65, 'Lockheed', 'Hercules' ),
( 66, 'McDonnell Douglas', 'Douglas DC-10' ),
( 67, 'McDonnell Douglas', 'Douglas DC-8' ),
( 68, 'McDonnell Douglas', 'Douglas DC-9' ),
( 69, 'McDonnell Douglas', 'MD-11' ),
( 70, 'McDonnell Douglas', 'MD-11CF' ),
( 71, 'McDonnell Douglas', 'MD-11ER' ),
( 72, 'McDonnell Douglas', 'MD-11F' ),
( 73, 'McDonnell Douglas', 'MD-82' ),
( 74, 'McDonnell Douglas', 'MD-83' ),
( 75, 'McDonnell Douglas', 'MD-87' ),
( 76, 'McDonnell Douglas', 'MD-88' ),
( 77, 'McDonnell Douglas', 'MD-90' ),
( 78, 'Raytheon Beech', '1900D' ),
( 79, 'Saab', 'Saab 2000' ),
( 80, 'Saab', 'Saab 340' ),
( 81, 'Short', 'Skyvan' ),
( 82, 'Sud Aviation', 'Caravelle' ),
( 83, 'Tupolev', 'Tu-104' ),
( 84, 'Tupolev', 'Tu-124' ),
( 85, 'Tupolev', 'Tu-134' ),
( 86, 'Tupolev', 'Tu-154M' ),
( 87, 'Vickers', 'VC-10' ),
( 88, 'Vickers', 'Viscount' ),
( 89, 'Yakovlev', 'Yak-42D' );

COMMIT;
