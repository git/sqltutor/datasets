BEGIN;

SET search_path TO sqltutor;

SELECT insert_problem ('letadla', 601, 1, 'select');
SELECT insert_question('letadla', 601, 1, 'cs', 
'Které společnosti přepravují cestující.
');
SELECT insert_answer  ('letadla', 601, 1,
'SELECT spolecnost FROM letecke_spolecnosti;
');

SELECT insert_problem ('letadla', 602, 1, 'select');
SELECT insert_question('letadla', 602, 1, 'cs', 
'Která letadla mají kapacitu větší než 300 pasažérů? Uveďte
výrobce, letadlo a kapacitu.
');
SELECT insert_answer  ('letadla', 602, 1,
'SELECT vyrobce, letadlo, kapacita 
  FROM dopravni_letadla
 WHERE kapacita > 300;
');

SELECT insert_problem ('letadla', 603, 4, 'join');
SELECT insert_question('letadla', 603, 1, 'cs', 
'Které letecké společnosti mají ve své flotile letadla Douglas DC-8?
Vypište společnost, zemi a počet letadel.
');
SELECT insert_answer  ('letadla', 603, 1,
'SELECT A.spolecnost, A.zeme, B.pocet_letadel
  FROM letecke_spolecnosti A
       JOIN 
       letecke_flotily     B  
       ON A.id = B.spolecnost_id
       JOIN 
       dopravni_letadla    C  
       ON C.id = B.letadlo_id        
 WHERE C.letadlo = ''Douglas DC-8'';
');
SELECT insert_answer  ('letadla', 603, 2,
'SELECT A.spolecnost, A.zeme, B.pocet_letadel
  FROM letecke_spolecnosti A
       JOIN 
       letecke_flotily     B  
       ON A.id = B.spolecnost_id
       JOIN 
       dopravni_letadla    C  
       ON C.id = B.letadlo_id        
          AND C.letadlo = ''Douglas DC-8'';
');

SELECT insert_problem ('letadla', 604, 4, 'subselect');
SELECT insert_question('letadla', 604, 1, 'cs', 
'Jak se jmenuje největší letadlo (tj. s největší kapacitou), kdo je
jeho výrobcem a jaká je jeho kapacita? 
');
SELECT insert_answer  ('letadla', 604, 1,
'SELECT vyrobce, letadlo, kapacita
  FROM dopravni_letadla
 WHERE kapacita = (SELECT MAX(kapacita) FROM dopravni_letadla);
');

SELECT insert_problem ('letadla', 605, 12, 'subselect');
SELECT insert_question('letadla', 605, 1, 'cs', 
'Které africké letecké společnosti nemají ve své flotile Airbusy?
Uvažujte případ, že některá společnost nevlastní žádná letadla.
');
SELECT insert_answer  ('letadla', 605, 1,
'SELECT spolecnost
  FROM letecke_spolecnosti 
 WHERE svetadil = ''Afrika'' 
   AND id NOT IN (SELECT S.id
                    FROM dopravni_letadla    AS L
                         JOIN 
                         letecke_flotily     AS F 
                         ON L.id=F.letadlo_id
                         JOIN 
                         letecke_spolecnosti AS S 
                         ON S.id=F.spolecnost_id
                   WHERE L.vyrobce = ''Airbus'');
');

SELECT insert_problem ('letadla', 606, 3, 'join');
SELECT insert_question('letadla', 606, 1, 'cs', 
'Která letadla a kolik mají společnosti ''Air Algerie'', ''Atlas Blue''
a ''Royal Air Maroc''?  Uveďte společnost, výrobce, letadlo a počet
letadel.
');
SELECT insert_answer  ('letadla', 606, 1,
'SELECT LS.spolecnost AS dopravce, DL.vyrobce AS vyrobce, 
       DL.letadlo, LF.pocet_letadel
  FROM letecke_spolecnosti LS
       JOIN 
       letecke_flotily     LF  
       ON LS.id=LF.spolecnost_id
       JOIN 
       dopravni_letadla    DL  
       ON LF.letadlo_id=DL.id
 WHERE LS.spolecnost IN (''Air Algerie'', ''Atlas Blue'', ''Royal Air Maroc'')
 ORDER BY LS.spolecnost, DL.vyrobce, DL.letadlo;
');

SELECT insert_problem ('letadla', 607, 6, 'aggregate');
SELECT insert_question('letadla', 607, 1, 'cs', 
'Které letecké společnosti mají sto a více letadel. Uveďte
společnost, zemi a celkový počet letadel dané společnosti.
');
SELECT insert_answer  ('letadla', 607, 1,
'SELECT spolecnost, zeme, SUM(pocet_letadel)
  FROM letecke_spolecnosti
       JOIN 
       letecke_flotily  
       ON id=spolecnost_id
 GROUP BY spolecnost, zeme
HAVING SUM(pocet_letadel) >= 100
 ORDER BY SUM(pocet_letadel) DESC;
');

SELECT insert_problem ('letadla', 608, 4, 'aggregate');
SELECT insert_question('letadla', 608, 1, 'cs', 
'Kolik letadel je v databázi a kolik letadel nemá v databázi
definovanou hodnotu kapacity? 
');
SELECT insert_answer  ('letadla', 608, 1,
'SELECT COUNT(*) AS celkem, COUNT(*) - COUNT(kapacita)
  FROM dopravni_letadla;
');
SELECT insert_answer  ('letadla', 608, 2,
'SELECT (SELECT COUNT(*) FROM dopravni_letadla) AS celkem,
       (SELECT COUNT(*) FROM dopravni_letadla 
                         WHERE kapacita IS NULL);    
');

SELECT insert_problem ('letadla', 609, 5, 'aggregate');
SELECT insert_question('letadla', 609, 1, 'cs', 
'Kolik je (registrováno v databázi) leteckých společností a jakou
mají celkovou přepravní kapacitu? Uveďte po jednotlivých
světadílech.
');
SELECT insert_answer  ('letadla', 609, 1,
'SELECT svetadil                     AS světadíl, 
       COUNT(DISTINCT S.spolecnost) AS počet,
       SUM(kapacita*pocet_letadel)  AS kapacita
  FROM letecke_spolecnosti S
       JOIN 
       letecke_flotily     F  
       ON S.id=F.spolecnost_id
       JOIN 
       dopravni_letadla    L  
       ON L.id=F.letadlo_id 
 GROUP BY svetadil;
');

SELECT insert_problem ('letadla', 610, 4, 'aggregate');
SELECT insert_question('letadla', 610, 1, 'cs', 
'Jaká je průměrná, maximalni a minimalni (známá) kapacita letadla?
Kolik je v databázi letadel? Průměrnou kapacitu zaokrouhlete
na celé číslo.
');
SELECT insert_answer  ('letadla', 610, 1,
'SELECT ROUND(AVG(kapacita)) AS prumer, 
       MAX(kapacita), MIN(kapacita), COUNT(*) AS celkem
  FROM dopravni_letadla;
');

SELECT insert_problem ('letadla', 611, 8, 'aggregate');
SELECT insert_question('letadla', 611, 1, 'cs', 
'Které jsou dvě největší letecké společnosti (tj. které mají
největší celkovou kapacitu všech letadel)? Poznámka: kapacita není
známa u všech letadel. Uveďte společnost a celkovou kapacitu.
');
SELECT insert_answer  ('letadla', 611, 1,
'SELECT LS.spolecnost, SUM(pocet_letadel*kapacita) AS "celkova kapacita"
  FROM letecke_spolecnosti LS
       JOIN
       letecke_flotily 
       ON spolecnost_id = LS.id
       JOIN
       dopravni_letadla DL
       ON letadlo_id = DL.id AND kapacita IS NOT NULL
 GROUP BY LS.spolecnost
 ORDER BY "celkova kapacita" desc 
 LIMIT 2;
');

SELECT insert_problem ('letadla', 612, 10, 'aggregate');
SELECT insert_question('letadla', 612,  1, 'cs', 
'U kterých společností není známa kapacita ani jednoho letadla?
Uveďte společnost, zemi a světadíl.
');
SELECT insert_answer  ('letadla', 612, 1,
'SELECT S.spolecnost, zeme, svetadil
  FROM letecke_spolecnosti   S
       LEFT JOIN 
       letecke_flotily  F  
       ON S.id=F.spolecnost_id
       LEFT JOIN 
       dopravni_letadla L  
       ON L.id=F.letadlo_id
 GROUP BY S.spolecnost, zeme, svetadil
HAVING SUM(kapacita) IS NULL
 ORDER BY svetadil, zeme, S.spolecnost;
');
SELECT insert_answer  ('letadla', 612, 2,
'SELECT spolecnost, zeme, svetadil
  FROM letecke_spolecnosti
 WHERE letecke_spolecnosti.id NOT IN (
          SELECT distinct letecke_spolecnosti.id
            FROM letecke_spolecnosti
                 JOIN letecke_flotily
                 ON letecke_flotily.spolecnost_id = letecke_spolecnosti.id
                 JOIN dopravni_letadla
                 ON letecke_flotily.letadlo_id = dopravni_letadla.id
           WHERE kapacita IS NOT null)
');

SELECT insert_problem ('letadla', 613, 4, 'aggregate');
SELECT insert_question('letadla', 613, 1, 'cs', 
'Kolik letadel je registrováno v databázi pro jednotlivé
výrobce. Počítejte pouze letadla u kterých je známa kapacita.
Výrobce, kteří nemají registrováno ani jedno letadlo se známou
kapacitou neuvádějte.
');
SELECT insert_answer  ('letadla', 613, 1,
'SELECT vyrobce, COUNT(kapacita)
  FROM dopravni_letadla
 GROUP BY vyrobce
HAVING COUNT(kapacita) > 0;
');

SELECT insert_problem ('letadla', 614, 3, 'select');
SELECT insert_question('letadla', 614, 1, 'cs', 
'Uveďte seznam všech alianci a společnosti, které je tvoří.
');
SELECT insert_answer  ('letadla', 614, 1,
'SELECT aliance, spolecnost
  FROM letecke_spolecnosti
 WHERE aliance IS NOT NULL
 ORDER BY aliance, spolecnost;
');

SELECT insert_problem ('letadla', 615, 5, 'aggregate');
SELECT insert_question('letadla', 615, 1, 'cs', 
'Která aliance má největší celkovou přepravní kapacitu? 
Uveďte její název kapacitu.
');
SELECT insert_answer  ('letadla', 615, 1,
'SELECT aliance, SUM(kapacita*pocet_letadel)
  FROM letecke_spolecnosti S
       JOIN 
       letecke_flotily     F 
       ON S.id=F.spolecnost_id
       JOIN 
       dopravni_letadla    L 
       ON L.id=F.letadlo_id
 WHERE aliance IS NOT NULL
 GROUP BY aliance
 ORDER BY SUM(kapacita*pocet_letadel) DESC
 LIMIT 1;
');

SELECT insert_problem ('letadla', 616, 9, 'subselect');
SELECT insert_question('letadla', 616, 1, 'cs', 
'Které společnosti mají nejstarší letadla (letadlo, jehož typ je
nejdéle v provozu)?  Uveďte leteckou společnost, výrobce, letadlo a
rok.
');
SELECT insert_answer  ('letadla', 616, 1,
'SELECT S.spolecnost AS dopravce, L.vyrobce AS vyrobce, 
       letadlo, v_provozu_od AS rok
  FROM dopravni_letadla L
       JOIN 
       letecke_flotily  F 
       ON F.letadlo_id=L.id
       JOIN 
       letecke_spolecnosti S 
       ON S.id=F.spolecnost_id
 WHERE L.v_provozu_od = (SELECT MIN(v_provozu_od)
                           FROM dopravni_letadla
                                JOIN letecke_flotily 
                                ON id=letadlo_id);
');

SELECT insert_problem ('letadla', 617, 12, 'subselect');
SELECT insert_question('letadla', 617, 1, 'cs', 
' Pro každou australskou leteckou společnost najděte její letadlo s
 největší kapacitou. Uveďte leteckou společnost, výrobce,
 letadlo a kapacitu.
');
SELECT insert_answer  ('letadla', 617, 1,
'SELECT spolecnost, vyrobce, letadlo, kapacita
FROM   letecke_spolecnosti LS
       JOIN letecke_flotily LF ON LS.id=LF.spolecnost_id
       JOIN dopravni_letadla DL ON LF.letadlo_id = DL.id
WHERE  svetadil=''Austrálie''
AND    (spolecnost, kapacita) IN (
SELECT spolecnost, max(kapacita)
FROM   letecke_spolecnosti LS
       join letecke_flotily LF ON LS.id=LF.spolecnost_id
       join dopravni_letadla DL ON LF.letadlo_id = DL.id
       WHERE  svetadil=''Austrálie''
GROUP by spolecnost
)
ORDER BY spolecnost, vyrobce;
');
SELECT insert_answer  ('letadla', 617, 2,
'SELECT DISTINCT LS.spolecnost AS "Dopravce", DL.vyrobce AS "Výrobce",
       letadlo, kapacita
  FROM letecke_spolecnosti  LS
       JOIN 
       letecke_flotily      LF 
       ON LS.id=LF.spolecnost_id
       JOIN 
       dopravni_letadla     DL 
       ON DL.id=LF.letadlo_id
          AND svetadil=''Austrálie''
          AND kapacita >= (SELECT MAX(kapacita)
                             FROM letecke_spolecnosti S
                                  JOIN
                                  letecke_flotily     F
                                  ON S.id = F.spolecnost_id
                                  JOIN
                                  dopravni_letadla    L
                                  ON L.id = F.letadlo_id
                                     AND LS.spolecnost = S.spolecnost);
');

SELECT insert_problem ('letadla', 618, 9, 'subselect');
SELECT insert_question('letadla', 618, 1, 'cs', 
'Které typy letadel mají společné letecké společnosti ''AirNorth
Regional'' a ''Skippers Aviation''. Uveďte výrobce a typ letadla.
');
SELECT insert_answer  ('letadla', 618, 1,
'SELECT vyrobce, letadlo                 -- prunik dvou mnozin
  FROM letecke_spolecnosti S
       JOIN 
       letecke_flotily     F 
       ON S.id=F.spolecnost_id
       JOIN 
       dopravni_letadla    L 
       ON L.id=F.letadlo_id
 WHERE S.spolecnost=''AirNorth Regional''
INTERSECT
SELECT vyrobce, letadlo 
  FROM letecke_spolecnosti S
       JOIN 
       letecke_flotily     F 
       ON S.id=F.spolecnost_id
       JOIN 
       dopravni_letadla    L 
       ON L.id=F.letadlo_id
 WHERE S.spolecnost=''Skippers Aviation'';
');
SELECT insert_answer  ('letadla', 618, 2,
'SELECT vyrobce, letadlo                 -- join derivovanych tabulek
  FROM (
        SELECT vyrobce, letadlo 
          FROM letecke_spolecnosti S
               JOIN 
               letecke_flotily     F 
               ON S.id=F.spolecnost_id
               JOIN 
               dopravni_letadla    L 
               ON L.id=F.letadlo_id
         WHERE S.spolecnost=''AirNorth Regional''
       ) AS AirNorth
       NATURAL JOIN
       (
        SELECT vyrobce, letadlo 
          FROM letecke_spolecnosti S
               JOIN 
               letecke_flotily     F 
               ON S.id=F.spolecnost_id
               JOIN 
               dopravni_letadla    L 
               ON L.id=F.letadlo_id
         WHERE S.spolecnost=''Skippers Aviation''
       ) AS Skippers;
');
SELECT insert_answer  ('letadla', 618, 3,
'SELECT vyrobce, letadlo                 -- trik pro pripad dvou spolecnosti
  FROM letecke_spolecnosti S
       JOIN 
       letecke_flotily     F 
       ON S.id=F.spolecnost_id
       JOIN 
       dopravni_letadla    L 
       ON L.id=F.letadlo_id
 WHERE S.spolecnost=''AirNorth Regional''
       AND L.id IN (SELECT L.id
                      FROM letecke_spolecnosti S
                           JOIN 
                           letecke_flotily     F 
                           ON S.id=F.spolecnost_id
                           JOIN 
                           dopravni_letadla    L 
                           ON L.id=F.letadlo_id 
                     WHERE S.spolecnost=''Skippers Aviation'');
');

SELECT insert_problem ('letadla', 619, 2, 'subselect');
SELECT insert_question('letadla', 619, 1, 'cs', 
'Které typy letadel byly uvedeny do provozu v letech 1993 až 1995
Uveďte vyrobce, typ letadla a rok.
');
SELECT insert_answer  ('letadla', 619, 1,
'SELECT vyrobce, letadlo, v_provozu_od
  FROM dopravni_letadla
 WHERE v_provozu_od BETWEEN 1993 AND 1995;
');

SELECT insert_problem ('letadla', 620, 5, 'subselect');
SELECT insert_question('letadla', 620, 1, 'cs', 
'Které evropské společnosti mají ve své flotile letadla, která byla
uvedena do provozu v letech 1993 až 1995? Uveďte společnost,
výrobce, typ letadla a rok.
');
SELECT insert_answer  ('letadla', 620, 1,
'SELECT DISTINCT S.spolecnost AS dopravce, L.vyrobce AS vyrobce, 
       letadlo, v_provozu_od
  FROM letecke_spolecnosti S
       JOIN 
       letecke_flotily     F 
       ON S.id=F.spolecnost_id
       JOIN 
       dopravni_letadla    L 
       ON L.id=F.letadlo_id
 WHERE svetadil=''Evropa''
       AND L.id IN (SELECT id
                      FROM dopravni_letadla
                     WHERE v_provozu_od BETWEEN 1993 AND 1995)
 ORDER BY S.spolecnost, v_provozu_od;
');

SELECT insert_problem ('letadla', 621, 13, 'leftjoin');
SELECT insert_question('letadla', 621, 1, 'cs', 
'Uveďte všechny německé letecké společnosti a kolik mají Airbusů,
u společností které Airbusy nemají uvádějte nulu - použijte funkci coalesce().
');
SELECT insert_answer ('letadla', 621,1,
$$WITH
  nemecke_spolecnosti (spolecnost_id, spolecnost) AS (
     SELECT id, spolecnost
     FROM   letecke_spolecnosti
     WHERE  zeme='Německo'
  ),
  airbusy (spolecnost_id, pocet) AS (
     SELECT spolecnost_id, pocet_letadel
     FROM   letecke_flotily
            JOIN dopravni_letadla ON letadlo_id=id
     WHERE  vyrobce='Airbus'
)
SELECT
   spolecnost, COALESCE(SUM(pocet),0)
FROM
   nemecke_spolecnosti
   LEFT JOIN airbusy USING (spolecnost_id)
GROUP BY spolecnost_id, spolecnost;
$$);

SELECT insert_answer ('letadla', 621,2,
$$SELECT spolecnost, coalesce(sum(pocet_letadel),0)
  FROM (SELECT id, spolecnost
          FROM letecke_spolecnosti
         WHERE zeme='Německo') AS N
       LEFT JOIN
       (SELECT spolecnost_id AS id, pocet_letadel
          FROM letecke_flotily
               JOIN 
               dopravni_letadla 
               ON letadlo_id = id
         WHERE vyrobce='Airbus') AS A
       ON N.id=A.id
 GROUP BY spolecnost;
$$);

SELECT insert_answer ('letadla', 621,3,
$$SELECT spolecnost, coalesce(sum(x.pocet_letadel),0) 
  FROM letecke_spolecnosti
       LEFT JOIN
       (
       SELECT letecke_flotily.spolecnost_id, pocet_letadel
       FROM letecke_flotily
       JOIN dopravni_letadla
            ON letecke_flotily.letadlo_id = dopravni_letadla.id
       WHERE vyrobce = 'Airbus'
       ) AS x
      ON  x.spolecnost_id = letecke_spolecnosti.id
WHERE zeme = 'Německo'
GROUP BY spolecnost;
$$);

SELECT insert_problem ('letadla', 622, 2, 'select');
SELECT insert_question('letadla', 622, 1, 'cs', 
'Které společnosti mají v názvu ''International''? Uveďte společnost,
zemi a světadíl.
');
SELECT insert_answer  ('letadla', 622, 1,
'SELECT spolecnost, zeme, svetadil
  FROM letecke_spolecnosti 
 WHERE spolecnost LIKE ''%International%'';
');

SELECT insert_problem ('letadla', 623, 4, 'subselect');
SELECT insert_question('letadla', 623, 1, 'cs', 
'Která je nejstarší letecká společnost? Uveďte společnost, zemi a
rok založení. Poznámka: není známo, jestli v daném roce zahájila
činnost jedna nebo více společností.
');
SELECT insert_answer  ('letadla', 623, 1,
'SELECT spolecnost, zeme, zalozeno
  FROM letecke_spolecnosti
 WHERE zalozeno = (SELECT MIN(zalozeno) FROM letecke_spolecnosti);
');

SELECT insert_problem ('letadla', 624, 4, 'subselect');
SELECT insert_question('letadla', 624, 1, 'cs', 
'Najděte nejmladší letecké společnosti Uveďte společnost, zemi a rok
založení.
');
SELECT insert_answer  ('letadla', 624, 1,
'SELECT spolecnost, zeme, zalozeno
  FROM letecke_spolecnosti
 WHERE zalozeno = (SELECT MAX(zalozeno) from letecke_spolecnosti);
');

SELECT insert_problem ('letadla', 625, 4, 'aggregate');
SELECT insert_question('letadla', 625, 1, 'cs', 
'Pro každého výrobce určete počet typů jeho letadel.
');
SELECT insert_answer  ('letadla', 625, 1,
'SELECT vyrobce, COUNT(letadlo) AS letadla
  FROM dopravni_letadla
 GROUP BY vyrobce
 ORDER BY COUNT(letadlo) DESC;
');

SELECT insert_problem ('letadla', 626, 6, 'subselect');
SELECT insert_question('letadla', 626, 1, 'cs', 
'Která letadla registrovaná v databázi nepoužívá žádná společnost?
Uveďte výrobce, letadlo a rok, ve kterém bylo letadlo uvedeno do
provozu.
');
SELECT insert_answer  ('letadla', 626, 1,
'SELECT vyrobce, letadlo, v_provozu_od
  FROM dopravni_letadla
 WHERE id NOT IN (SELECT letadlo_id FROM letecke_flotily);
');
SELECT insert_answer  ('letadla', 626, 2,
'SELECT vyrobce, letadlo, v_provozu_od
  FROM dopravni_letadla
 WHERE id NOT IN (SELECT DISTINCT letadlo_id FROM letecke_flotily);
');

SELECT insert_problem ('letadla', 627, 4, 'subselect');
SELECT insert_question('letadla', 627, 1, 'cs', 
'Které letadlo má největší dolet? Uveďte výrobce, letadlo a dolet.
');
SELECT insert_answer  ('letadla', 627, 1,
'SELECT vyrobce, letadlo, dolet_km
  FROM dopravni_letadla
 WHERE dolet_km = (SELECT MAX(dolet_km) FROM dopravni_letadla);
');
SELECT insert_answer  ('letadla', 627, 2,
'SELECT vyrobce, letadlo, dolet_km
  FROM dopravni_letadla
 WHERE dolet_km >= ALL (SELECT dolet_km 
                          FROM dopravni_letadla
                         WHERE dolet_km IS NOT NULL);
');
SELECT insert_answer  ('letadla', 627, 3,
'SELECT vyrobce, letadlo, MAX(dolet_km) AS dolet
 FROM dopravni_letadla
WHERE dolet_km IS NOT NULL
GROUP BY vyrobce, letadlo 
ORDER BY dolet DESC
LIMIT 1;
');

SELECT insert_problem ('letadla', 628, 2, 'select');
SELECT insert_question('letadla', 628, 1, 'cs', 
'Která letadla mají dolet větší než 10000 km? Uveďte výrobce,
letadlo, kapacitu, rok uvedení do provozu a dolet.
');
SELECT insert_answer  ('letadla', 628, 1,
'SELECT vyrobce, letadlo, kapacita, v_provozu_od, dolet_km
  FROM dopravni_letadla
 WHERE dolet_km > 10000
 ORDER BY dolet_km DESC;
');

COMMIT;

