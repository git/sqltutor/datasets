SELECT sqltutor.init_dataset ('vodocty');
SELECT sqltutor.add_ds_source('vodocty', 2007, 
                              'http://hydro.chmi.cz/hpps/op_list.php');
SELECT sqltutor.add_ds_table ('vodocty', 1, 'toky',      'id, jmeno');
SELECT sqltutor.add_ds_table ('vodocty', 2, 'stanice',   'id, nazev');
SELECT sqltutor.add_ds_table ('vodocty', 3, 'vodocty',   
                              'tok_id, stanice_id, cas, vodocet_cm');
SELECT sqltutor.add_ds_table ('vodocty', 4, 'limity_cm', 
                      'tok_id, stanice_id, bdelost, pohotovost, ohrozeni');
SELECT sqltutor.add_ds_table ('vodocty', 5, 'cleneni',   
                              'tok_id, stanice_id, kraj, pobocka, povodi');

