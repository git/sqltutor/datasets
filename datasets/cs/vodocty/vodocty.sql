BEGIN;

SET search_path TO sqltutor;

SELECT insert_problem ('vodocty', 801, 1, 'select');
SELECT insert_question('vodocty', 801, 1, 'cs', 
'Kolik vodních toků je v databázi?
');
SELECT insert_answer  ('vodocty', 801, 1,
'SELECT count(*) FROM toky;
');

SELECT insert_problem ('vodocty', 802, 3, 'join');
SELECT insert_question('vodocty', 802, 1, 'cs', 
'Vypište vodní toky z povodí Berounky.
');
SELECT insert_answer  ('vodocty', 802, 1,
'SELECT DISTINCT jmeno
  FROM toky
       JOIN
       cleneni
       ON id = tok_id
 WHERE povodi = ''Berounka'';
');

SELECT insert_problem ('vodocty', 803, 13, 'join|aggregate');
SELECT insert_question('vodocty', 803, 1, 'cs', 
'Jaké jsou denní průměry vodočtů na Dyji? 

Uveďte název stanice, datum a průměrnou hodnotu vodočtu na
stanici. Zaokrouhlete na celé centimetry pomocí funkce round().
');
SELECT insert_answer  ('vodocty', 803, 1,
'SELECT nazev, date(cas), round(avg(vodocet_cm))
FROM   stanice s
       JOIN vodocty v ON s.id=v.stanice_id
       JOIN toky t ON v.tok_id=t.id
WHERE  jmeno=''Dyje''
GROUP BY nazev, date(cas)
ORDER BY nazev, date(cas)
');
SELECT insert_answer  ('vodocty', 803, 2,
'SELECT nazev, date(cas) AS datum,
       round(AVG(vodocet_cm)) AS prumer
  FROM toky
       JOIN
       vodocty
       ON toky.id=tok_id
       JOIN
       stanice
       ON stanice.id=stanice_id
 WHERE tok_id = (SELECT id FROM toky WHERE jmeno=''Dyje'')
 GROUP BY nazev, DATE(cas)
 ORDER BY nazev, DATE(cas);
');

SELECT insert_problem ('vodocty', 804, 12, 'join|aggregate');
SELECT insert_question('vodocty', 804, 1, 'cs', 
'Které vodočty na Blanici překročily některý z limitů SPA (stav povodňové
aktivity platí i při dosažení rovnosti).

Uveďte název stanice, tři stupně SPA (stav povodňové aktivity),
hodnotu vodočtu a čas.
');
SELECT insert_answer  ('vodocty', 804, 1,
'SELECT stanice.nazev, bdelost, pohotovost, ohrozeni, 
       vodocet_cm, cas
  FROM vodocty
       JOIN 
       toky 
       ON vodocty.tok_id = toky.id
       JOIN
       stanice 
       ON vodocty.stanice_id = stanice.id
       NATURAL JOIN
       limity_cm
       NATURAL JOIN
       cleneni
 WHERE vodocet_cm > bdelost AND jmeno=''Blanice''
 ORDER BY povodi, toky.jmeno, stanice.nazev, cas; 
');

SELECT insert_problem ('vodocty', 805, 15, 'join|subselect');
SELECT insert_question('vodocty', 805, 1, 'cs', 
'Najděte začátek a konec dosažení limitů SPA (stav povodňové
aktivity), tj. první a poslední den ve kterém byly naměřeny
vodočty, které překročily minimálně stav bdělosti (platí i pro rovnost).
');
SELECT insert_answer  ('vodocty', 805, 1,
'SELECT min(cas), max(cas)
FROM   vodocty
       NATURAL JOIN limity_cm
WHERE  vodocet_cm >= bdelost
');
SELECT insert_answer  ('vodocty', 805, 2,
'SELECT MIN(cas) AS zacatek , MAX(cas) AS konec
  FROM (SELECT toky.jmeno, stanice.nazev, povodi,
               bdelost, pohotovost, ohrozeni,
               vodocet_cm, cas
          FROM vodocty
               JOIN 
               toky
               ON vodocty.tok_id = toky.id
               JOIN
               stanice 
               ON vodocty.stanice_id = stanice.id
               NATURAL JOIN
               limity_cm
               NATURAL JOIN
               cleneni
         WHERE vodocet_cm >= bdelost) AS prehled;
');

SELECT insert_problem ('vodocty', 806, 15, 'leftjoin|aggregate|coalesce');
SELECT insert_question('vodocty', 806, 1, 'cs',
 'Jaký je počet překročení limitů SPA za den (stav povodňové
aktivity platí i při dosažení rovnosti).
Uveďte i nulové hodnoty, tj. dny ve kterých
nebyly limity SPA překročeny.

Pozn.: pro převod typu timestamp na datum použijte funkci date(cas)
nebo konverzi cast(cas as date).
');
SELECT insert_answer  ('vodocty', 806, 1,
'SELECT cast(cas as date), 
       sum (CASE WHEN vodocet_cm >= bdelost THEN 1 ELSE 0 END)
  FROM vodocty 
       NATURAL LEFT JOIN
       limity_cm
 GROUP BY cast (cas as date);
');
SELECT insert_answer  ('vodocty', 806, 2,
'SELECT cast(cas as date), count(bdelost)
  FROM vodocty AS V
       LEFT JOIN
       limity_cm AS L
       ON V.tok_id = L.tok_id 
          AND V.stanice_id  = L.stanice_id
          AND V.vodocet_cm >= L.bdelost
 GROUP BY cast (cas as date);
');
SELECT insert_answer  ('vodocty', 806, 3,
'SELECT A.den, x+COALESCE(y,0)
  FROM (
        SELECT DISTINCT DATE(cas) AS den, 0 AS x
        FROM vodocty
       ) A
       LEFT JOIN
       (
        SELECT DATE(cas) AS den, COUNT(*) AS y
          FROM vodocty
               NATURAL JOIN
               limity_cm
         WHERE vodocet_cm >= bdelost
         GROUP BY DATE(cas)
        ) B
        ON A.den = B.den;
');

SELECT insert_problem ('vodocty', 807, 8, 'join|notin');
SELECT insert_question('vodocty', 807, 1, 'cs', 
'Které vodní toky nemají definovány limity SPA (stav povodňové
aktivity), tj. nejsou uvedeny v tabulce limity_cm. Uveďte jméno
toku a povodí a odstraňte případné duplicity.
');
SELECT insert_answer  ('vodocty', 807, 1,
'SELECT DISTINCT jmeno, povodi
  FROM toky
       JOIN
       cleneni 
       ON tok_id = toky.id
          AND tok_id NOT IN (SELECT tok_id 
                               FROM limity_cm);
');

SELECT insert_problem ('vodocty', 808, 11, 'join|case');
SELECT insert_question('vodocty', 808, 1, 'cs', 
'Vyberte měření na řece ''Osoblaha'' (stanice stejného jména). Uveďte
čas měření, hodnoty vodočtu a hodnoty SPA (NULL pokud nebyl dosažen
žádný stav povodňové aktivity, 1 pro stav bdělosti, 2 pro
pohotovost, 3 pro stav ohrožení)
');
SELECT insert_answer  ('vodocty', 808, 1,
'SELECT cas, vodocet_cm AS h, 
       CASE 
          WHEN vodocet_cm >= ohrozeni   THEN 3
          WHEN vodocet_cm >= pohotovost THEN 2
          WHEN vodocet_cm >= bdelost    THEN 1
          ELSE NULL
       END AS SPA
  FROM limity_cm
       JOIN
       toky
       ON toky.id = limity_cm.tok_id
          AND jmeno = ''Osoblaha''
       -- JOIN
       -- stanice
       -- ON stanice.id = limity_cm.stanice_id
       --    AND nazev = ''Osoblaha''
       JOIN 
       vodocty A
       ON toky.id = A.tok_id
 ORDER BY cas;
');

COMMIT;

