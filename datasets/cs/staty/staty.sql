BEGIN;

SET search_path TO sqltutor;

-- example of multiple questions to a given problem
SELECT insert_problem ('staty', 501, 1, 'select');
SELECT insert_question('staty', 501, 1, 'cs', 
'Vypište názvy státu, region a počet obyvatel.
');
SELECT insert_question('staty', 501, 2, 'cs', 
'Pro všechny státy vypište název státu, region a počet obyvatel.
');
SELECT insert_answer  ('staty', 501, 1,
'SELECT stat, region, populace FROM staty;
');

SELECT insert_problem ('staty', 502, 1, 'select');
SELECT insert_question('staty', 502, 1, 'cs', 
'Vypište státy, které mají více než 200 milionů obyvatel.
');
SELECT insert_answer  ('staty', 502, 1,
'SELECT stat FROM staty WHERE populace > 200000000;
');

SELECT insert_problem ('staty', 504, 2, 'select');
SELECT insert_question('staty', 504, 1, 'cs', 
'Vypište státy a hdp na hlavu, které mají více než 200 milionů obyvatel.
');
SELECT insert_answer  ('staty', 504, 1,
'SELECT stat, hdp/populace FROM staty WHERE populace > 200000000;
');

SELECT insert_problem ('staty', 505, 2, 'select');
SELECT insert_question('staty', 505, 1, 'cs', 
'Vypište asijske státy a jejich populaci v milionech.
');
SELECT insert_answer  ('staty', 505, 1,
'SELECT stat, populace/1000000 FROM staty WHERE region LIKE ''%Asia%'';
');

SELECT insert_problem ('staty', 506, 2, 'select');
SELECT insert_question('staty', 506, 1, 'cs', 
'Vypište stát a počet obyvatel pro staty ''France'', ''Germany'' a ''Italy'',
');
SELECT insert_answer  ('staty', 506, 1,
'SELECT stat, populace FROM staty 
WHERE stat IN (''France'', ''Germany'', ''Italy'');
');

SELECT insert_problem ('staty', 507, 4, 'subselect');
SELECT insert_question('staty', 507, 1, 'cs', 
'Které státy mají více obyvatel než Rusko (''Russian Federation'').
');
SELECT insert_answer  ('staty', 507, 1,
'SELECT stat FROM staty 
WHERE populace > (SELECT populace 
                  FROM staty WHERE stat=''Russian Federation'');
');

SELECT insert_problem ('staty', 508, 4, 'subselect');
SELECT insert_question('staty', 508, 1, 'cs', 
'Vypište stát a region pro země, které patří do stejných regionů
jako Indie a Turecko (''India'' a ''Turkey'').
');
SELECT insert_answer  ('staty', 508, 1,
'SELECT stat, region FROM staty 
WHERE region IN (SELECT region FROM staty 
                WHERE stat IN (''India'', ''Turkey''));
');

SELECT insert_problem ('staty', 509, 5, 'subselect');
SELECT insert_question('staty', 509, 1, 'cs', 
'Ktere země mají více obyvatel než ''Canada'', ale méně než ''Argentina''.
');
SELECT insert_answer  ('staty', 509, 1,
'SELECT stat FROM staty
WHERE  populace > (SELECT populace from staty where stat=''Canada'')
AND    populace < (SELECT populace from staty where stat=''Argentina'');
');

SELECT insert_problem ('staty', 510, 6, 'subselect');
SELECT insert_question('staty', 510, 1, 'cs', 
'Které evropské země (Europe) mají HDP na hlavu vyšší než Velká Británie
(United Kingdom).
');
SELECT insert_answer  ('staty', 510, 1,
'SELECT stat FROM staty
WHERE  region LIKE ''%Europe%''
AND    hdp/populace > (SELECT hdp/populace FROM staty 
                       WHERE stat=''United Kingdom'');
');

SELECT insert_problem ('staty', 511, 5, 'subselect');
SELECT insert_question('staty', 511, 1, 'cs', 
'Které země mají HDP vyšší než všechny evropské země (Europe). 
Jinak řečeno, které země mají HDP vyšší, než je nejvyšší HDP
v Evropě.
');
SELECT insert_answer  ('staty', 511, 1,
'SELECT stat FROM staty
WHERE  hdp > ALL (SELECT hdp FROM staty
                  WHERE region LIKE ''%Europe%'');
');
SELECT insert_answer  ('staty', 511, 2,
'SELECT stat FROM staty
WHERE  hdp > (SELECT max(hdp) FROM staty
               WHERE region LIKE ''%Europe%'');
');

SELECT insert_problem ('staty', 512, 6, 'subselect');
SELECT insert_question('staty', 512, 1, 'cs', 
'Najděte země z regionů, u kterých je celková populace regionu menší
než 25 milionů. Vypište zemi, region a počet obyvatel
');
SELECT insert_answer  ('staty', 512, 1,
'SELECT stat, region, populace FROM staty
WHERE  region IN (SELECT region FROM staty GROUP BY region 
                  HAVING SUM(populace) < 25000000);
');

SELECT insert_problem ('staty', 513, 9, 'subselect');
SELECT insert_question('staty', 513, 1, 'cs', 
'Najděte v každém regionu zemi s nejvyšším počtem obyvatel.
Uveďte stát, region a populaci.
');
SELECT insert_answer  ('staty', 513, 1,
'SELECT stat, region, populace
  FROM staty
 WHERE (region, populace) IN 
              (SELECT region, MAX(populace) 
                 FROM staty
                GROUP BY region);
');
SELECT insert_answer  ('staty', 513, 2,
'SELECT stat, region, populace
FROM   staty A
WHERE  populace = (SELECT MAX(populace) FROM staty B
                   WHERE A.region=B.region);
');
SELECT insert_answer  ('staty', 513, 3,
'SELECT A.stat, A.region, A.populace
FROM   staty A
       JOIN  (SELECT region, MAX(populace) AS maxpop FROM staty
                     GROUP BY region) B
       ON     A.region=B.region AND A.populace=B.maxpop;
');

SELECT insert_problem ('staty', 514,10, 'subselect');
SELECT insert_question('staty', 514, 1, 'cs', 
'Některé země mají více jak trojnásobek obyvatel v porovnání 
s ostatními státy daného regionu (jinak řečeno
počet obyvatel je větší než trojnásobek druhé nejlidnatější
země regionu). Vypište populaci, stát a region. 
');
SELECT insert_answer  ('staty', 514, 1,
'SELECT populace, stat, region 
FROM   staty A
WHERE  populace > ALL (SELECT 3*populace
                       FROM staty B
                       WHERE B.stat<>A.stat
                       AND   B.region=A.region);
');

SELECT insert_problem ('staty', 515, 2, 'aggregate');
SELECT insert_question('staty', 515, 1, 'cs', 
'Kolik je celkový počet obyvatel všech států.
');
SELECT insert_answer  ('staty', 515, 1,
'SELECT SUM(populace) FROM staty;
');

SELECT insert_problem ('staty', 516, 2, 'aggregate');
SELECT insert_question('staty', 516, 1, 'cs', 
'Vypište všechny regiony (každý region uveďte jen jednou).
');
SELECT insert_answer  ('staty', 516, 1,
'SELECT DISTINCT region FROM staty;
');

SELECT insert_problem ('staty', 517, 3, 'aggregate');
SELECT insert_question('staty', 517, 1, 'cs', 
'Jaké je celkové HDP pro Afriku (''Africa''), tj. pro všechny
africké regiony.
');
SELECT insert_answer  ('staty', 517, 1,
'SELECT SUM(hdp) FROM staty WHERE region LIKE ''%Africa'';
');

SELECT insert_problem ('staty', 518, 3, 'aggregate');
SELECT insert_question('staty', 518, 1, 'cs', 
'Jaká je celková populace Francie, Německa a Španělska
(''France'',''Germany'',''Spain'')?
');
SELECT insert_answer  ('staty', 518, 1,
'SELECT SUM(populace) FROM staty 
WHERE stat in (''France'',''Germany'',''Spain'');
');

SELECT insert_problem ('staty', 519, 3, 'aggregate');
SELECT insert_question('staty', 519, 1, 'cs', 
'Kolik je států v jednotlivých regionech? Vypište vždy 
počet a region
');
SELECT insert_answer  ('staty', 519, 1,
'SELECT COUNT(stat), region 
FROM   staty 
GROUP BY region;
');

SELECT insert_problem ('staty', 520, 7, 'aggregate|case');
SELECT insert_question('staty', 520, 1, 'cs', 
'Pro každý region vypište jeho označení a počet státu s 
počtem obyvatel větším než 10 milionů.
Poznámka: uveďte i regiony s nulovým počtem států.
');
SELECT insert_answer  ('staty', 520, 1,
'SELECT region, sum(CASE WHEN populace > 10000000 THEN 1 ELSE 0 END) 
  FROM staty 
 GROUP BY region;
');

SELECT insert_problem ('staty', 521, 4, 'aggregate');
SELECT insert_question('staty', 521, 1, 'cs', 
'Vypište regiony s celkovým počtem obyvatel větším než 100 milionů.
');
SELECT insert_answer  ('staty', 521, 1,
'SELECT region
FROM   staty 
GROUP BY region
HAVING SUM(populace) > 100000000;
');

COMMIT;

