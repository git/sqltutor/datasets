BEGIN;

SET search_path TO sqltutor;

SELECT insert_problem ('tramvaje', 301, 1, 'select');
SELECT insert_question('tramvaje', 301, 1, 'cs', 
'Kolik zastavek je v databázi?
');
SELECT insert_answer  ('tramvaje', 301, 1,
'SELECT count(*) FROM zastavky;
');

SELECT insert_problem ('tramvaje', 302, 2, 'subselect');
SELECT insert_question('tramvaje', 302, 1, 'cs', 
'Kolik linek je v databázi?
');
SELECT insert_answer  ('tramvaje', 302, 1,
'SELECT count(DISTINCT linka) FROM linky;
');

SELECT insert_problem ('tramvaje', 303, 3, 'join');
SELECT insert_question('tramvaje', 303, 1, 'cs', 
'Vypište jména zastávek na lince 4.
');
SELECT insert_answer  ('tramvaje', 303, 1,
'SELECT DISTINCT zastavka
FROM   linky JOIN zastavky ON zastavka_id=id
WHERE  linka=4;
');

SELECT insert_problem ('tramvaje', 304, 3, 'join');
SELECT insert_question('tramvaje', 304, 1, 'cs', 
'Které linky projíždějí stanicí Palmovka?
');
SELECT insert_answer  ('tramvaje', 304, 1,
'SELECT DISTINCT linka
FROM   linky JOIN zastavky ON zastavka_id=id
WHERE  zastavka=''Palmovka'';
');

SELECT insert_problem ('tramvaje', 305, 6, 'aggregate');
SELECT insert_question('tramvaje', 305, 1, 'cs', 
'Kterými zastávkami projíždí více než 6 linek. Vypište jména
zastávek a počet linek, které jimi projíždí. 
Poznámka: na některých linkách jsou dvě zastávky
stejného jména, linku musíte ale započítat jen jednou.
');
SELECT insert_answer  ('tramvaje', 305, 1,
'SELECT zastavka, count(DISTINCT linka) AS pocet
  FROM zastavky
       JOIN
       linky
       ON zastavka_id = zastavky.id
 GROUP BY zastavka
HAVING count(DISTINCT linka) > 6
 ORDER BY pocet, zastavka;
');

SELECT insert_problem ('tramvaje', 306, 6, 'subselect');
SELECT insert_question('tramvaje', 306, 1, 'cs', 
'Kterou zastávkou, resp. kterymi zastávkami, projíždí nejvíc linek?
');
SELECT insert_answer  ('tramvaje', 306, 1,
'SELECT zastavka  
FROM linky JOIN zastavky ON zastavka_id = id 
GROUP BY zastavka HAVING count(distinct linka) >= ALL 
          (SELECT count(distinct linka)
             FROM linky JOIN zastavky ON zastavka_id = id GROUP BY id);
');
SELECT insert_answer  ('tramvaje', 306, 2,
'SELECT zastavka
FROM   zastavky
WHERE  id IN (SELECT zastavka_id
              FROM linky
              GROUP BY zastavka_id
              HAVING count(distinct linka)=(SELECT MAX(pocet) FROM ( 
                        SELECT count(distinct linka) AS pocet 
                        FROM linky
                        GROUP BY zastavka_id) pocty) );
');

SELECT insert_problem ('tramvaje', 307,10, 'join');
SELECT insert_question('tramvaje', 307, 1, 'cs', 
'Které linky mají na trase v jednom nebo obou směrech různé zastávky
stejného jména. Vypiště čisla linek a opakující se jména zastávek
(pro každou linku a zastávku jen jednou).
');
SELECT insert_answer  ('tramvaje', 307, 1,
'SELECT DISTINCT linka, zastavka
FROM   zastavky
       JOIN linky ON id=zastavka_id
GROUP BY linka, zastavka, smer
HAVING count(linka) > 1;
'); 
SELECT insert_answer  ('tramvaje', 307, 2,
'SELECT DISTINCT A.linka, zastavka
  FROM linky A
       JOIN linky B
          ON  A.linka = B.linka
          AND A.smer  = B.smer
          AND A.zastavka_id = B.zastavka_id
          AND A.poradi <> B.poradi
        JOIN zastavky
          ON A.zastavka_id = id
 ORDER BY A.linka;
');

SELECT insert_problem ('tramvaje', 308, 10, 'subselect');
SELECT insert_question('tramvaje', 308, 1, 'cs', 
'Vypište seznam zastávek, které jsou dosažitelné bez přestupu
ze stanice Thákurova.
');
SELECT insert_answer  ('tramvaje', 308, 1,
'SELECT DISTINCT AZ.zastavka
  FROM linky AL
       JOIN zastavky AZ
          ON AL.zastavka_id = AZ.id
       JOIN linky BL
          ON AL.linka=BL.linka
       JOIN zastavky BZ
          ON BL.zastavka_id = BZ.id
 WHERE AZ.zastavka <> ''Thákurova''
   AND BZ.zastavka =  ''Thákurova'';
');
SELECT insert_answer  ('tramvaje', 308, 2,
'SELECT DISTINCT zastavka
  FROM zastavky
       JOIN
       linky
       ON id=zastavka_id
 WHERE zastavka <> ''Thákurova''
       AND linka IN (SELECT linka 
                       FROM zastavky
                            JOIN
                            linky
                            ON id=zastavka_id 
                               AND zastavka = ''Thákurova'');
');
SELECT insert_answer  ('tramvaje', 308, 3,
'SELECT DISTINCT zastavka
  FROM zastavky
       JOIN 
       linky ON id=zastavka_id AND zastavka <> ''Thákurova''
 WHERE linka IN ( SELECT linka 
                    FROM linky 
                   WHERE zastavka_id = (SELECT id 
                                          FROM zastavky 
                                         WHERE zastavka=''Thákurova''));
');

SELECT insert_problem ('tramvaje', 309, 4, 'aggregate');
SELECT insert_question('tramvaje', 309, 1, 'cs', 
'Na kterých tramvajových linkách je čtyřicet a více zastávek?
Uveďte číslo linky, směr a počet zastávek.  Poznámka: počet
zastávek se na jedné lince může lišit pro oba směry.
');
SELECT insert_answer  ('tramvaje', 309, 1,
'SELECT linka, smer, COUNT(zastavka_id) AS zastávky
  FROM linky
 GROUP BY linka, smer
HAVING COUNT(zastavka_id) >= 40;
');

SELECT insert_problem ('tramvaje', 310, 6, 'aggregate');
SELECT insert_question('tramvaje', 310, 1, 'cs', 
'Jaký je nejvyšší počet linek projíždějících jednou zastávkou?
');
SELECT insert_answer  ('tramvaje', 310, 1,
'SELECT MAX(pocet)
  FROM (SELECT COUNT(distinct linka) AS pocet
          FROM linky
         GROUP BY zastavka_id) AS dt;
');
SELECT insert_answer  ('tramvaje', 310, 2,
'SELECT COUNT(DISTINCT linka) AS pocet
  FROM zastavky
       JOIN
       linky
       ON id = zastavka_id
 GROUP BY zastavka
 ORDER BY pocet DESC
 LIMIT 1;
');

SELECT insert_problem ('tramvaje', 311, 12, 'derivedtable');
SELECT insert_question('tramvaje', 311, 1, 'cs', 
'Které zastávky na tramvajových linkách jsou konečné (tj. zastávky,
které mají pořadí 1 nebo maximální pořadí na dané lince)?  Uveďte
číslo linky a konečnou zastávku.
');
SELECT insert_answer  ('tramvaje', 311, 1,
'SELECT DISTINCT linka, zastavka
  FROM linky A
       JOIN
       zastavky
       ON id = zastavka_id
 WHERE poradi = 1 
    OR (linka, smer, poradi) IN (SELECT linka, smer, max(poradi)
                                   FROM linky B
                                  WHERE A.linka=B.linka 
                                    AND A.smer=B.smer
                                  GROUP BY linka, smer)
 ORDER BY linka, zastavka;
');
SELECT insert_answer  ('tramvaje', 311, 2,
$$
select L.linka, Z.zastavka
from   linky as L
       join zastavky as Z
       on L.zastavka_id=Z.id and L.poradi=1 -- and smer in (1,2)
order by L.linka, L.smer;
$$
);

COMMIT;

