BEGIN;

SET search_path TO sqltutor;

SELECT insert_problem ('premyslovci', 901, 2, 'aggregate');
SELECT insert_question('premyslovci', 901, 1, 'cs',
'Kolik přemyslovců je registrováno v databázi?
');
SELECT insert_answer  ('premyslovci', 901, 1,
'SELECT count(*) FROM premyslovci WHERE rod=''Přemyslovci'';
');

SELECT insert_problem ('premyslovci', 902, 10, 'selfjoin');
SELECT insert_question('premyslovci', 902, 1, 'cs',
'Kdo byli rodiče Elišky Rejčky?
');
SELECT insert_answer  ('premyslovci', 902, 1,
'SELECT A.jmeno
  FROM premyslovci A
       JOIN
       premyslovci B
       ON A.id IN (B.otec, B.matka)
          AND B.jmeno = ''Eliška Rejčka'';
');

SELECT insert_problem ('premyslovci', 903, 10, 'selfjoin');
SELECT insert_question('premyslovci', 903, 1, 'cs',
'Uveďte děti Karla IV. Lucemburského.
');
SELECT insert_answer  ('premyslovci', 903, 1,
'SELECT B.jmeno
  FROM premyslovci A
       JOIN
       premyslovci B
       ON A.jmeno = ''Karel IV. Lucemburský''
          AND A.id = B.otec;
');

SELECT insert_problem ('premyslovci', 905, 14, 'selfjoin');
SELECT insert_question('premyslovci', 905, 1, 'cs',
'Kdo byli prarodiče knížete Václava? Uveďte jméno a rod.
');
SELECT insert_answer  ('premyslovci', 905, 1,
'SELECT p.jmeno, p.rod
FROM   premyslovci p
       JOIN premyslovci r on p.id IN (r.otec, r.matka)
       JOIN premyslovci v on r.id IN (v.otec, v.matka)
WHERE  v.jmeno = ''Václav''
');

SELECT insert_answer  ('premyslovci', 905, 2,
'SELECT C.jmeno, C.rod
  FROM premyslovci A
       JOIN
       premyslovci B
       ON A.jmeno=''Václav''
          AND B.id IN (A.otec, A.matka)
       JOIN
       premyslovci C
       ON C.id IN (B.otec, B.matka);
');

SELECT insert_problem ('premyslovci', 906, 14, 'selfjoin');
SELECT insert_question('premyslovci', 906, 1, 'cs',
'Uveďte vnuky a vnučky Boleslava I.
');
SELECT insert_answer  ('premyslovci', 906, 1,
'SELECT C.jmeno
  FROM premyslovci A
       JOIN
       premyslovci B
       ON A.jmeno=''Boleslav I.''
          AND A.id = B.otec
       JOIN
       premyslovci C
       ON B.id IN (C.otec, C.matka);
');

SELECT insert_problem ('premyslovci', 908, 15, 'selfjoin');
SELECT insert_question('premyslovci', 908, 1, 'cs',
'Vyhledejte sourozence Oldřicha Brněnského. Uveďte jméno a rod.
');
SELECT insert_answer  ('premyslovci', 908, 1,
'SELECT DISTINCT C.jmeno, C.rod
  FROM premyslovci A
       JOIN
       premyslovci B
       ON A.jmeno = ''Oldřich Brněnský''
          AND B.id IN (A.otec, A.matka)
       JOIN
       premyslovci C
       ON B.id IN (C.otec, C.matka)
          AND A.id <> C.id;
');

SELECT insert_problem ('premyslovci', 909, 15, 'selfjoin');
SELECT insert_question('premyslovci', 909, 1, 'cs',
'Kolik dětí měli Přemyslovci? Uveďte vždy jméno a počet dětí
(u bezdětných uvádějte nulu).
');
SELECT insert_answer  ('premyslovci', 909, 1,
'SELECT rodice.jmeno, count(deti.jmeno)
  FROM premyslovci as rodice
       LEFT JOIN
       premyslovci as deti
       ON rodice.id=deti.otec OR rodice.id=deti.matka
 WHERE rodice.rod = ''Přemyslovci''
 GROUP BY rodice.id, rodice.jmeno;
');

SELECT insert_problem ('premyslovci', 910, 15, 'with');
SELECT insert_question('premyslovci', 910, 1, 'cs',
$$Najděte potomky Karla IV. Lucemburského. Uveďte pokolení, id a jméno
(v této rodové hierarchii má Karel IV. pokolení 0, jeho děti 1, atd.).
$$);
SELECT insert_answer  ('premyslovci', 910, 1,
$$
WITH RECURSIVE potomci (pokoleni, id, jmeno) AS (
     SELECT 0, id, jmeno
     FROM premyslovci
     WHERE jmeno='Karel IV. Lucemburský'
     UNION ALL
     SELECT pokoleni+1, p.id, p.jmeno
     FROM   premyslovci AS p
     JOIN   potomci
            ON potomci.id IN (p.otec, p.matka)
     where  pokoleni < 5
)
SELECT * FROM potomci;
$$);

SELECT insert_problem ('premyslovci', 911, 15, 'with');
SELECT insert_question('premyslovci', 911, 1, 'cs',
$$Najděte předky svatého Václava.
Uvádějte pokolení, id a jméno
(v této rodové hierarchii má 'Václav' pokolení 0, jeho rodiče 1, atd.).
$$);
SELECT insert_answer  ('premyslovci', 911, 1,
$$
WITH RECURSIVE predci (n, id, otec, matka, jmeno, rod) AS (
     SELECT 0, id, otec, matka, jmeno, rod
     FROM   premyslovci
     WHERE  jmeno='Václav'
     UNION ALL
     SELECT n+1, p.id, p.otec, p.matka, p.jmeno, p.rod
     FROM   premyslovci AS p
     JOIN   predci
            ON p.id IN (predci.otec, predci.matka)
     -- WHERE  n < 2 -- prarodice,
)
SELECT n, id, jmeno
FROM   predci;
$$);

COMMIT;
