SELECT sqltutor.init_dataset('filmy');
SELECT sqltutor.add_ds_source('filmy', 2007, 'http://www.fdb.cz/');
SELECT sqltutor.add_ds_table('filmy', 1, 'filmy',    'id, rok, titul');
SELECT sqltutor.add_ds_table('filmy', 2, 'umelci',   'id, jmeno');
SELECT sqltutor.add_ds_table('filmy', 3, 'obsazeni', 'film_id, umelec_id, poradi');
SELECT sqltutor.add_ds_table('filmy', 4, 'rezie',    'film_id, umelec_id');
