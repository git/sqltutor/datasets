/*
   This file is public domain.

   The data is a matter of public record, and comes from
   http://www.fdb.cz/
 */


SET search_path TO sqltutor_data;

DROP TABLE IF EXISTS filmy    CASCADE;
DROP TABLE IF EXISTS umelci   CASCADE;
DROP TABLE IF EXISTS obsazeni CASCADE;
DROP TABLE IF EXISTS rezie    CASCADE;

CREATE TABLE filmy   (id INT PRIMARY KEY, rok INT, titul VARCHAR(50));
CREATE TABLE umelci  (id INT PRIMARY KEY, jmeno VARCHAR(30));
CREATE TABLE obsazeni(film_id   INT REFERENCES filmy (id),
                      umelec_id INT REFERENCES umelci(id),
                      poradi    INT);
CREATE TABLE rezie   (film_id   INT REFERENCES filmy (id),
                      umelec_id INT REFERENCES umelci(id));

BEGIN;

INSERT INTO filmy (id, rok, titul) VALUES
( 1, 1994, 'Forrest Gump' ),
( 2, 1994, 'Vykoupení z věznice Shawshank' ),
( 3, 1999, 'Matrix' ),
( 4, 1993, 'Pulp Fiction - Historky z podsvětí' ),
( 5, 1998, 'Zachraňte vojína Ryana' ),
( 6, 2003, 'Pán prstenů: Návrat krále' ),
( 7, 1993, 'Schindlerův seznam' ),
( 8, 2000, 'Pán prstenů: Společenstvo prstenu' ),
( 9, 1972, 'Kmotr' ),
( 10, 1968, 'Tenkrát na Západě' ),
( 11, 2007, 'Astronaut' ),
( 12, 2007, 'Black Water Transit' ),
( 13, 2007, 'Morgan''s Summit' ),
( 14, 2007, 'Neznámý svůdce' ),
( 15, 2007, 'Smrtonosná past 4.0' ),
( 16, 2000, 'Můj soused zabiják' ),
( 17, 2007, 'Piráti z Karibiku: Na konci světa' ),
( 18, 2006, 'Piráti z Karibiku - Truhla mrtvého muže' ),
( 19, 2005, 'Karlík a továrna na čokoládu' ),
( 20, 2005, 'Libertin' ),
( 21, 1997, 'Bojovník' ),
( 22, 1996, 'To je náš hit!' ),
( 23, 2006, 'Šifra mistra Leonarda' ),
( 24, 2004, 'Lupiči paní domácí' ),
( 25, 2004, 'Polární expres' ),
( 26, 2004, 'Terminál' ),
( 27, 2002, 'Chyť mě, když to dokážeš' ),
( 28, 2006, 'Růžový panter' ),
( 29, 2006, 'Rytíři nebes' ),
( 30, 2005, 'Říše vlků' ),
( 31, 2005, 'Tygr a sníh' ),
( 32, 1992, 'Návštěvníci' ),
( 33, 1998, 'Návštěvníci 2' ),
( 34, 2007, 'Božský Evan' ),
( 35, 2007, 'Wanted' ),
( 36, 2006, 'Kontrakt' ),
( 37, 2005, 'Mimo zákon' ),
( 38, 2007, 'Hvězdný prach' ),
( 39, 2006, 'Good Shepherd' ),
( 40, 2005, 'Hra na schovávanou' ),
( 41, 2004, 'Godsend' ),
( 42, 2004, 'Jeho fotr, to je lotr!' ),
( 43, 2007, 'Ghost Rider' ),
( 44, 2007, 'Next' ),
( 45, 2006, 'Rituál' ),
( 46, 2006, 'World Trade Center' ),
( 47, 2007, '88 minut' ),
( 48, 2007, 'Dannyho parťáci 3' ),
( 49, 2005, 'Maximální limit' ),
( 50, 2004, 'Kupec benátský' ),
( 51, 2004, 'Paparazzi' ),
( 52, 2002, 'Údolí stínů' ),
( 53, 2002, 'Znamení' ),
( 54, 2002, 'Zpívající detektiv' ),
( 55, 2000, 'Patriot' ),
( 56, 1995, 'Statečné srdce' ),
( 57, 2007, 'Beowulf' ),
( 58, 2005, 'Mr. & Mrs. Smith' ),
( 59, 2004, 'Alexander Veliký' ),
( 60, 2005, 'Domino' ),
( 61, 2004, 'Král Artuš' ),
( 62, 2006, 'Moje Superbejvalka' ),
( 63, 2005, 'Buď v klidu' ),
( 64, 2005, 'Ideál' ),
( 65, 2005, 'Producenti' ),
( 66, 2003, 'Kill Bill' ),
( 67, 2007, 'Invaze' ),
( 68, 2007, 'The Golden Compass' ),
( 69, 2005, 'Moje krásná čarodějka' ),
( 70, 2002, 'Hra o čas' ),
( 71, 2006, 'Ráže 45' ),
( 72, 2006, 'Elizabeth: The Golden Age' ),
( 73, 2006, 'Babel' ),
( 74, 2004, 'Dannyho parťáci 2' ),
( 75, 2003, 'Úsměv Mony Lisy' ),
( 76, 2006, 'Goyovy přízraky' ),
( 77, 2006, 'Dům u jezera' ),
( 78, 2000, 'Slečna Drsňák' ),
( 79, 2004, 'Slečna Drsňák 2: Ještě drsnější' ),
( 80, 2006, 'Eragon' );



INSERT INTO umelci (id, jmeno) VALUES
( 1, 'Robert Zemeckis' ),
( 2, 'Tom Hanks' ),
( 3, 'Robin Wright Penn' ),
( 4, 'Gary Sinise' ),
( 5, 'Mykelti Williamson' ),
( 6, 'Sally Field' ),
( 7, 'Michael Connor Humphreys' ),
( 8, 'Hanna Hall' ),
( 9, 'Haley Joel Osment' ),
( 10, 'Frank Darabont' ),
( 11, 'Tim Robbins' ),
( 12, 'Morgan Freeman' ),
( 13, 'William Sadler' ),
( 14, 'Clancy Brown' ),
( 15, 'Gil Bellows' ),
( 16, 'Andy Wachowski' ),
( 17, 'Larry Wachowski' ),
( 18, 'Keanu Reeves' ),
( 19, 'Carrie-Anne Moss' ),
( 20, 'Hugo Weaving' ),
( 21, 'Gloria Foster' ),
( 22, 'Joe Pantoliano' ),
( 23, 'Quentin Tarantino' ),
( 24, 'John Travolta' ),
( 25, 'Samuel L. Jackson' ),
( 26, 'Uma Thurman' ),
( 27, 'Harvey Keitel' ),
( 28, 'Tim Roth' ),
( 29, 'Steven Spielberg' ),
( 30, 'Tom Sizemore' ),
( 31, 'Edward Burns' ),
( 32, 'Barry Pepper' ),
( 33, 'Adam Goldberg' ),
( 34, 'Peter Jackson' ),
( 35, 'Elijah Wood' ),
( 36, 'Viggo Mortensen' ),
( 37, 'Ian McKellen' ),
( 38, 'Sean Astin' ),
( 39, 'Orlando Bloom' ),
( 40, 'Liam Neeson' ),
( 41, 'Ben Kingsley' ),
( 42, 'Ralph Fiennes' ),
( 43, 'Caroline Godall' ),
( 44, 'Jonathan Sagalle' ),
( 45, 'Liv Tyler' ),
( 46, 'Francis Ford Coppola' ),
( 47, 'Marlon Brando' ),
( 48, 'Al Pacino' ),
( 49, 'James Caan' ),
( 50, 'Richard Castellano' ),
( 51, 'Robert Duvall' ),
( 52, 'Sergio Leone' ),
( 53, 'Claudia Cardinale' ),
( 54, 'Henry Fonda' ),
( 55, 'Jason Robards' ),
( 56, 'Charles Bronson' ),
( 57, 'Gabriele Ferzetti' ),
( 58, 'Michael Polish' ),
( 59, 'Bruce Willis' ),
( 60, 'Billy Bob Thornton' ),
( 61, 'Virginia Madsen' ),
( 62, 'Max Thieriot' ),
( 63, 'Jasper Polish' ),
( 64, 'Logan Polish' ),
( 65, 'Tony Kaye' ),
( 66, 'Tom Schulman' ),
( 67, 'Julianne Moore' ),
( 68, 'James Foley' ),
( 69, 'Halle Berry' ),
( 70, 'Giovanni Ribisi' ),
( 71, 'Richard Portnow' ),
( 72, 'Gary Dourdan' ),
( 73, 'Len Wiseman' ),
( 74, 'Timothy Olyphant' ),
( 75, 'Maggie Quigley' ),
( 76, 'Justin Long' ),
( 77, 'Jeffrey Wright' ),
( 78, 'Jonathan Lynn' ),
( 79, 'Matthew Perry' ),
( 80, 'Rosanna Arquette' ),
( 81, 'Michael Clarke Duncan' ),
( 82, 'Natasha Henstridge' ),
( 83, 'Gore Verbinski' ),
( 84, 'Johnny Depp' ),
( 85, 'Keira Knightley' ),
( 86, 'Geoffrey Rush' ),
( 87, 'Jonathan Pryce' ),
( 88, 'Jack Davenport' ),
( 89, 'Bill Nighy' ),
( 90, 'Tim Burton' ),
( 91, 'Freddie Highmore' ),
( 92, 'Helen Bonham Carter' ),
( 93, 'Christopher Lee' ),
( 94, 'David Kelly' ),
( 95, 'Laurence Dunmore' ),
( 96, 'Samantha Morton' ),
( 97, 'John Malkovich' ),
( 98, 'Paul Ritter' ),
( 99, 'Stanley Townsend' ),
( 100, 'Marshall Bell' ),
( 101, 'Elpidia Carrillo' ),
( 102, 'Frederic Forrest' ),
( 103, 'Tom Everett Scott' ),
( 104, 'Johnathon Schaech' ),
( 105, 'Steve Zahn' ),
( 106, 'Ethan Embry' ),
( 107, 'Ron Howard' ),
( 108, 'Audrey Tautou' ),
( 109, 'Jean Reno' ),
( 110, 'Alfred Molina' ),
( 111, 'Joel Coen' ),
( 112, 'Ethan Coen' ),
( 113, 'Irma P. Hall' ),
( 114, 'Marlon Wayans' ),
( 115, 'J.K. Simmons' ),
( 116, 'Tzi Ma' ),
( 117, 'Michael Jeter' ),
( 118, 'Peter Scolari' ),
( 119, 'Nona Gaye' ),
( 120, 'Eddie Deezen' ),
( 121, 'Catherine Zeta-Jones' ),
( 122, 'Stanley Tucci' ),
( 123, 'Chi McBride' ),
( 124, 'Diego Luna' ),
( 125, 'Leonardo DiCaprio' ),
( 126, 'Christopher Walken' ),
( 127, 'Martin Sheen' ),
( 128, 'Nathalie Baye' ),
( 129, 'Shawn Levy' ),
( 130, 'Steve Martin' ),
( 131, 'Kevin Kline' ),
( 132, 'Beyoncé Knowles' ),
( 133, 'Emily Mortimer' ),
( 134, 'Tony Bill' ),
( 135, 'James Franco' ),
( 136, 'Martin Henderson' ),
( 137, 'Jennifer Decker' ),
( 138, 'Tyler Labine' ),
( 139, 'Chris Nahon' ),
( 140, 'Jocelyn Quivrin' ),
( 141, 'Arly Jover' ),
( 142, 'Laura Morante' ),
( 143, 'Philippe Bas' ),
( 144, 'Roberto Benigni' ),
( 145, 'Nicoletta Braschi' ),
( 146, 'Steven Beckingham' ),
( 147, 'Emilia Fox' ),
( 148, 'Jean-Marie Poiré' ),
( 149, 'Christian Clavier' ),
( 150, 'Valérie Lemercier' ),
( 151, 'Marie-Anne Chazal' ),
( 152, 'Christian Bujeau' ),
( 153, 'Muriel Robin' ),
( 154, 'Tom Shadyac' ),
( 155, 'Steve Carell' ),
( 156, 'Lauren Graham' ),
( 157, 'Jimmy Bennett' ),
( 158, 'Johnny Simmons' ),
( 159, 'Timur Bekmambetov' ),
( 160, 'James McAvoy' ),
( 161, 'Angelina Jolie' ),
( 162, 'Bruce Beresford' ),
( 163, 'Jamie Anderson' ),
( 164, 'Ned Bellamy' ),
( 165, 'Margarita Blush' ),
( 166, 'John Cusack' ),
( 167, 'Doug Dearth' ),
( 168, 'David J. Burke' ),
( 169, 'Kevin Spacey' ),
( 170, 'Justin Timberlake' ),
( 171, 'J LL Cool' ),
( 172, 'Dylan McDermott' ),
( 173, 'Matthew Vaughn' ),
( 174, 'Charlie Cox' ),
( 175, 'Michelle Pfeiffer' ),
( 176, 'Claire Danes' ),
( 177, 'Robert De Niro' ),
( 178, 'Matt Damon' ),
( 179, 'Joe Pesci' ),
( 180, 'Alec Baldwin' ),
( 185, 'Nick Hamm' ),
( 186, 'Rebecca Romijn-Stamos' ),
( 187, 'Greg Kinnear' ),
( 188, 'Cameron Bright' ),
( 189, 'Merwin Mondesir' ),
( 195, 'Mark Steven Johnson' ),
( 196, 'Nicolas Cage' ),
( 197, 'Wes Bentley' ),
( 198, 'Peter Fonda' ),
( 199, 'Eva Mendes' ),
( 200, 'Donal Logue' ),
( 181, 'John Polson' ),
( 182, 'Dakota Fanning' ),
( 183, 'Famke Janssen' ),
( 184, 'Elisabeth Shue' ),
( 190, 'Jay Roach' ),
( 191, 'Ben Stiller' ),
( 192, 'Dustin Hoffman' ),
( 193, 'Barbra Streisand' ),
( 194, 'Blythe Danner' ),
( 201, 'Lee Tamahori' ),
( 202, 'Nicolas Pajon' ),
( 203, 'Paul Rae' ),
( 204, 'Alan Kemper Armani' ),
( 205, 'Neill LaBute' ),
( 206, 'Ellen Burstyn' ),
( 207, 'Kate Beahan' ),
( 208, 'Frances Conroy' ),
( 209, 'Molly Parker' ),
( 210, 'Oliver Stone' ),
( 211, 'Maria Bello' ),
( 212, 'Michael Peña' ),
( 213, 'Jay Hernandez' ),
( 214, 'Armando Riesco' ),
( 215, 'Jon Avnet' ),
( 216, 'Alicia Witt' ),
( 217, 'Amy Brenneman' ),
( 218, 'Leelee Sobieski' ),
( 219, 'Benjamin McKenzie' ),
( 220, 'Steven Soderbergh' ),
( 221, 'George Clooney' ),
( 222, 'Brad Pitt' ),
( 223, 'Ellen Barkin' ),
( 224, 'D.J. Caruso' ),
( 225, 'Matthew McConaughey' ),
( 226, 'René Russo' ),
( 227, 'Jeremy Piven' ),
( 228, 'Jaime King' ),
( 229, 'Michael Radford' ),
( 230, 'Jeremy Irons' ),
( 231, 'Joseph Fiennes' ),
( 232, 'Lynn Collins' ),
( 233, 'Zuleikha Robinson' ),
( 234, 'Paul Abascal' ),
( 235, 'Cole Hauser' ),
( 236, 'Robin Tunney' ),
( 237, 'Dennis Farina' ),
( 238, 'Daniel Baldwin' ),
( 239, 'Mel Gibson' ),
( 240, 'Randall Wallace' ),
( 241, 'Madeleine Stowe' ),
( 242, 'Sam Elliott' ),
( 243, 'Chris Klein' ),
( 244, 'M. Night Shyamalan' ),
( 245, 'Joaquin Phoenix' ),
( 246, 'Rory Culkin' ),
( 247, 'Abigail Breslin' ),
( 248, 'Cherry Jones' ),
( 249, 'Keith Gordon' ),
( 250, 'Robert Downey' ),
( 251, 'Jeremy Northam' ),
( 252, 'Katie Holmes' ),
( 253, 'Roland Emmerich' ),
( 254, 'Heath Ledger' ),
( 255, 'Joely Richardson' ),
( 256, 'Tchéky Karyo' ),
( 257, 'Tom Wilkinson' ),
( 258, 'Sophie Marceau' ),
( 259, 'Patrick McGoohan' ),
( 260, 'Catherine McCormack' ),
( 261, 'Brendan Gleeson' ),
( 262, 'Sharisse Baker-Bernard' ),
( 263, 'Chris Coppola' ),
( 264, 'Crispin Glover' ),
( 265, 'Anthony Hopkins' ),
( 266, 'Doug Liman' ),
( 267, 'Adam Brody' ),
( 268, 'William Fichtner' ),
( 269, 'Vince Vaughn' ),
( 273, 'Tony Scott' ),
( 274, 'Jacqueline Bisset' ),
( 275, 'Mickey Rourke' ),
( 276, 'Mena Suvari' ),
( 270, 'Colin Farrell' ),
( 271, 'Jared Leto' ),
( 272, 'Val Kilmer' ),
( 277, 'Antoine Fuqua' ),
( 278, 'Clive Owen' ),
( 279, 'Stephen Dillane' ),
( 280, 'Ioan Gruffudd' ),
( 281, 'Stellan Skarsgård' ),
( 282, 'Ivan Reitman' ),
( 283, 'Luke Wilson' ),
( 284, 'Anna Faris' ),
( 285, 'Ilona Alexandra' ),
( 286, 'Andrea Bertola' ),
( 287, 'Gary F. Gray' ),
( 288, 'Danny DeVito' ),
( 289, 'Dwayne "The Rock" Johnson' ),
( 290, 'Ben Younger' ),
( 291, 'Meryl Streep' ),
( 292, 'Bryan Greenberg' ),
( 293, 'Annie Parisse' ),
( 294, 'Jon Abrahams' ),
( 295, 'Susan Stroman' ),
( 296, 'Nathan Lane' ),
( 297, 'Matthew Broderick' ),
( 298, 'Will Ferrell' ),
( 299, 'Roger Bart' ),
( 303, 'Oliver Hirschbiegel' ),
( 304, 'James McTeigue' ),
( 305, 'Nicole Kidman' ),
( 306, 'Daniel Craig' ),
( 307, 'Jackson Bond' ),
( 308, 'Jeffrey Wrigh' ),
( 300, 'David Carradine' ),
( 301, 'Lucy Liu' ),
( 302, 'Michael Madsen' ),
( 309, 'Chris Weitz' ),
( 310, 'Dakota Blue Richards' ),
( 311, 'Eva Green' ),
( 312, 'Jim Carter' ),
( 313, 'Nora Ephron' ),
( 314, 'Shirley MacLaine' ),
( 315, 'Michael Caine' ),
( 316, 'Jason Schwartzman' ),
( 317, 'Bob Rafelson' ),
( 318, 'Milla Jovovich' ),
( 319, 'Doug Hutchison' ),
( 320, 'Gary Lennon' ),
( 321, 'Angus Macfadyen' ),
( 322, 'Stephen Dorff' ),
( 323, 'Aisha Tyler' ),
( 324, 'Sarah Strange' ),
( 327, 'Alejandro González Iñárritu' ),
( 328, 'Gael García Bernal' ),
( 329, 'Jamie McBride' ),
( 330, 'Kôji Yakusho' ),
( 325, 'Shekhar Kapur' ),
( 326, 'Cate Blanchett' ),
( 331, 'Julia Roberts' ),
( 332, 'Andy Garcia' ),
( 337, 'Miloš Forman' ),
( 338, 'Natalie Portman' ),
( 339, 'Javier Bardem' ),
( 340, 'Stellan Skarsgårds' ),
( 341, 'Randy Quaid' ),
( 342, 'Antonio Bellido' ),
( 343, 'Alejandro Agresti' ),
( 344, 'Sandra Bullock' ),
( 345, 'Willeke van Ammelroy' ),
( 346, 'Mike Bacarella' ),
( 347, 'Donald Petrie' ),
( 348, 'Benjamin Bratt' ),
( 349, 'William Shatner' ),
( 350, 'Ernie Hudson' ),
( 333, 'Mike Newell' ),
( 334, 'Julia Stiles' ),
( 335, 'Kirsten Dunst' ),
( 336, 'Marcia Gay Harden' ),
( 351, 'John Pasquin' ),
( 352, 'Regina King' ),
( 353, 'Enrique Murciano' ),
( 354, 'Treat Williams' ),
( 355, 'Stefen Fangmeier' ),
( 356, 'Edward Speleers' ),
( 357, 'Sienna Guillory' ),
( 358, 'Garrett Hedlund' ),
( 359, 'Djimon Hounsou' );


INSERT INTO obsazeni ( film_id, umelec_id, poradi ) VALUES
( 1, 2, 1 ),
( 1, 3, 2 ),
( 1, 4, 2 ),
( 1, 5, 2 ),
( 1, 6, 2 ),
( 1, 7, 2 ),
( 1, 8, 2 ),
( 1, 9, 2 ),
( 2, 11, 1 ),
( 2, 12, 2 ),
( 2, 13, 2 ),
( 2, 14, 2 ),
( 2, 15, 2 ),
( 3, 18, 1 ),
( 3, 19, 2 ),
( 3, 20, 2 ),
( 3, 21, 2 ),
( 3, 22, 2 ),
( 4, 24, 1 ),
( 4, 25, 2 ),
( 4, 26, 2 ),
( 4, 27, 2 ),
( 4, 28, 2 ),
( 5, 2, 1 ),
( 5, 30, 2 ),
( 5, 31, 2 ),
( 5, 32, 2 ),
( 5, 33, 2 ),
( 6, 35, 1 ),
( 6, 36, 2 ),
( 6, 37, 2 ),
( 6, 38, 2 ),
( 6, 39, 2 ),
( 7, 40, 1 ),
( 7, 41, 2 ),
( 7, 42, 2 ),
( 7, 43, 2 ),
( 7, 44, 2 ),
( 8, 35, 1 ),
( 8, 37, 2 ),
( 8, 36, 2 ),
( 8, 38, 2 ),
( 8, 45, 2 ),
( 9, 47, 1 ),
( 9, 48, 2 ),
( 9, 49, 2 ),
( 9, 50, 2 ),
( 9, 51, 2 ),
( 10, 53, 1 ),
( 10, 54, 2 ),
( 10, 55, 2 ),
( 10, 56, 2 ),
( 10, 57, 2 ),
( 11, 59, 1 ),
( 11, 60, 2 ),
( 11, 61, 2 ),
( 11, 62, 2 ),
( 11, 63, 2 ),
( 11, 64, 2 ),
( 12, 59, 1 ),
( 12, 25, 2 ),
( 13, 59, 1 ),
( 13, 67, 2 ),
( 14, 59, 1 ),
( 14, 69, 2 ),
( 14, 70, 2 ),
( 14, 71, 2 ),
( 14, 72, 2 ),
( 15, 59, 1 ),
( 15, 74, 2 ),
( 15, 75, 2 ),
( 15, 76, 2 ),
( 15, 77, 2 ),
( 16, 59, 1 ),
( 16, 79, 2 ),
( 16, 80, 2 ),
( 16, 81, 2 ),
( 16, 82, 2 ),
( 17, 84, 1 ),
( 17, 39, 2 ),
( 17, 85, 2 ),
( 17, 86, 2 ),
( 17, 87, 2 ),
( 18, 84, 1 ),
( 18, 39, 2 ),
( 18, 85, 2 ),
( 18, 88, 2 ),
( 18, 89, 2 ),
( 19, 84, 1 ),
( 19, 91, 2 ),
( 19, 92, 2 ),
( 19, 93, 2 ),
( 19, 94, 2 ),
( 20, 84, 1 ),
( 20, 96, 2 ),
( 20, 97, 2 ),
( 20, 98, 2 ),
( 20, 99, 2 ),
( 21, 84, 1 ),
( 21, 47, 2 ),
( 21, 100, 2 ),
( 21, 101, 2 ),
( 21, 102, 2 ),
( 22, 103, 1 ),
( 22, 45, 2 ),
( 22, 104, 2 ),
( 22, 105, 2 ),
( 22, 106, 2 ),
( 23, 2, 1 ),
( 23, 108, 2 ),
( 23, 109, 2 ),
( 23, 37, 2 ),
( 23, 110, 2 ),
( 24, 2, 1 ),
( 24, 113, 2 ),
( 24, 114, 2 ),
( 24, 115, 2 ),
( 24, 116, 2 ),
( 25, 2, 1 ),
( 25, 117, 2 ),
( 25, 118, 2 ),
( 25, 119, 2 ),
( 25, 120, 2 ),
( 26, 2, 1 ),
( 26, 121, 2 ),
( 26, 122, 2 ),
( 26, 123, 2 ),
( 26, 124, 2 ),
( 27, 125, 1 ),
( 27, 2, 2 ),
( 27, 126, 2 ),
( 27, 127, 2 ),
( 27, 128, 2 ),
( 28, 130, 1 ),
( 28, 131, 2 ),
( 28, 132, 2 ),
( 28, 109, 2 ),
( 28, 133, 2 ),
( 29, 135, 1 ),
( 29, 109, 2 ),
( 29, 136, 2 ),
( 29, 137, 2 ),
( 29, 138, 2 ),
( 30, 109, 1 ),
( 30, 140, 2 ),
( 30, 141, 2 ),
( 30, 142, 2 ),
( 30, 143, 2 ),
( 31, 144, 1 ),
( 31, 109, 2 ),
( 31, 145, 2 ),
( 31, 146, 2 ),
( 31, 147, 2 ),
( 32, 149, 1 ),
( 32, 109, 2 ),
( 32, 150, 2 ),
( 32, 151, 2 ),
( 32, 152, 2 ),
( 33, 109, 1 ),
( 33, 149, 2 ),
( 33, 153, 2 ),
( 34, 155, 1 ),
( 34, 156, 2 ),
( 34, 157, 2 ),
( 34, 158, 2 ),
( 34, 12, 2 ),
( 35, 160, 1 ),
( 35, 12, 2 ),
( 35, 161, 2 ),
( 36, 163, 1 ),
( 36, 164, 2 ),
( 36, 165, 2 ),
( 36, 166, 2 ),
( 36, 167, 2 ),
( 36, 12, 2 ),
( 37, 12, 1 ),
( 37, 169, 2 ),
( 37, 170, 2 ),
( 37, 171, 2 ),
( 37, 172, 2 ),
( 38, 174, 1 ),
( 38, 175, 2 ),
( 38, 176, 2 ),
( 38, 177, 2 ),
( 39, 178, 1 ),
( 39, 177, 2 ),
( 39, 161, 2 ),
( 39, 179, 2 ),
( 39, 180, 2 ),
( 40, 177, 1 ),
( 40, 182, 2 ),
( 40, 183, 2 ),
( 40, 184, 2 ),
( 41, 177, 1 ),
( 41, 186, 2 ),
( 41, 187, 2 ),
( 41, 188, 2 ),
( 41, 189, 2 ),
( 42, 191, 1 ),
( 42, 177, 2 ),
( 42, 192, 2 ),
( 42, 193, 2 ),
( 42, 194, 2 ),
( 43, 196, 1 ),
( 43, 197, 2 ),
( 43, 198, 2 ),
( 43, 199, 2 ),
( 43, 200, 2 ),
( 44, 196, 1 ),
( 44, 67, 2 ),
( 44, 202, 2 ),
( 44, 203, 2 ),
( 44, 204, 2 ),
( 45, 196, 1 ),
( 45, 206, 2 ),
( 45, 207, 2 ),
( 45, 208, 2 ),
( 45, 209, 2 ),
( 46, 196, 1 ),
( 46, 211, 2 ),
( 46, 212, 2 ),
( 46, 213, 2 ),
( 46, 214, 2 ),
( 47, 48, 1 ),
( 47, 216, 2 ),
( 47, 217, 2 ),
( 47, 218, 2 ),
( 47, 219, 2 ),
( 48, 221, 1 ),
( 48, 222, 2 ),
( 48, 178, 2 ),
( 48, 223, 2 ),
( 48, 48, 2 ),
( 49, 48, 1 ),
( 49, 225, 2 ),
( 49, 226, 2 ),
( 49, 227, 2 ),
( 49, 228, 2 ),
( 50, 48, 1 ),
( 50, 230, 2 ),
( 50, 231, 2 ),
( 50, 232, 2 ),
( 50, 233, 2 ),
( 51, 235, 1 ),
( 51, 236, 2 ),
( 51, 237, 2 ),
( 51, 30, 2 ),
( 51, 238, 2 ),
( 51, 239, 2 ),
( 52, 239, 1 ),
( 52, 241, 2 ),
( 52, 187, 2 ),
( 52, 242, 2 ),
( 52, 243, 2 ),
( 53, 239, 1 ),
( 53, 245, 2 ),
( 53, 246, 2 ),
( 53, 247, 2 ),
( 53, 248, 2 ),
( 54, 239, 1 ),
( 54, 250, 2 ),
( 54, 3, 2 ),
( 54, 251, 2 ),
( 54, 252, 2 ),
( 55, 239, 1 ),
( 55, 254, 2 ),
( 55, 255, 2 ),
( 55, 256, 2 ),
( 55, 257, 2 ),
( 56, 239, 1 ),
( 56, 258, 2 ),
( 56, 259, 2 ),
( 56, 260, 2 ),
( 56, 261, 2 ),
( 57, 262, 1 ),
( 57, 263, 2 ),
( 57, 261, 2 ),
( 57, 264, 2 ),
( 57, 265, 2 ),
( 57, 161, 2 ),
( 58, 222, 1 ),
( 58, 161, 2 ),
( 58, 267, 2 ),
( 58, 268, 2 ),
( 58, 269, 2 ),
( 59, 270, 1 ),
( 59, 161, 2 ),
( 59, 265, 2 ),
( 59, 271, 2 ),
( 59, 272, 2 ),
( 60, 85, 1 ),
( 60, 274, 2 ),
( 60, 275, 2 ),
( 60, 276, 2 ),
( 60, 126, 2 ),
( 61, 278, 1 ),
( 61, 279, 2 ),
( 61, 85, 2 ),
( 61, 280, 2 ),
( 61, 281, 2 ),
( 62, 26, 1 ),
( 62, 283, 2 ),
( 62, 284, 2 ),
( 62, 285, 2 ),
( 62, 286, 2 ),
( 63, 24, 1 ),
( 63, 26, 2 ),
( 63, 27, 2 ),
( 63, 288, 2 ),
( 63, 289, 2 ),
( 64, 26, 1 ),
( 64, 291, 2 ),
( 64, 292, 2 ),
( 64, 293, 2 ),
( 64, 294, 2 ),
( 65, 296, 1 ),
( 65, 297, 2 ),
( 65, 26, 2 ),
( 65, 298, 2 ),
( 65, 299, 2 ),
( 66, 26, 1 ),
( 66, 300, 2 ),
( 66, 301, 2 ),
( 66, 302, 2 ),
( 67, 305, 1 ),
( 67, 306, 2 ),
( 67, 251, 2 ),
( 67, 307, 2 ),
( 67, 308, 2 ),
( 68, 305, 1 ),
( 68, 306, 2 ),
( 68, 310, 2 ),
( 68, 311, 2 ),
( 68, 312, 2 ),
( 69, 305, 1 ),
( 69, 314, 2 ),
( 69, 298, 2 ),
( 69, 315, 2 ),
( 69, 316, 2 ),
( 70, 25, 1 ),
( 70, 318, 2 ),
( 70, 281, 2 ),
( 70, 319, 2 ),
( 71, 318, 1 ),
( 71, 321, 2 ),
( 71, 322, 2 ),
( 71, 323, 2 ),
( 71, 324, 2 ),
( 72, 326, 1 ),
( 72, 278, 2 ),
( 73, 326, 1 ),
( 73, 222, 2 ),
( 73, 328, 2 ),
( 73, 329, 2 ),
( 73, 330, 2 ),
( 74, 221, 1 ),
( 74, 222, 2 ),
( 74, 178, 2 ),
( 74, 331, 2 ),
( 74, 332, 2 ),
( 75, 331, 1 ),
( 75, 334, 2 ),
( 75, 335, 2 ),
( 75, 336, 2 ),
( 76, 338, 1 ),
( 76, 339, 2 ),
( 76, 340, 2 ),
( 76, 341, 2 ),
( 76, 342, 2 ),
( 77, 18, 1 ),
( 77, 344, 2 ),
( 77, 345, 2 ),
( 77, 346, 2 ),
( 77, 232, 2 ),
( 78, 344, 1 ),
( 78, 315, 2 ),
( 78, 348, 2 ),
( 78, 349, 2 ),
( 78, 350, 2 ),
( 79, 344, 1 ),
( 79, 352, 2 ),
( 79, 353, 2 ),
( 79, 354, 2 ),
( 79, 349, 2 ),
( 80, 356, 1 ),
( 80, 357, 2 ),
( 80, 358, 2 ),
( 80, 359, 2 ),
( 80, 230, 2 );


INSERT INTO rezie ( film_id, umelec_id ) VALUES
( 1, 1 ),
( 2, 10 ),
( 3, 16 ),
( 3, 17 ),
( 4, 23 ),
( 5, 29 ),
( 6, 34 ),
( 7, 29 ),
( 8, 34 ),
( 9, 46 ),
( 10, 52 ),
( 11, 58 ),
( 12, 65 ),
( 13, 66 ),
( 14, 68 ),
( 15, 73 ),
( 16, 78 ),
( 17, 83 ),
( 18, 83 ),
( 19, 90 ),
( 20, 95 ),
( 21, 84 ),
( 22, 2 ),
( 23, 107 ),
( 24, 111 ),
( 24, 112 ),
( 25, 1 ),
( 26, 29 ),
( 27, 29 ),
( 28, 129 ),
( 29, 134 ),
( 30, 139 ),
( 31, 144 ),
( 32, 148 ),
( 33, 148 ),
( 34, 154 ),
( 35, 159 ),
( 36, 162 ),
( 37, 168 ),
( 38, 173 ),
( 39, 177 ),
( 40, 181 ),
( 41, 185 ),
( 42, 190 ),
( 43, 195 ),
( 44, 201 ),
( 45, 205 ),
( 46, 210 ),
( 47, 215 ),
( 48, 220 ),
( 49, 224 ),
( 50, 229 ),
( 51, 234 ),
( 52, 240 ),
( 53, 244 ),
( 54, 249 ),
( 55, 253 ),
( 56, 239 ),
( 57, 1 ),
( 58, 266 ),
( 59, 210 ),
( 60, 273 ),
( 61, 277 ),
( 62, 282 ),
( 63, 287 ),
( 64, 290 ),
( 65, 295 ),
( 66, 23 ),
( 67, 303 ),
( 67, 304 ),
( 68, 309 ),
( 69, 313 ),
( 70, 317 ),
( 71, 320 ),
( 72, 325 ),
( 73, 327 ),
( 74, 220 ),
( 75, 333 ),
( 76, 337 ),
( 77, 343 ),
( 78, 347 ),
( 79, 351 ),
( 80, 355 );

COMMIT;
