BEGIN;

SET search_path TO sqltutor;

SELECT insert_problem ('filmy', 201, 1, 'select');
SELECT insert_question('filmy', 201, 1, 'cs', 
'Vypište id a titul filmů natočených v roce 2004.
');
SELECT insert_answer  ('filmy', 201, 1,
'SELECT id, titul
FROM    filmy
WHERE   rok=2004;
');

SELECT insert_problem ('filmy', 202, 1, 'select');
SELECT insert_question('filmy', 202, 1, 'cs', 
'Zjistěte rok uvedení filmu ''Domino''.
');
SELECT insert_answer  ('filmy', 202, 1,
'SELECT  rok
FROM    filmy
WHERE   titul=''Domino'';
');

SELECT insert_problem ('filmy', 203, 2, 'select');
SELECT insert_question('filmy', 203, 1, 'cs', 
'Vypište všechny filmy Piráti z Karibiku z databáze, uveďte id, titul a rok.
');
SELECT insert_answer  ('filmy', 203, 1,
'SELECT  id, titul, rok
FROM    filmy
WHERE   titul like ''Piráti z Karibiku%'';
');

SELECT insert_problem ('filmy', 204, 2, 'select');
SELECT insert_question('filmy', 204, 1, 'cs', 
'Jaké jsou tituly filmů s id 1, 3, 5, 7, 11?
');
SELECT insert_answer  ('filmy', 204, 1,
'SELECT  titul
FROM    filmy
WHERE   id IN (1, 3, 5, 7, 11);
');

SELECT insert_problem ('filmy', 205, 1, 'select');
SELECT insert_question('filmy', 205, 1, 'cs', 
'Jaké id má Miloš Forman?
');
SELECT insert_answer  ('filmy', 205, 1,
'SELECT  id
FROM    umelci
WHERE   jmeno=''Miloš Forman'';
');

SELECT insert_problem ('filmy', 206, 1, 'select');
SELECT insert_question('filmy', 206, 1, 'cs', 
'Jaké je id filmu ''Dům u jezera''?
');
SELECT insert_answer  ('filmy', 206, 1,
'SELECT  id
FROM    filmy
WHERE   titul=''Dům u jezera'';
');

SELECT insert_problem ('filmy', 207, 3, 'subselect');
SELECT insert_question('filmy', 207, 1, 'cs', 
'Jaké je herecké obsazení filmu ''Dům u jezera''
');
SELECT insert_answer  ('filmy', 207, 1,
'SELECT jmeno             
  FROM umelci 
       JOIN 
       obsazeni 
       ON umelci.id=umelec_id
       JOIN
       filmy
       ON filmy.id=film_id
 WHERE titul = ''Dům u jezera'';
');
SELECT insert_answer  ('filmy', 207, 2,
'SELECT jmeno                               
  FROM umelci 
       JOIN 
       obsazeni 
       ON id=umelec_id
 WHERE film_id = (SELECT id FROM filmy WHERE titul = ''Dům u jezera'');
');
SELECT insert_answer  ('filmy', 207, 3,
'SELECT jmeno                             
  FROM umelci
       JOIN
       obsazeni A                            -- vnější dotaz
       ON id = umelec_id
 WHERE ''Dům u jezera'' IN (SELECT titul
                            FROM filmy
                                 JOIN
                                 obsazeni B  -- vnitřní dotaz
                                 ON id = film_id
                           WHERE A.film_id = B.film_id);
');

SELECT insert_problem ('filmy', 208, 4, 'subselect');
SELECT insert_question('filmy', 208, 1, 'cs', 
'Ve kterých filmech hrál Tom Hanks?
');
SELECT insert_answer  ('filmy', 208, 1,
'SELECT  titul
FROM    filmy JOIN obsazeni ON id=film_id
WHERE   umelec_id = (SELECT id FROM umelci WHERE jmeno=''Tom Hanks'');
');

SELECT insert_problem ('filmy', 209, 5, 'subselect');
SELECT insert_question('filmy', 209, 1, 'cs', 
'Ve kterých filmech hrál Tom Hanks, ale ne v hlavni roli 
(tj. pořadí není 1)?
');
SELECT insert_answer  ('filmy', 209, 1,
'SELECT  titul
FROM    filmy JOIN obsazeni ON id=film_id
WHERE   umelec_id = (SELECT id FROM umelci WHERE jmeno=''Tom Hanks'')
        AND poradi != 1;
');

SELECT insert_problem ('filmy', 210, 6, 'join');
SELECT insert_question('filmy', 210, 1, 'cs', 
'Vypište všechny filmy a herce hlavních rolí (tj. těch, kteří mají
pořadí 1) z roku 2003
');
SELECT insert_answer  ('filmy', 210, 1,
'SELECT  titul, jmeno
FROM    filmy JOIN obsazeni ON filmy.id=film_id
        JOIN umelci on umelci.id=umelec_id
WHERE   rok=2003 AND poradi=1;
');

SELECT insert_problem ('filmy', 211, 10, 'join');
SELECT insert_question('filmy', 211, 1, 'cs', 
'Ve kterém roce natočil Tom Hanks nejvíc filmů? Vypište rok a počet filmů.
');
SELECT insert_answer  ('filmy', 211, 1,
'SELECT  rok, count(titul)
FROM    filmy JOIN obsazeni ON filmy.id=film_id
        JOIN umelci ON umelci.id=umelec_id
WHERE   jmeno=''Tom Hanks''
GROUP BY rok
HAVING count(titul) >= ALL
     (SELECT  count(titul)
      FROM    filmy JOIN obsazeni ON filmy.id=film_id
              JOIN umelci ON umelci.id=umelec_id
      WHERE   jmeno=''Tom Hanks''
      GROUP BY rok);
');
SELECT insert_answer  ('filmy', 211, 2,
'SELECT  rok, count(titul)
FROM    filmy JOIN obsazeni ON filmy.id=film_id
        JOIN umelci ON umelci.id=umelec_id
WHERE   jmeno=''Tom Hanks''
GROUP BY rok
HAVING count(titul) =  (SELECT max(pocet) FROM
     (SELECT  count(titul) as pocet
      FROM    filmy JOIN obsazeni ON filmy.id=film_id
              JOIN umelci ON umelci.id=umelec_id
      WHERE   jmeno=''Tom Hanks''
      GROUP BY rok) tituly);
');

SELECT insert_problem ('filmy', 212, 5, 'join');
SELECT insert_question('filmy', 212, 1, 'cs', 
'Ve kterých filmech měl hlavní roli Johnny Depp?
');
SELECT insert_answer  ('filmy', 212, 1,
'SELECT  titul
FROM    filmy JOIN obsazeni ON filmy.id=film_id
        JOIN umelci ON umelci.id=umelec_id
WHERE   jmeno=''Johnny Depp'' and poradi=1;
');

SELECT insert_problem ('filmy', 213, 6, 'subselect');
SELECT insert_question('filmy', 213, 1, 'cs', 
'Kteří herci hráli alespoň pětkrát v hlavní roli?
');
SELECT insert_answer  ('filmy', 213, 1,
'SELECT jmeno
  FROM umelci
       JOIN obsazeni
       ON id = umelec_id
 WHERE poradi=1
 GROUP BY jmeno
HAVING COUNT(poradi) >= 5;
');
SELECT insert_answer  ('filmy', 213, 2,
'SELECT jmeno
  FROM umelci 
 WHERE id in (SELECT umelec_id
                 FROM obsazeni
                WHERE poradi=1
                GROUP BY umelec_id
               HAVING count(film_id) >= 5);
');

SELECT insert_problem ('filmy', 214, 6, 'subselect');
SELECT insert_question('filmy', 214, 1, 'cs', 
'Vypište filmy z roku 2006 a počet herců, kteří v nich hráli. 
Uveďte počet herců a titul filmu.
');
SELECT insert_answer  ('filmy', 214, 1,
'SELECT   count(umelec_id), titul
FROM     filmy JOIN obsazeni ON filmy.id=film_id
WHERE    rok=2006
GROUP BY rok, titul
ORDER BY count(umelec_id);
');

SELECT insert_problem ('filmy', 215, 11, 'subselect');
SELECT insert_question('filmy', 215, 1, 'cs', 
'Vypište seznam herců, kteří hráli v některém filmu s Al Pacinem
');
SELECT insert_answer  ('filmy', 215, 1,
'SELECT DISTINCT jmeno
FROM    umelci JOIN obsazeni ON umelci.id=umelec_id
WHERE   jmeno != ''Al Pacino'' AND
        film_id IN (SELECT  film_id 
                    FROM    obsazeni 
                    WHERE   umelec_id = 
                            (SELECT id FROM umelci WHERE jmeno=''Al Pacino''));
');

SELECT insert_problem ('filmy', 216, 4, 'join');
SELECT insert_question('filmy', 216, 1, 'cs', 
'Vypište všechny filmy a jejich režisery pro rok 2003.
');
SELECT insert_answer  ('filmy', 216, 1,
'SELECT  titul, jmeno 
FROM    filmy JOIN rezie ON id=film_id
        JOIN umelci ON umelci.id=rezie.umelec_id
WHERE   rok=2003;
');

SELECT insert_problem ('filmy', 217, 4, 'join');
SELECT insert_question('filmy', 217, 1, 'cs', 
'Vypište všechny filmy a herce za rok 2003.
');
SELECT insert_answer  ('filmy', 217, 1,
'SELECT  titul, jmeno 
FROM    filmy JOIN obsazeni ON filmy.id=obsazeni.film_id
        JOIN umelci ON umelci.id=obsazeni.umelec_id
WHERE   rok=2003;
');

SELECT insert_problem ('filmy', 218, 4, 'aggregate');
SELECT insert_question('filmy', 218, 1, 'cs', 
'Které filmy měly více než jednoho režiséra? Vypište titul filmu a 
počet režisérů.
');
SELECT insert_answer  ('filmy', 218, 1,
'SELECT   titul, count(umelec_id)
FROM     filmy JOIN rezie ON filmy.id=film_id
GROUP BY titul
HAVING   count(umelec_id) > 1;
');

SELECT insert_problem ('filmy', 219, 7, 'join');
SELECT insert_question('filmy', 219, 1, 'cs', 
'Ve kterých filmech byl režisér zároveň hercem? Vypište titul a 
jméno režiséra/herce.
');
SELECT insert_answer  ('filmy', 219, 1,
'SELECT titul, jmeno
  FROM obsazeni
       JOIN rezie
          ON  obsazeni.film_id   = rezie.film_id
          AND obsazeni.umelec_id = rezie.umelec_id
       JOIN filmy
          ON filmy.id  = obsazeni.film_id
       JOIN umelci
          ON umelci.id = rezie.umelec_id;
');
SELECT insert_answer  ('filmy', 219, 2,
'SELECT  titul, umelci.jmeno
FROM    filmy JOIN rezie ON filmy.id=rezie.film_id
        JOIN umelci ON rezie.umelec_id=umelci.id
WHERE   filmy.id IN 
        (SELECT  rezie.film_id
         FROM    rezie JOIN obsazeni ON rezie.umelec_id=obsazeni.umelec_id
                                    AND rezie.film_id=obsazeni.film_id);
');

COMMIT;

