/* This work is licensed under a Creative Commons Attribution 4.0
   International License.

   ---------------------------------------------------------------------------
   Copyright (c) 2017- 2018 by Burkhardt Renz. All rights reserved.
   Database Twitter Tutorial with SQLTutor
   $Id: twitter-tutor.sql 4262 2017-12-14 07:47:34Z br $
   ---------------------------------------------------------------------------
*/

BEGIN;

CREATE SCHEMA IF NOT EXISTS sqltutor;
SET search_path TO sqltutor;

select init_dataset ('twitter');
select add_ds_source('twitter', 2018, 'Burkhardt Renz');
select add_ds_table ('twitter', 1, 'twitter', 'twuser, follower');


/* function parameters
   -------------------
   init_dataset           (dsname text)
   sqltutor.add_ds_source (dataset text, year int, source text)
   add_ds_table           (dataset text, ord int, ds_table text, columns text)
   insert_problem         (dataset text, problem_id int, points int,
                           category text)
   insert_question        (dataset text, problem_id int, q_ord int,
                           lang char(2), question text)s
   insert_answer          (dataset text, problem_id int, priority int,
                           answer text)
*/

/* The questions and answers
*/

select insert_problem ('twitter', 1, 1, 'select');

select insert_question('twitter', 1, 1, 'en',
$$
Find all followers of 'Adam'.
$$);

select insert_question('twitter', 1, 1, 'de',
$$
Ermitteln Sie alle Follower von 'Adam'.
$$);

select insert_question('twitter', 1, 1, 'cs',
$$
Najděte všechny sledující uživatele 'Adam'.
$$);

/* ----------------------------------------------------------------- */

select insert_answer('twitter', 1, 1,
$$
SELECT follower
FROM   twitter
WHERE  twuser = 'Adam'
$$);

/* ================================================================= */

select insert_problem ('twitter', 2, 2, 'select');

select insert_question('twitter', 2, 1, 'en',
$$
Retrieve all followers of the followers of 'Adam'.
$$);

select insert_question('twitter', 2, 1, 'de',
$$
Ermitteln Sie die Follower der Follower von 'Adam'.
$$);

select insert_question('twitter', 2, 1, 'cs',
$$
Vyhledejte všechny sledující sledujících Adama ('Adam').
$$);

/* ----------------------------------------------------------------- */

select insert_answer('twitter', 2, 1,
$$
SELECT F2.follower
FROM   twitter F1, twitter F2
WHERE  F1.follower = F2.twuser
  AND  F1.twuser = 'Adam'
$$);

select insert_answer('twitter', 2, 2,
$$
WITH followers_of_followers
AS   (SELECT F1.twuser as T, F2.follower as F
      FROM twitter F1, twitter F2
      WHERE F1.follower = F2.twuser)
  SELECT F
  FROM followers_of_followers
  WHERE T = 'Adam'
$$);

select insert_answer('twitter', 2, 3,
$$
SELECT F2.follower
FROM   twitter AS F2
       JOIN
       twitter AS F1
       ON F1.follower = F2.twuser AND F1.twuser = 'Adam'
$$);

/* ================================================================= */

select insert_problem ('twitter', 3, 4, 'select');

select insert_question('twitter', 3, 1, 'en',
$$
Retrieve the twitter users and all followers of the followers of every
one of them.
$$);

select insert_question('twitter', 3, 1, 'de',
$$
Ermitteln Sie die Twitter-User und die Follower der Follower von ihnen.
$$);

select insert_question('twitter', 3, 1, 'cs',
$$
Vyhledejte uživatele twitteru a všechny sledující sledujících pro
každého z nich.
$$);

/* ----------------------------------------------------------------- */

select insert_answer('twitter', 3, 1,
$$
SELECT F1.twuser, F2.follower
FROM   twitter F1, twitter F2
WHERE  F1.follower = F2.twuser
$$);

select insert_answer('twitter', 3, 2,
$$
WITH followers_of_followers
AS   (SELECT F1.twuser as T, F2.follower as F
      FROM twitter F1, twitter F2
      WHERE F1.follower = F2.twuser)
  SELECT T, F
  FROM followers_of_followers
$$);

select insert_answer('twitter', 3, 3,
$$
SELECT TU.twuser, FF.follower
FROM   twitter AS FF
       JOIN twitter AS TU
       ON TU.follower = FF.twuser;
$$);

/* ================================================================= */

select insert_problem ('twitter', 4, 8, 'select');

select insert_question('twitter', 4, 1, 'en',
$$
Retrieve all the direct and indirect followers of 'Anne'.
$$);

select insert_question('twitter', 4, 1, 'de',
$$
Ermitteln Sie die direkten und indirekten Anhänger von 'Anne'.
$$);

select insert_question('twitter', 4, 1, 'cs',
$$
Najděte všechny přímé a nepřímé sledující uživatele 'Anne'.
$$);

/* ----------------------------------------------------------------- */

select insert_answer('twitter', 4, 1,
$$
WITH RECURSIVE follower(T,F)
AS   (SELECT twuser as T, follower as F FROM twitter
            UNION
      SELECT follower.T, twitter.follower as F
      FROM follower, twitter
      WHERE follower.F = twitter.twuser)
  SELECT F
  FROM follower
  WHERE T = 'Anne'
$$);

/* ================================================================= */

select insert_problem ('twitter', 5, 12, 'select');

select insert_question('twitter', 5, 1, 'en',
$$
Count all direct and indirect followers of 'Art'. If by chance there is one
of them that follows 'Art' himself, do not count 'Art' as one of his indirect
followers.
$$);

select insert_question('twitter', 5, 1, 'de',
$$
Ermitteln Sie die Zahl der direkten und indirekten Follwer von 'Art'.
Ist unter ihnen einer, der selbst 'Art' folgt, so zählen Sie 'Art' selbst
nicht mit.
$$);

select insert_question('twitter', 5, 1, 'cs',
$$
Spočítejte všechny přímé a nepřímé sledující uživatele 'Art'. Pokud se mezi
nimi náhodou vyskytne někdo, koho sleduje 'Art', pak Arta
nepočítejte mezi nepřímé sledující.
$$);

/* ----------------------------------------------------------------- */

select insert_answer('twitter', 5, 1,
$$
WITH RECURSIVE follower(T,F)
AS   (SELECT twuser as T, follower as F FROM twitter
            UNION
      SELECT follower.T, twitter.follower as F
      FROM follower, twitter
      WHERE follower.F = twitter.twuser)
  SELECT COUNT(*) FROM (SELECT F
                        FROM follower
                        WHERE T = 'Art'
                        EXCEPT
                        SELECT 'Art') AS X
$$);

/* ================================================================= */

select insert_problem ('twitter', 6, 12, 'select');

select insert_question('twitter', 6, 1, 'en',
$$
Retrieve the twitter users and all the direct and indirect followers of them,
i.e. the transitive closure of the relation twitter.
$$);

select insert_question('twitter', 6, 1, 'de',
$$
Ermitteln Sie die Twitter-User und all ihre direkten und indirekten Follower,
d.h. den transitiven Abschluss der Relation twitter.
$$);

select insert_question('twitter', 6, 1, 'cs',
$$
Vyhledejte všechny uživatele twitteru a všechny jejich přímé a nepřímé
sledující, tj. transitivní uzávěr relace twitter.
$$);

/* ----------------------------------------------------------------- */

select insert_answer('twitter', 6, 1,
$$
WITH RECURSIVE follower(T,F)
AS   (SELECT twuser as T, follower as F FROM twitter
            UNION
      SELECT follower.T, twitter.follower as F
      FROM follower, twitter
      WHERE follower.F = twitter.twuser)
  SELECT T, F
  FROM follower
  ORDER BY T
$$);

/* ================================================================= */

select insert_problem ('twitter', 7, 16, 'select');

select insert_question('twitter', 7, 1, 'en',
$$
Retrieve the twitter users and the number of all the direct and
indirect follower of them. Take into account the users that do not
have any followers.
$$);

select insert_question('twitter', 7, 1, 'de',
$$
Ermitteln Sie die Twitter-User und die Zahl all ihrer direkten oder indirekten
Follower. Berücksichtigen Sie dabei auch diejenigen, die keine Follower haben.
$$);

select insert_question('twitter', 7, 1, 'cs',
$$
Vyhledejte uživatele twitteru a uveďte počet jejich přímých a
nepřímých sledujících. Uvažujte také uživatele, kteří nemají žádné
sledující.
$$);

/* ----------------------------------------------------------------- */

select insert_answer('twitter', 7, 1,
$$
WITH RECURSIVE follower(T,F)
AS   (SELECT twuser as T, follower as F FROM twitter
            UNION
      SELECT follower.T, twitter.follower as F
      FROM follower, twitter
      WHERE follower.F = twitter.twuser)
  SELECT T, Count(F)
  FROM follower
  GROUP BY T
  UNION
  SELECT follower, 0
  FROM twitter WHERE follower NOT IN (SELECT twuser FROM twitter)
  ORDER BY 2 DESC, 1
$$);


/* ================================================================= */

-- /* tutorial
--  */
--
-- select init_tutorial('en', 'SQLTutor');
-- select insert_dataset('SQLTutor', 'en', 'twitter');
--
-- select init_tutorial('de', 'SQLTutor');
-- select insert_dataset('SQLTutor', 'de', 'twitter');
--
-- moved to ../tutorials

COMMIT;
