/* This work is licensed under a Creative Commons Attribution 4.0
   International License.

   ---------------------------------------------------------------------------
   Copyright (c) 2015 - 2018 by Burkhardt Renz. All rights reserved.
   Datenbank Twitter für Beispiele von Rekusrion in SQL
   $Id: twitter-create.sql 4262 2017-12-14 07:47:34Z br $
   ---------------------------------------------------------------------------
*/

/* The Twitter Database Structure
*/

BEGIN;

CREATE SCHEMA IF NOT EXISTS sqltutor_data;
SET search_path TO sqltutor_data;

DROP TABLE IF EXISTS twitter CASCADE;

create table twitter (
  twuser     varchar(20),
  follower   varchar(20) not null,
  primary key (twuser, follower)
);

/* The Twitter Database Content
*/

/*
-- added more names and shufled to hide the results
insert into twitter (twuser, follower)
  values ('Harry',    'Jessica' ),
         ('Grace',    'Joshua'  ),
         ('Lily',     'Emily'   ),
         ('Leo',      'Eva'     ),
         ('Poppy',    'Mia'     ),
         ('Lily',     'William' ),
         ('Emily',    'Harry'   ),
         ('Jessica',  'Jill'    ),
         ('Ruby',     'Sophia'  ),
         ('Anne',     'Bob'     ),
         ('Ruby',     'Harry'   ),
         ('Sophie',   'Jill'    ),
         ('James',    'Poppy'   ),
         ('Oscar',    'William' ),
         ('Sophia',   'Leo'     ),
         ('Grace',    'Jill'    ),
         ('Mia',      'Archie'  ),
         ('Jessica',  'Grace'   ),
         ('Mia',      'Isabella'),
         ('Emily',    'Thomas'  ),
         ('Sophie',   'William' ),
         ('Jacob',    'Sophia'  ),
         ('Tommy',    'Jack'    ),
         ('Lily',     'Sophie'  ),
         ('Poppy',    'Sophie'  ),
         ('James',    'Jacob'   ),
         ('Jessica',  'Oliver'  ),
         ('Charlie',  'Thomas'  ),
         ('Ruby',     'Charlie' ),
         ('Bob',      'Clara'   ),
         ('Jill',     'Ruby'    ),
         ('Jessica',  'Archie'  ),
         ('James',    'Oscar'   ),
         ('Emily',    'Mia'     ),
         ('Sophia',   'Oliver'  ),
         ('Sophia',   'Jessica' ),
         ('Isabella', 'Poppy'   ),
         ('Poppy',    'Jacob'   ),
         ('Justin',   'Tommy'   ),
         ('Jessica',  'Sophie'  ),
         ('Leo',      'Harry'   ),
         ('Eva',      'Ruby'    ),
         ('Joshua',   'Sophia'  ),
         ('Archie',   'Poppy'   ),
         ('Thomas',   'Archie'  ),
         ('Jack',     'Jill'    ),  -- Jack and Jill went up the hill
         ('Jill',     'Thomas'  ),
         ('Grace',    'Jessica' ),
         ('Mia',      'Thomas'  ),
         ('Poppy',    'Oscar'   ),
         ('Oscar',    'Grace'   ),
         ('Ruby',     'Jacob'   ),
         ('Sophie',   'Isabella'),
         ('Emily',    'George'  ),
         ('Emily',    'Leo'     ),
         ('Grace',    'Poppy'   ),
         ('Jacob',    'Sophie'  ),
         ('Harry',    'Joshua'  ),
         ('Jessica',  'Jacob'   ),
         ('William',  'Jacob'   ),
         ('Grace',    'Eva'     ),
         ('William',  'Lily'    ),
         ('Charlie',  'Oscar'   ),
         ('Oliver',   'Ruby'    ),
         ('Joshua',   'Oliver'  ),
         ('George',   'Emily'   ),
         ('Leo',      'George'  ),
         ('Jill',     'Grace'   ),
         ('Jill',     'Isabella'),
         ('George',   'Mia'     ),
         ('Joshua',   'Jill'    ),
         ('Sophie',   'Archie'  ),
         ('William',  'Poppy'   ),
         ('Poppy',    'Grace'   ),
         ('Poppy',    'Archie'  ),
         ('William',  'Isabella'),
         ('Jack',     'Sarah'   ),
         ('Harry',    'Isabella'),
         ('Leo',      'James'   ),
         ('Eva',      'Emily'   ),
         ('Leo',      'Jessica' ),
         ('Eva',      'Grace'   ),
         ('George',   'Eva'     ),
         ('Sarah',    'Jenny'   ),
         ('Leo',      'Emily'   ),
         ('Mia',      'Mia'     ),
         ('Ruby',     'Oscar'   ),
         ('James',    'Lily'    ),
         ('Joshua',   'William' ),
         ('Joshua',   'Ruby'    ),
         ('George',   'Isabella'),
         ('Jacob',    'Harry'   ),
         ('Sophie',   'Oscar'   ),
         ('Mia',      'George'  ),
         ('Lily',     'Jessica' ),
         ('Jacob',    'Jessica' ),
         ('Sophia',   'Jill'    ),
         ('Eva',      'Lily'    ),
         ('Charlie',  'George'  ),
         ('Mia',      'Joshua'  ),
         ('Ruby',     'Lily'    ),
         ('Emily',    'Oliver'  ),
         ('Eva',      'James'   ),
         ('Lily',     'Oliver'  ),
         ('Harry',    'James'   ),
         ('Sarah',    'Justin'  ),
         ('Thomas',   'James'   ),
         ('Jacob',    'Eva'     ),
         ('Charlie',  'Sophia'  ),
         ('Sophia',   'William' ),
         ('William',  'Charlie' ),
         ('Lily',     'Thomas'  ),
         ('Eva',      'George'  ),
         ('Jill',     'Sophia'  ),
         ('Eva',      'Charlie' ),
         ('Archie',   'Harry'   ),
         ('Leo',      'Mia'     ),
         ('Jill',     'Sophie'  ),
         ('Oscar',    'James'   ),
         ('Joshua',   'Leo'     ),
         ('Sophie',   'George'  ),
         ('Oscar',    'Jacob'   ),
         ('Justin',   'Jenny'   ),
         ('Archie',   'Grace'   ),
         ('William',  'Sophie'  ),
         ('Charlie',  'Eva'     ),
         ('Mia',      'Emily'   ),
         ('Grace',    'Ruby'    ),
         ('Oscar',    'Charlie' ),
         ('Jill',     'Jack'    ),
         ('Sophia',   'Lily'    );
*/

delete from twitter;

/* the chain */
insert into twitter (twuser, follower)
  values ('Anne', 'Bob'),
         ('Bob', 'Clara'),
         ('Clara', 'Dennis'),
         ('Dennis', 'Eve'),
         ('Eve', 'Frank'),
         ('Frank', 'Gina');

/* the binary tree */
insert into twitter (twuser, follower)
  values ('Adam', 'Brice'),
         ('Adam', 'Brittany'),
         ('Brice', 'Christian'),
         ('Brice', 'Cameron'),
         ('Brittany', 'Connor'),
         ('Brittany', 'Chase'),
         ('Christian', 'Destiny'),
         ('Christian', 'Danielle'),
         ('Cameron', 'Dacy'),
         ('Cameron', 'Debbie'),
         ('Connor', 'Deborah'),
         ('Connor', 'Diana'),
         ('Chase', 'Dona'),
         ('Chase', 'Drucy');

/* the cycle */
insert into twitter (twuser, follower)
  values ('Art', 'Benny'),
         ('Benny', 'Count'),
         ('Count', 'Dizzy'),
         ('Dizzy', 'Earl'),
         ('Earl', 'Frankie'),
         ('Frankie', 'Gregory'),
         ('Gregory', 'Hubert'),
         ('Hubert', 'Ivie'),
         ('Ivie', 'June'),
         ('June', 'Karin'),
         ('Karin', 'Louis'),
         ('Louis', 'Miles'),
         ('Miles', 'Art');

COMMIT;
