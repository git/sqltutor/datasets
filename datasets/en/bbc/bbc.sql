BEGIN;

SET search_path TO sqltutor;

SELECT insert_problem ('bbc', 1101, 1, 'select');
SELECT insert_question('bbc', 1101, 1, 'en', 
'Show the name for the four countries that have a population of at
least 200 million. (200 million is 200000000, there are eight zeros).
');
SELECT insert_answer  ('bbc', 1101, 1,
'SELECT name FROM bbc WHERE population>200000000
');

SELECT insert_problem ('bbc', 1102, 1, 'select');
SELECT insert_question('bbc', 1102, 1, 'en', 
'Give the name and the per capita GDP for those countries with a
population of at least 200 million.
');
SELECT insert_answer  ('bbc', 1102, 1,
'SELECT name, gdp/population FROM bbc
  WHERE population>200000000
');

SELECT insert_problem ('bbc', 1103, 1, 'select');
SELECT insert_question('bbc', 1103, 1, 'en', 
'Show the name and population for ''France'', ''Germany'', ''Italy''
');
SELECT insert_answer  ('bbc', 1103, 1,
'SELECT name, population FROM bbc
  WHERE name IN (''France'',''Germany'',''Italy'')
');

SELECT insert_problem ('bbc', 1104, 1, 'select');
SELECT insert_question('bbc', 1104, 1, 'en', 
'Identify the countries which have names including the word ''United''
');
SELECT insert_answer  ('bbc', 1104, 1,
'SELECT name FROM bbc
  WHERE name LIKE ''%United%''
');

SELECT insert_problem ('bbc', 1105, 3, 'subselect');
SELECT insert_question('bbc', 1105, 1, 'en', 
'List each country name where the population is larger than
''Russia''.
');
SELECT insert_answer  ('bbc', 1105, 1,
'SELECT name FROM bbc
  WHERE population>
     (SELECT population FROM bbc
      WHERE name=''Russia'')
');

SELECT insert_problem ('bbc', 1106, 3, 'subselect');
SELECT insert_question('bbc', 1106, 1, 'en', 
'List the name and region of countries in the regions containing
''India'', ''Iran''.
');
SELECT insert_answer  ('bbc', 1106, 1,
'SELECT name,region FROM bbc
  WHERE region IN
    (SELECT region FROM bbc
     WHERE name IN (''India'',''Iran''))
');

SELECT insert_problem ('bbc', 1107, 3, 'subselect');
SELECT insert_question('bbc', 1107, 1, 'en', 
'Show the countries in Europe with a per capita GDP greater than
''United Kingdom''.
');
SELECT insert_answer  ('bbc', 1107, 1,
'SELECT name FROM bbc
  WHERE region=''Europe'' AND gdp/population>
     (SELECT gdp/population FROM bbc
      WHERE name=''United Kingdom'')
');

SELECT insert_problem ('bbc', 1108, 4, 'subselect');
SELECT insert_question('bbc', 1108, 1, 'en', 
'Which country has a population that is more than Canada but less
than Algeria?
');
SELECT insert_answer  ('bbc', 1108, 1,
'SELECT name FROM bbc WHERE
 population >
 (SELECT population FROM bbc WHERE name=''Canada'') 
    AND
  population <
 (SELECT population FROM bbc WHERE name=''Algeria'')
');

SELECT insert_problem ('bbc', 1109, 4, 'subselect');
SELECT insert_question('bbc', 1109, 1, 'en', 
'Which countries have a GDP greater than any country in Europe? 
[Give the name only.]
');
SELECT insert_answer  ('bbc', 1109, 1,
'SELECT name FROM bbc x
  WHERE population >= ALL
    (SELECT population FROM bbc y
        WHERE y.region=x.region
          AND population>0)
');

SELECT insert_problem ('bbc', 1110, 4, 'subselect');
SELECT insert_question('bbc', 1110, 1, 'en', 
'Find the largest country in each region.
');
SELECT insert_answer  ('bbc', 1110, 1,
'SELECT region, name, population FROM bbc x
  WHERE population >= ALL
    (SELECT population FROM bbc y
        WHERE y.region=x.region
          AND population>0)
');

SELECT insert_problem ('bbc', 1111, 5, 'subselect');
SELECT insert_question('bbc', 1111, 1, 'en', 
'Find each country that belongs to a region where all populations
are less than 25000000. Show name, region and population.
');
SELECT insert_answer  ('bbc', 1111, 1,
'SELECT name,region,population FROM bbc x
  WHERE 25000000 >= ALL (
    SELECT population FROM bbc y
     WHERE x.region=y.region
       AND y.population>0)
');

SELECT insert_problem ('bbc', 1112, 5, 'subselect');
SELECT insert_question('bbc', 1112, 1, 'en', 
'Some countries have populations more than three times that of any
of their neighbours (in the same region). Give the countries and
regions.
');
SELECT insert_answer  ('bbc', 1112, 1,
'SELECT name, region FROM bbc x WHERE
 population > ALL
 (SELECT population*3 FROM bbc y
 WHERE y.region = x.region
 AND y.name <> x.name)
');

SELECT insert_problem ('bbc', 1113, 1, 'aggregate');
SELECT insert_question('bbc', 1113, 1, 'en', 
'Show the total population of the world.
');
SELECT insert_answer  ('bbc', 1113, 1,
'SELECT SUM(population) FROM bbc
');

SELECT insert_problem ('bbc', 1114, 1, 'aggregate');
SELECT insert_question('bbc', 1114, 1, 'en', 
'List all the regions - just once each.
');
SELECT insert_answer  ('bbc', 1114, 1,
'SELECT DISTINCT region 
  FROM bbc
');
SELECT insert_answer  ('bbc', 1114, 2,
'SELECT region 
  FROM bbc
 GROUP BY region
');

SELECT insert_problem ('bbc', 1115, 2, 'aggregate');
SELECT insert_question('bbc', 1115, 1, 'en', 
'Give the total GDP of Africa
');
SELECT insert_answer  ('bbc', 1115, 1,
'SELECT sum(gdp)
  FROM bbc
 WHERE region = ''Africa''
');

SELECT insert_problem ('bbc', 1116, 2, 'aggregate');
SELECT insert_question('bbc', 1116, 1, 'en', 
'How many countries have an area of at least 1000000
');
SELECT insert_answer  ('bbc', 1116, 1,
'SELECT COUNT(name) FROM bbc
  WHERE area>=1000000
');

SELECT insert_problem ('bbc', 1117, 2, 'aggregate');
SELECT insert_question('bbc', 1117, 1, 'en', 
'What is the total population of (''France'',''Germany'',''Spain'')
');
SELECT insert_answer  ('bbc', 1117, 1,
'SELECT SUM(population) FROM bbc
  WHERE name IN (''France'',''Germany'',''Spain'')
');

SELECT insert_problem ('bbc', 1118, 2, 'aggregate');
SELECT insert_question('bbc', 1118, 1, 'en', 
'For each region show the region and number of countries.
');
SELECT insert_answer  ('bbc', 1118, 1,
'SELECT region, COUNT(name) 
  FROM bbc 
 GROUP BY region
');

SELECT insert_problem ('bbc', 1119, 3, 'aggregate');
SELECT insert_question('bbc', 1119, 1, 'en', 
'For each region show the region and number of countries with
populations of at least 10 million.
');
SELECT insert_answer  ('bbc', 1119, 1,
'SELECT region, COUNT(name) FROM bbc
  WHERE population>=10000000
  GROUP BY region
');

SELECT insert_problem ('bbc', 1120, 3, 'aggregate');
SELECT insert_question('bbc', 1120, 1, 'en', 
'List the regions with total populations of at least 100 million.
');
SELECT insert_answer  ('bbc', 1120, 1,
'SELECT region FROM bbc
 GROUP BY region
HAVING sum(population)>100000000
');

COMMIT;

