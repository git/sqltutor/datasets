﻿/* This work is licensed under a Creative Commons Attribution 4.0
   International License.

   ---------------------------------------------------------------------------
   Copyright (c) 2017 by Burkhardt Renz. All rights reserved.
   Database Suppliers and Parts Tutorial with SQLTutor
   $Id: $
   ---------------------------------------------------------------------------
*/

/* The Suppliers-and-Parts Database Structure
*/

BEGIN;

CREATE SCHEMA IF NOT EXISTS sqltutor;
SET search_path TO sqltutor;

select init_dataset ('suppliers');
select add_ds_source('suppliers',2017, 'Burkhardt Renz');
select add_ds_table ('suppliers',1, 'suppliers', 'sno, sname, status, city');
select add_ds_table ('suppliers',2, 'parts','pno, pname, color, weight, city');
select add_ds_table ('suppliers',3, 'shipments', 'sno, pno, qty');


/* function parameters
   -------------------
   init_dataset           (dsname text)
   sqltutor.add_ds_source (dataset text, year int, source text)
   add_ds_table           (dataset text, ord int, ds_table text, columns text)
   insert_problem         (dataset text, problem_id int, points int,
			   category text)
   insert_question        (dataset text, problem_id int, q_ord int,
			   lang char(2), question text)s
   insert_answer          (dataset text, problem_id int, priority int,
			   answer text)
*/

/* The questions and answers
*/

select insert_problem ('suppliers', 1, 1, 'select');

select insert_question('suppliers', 1, 1, 'en',
'Select supplier number sno and status for all suppliers in Paris.');

select insert_question('suppliers', 1, 1, 'de',
'Ermitteln Sie Lieferantennummer sno und Status status für alle
 Lieferanten  in Paris.');

select insert_answer('suppliers', 1, 1,
$$
SELECT sno, status
FROM   suppliers
WHERE  city = 'Paris'
$$);


select insert_problem ('suppliers', 2, 1, 'select');

select insert_question('suppliers', 2, 1, 'en',
'Retrieve the part number pno of all actually delivered parts,
 show each occuring number just once.');

select insert_question('suppliers', 2, 1, 'de',
'Ermitteln Sie die Teilenummer pno von allen gelieferten Teilen, zeigen
 Sie jede vorkommende Nummer nur einmal an.');

select insert_answer('suppliers', 2, 1,
$$
SELECT DISTINCT pno
FROM   shipments
$$);


select insert_problem ('suppliers', 3, 1, 'select');

select insert_question('suppliers', 3, 1, 'en',
'Show all informations for all suppliers.');

select insert_question('suppliers', 3, 1, 'de',
'Zeigen Sie alle Angaben zu allen Lieferanten.');

select insert_answer  ('suppliers', 3, 1,
$$
SELECT * FROM suppliers
$$);


select insert_problem ('suppliers', 4, 2, 'select');

select insert_question('suppliers', 4, 1, 'en',
'Retrieve the supplier numbers of the suppliers
 in Paris having status greater than 20.');

select insert_question('suppliers', 4, 1, 'de',
'Ermitteln Sie die Lieferantennummer aller
 Lieferanten in Paris mit Status > 20.');
select insert_answer  ('suppliers', 4, 1,
$$
SELECT sno
FROM   suppliers
WHERE  city = 'Paris' AND status > 20
$$);


select insert_problem ('suppliers', 5, 2, 'select');

select insert_question('suppliers', 5, 1, 'en',
'Show the suppliers numbers and status of all
 the suppliers based in Paris
 in descending order of status.');

select insert_question ('suppliers', 5, 1, 'de',
'Erstellen Sie eine Liste mit Lieferantennummer
 und Status aller Lieferanten in Paris, sortiert
 in absteigender Reihenfolge des Status.');

select insert_answer('suppliers', 5, 1,
$$
SELECT sno, status
FROM   suppliers
WHERE  city = 'Paris' ORDER BY status DESC
$$);


select insert_problem ('suppliers', 6, 2, 'select');

select insert_question('suppliers', 6, 1, 'en',
'Show parts number pno, name pname and weight
 of all the parts which weigh between 16 and 19.');

select insert_question('suppliers', 6, 1, 'de',
'Erstellen Sie eine Liste mit Teilenummer pno, Name pname und Gewicht
 aller Teile deren Gewicht zwischen 16 und 19 liegt
 (Bereichsgrenzen inklusive).');

select insert_answer('suppliers', 6, 1,
$$
SELECT pno, pname, weight
FROM   parts
WHERE  weight BETWEEN 16 AND 19
$$);


select insert_problem ('suppliers', 7, 2, 'select');

select insert_question('suppliers', 7, 1, 'en',
'Show parts number pno, name pname and weight of all the parts which
 weigh 12, 14 or 19.');

select insert_question('suppliers', 7, 1, 'de',
'Erstellen Sie eine Liste mit Teilenummer pno, Name pname und Gewicht
 aller Teile deren Gewicht eines aus folgender Liste ist: 12, 14, 19.');

select insert_answer('suppliers', 7, 1,
$$
SELECT pno, pname, weight
FROM   parts
WHERE  weight IN (12, 14, 19)
$$);


select insert_problem ('suppliers', 8, 8, 'select');

select insert_question('suppliers', 8, 1, 'en',
'Show all informations about suppliers and parts where the supplier is
 based in the same city as the part is stored.');

select insert_question('suppliers', 8, 1, 'de',
'Erstellen Sie eine Liste aller Informationen über Lieferanten und Teile,
 bei denen der Lieferant in derselben Stadt seinen Sitz hat
 wo ein Teil gelagert wird.');

select insert_answer('suppliers', 8, 1,
$$
SELECT *
FROM   suppliers CROSS JOIN parts
WHERE  suppliers.city = parts.city
$$);


select insert_problem ('suppliers', 9, 8, 'select');

select insert_question('suppliers', 9, 1, 'en',
'Show a list of pairs of cities where the first one (scity) is the city of a
 suppliers who delivers parts that are stored in the second city (pcity).');

select insert_question('suppliers', 9, 1, 'de',
      'Erstellen Sie eine Liste von Paaren von Stadtnamen, bei denen ein
       Lieferant aus der ersten Stadt (scity) ein Teil liefert, das
       in der zweiten Stadt (pcity) gelagert ist.');

select insert_answer('suppliers', 9, 1,
$$
SELECT DISTINCT suppliers.city AS scity, parts.city AS pcity
FROM   suppliers
       JOIN shipments USING(sno)
       JOIN parts USING (pno)
$$);


select insert_problem ('suppliers', 10, 8, 'select');

select insert_question('suppliers', 10, 1, 'en',
'Show all pairs of supplier numbers where both are based in the same city.');

select insert_question('suppliers', 10, 1, 'de',
'Erstellen Sie eine Liste von Paaren von Lieferantennummern,
 bei denen die beiden Lieferanten aus derselben Stadt sind.');

select insert_answer('suppliers', 10, 1,
$$
SELECT s1.sno, s2.sno
FROM   suppliers AS s1 CROSS JOIN suppliers AS s2
WHERE  s1.city = s2.city AND s1.sno < s2.sno
$$);


select insert_problem ('suppliers', 11, 8, 'select');

select insert_question('suppliers', 11, 1, 'en',
'Show the names of suppliers that deliver the part with pno P2.');

select insert_question('suppliers', 11, 1, 'de',
'Ermitteln Sie die Namen der Lieferanten, die das Teil mit der pno P2
 liefern.');

select insert_answer('suppliers', 11, 1,
$$
SELECT sname
FROM   suppliers
WHERE  sno IN (SELECT sno FROM shipments WHERE pno = 'P2')
$$);


select insert_problem ('suppliers', 12, 8, 'select');

select insert_question('suppliers', 12, 1, 'en',
'Show the names of suppliers that deliver parts with color Red.');

select insert_question('suppliers', 12, 1, 'de',
'Ermitteln Sie die Namen der Lieferanten, die Teile der Farbe Red liefern.');

select insert_answer('suppliers', 12, 1,
$$
SELECT DISTINCT sname
FROM   suppliers
       JOIN shipments USING (sno)
       JOIN parts USING (pno)
WHERE  parts.color = 'Red'
$$);

select insert_answer('suppliers', 12, 2,
$$
SELECT sname
FROM   suppliers
WHERE  sno IN  (SELECT sno FROM shipments WHERE pno IN
		       (SELECT pno FROM parts WHERE color = 'Red'))
$$);


select insert_problem ('suppliers', 13, 2, 'select');

select insert_question('suppliers', 13, 1, 'en',
'Retrieve the number of suppliers.');

select insert_question('suppliers', 13, 1, 'de',
'Ermitteln Sie die Anzahl der Lieferanten.');

select insert_answer('suppliers', 13, 1,
$$
SELECT COUNT(*) FROM suppliers
$$);

select insert_answer('suppliers', 13, 2,
$$
SELECT COUNT(sno) FROM suppliers
$$);


select insert_problem ('suppliers', 14, 2, 'select');

select insert_question('suppliers', 14, 1, 'en',
'Retrieve the number of suppliers that actually delivered parts.');

select insert_question('suppliers', 14, 1, 'de',
'Ermitteln Sie die Anzahl der Lieferanten, die tatsächlich Teile
 geliefert haben.');

select insert_answer('suppliers', 14, 1,
$$
SELECT COUNT(DISTINCT sno) FROM shipments
$$);


select insert_problem ('suppliers', 15, 2, 'select');

select insert_question('suppliers', 15, 1, 'en',
'Show how many of the parts with pno P2 have been delivered.');

select insert_question('suppliers', 15, 1, 'de',
      'Ermitteln Sie wieviele Teile mit pno P2 geliefert wurden.');

select insert_answer('suppliers', 15, 1,
$$
SELECT SUM(qty)
FROM   shipments
WHERE  pno = 'P2'
$$);


select insert_problem ('suppliers', 16, 2, 'select');

select insert_question('suppliers', 16, 1, 'en',
'Show the numbers of all parts and how many of them have been delivered
 all together.');

select insert_question('suppliers', 16, 1, 'de',
'Erstellen Sie eine Liste mit Teilenummer und Gesamtmenge aus allen
 Lieferungen des entsprechenden Teils.');

select insert_answer('suppliers', 16, 1,
$$
SELECT pno, sum(qty)
FROM   shipments
GROUP BY pno
$$);


select insert_problem ('suppliers', 17, 6, 'select');

select insert_question('suppliers', 17, 1, 'en',
'Retrieve the part numbers pno of parts being delivered by more than
 1 supplier.');

select insert_question('suppliers', 17, 1, 'de',
'Erstellen Sie eine Liste von Teilenummern all der Teile, die von mehr
 als einem Lieferanten geliefert werden.');

select insert_answer('suppliers', 17, 1,
$$
SELECT pno
FROM   shipments
GROUP BY pno
HAVING COUNT(*) > 1
$$);

select insert_problem ('suppliers', 18, 4, 'select');

select insert_question('suppliers', 18, 1, 'en',
'Show all cities where suppliers are based or parts are stored.');

select insert_question('suppliers', 18, 1, 'de',
'Ermitteln Sie alle Städte, in denen Lieferanten ihren Firmensitz haben oder
Teile gelagert werden.');

select insert_answer('suppliers', 18, 1,
$$
SELECT city FROM suppliers
UNION
SELECT city FROM parts
$$);

select insert_problem ('suppliers', 19, 4, 'select');

select insert_question('suppliers', 19, 1, 'en',
'Show all cities where suppliers are based and parts are stored.');

select insert_question('suppliers', 19, 1, 'de',
'Ermitteln Sie alle Städte, in denen sowohl Lieferanten ihren Firmensitz haben
 als auch Teile gelagert werden.');

select insert_answer('suppliers', 19, 1,
$$
SELECT city FROM suppliers
INTERSECT
SELECT city FROM parts
$$);

select insert_problem ('suppliers', 20, 4, 'select');

select insert_question('suppliers', 20, 1, 'en',
'Show all cities where suppliers are based but no parts are stored.');

select insert_question('suppliers', 20, 1, 'de',
'Ermitteln Sie alle Städte, in denen Lieferanten ihren Firmensitz haben aber
keine Teile gelagert werden.');

select insert_answer('suppliers', 20, 1,
$$
SELECT city FROM suppliers
EXCEPT
SELECT city FROM parts
$$);

COMMIT;
