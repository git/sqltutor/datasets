BEGIN;

SET search_path TO sqltutor;

SELECT insert_problem ('buses', 1501, 1, 'aggregate');
SELECT insert_question('buses', 1501, 1, 'en', 
'How many stops are in the database?
');
SELECT insert_answer  ('buses', 1501, 1,
'SELECT COUNT(*) FROM stops;
');

SELECT insert_problem ('buses', 1502, 2, 'select');
SELECT insert_question('buses', 1502, 1, 'en', 
'Find the id value for the stop ''Craiglockhart''
');
SELECT insert_answer  ('buses', 1502, 1,
'SELECT id FROM stops WHERE name=''Craiglockhart'';
');

SELECT insert_problem ('buses', 1503, 3, 'select');
SELECT insert_question('buses', 1503, 1, 'en', 
'Give the id and the name for the stops on the ''4'' ''LRT'' service.
');
SELECT insert_answer  ('buses', 1503, 1,
'SELECT id, name 
  FROM stops, route
 WHERE id=stop
   AND company=''LRT''
   AND num=''4'';
');

SELECT insert_problem ('buses', 1507, 4, 'selfjoin');
SELECT insert_question('buses', 1507, 1, 'en', 
'Give a list of all the services which connect stops 115 and 137
(''Haymarket'' and ''Leith'')
');
SELECT insert_answer  ('buses', 1507, 1,
'SELECT R1.company, R1.num
  FROM route R1, route R2
  WHERE R1.num=R2.num AND R1.company=R2.company
    AND R1.stop=115 AND R2.stop=137;
');

/*
SELECT insert_problem ('buses', 1508, 5, 'selfjoin');
SELECT insert_question('buses', 1508, 1, 'en', 
'Give a list of the services which connect the stops ''Craiglockhart''
and ''Tollcross''
');
SELECT insert_answer  ('buses', 1508, 1,
'SELECT R1.company, R1.num
  FROM route R1, route R2, stops S1, stops S2
  WHERE R1.num=R2.num AND R1.company=R2.company
    AND R1.stop=S1.id AND R2.stop=S2.id
    AND S1.name=''Craiglockhart''
    AND S2.name=''Tollcross'';     
');

SELECT insert_problem ('buses', 1509, 12, 'selfjoin');
SELECT insert_question('buses', 1509, 1, 'en', 
'Give a list of the stops which may be reached from ''Craiglockhart''
by taking one bus. Include the details of the appropriate service.
');
SELECT insert_answer  ('buses', 1509, 1,
'SELECT S2.id, S2.name, R2.company, R2.num
  FROM stops S1, stops S2, route R1, route R2
  WHERE S1.name=''Craiglockhart''
    AND S1.id=R1.stop
    AND R1.company=R2.company AND R1.num=R2.num
    AND R2.stop=S2.id;
');

SELECT insert_problem ('buses', 1512, 15, 'subselect');
SELECT insert_question('buses', 1512, 1, 'en', 
'Show how it is possible to get from Craiglockhart to Sighthill 
(with one change)
');
SELECT insert_answer  ('buses', 1512, 1,
'SELECT a.name, b.num, b.company,
       e.name, f.num, f.company,
       g.name
 FROM  stops a, route b,
       stops c, route d,
       stops e, route f,
       stops g, route h
WHERE  a.id = b.stop                 
  AND  c.id = d.stop
  AND  e.id = f.stop
  AND  g.id = h.stop  
  AND  b.num = d.num
  AND  b.company = d.company   
  AND  f.num = h.num
  AND  f.company = h.company   
  AND  c.name = e.name
  AND  a.name = ''Craiglockhart''
  AND  g.name = ''Sighthill''
  AND  d.pos > b.pos
  AND  h.pos > f.pos;
');
SELECT insert_answer  ('buses', 1512, 2,
'SELECT X.name, 
       A.num, A.company, Y.name, D.num, C.company,
       Z.name
  FROM route A
       join route B
            on A.num=B.num AND A.company=B.company AND A.pos < B.pos
       join route C
            on B.stop=C.stop
       join route D
            on C.num=D.num AND C.company=D.company and C.pos < D.pos
       join stops X
            on A.stop=X.id AND X.name=''Craiglockhart''
       join stops Y
            on C.stop=Y.id 
       join stops Z
            on D.stop=Z.id AND Z.name=''Sighthill'';
');
*/

COMMIT;

