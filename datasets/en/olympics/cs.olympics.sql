/*
   This file is public domain.

   The data is a matter of public record, and comes from
   http://www.gnu.org/software/sqltutor
 */

SET search_path TO sqltutor;

-- translated quiz from sqlzoo.ne

BEGIN;

SELECT insert_question('ttms', 1201, 1, 'cs',
/* Men''s Singles Table Tennis Olympics Database:
Show the athelete (who) and the country name for medal winners in
2000. */
$$
Stolní tenis (singly muži):
Uveďte zemi a atlety (who), kteří získali olympijskou medaili v roce 2000.
$$
);

/* 'Men''s Singles Table Tennis Olympics Database: Show the who and
the color of the medal for the medal winners from 'Sweden'. */
SELECT insert_question('ttms', 1202, 1, 'cs',
$$
Stolní tenis (singly muži):
Kteří olympionici ze Švédska ('Sweden') získal medaili a jakou?
$$
);


/* Men''s Singles Table Tennis Olympics Database: Show the years in
which ''China'' won a ''gold'' medal. */
SELECT insert_question('ttms', 1203, 1, 'cs',
$$
Stolní tenis (singly muži):
Uveďte roky, ve kterých Čína ('China') získala zlatou medaili ('gold').
$$
);

/* Women''s Singles Table Tennis Olympics Database:
Show who won medals in the ''Barcelona'' games.*/
SELECT insert_question('ttws', 1204, 1, 'cs',
$$
Stolní tenis (singly ženy):
Kdo získal medaile na olympiádě v Barceloně?
$$
);

-- ambiquious
--
-- /* Women''s Singles Table Tennis Olympics Database: Show which city
-- ''CHEN, Jing'' won medals. Show the city and the medal color. */
-- SELECT insert_question('ttws', 1205, 1, 'cs',
-- $$
-- Stolní tenis (singly ženy):
-- Na kterých olympijských hrách získala 'CHEN, Jing' medaili?
-- Uveďte město a medaili.
-- $$
-- );


/* Women''s Singles Table Tennis Olympics Database:
Show who won the gold medal and the city. */
SELECT insert_question('ttws', 1206, 1, 'cs',
$$
Stolní tenis (singly ženy):
Uveďte kdo získal zlato a ve kterém městě.
$$
);

/* Table Tennis Men''s Doubles:
Show the games and color of the medal won by the team that includes
'YAN, Sen'. */
SELECT insert_problem ('ttmd', 1207, 3, 'join');
SELECT insert_question('ttmd', 1207, 1, 'cs',
$$
Stolní tenis (dvouhry mužů):
Uveďte rok a olympijskou medaili, kterou získal tým ve kterém hrál 'YAN, Sen'.
$$
);

/* Table Tennis Men''s Doubles:
Show the ''gold'' medal winners in 2004. */
SELECT insert_question('ttmd', 1208, 1, 'cs',
$$
Stolní tenis (dvouhry mužů):
Uveďte kdo získal zlato na olympijských hrách 2004.
$$
);

/* Table Tennis Men''s Doubles: Show the name of each medal winner
country 'FRA'. */
SELECT insert_question('ttmd', 1209, 1, 'cs',
$$
Stolní tenis (dvouhry mužů):
Uveďte jména všech franzouských medailistů ('FRA').
$$
);

COMMIT;


-- above queries from sqlzoo applied to 'olympics' dataset

BEGIN;

/* Table Tennis (singles Men):
Show the athelete and the country name for medal winners in
2000. */
SELECT insert_question('olympics', 1, 1, 'cs',
$$
Discipline Table Tennis (event singles Men):
Uveďte zemi a atlety (who), kteří získali olympijské medaile v roce 2000.
$$
);

/* Table Tennis (singles Men): Show athlete name and the color of the
medal for the medal winners from 'Sweden'. */
SELECT insert_question('olympics', 2, 1, 'cs',
$$
Table Tennis (singles Men):
Kteří olympionici ze Švédska ('Sweden') získal medaili?
Uveďte uveďte jméno, příjmení, medaili a rok.
$$
);

/* Table Tennis (singles Men):
Show the years in which ''China'' won a ''gold'' medal. */
SELECT insert_question('olympics', 3, 1, 'cs',
$$
Discipline Table Tennis (event singles Men):
Uveďte roky, ve kterých Čína ('China') získala zlatou medaili ('gold').
$$
);

/* Table Tennis (singles Women):
Show who won medals in the ''Barcelona'' olympics. */
SELECT insert_question('olympics', 4, 1, 'cs',
$$
Discipline Table Tennis (event singles Women):
Kdo získal medaile na olympiádě v Barceloně?
$$
);

/* Table Tennis (singles Women): Show which city ''Chen, Jing'' won
medals. Show the city and the medal color. */
SELECT insert_question('olympics', 5, 1, 'cs',
$$
Discipline Table Tennis (event singles Women):
Na kterých olympijských hrách získala 'Chen, Jing' medaili?
Uveďte město a medaili.
$$
);


/* Show who won the gold medal and the city. */
SELECT insert_question('olympics', 6, 1, 'cs',
$$
Discipline Table Tennis (event singles Women):
Uveďte kdo získal zlato a ve kterém městě.
Pozn.: pamatujte, že v některých letech se konaly zároveň
letní i zimní Olympijské hry.
$$);


/* Show the olympics year and color of the medal won by the team that includes
'Yan, Sen'. */
SELECT insert_question('olympics', 7, 1, 'cs',
$$
Discipline Table Tennis (event doubles Men):
Uveďte rok a olympijskou medaili, kterou získal tým ve kterém hrál 'Yan, Sen'.
$$
);

/* Show the 'gold' medal winners in 2004. */
SELECT insert_question('olympics', 8, 1, 'cs',
$$
Discipline Table Tennis (event doubles Men):
Uveďte kdo získal zlato na olympijských hrách 2004.
$$
);


/* Show the name of each medal winner country 'FRA'. */
SELECT insert_question('olympics', 9, 1, 'cs',
$$
Discipline Table Tennis (event doubles Men):
Uveďte jména všech franzouských medailistů ('FRA').
$$
);

COMMIT;
