BEGIN;

SET search_path TO sqltutor;

SELECT insert_problem ('olympics', 1, 8, 'select');
SELECT insert_question('olympics', 1, 1, 'en', '
Discipline Table Tennis (event singles Men):
Show the athelete and the country name for medal winners in
2000.');
SELECT insert_answer  ('olympics', 1, 1, $$
SELECT forename, surname, country 
  FROM sports
       JOIN medals USING (discipline_id, event_id)
       JOIN athletes USING (athlete_id)
       JOIN ioc_codes USING (country_code)
 WHERE discipline='Table Tennis'
   AND event='singles Men'
   AND year=2000;
$$);


SELECT insert_problem ('olympics', 2, 8, 'select');
SELECT insert_question('olympics', 2, 1, 'en', '
Discipline Table Tennis (event singles Men):
Show athlete name, the color of the medal and year for the medal winners from
''Sweden''.
');
SELECT insert_answer  ('olympics', 2, 1, $$
SELECT forename, surname, medal, year
  FROM sports
       JOIN medals USING (event_id, discipline_id)
       JOIN athletes USING (athlete_id)
       JOIN ioc_codes USING (country_code)
 WHERE discipline='Table Tennis'
   AND event='singles Men' 
   AND country='Sweden';$$);


SELECT insert_problem ('olympics', 3, 8, 'select');
SELECT insert_question('olympics', 3, 1, 'en', '
Discipline Table Tennis (event singles Men):
Show the years in which ''China'' won a ''gold'' medal.
');
SELECT insert_answer  ('olympics', 3, 1, $$
SELECT year
  FROM sports
       JOIN medals USING (discipline_id, event_id)
       JOIN athletes USING (athlete_id)
       JOIN ioc_codes USING (country_code)
 WHERE discipline='Table Tennis'
   AND event='singles Men' 
   AND country='China' 
   AND medal='gold';$$);


SELECT insert_problem ('olympics', 4, 8, 'select');
SELECT insert_question('olympics', 4, 1, 'en', '
Discipline Table Tennis (event singles Women):
Show who won medals in the ''Barcelona'' olympics.
');
SELECT insert_answer  ('olympics', 4, 1, $$
SELECT forename, surname
  FROM sports
       JOIN medals USING (discipline_id, event_id)
       JOIN olympics USING (year, category)
       JOIN athletes USING (athlete_id)
 WHERE discipline='Table Tennis'
   AND event='singles Women'
   AND city='Barcelona'; 
$$);


SELECT insert_problem ('olympics', 5, 8, 'select');
SELECT insert_question('olympics', 5, 1, 'en', '
Discipline Table Tennis (event singles Women):
Show which city ''Chen, Jing'' won medals. Show the city and the medal
color.');
SELECT insert_answer  ('olympics', 5, 1, $$
SELECT city, medal
  FROM olympics
       JOIN medals USING (year)
       JOIN sports USING (discipline_id, event_id, category)
       JOIN athletes USING (athlete_id)
 WHERE discipline='Table Tennis'
   AND event='singles Women' 
   AND forename='Jing'
   AND surname='Chen';
$$);


SELECT insert_problem ('olympics', 6, 8, 'select');
SELECT insert_question('olympics', 6, 1, 'en', '
Discipline Table Tennis (event singles Women):
Show who won the gold medal and the city.
Note: remember that in some years both summer and winter
Olympics were held.
');
SELECT insert_answer  ('olympics', 6, 1, $$
SELECT forename, surname, city
  FROM sports
       JOIN medals USING (discipline_id, event_id)
       JOIN olympics USING (year, category)
       JOIN athletes USING (athlete_id)
 WHERE discipline='Table Tennis'
   AND event='singles Women' 
   AND medal='gold';
$$);


SELECT insert_problem ('olympics', 7, 8, 'select');
SELECT insert_question('olympics', 7, 1, 'en', '
Discipline Table Tennis (event doubles Men):
Show the olympics year and color of the medal won by the team that includes
''Yan, Sen''.');
SELECT insert_answer  ('olympics', 7, 1, $$
SELECT year, medal
  FROM sports
       JOIN medals USING (discipline_id, event_id)
       JOIN athletes USING (athlete_id)
 WHERE discipline='Table Tennis'
   AND event='doubles Men'
   AND forename='Sen'
   AND surname='Yan';
$$);


SELECT insert_problem ('olympics', 8, 8, 'select');
SELECT insert_question('olympics', 8, 1, 'en', '
Discipline Table Tennis (event doubles Men):
Show the ''gold'' medal winners in 2004.
');
SELECT insert_answer  ('olympics', 8, 1, $$
SELECT forename, surname
  FROM sports
       JOIN medals USING (discipline_id, event_id)
       JOIN olympics USING (year, category)
       JOIN athletes USING (athlete_id)
 WHERE discipline='Table Tennis'
   AND event='doubles Men'
   AND medal='gold'
   AND year=2004;
$$);


SELECT insert_problem ('olympics', 9, 8, 'select');
SELECT insert_question('olympics', 9, 1, 'en', '
Discipline Table Tennis (event doubles Men):
Show the name of each medal winner country ''FRA''.
');
SELECT insert_answer  ('olympics', 9, 1, $$
SELECT forename, surname
  FROM sports
       JOIN medals USING (discipline_id, event_id)
       JOIN olympics USING (year, category)
       JOIN athletes USING (athlete_id)
 WHERE discipline='Table Tennis'
   AND event='doubles Men'
   AND medals.country_code = 'FRA';
$$);


COMMIT;
