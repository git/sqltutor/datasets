/*
   This file is public domain.

   The data is a matter of public record, and the views mimic base
   tables ttmd, team, ttms, ttws and games from http://sqlzoo.net/
 */

BEGIN;

SET search_path TO sqltutor;

/* Give name, medal and country of marathon medallist in the first olympics
in Athens 1896. */
SELECT insert_question('olympics', 101, 1, 'cs',
$$
Discipline Athletics (event marathon Men):
Uveďte jméno, medaili a zemi medailistů z první olympiády v Athénách 1896.
$$
);


/* Give name, country, year and city of olympics marathon winners. */
SELECT insert_question('olympics', 102, 1, 'cs',
$$
Discipline Athletics (event marathon Men):
Uveďte jméno, zemi, rok a město maratonských vítězů, kteří získali zlato.
$$
);


/* Who won gold medal more than once? Give name and count. */
SELECT insert_question('olympics', 103, 1, 'cs',
$$
Discipline Athletics (event marathon Men):
Kdo získal více než jednu zlatou medaili v olympijském maratonu?
Uveďte jméno a počet.
$$
);


/* Who won more than one marathon medals?
Give name, country, medal, year and city */
SELECT insert_question('olympics', 104, 1, 'cs',
$$
Discipline Athletics (event marathon Men):
Kdo získal více než jednu marathonskou medaili?
Uveďte jméno, zemi, medaili, rok a město.
$$
);


/* Give name and medal of marathon medallist from Greece. */
SELECT insert_question('olympics', 105, 1, 'cs',
$$
Discipline Athletics (event marathon Men):
Uveďte jméno a medaili řeckých maratonců ('Greece').
$$
);


/* Give country and count of medals. Do not list coutries 
without marathon medals. */
SELECT insert_question('olympics', 106, 1, 'cs',
$$
Discipline Athletics (event marathon Men):
Uveďte zemi a počet medailí v maratonu mužů. Země bez medailí neuvádějte.
$$
);


COMMIT;

