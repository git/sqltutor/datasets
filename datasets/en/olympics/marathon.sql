/*
   This file is public domain.

   The data is a matter of public record, and the views mimic base
   tables ttmd, team, ttms, ttws and games from http://sqlzoo.net/
 */

BEGIN;

SET search_path TO sqltutor;

SELECT insert_problem ('olympics', 101, 8, 'select');
SELECT insert_question('olympics', 101, 1, 'en',
$$
Discipline Athletics (event marathon Men):
Give name, medal and country of marathon medallist in the first olympics
in Athens 1896.
$$
);
SELECT insert_answer  ('olympics', 101, 1,
$$
SELECT forename, surname, medal, country
  FROM sports
       JOIN medals USING (discipline_id, event_id)
       JOIN athletes USING (athlete_id)
       JOIN ioc_codes USING (country_code)
 WHERE discipline='Athletics'
   AND event='marathon Men'
   AND year=1896;
$$
);


SELECT insert_problem ('olympics', 102,10, 'select');
SELECT insert_question('olympics', 102, 1, 'en',
$$
Discipline Athletics (event marathon Men):
Give name, country, year and city of olympics marathon winners (gold medal).
$$
);
SELECT insert_answer  ('olympics', 102, 1,
$$
SELECT forename, surname, country, year, city
  FROM sports
       JOIN medals USING (discipline_id, event_id)
       JOIN athletes USING (athlete_id)
       JOIN ioc_codes USING (country_code)
       JOIN olympics USING (year, category)
 WHERE discipline='Athletics'
   AND event='marathon Men'
   AND medal='gold';
$$
);


SELECT insert_problem ('olympics', 103,12, 'select');
SELECT insert_question('olympics', 103, 1, 'en',
$$
Discipline Athletics (event marathon Men):
Who won gold medal more than once?
Give name and count.
$$
);
SELECT insert_answer  ('olympics', 103, 1,
$$
SELECT forename, surname, count(*)
  FROM sports
       JOIN medals USING (discipline_id, event_id)
       JOIN athletes USING (athlete_id)
       JOIN ioc_codes USING (country_code)
       JOIN olympics USING (year, category)
 WHERE discipline='Athletics'
   AND event='marathon Men'
   AND medal='gold'
 GROUP BY athlete_id, forename, surname
HAVING count(*) > 1;
$$
);


SELECT insert_problem ('olympics', 104,15, 'select');
SELECT insert_question('olympics', 104, 1, 'en',
$$
Discipline Athletics (event marathon Men):
Who won more than one marathon medals?
Give name, country, medal, year and city
$$
);
SELECT insert_answer  ('olympics', 104, 1,
$$
SELECT forename, surname, country, medal, year, city
  FROM sports
       JOIN medals USING (discipline_id, event_id)
       JOIN athletes USING (athlete_id)
       JOIN ioc_codes USING (country_code)
       JOIN olympics USING (year, category)
 WHERE discipline='Athletics'
   AND event='marathon Men'
   AND athlete_id IN (SELECT athlete_id
                        FROM sports
                             JOIN medals USING (discipline_id, event_id)
                             JOIN athletes USING (athlete_id)
                             JOIN ioc_codes USING (country_code)
                             JOIN olympics USING (year, category)
                       WHERE discipline='Athletics'
                         AND event='marathon Men'
                       GROUP BY athlete_id
                      HAVING count(*) > 1
                     )
 ORDER BY athlete_id;
$$
);


SELECT insert_problem ('olympics', 105, 8, 'select');
SELECT insert_question('olympics', 105, 1, 'en',
$$
Discipline Athletics (event marathon Men):
Give name and medal of marathon medallist from Greece.
$$
);
SELECT insert_answer  ('olympics', 105, 1,
$$
SELECT forename, surname, medal
  FROM sports
       JOIN medals USING (discipline_id, event_id)
       JOIN athletes USING (athlete_id)
       JOIN ioc_codes USING (country_code)
       JOIN olympics USING (year, category)
 WHERE discipline='Athletics'
   AND event='marathon Men'
   AND country='Greece';
$$
);


SELECT insert_problem ('olympics', 106,10, 'select');
SELECT insert_question('olympics', 106, 1, 'en',
$$
Athletics (marathon Men):
Give country and count of medals. Do not list coutries 
without marathon medals.
$$
);
SELECT insert_answer  ('olympics', 106, 1,
$$
SELECT ioc_codes.country, count(*)
  FROM sports
       JOIN medals USING (discipline_id, event_id)
       JOIN athletes USING (athlete_id)
       JOIN ioc_codes USING (country_code)
       JOIN olympics USING (year, category)
 WHERE discipline='Athletics'
   AND event='marathon Men'
 GROUP BY ioc_codes.country_code, country
HAVING count(*)>0;
$$
);


COMMIT;
