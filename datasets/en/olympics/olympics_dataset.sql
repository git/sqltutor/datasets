SELECT sqltutor.init_dataset ('olympics');
SELECT sqltutor.add_ds_source('olympics', 2009, 'http://www.olympics.org/');
SELECT sqltutor.add_ds_source('olympics', 2009, 'http://en.wikipedia.org/wiki/Olympic_Games');
SELECT sqltutor.add_ds_table ('olympics', 1, 'olympics', 'year, city, category, country_code, opening, closing, nations, athletes, sports, events');
SELECT sqltutor.add_ds_table ('olympics', 2, 'athletes', 'athlete_id, forename, surname');
SELECT sqltutor.add_ds_table ('olympics', 3, 'sports', 'sport, category, discipline_id, discipline, event_id, event, status');
SELECT sqltutor.add_ds_table ('olympics', 4, 'medals', 'year, discipline_id, event_id, athlete_id, medal, country_code');
SELECT sqltutor.add_ds_table ('olympics', 5, 'ioc_codes', 'country_code, country');

