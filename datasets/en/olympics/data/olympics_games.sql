/*
   This file is public domain.

   The data is a matter of public record, and comes from

      http://www.olympics.org/
      http://en.wikipedia.org/wiki/Olympic_Games
*/


SET search_path TO sqltutor_data;

BEGIN;

INSERT INTO olympics (year, city, category, country_code, opening, closing,
                      nations, athletes, sports, events) VALUES

-- Olympic Summer Games

(1896, 'Athens',                  'Summer',
       'GRE', '06 April 1896',    '15 April 1896',      14,   241,  9,  43),
(1900, 'Paris',                   'Summer',
       'FRA', '14 May 1900',      '28 October 1900',    24,   997, 18,  95),
(1904, 'St. Louis',               'Summer',
       'USA', '01 July 1904',     '23 November 1904',   12,   651, 17,  91),
(1908, 'London',                  'Summer',
       'GBR', '27 April 1908',    '31 October 1908',    22,  2008, 22, 110),
(1912, 'Stockholm',               'Summer',
       'SWE', '05 May 1912',      '27 July 1912',       28,  2407, 14, 102),
(1920, 'Antwerp',                 'Summer',
       'BEL', '20 April 1920',    '12 September 1920',  29,  2626, 22, 154),
(1924, 'Paris',                   'Summer',
       'FRA', '04 May 1924',      '27 July 1924',       44,  3089, 17, 126),
(1928, 'Amsterdam',               'Summer',
       'NED', '17 May 1928',      '12 August 1928',     46,  2883, 14, 109),
(1932, 'Los Angeles',             'Summer',
       'USA', '30 July 1932',     '14 August 1932',     37,  1332, 14, 117),
(1936, 'Berlin',                  'Summer',
       'GER', '01 August 1936',   '16 August 1936',     49,  3963, 19, 129),
(1948, 'London',                  'Summer',
       'GBR', '29 July 1948',     '14 August 1948',     59,  4104, 17, 136),
(1952, 'Helsinki',                'Summer',
       'FIN', '19 July 1952',     '03 August 1952',     69,  4955, 17, 149),
(1956, 'Melbourne',               'Summer',
       'AUS', '22 November 1956', '22 November 1956',   72,  3314, 17, 145),
(1960, 'Rome',                    'Summer',
       'ITA', '25 August 1960',   '11 September 1960',  83,  5338, 17, 150),
(1964, 'Tokyo',                   'Summer',
       'JPN', '10 October 1964',  '24 October 1964',    93,  5151, 19, 163),
(1968, 'Mexico City',             'Summer',
       'MEX', '12 October 1968',  '27 October 1968',   112,  5516, 20, 172),
(1972, 'Munich',                  'Summer',
       'FRG', '26 August 1972',   '11 September 1972', 121,  7134, 23, 195),
(1976, 'Montreal',                'Summer',
       'CAN', '17 July 1976',     '01 August 1976',     92,  6084, 21, 198),
(1980, 'Moscow',                  'Summer',
       'URS', '19 July 1980',     '03 August 1980',     80,  5179, 21, 203),
(1984, 'Los Angeles',             'Summer',
       'USA', '28 July 1984',     '12 August 1984',    140,  6829, 23, 221),
(1988, 'Seoul',                   'Summer',
       'KOR', '17 September 1988','02 October 1988',   159,  8391, 25, 237),
(1992, 'Barcelona',               'Summer',
       'ESP', '25 July 1992',     '09 August 1992',    169,  9356, 28, 257),
(1996, 'Atlanta',                 'Summer',
       'USA', '9 July 1996',      '04 August 1996',    197, 10318, 26, 271),
(2000, 'Sydney',                  'Summer',
       'AUS', '15 September 2000','01 October 2000',   199, 10651, 28, 300),
(2004, 'Athens',                  'Summer',
       'GRE', '13 August 2004',   '29 August 2004',    201, 10625, 28, 301),
(2008, 'Beijing',                 'Summer',
       'CHN', '08 August 2008',   '24 August 2008',    204, 11028, 28, 302),


-- Olympis Winter Games

(1924, 'Chamonix',                'Winter',
       'FRA', '25 January 1924',  '05 February 1924',   16,   258,  6,  16),
(1928, 'St. Moritz',              'Winter',
       'SUI', '11 February 1928', '19 February 1928',   25,   464,  4,  14),
(1932, 'Lake Placid',             'Winter',
       'USA', '04 February 1932', '15 February 1932',   17,   252,  4,  14),
(1936, 'Garmisch-Partenkirchen',  'Winter',
       'SUI', '06 February 1936', '16 February 1936',   28,   646,  4,  17),
(1948, 'St. Moritz',              'Winter',
       'SUI', '30 January 1948',  '08 February 1948',   28,   669,  4,  22),
(1952, 'Oslo',                    'Winter',
       'NOR', '14 February 1952', '25 February 1952',   30,   694,  4,  22),
(1956, 'Cortina d''Ampezzo',      'Winter',
       'ITA', '26 January 1956',  '05 February 1956',   32,   821,  4,  24),
(1960, 'Squaw Valley',            'Winter',
       'USA', '18 February 1960', '28 February 1960',   30,   665,  4,  27),
(1964, 'Innsbruck',               'Winter',
       'AUT', '29 January 1964',  '09 February 1964',   36,  1091,  6,  34),
(1968, 'Grenoble',                'Winter',
       'FRA', '06 February 1968', '18 February 1968',   37,  1158,  6,  35),
(1972, 'Sapporo',                 'Winter',
       'JPN', '03 February 1972', '13 February 1972',   35,  1006,  6,  35),
(1976, 'Innsbruck',               'Winter',
       'AUT', '04 February 1976', '15 February 1976',   37,  1123,  6,  37),
(1980, 'Lake Placid',             'Winter',
       'USA', '13 February 1980', '24 February 1980',   37,  1072,  6,  38),
(1984, 'Sarajevo',                'Winter',
       'YUG', '08 February 1984', '19 February 1984',   49,  1272,  6,  39),
(1988, 'Calgary',                 'Winter',
       'CAN', '13 February 1988', '28 February 1988',   57,  1423,  6,  46),
(1992, 'Albertville',             'Winter',
       'FRA', '08 February 1992', '23 February 1992',   64,  1810,  7,  57),
(1994, 'Lillehammer',             'Winter',
       'NOR', '12 February 1994', '27 February 1994',   67,  1737,  6,  61),
(1998, 'Nagano',                  'Winter',
       'JPN', '07 February 1998', '22 February 1998',   72,  2176,  7,  68),
(2002, 'Salt Lake City',          'Winter',
       'USA', '08 February 2002', '24 February 2002',   77,  2399,  7,  78),
(2006, 'Turin',                   'Winter',
       'ITA', '10 February 2006', '26 February 2006',   80,  2508,  7,  84);

COMMIT;
