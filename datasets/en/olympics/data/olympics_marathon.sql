/*
   This file is public domain.

   The data is a matter of public record, and comes from

      http://www.olympics.org/
      http://en.wikipedia.org/wiki/Olympic_Games
*/


SET search_path TO sqltutor_data;

BEGIN;


SELECT ioc_insert(    'Athletics',   'marathon Men',  1896,'Athens',   1,   'LOUIS, Spyridon',   'GRE');
SELECT ioc_insert(    'Athletics',   'marathon Men',  1896,'Athens',   2,   'VASILAKOS, Kharilaos',   'GRE');
SELECT ioc_insert(    'Athletics',   'marathon Men',  1896,'Athens',   3,   'KELLNER, Gyula',   'HUN');
SELECT ioc_insert(     'Athletics',   'marathon Men',  1900,'Paris',   1,   'THEATO, Michel',   'FRA');
SELECT ioc_insert(     'Athletics',   'marathon Men',  1900,'Paris',   2,   'CHAMPION, Emile',   'FRA');
SELECT ioc_insert(     'Athletics',   'marathon Men',  1900,'Paris',   3,   'FAST, Ernst',   'SWE');
SELECT ioc_insert(  'Athletics',   'marathon Men',  1904,'St. Louis',   1,   'HICKS, Thomas',   'USA');
SELECT ioc_insert(  'Athletics',   'marathon Men',  1904,'St. Louis',   2,   'CORAY, Albert',   'USA');
SELECT ioc_insert(  'Athletics',   'marathon Men',  1904,'St. Louis',   3,   'NEWTON, Arthur',   'USA');
SELECT ioc_insert(    'Athletics',   'marathon Men',  1908,'London',   1,   'HAYES, John Joseph',   'USA');
SELECT ioc_insert(    'Athletics',   'marathon Men',  1908,'London',   2,   'HEFFERON, Charles A.',   'RSA');
SELECT ioc_insert(    'Athletics',   'marathon Men',  1908,'London',   3,   'FORSHAW, Joseph',   'USA');
SELECT ioc_insert( 'Athletics',   'marathon Men',  1912,'Stockholm',   1,   'MCARTHUR, Kennedy Kane',   'RSA');
SELECT ioc_insert( 'Athletics',   'marathon Men',  1912,'Stockholm',   2,   'GITSHAM, Christian W.',   'RSA');
SELECT ioc_insert( 'Athletics',   'marathon Men',  1912,'Stockholm',   3,   'STROBINO, Gaston',   'USA');
SELECT ioc_insert(   'Athletics',   'marathon Men',  1920,'Antwerp',   1,   'KOLEHMAINEN, Hannes',   'FIN');
SELECT ioc_insert(   'Athletics',   'marathon Men',  1920,'Antwerp',   2,   'LOSSMANN, Jüri',   'EST');
SELECT ioc_insert(   'Athletics',   'marathon Men',  1920,'Antwerp',   3,   'ARRI, Valerio',   'ITA');
SELECT ioc_insert(     'Athletics',   'marathon Men',  1924,'Paris',   1,   'STENROOS, Albin Oskar',   'FIN');
SELECT ioc_insert(     'Athletics',   'marathon Men',  1924,'Paris',   2,   'BERTINI, Romeo',   'ITA');

SELECT ioc_insert(                 'Athletics',   'marathon Men',  1924,'Paris',   3,                                  'DE MAR, Clarence Harrison',   'USA');
SELECT ioc_insert(             'Athletics',   'marathon Men',  1928,'Amsterdam',   1,                                  'EL OUAFI, Boughèra Mohamed',   'FRA');
SELECT ioc_insert(             'Athletics',   'marathon Men',  1928,'Amsterdam',   2,                                  'PLAZA REYES, Manuel',   'CHI');
SELECT ioc_insert(             'Athletics',   'marathon Men',  1928,'Amsterdam',   3,                                  'MARTTELIN, Martti B.',   'FIN');
SELECT ioc_insert(           'Athletics',   'marathon Men',  1932,'Los Angeles',   1,                                  'ZABALA, Juan Carlos',   'ARG');
SELECT ioc_insert(           'Athletics',   'marathon Men',  1932,'Los Angeles',   2,                                  'FERRIS, Samuel',   'GBR');
SELECT ioc_insert(           'Athletics',   'marathon Men',  1932,'Los Angeles',   3,                                  'TOIVONEN, Armas Adam',   'FIN');
SELECT ioc_insert(                'Athletics',   'marathon Men',  1936,'Berlin',   1,                                  'SON, Kitei',   'JPN');
SELECT ioc_insert(                'Athletics',   'marathon Men',  1936,'Berlin',   2,                                  'HARPER, Ernest',   'GBR');
SELECT ioc_insert(                'Athletics',   'marathon Men',  1936,'Berlin',   3,                                  'NAN, Shoryu',   'JPN');
SELECT ioc_insert(                'Athletics',   'marathon Men',  1948,'London',   1,                                  'CABRERA, Delfo',   'ARG');
SELECT ioc_insert(                'Athletics',   'marathon Men',  1948,'London',   2,                                  'RICHARDS, Thomas John Henry',   'GBR');
SELECT ioc_insert(                'Athletics',   'marathon Men',  1948,'London',   3,                                  'GAILLY, Etienne',   'BEL');
SELECT ioc_insert(              'Athletics',   'marathon Men',  1952,'Helsinki',   1,   'ZATOPEK, Emil',   'TCH');
SELECT ioc_insert(              'Athletics',   'marathon Men',  1952,'Helsinki',   2,                                  'GORNO, Reinaldo',   'ARG');
SELECT ioc_insert(              'Athletics',   'marathon Men',  1952,'Helsinki',   3,                                  'JANSSON, Gustaf',   'SWE');
SELECT ioc_insert( 'Athletics',   'marathon Men',  1956,'Melbourne / Stockholm',   1,                                  'MIMOUN, Alain',   'FRA');
SELECT ioc_insert( 'Athletics',   'marathon Men',  1956,'Melbourne / Stockholm',   2,   'MIHALIC, Franjo',   'YUG');
SELECT ioc_insert( 'Athletics',   'marathon Men',  1956,'Melbourne / Stockholm',   3,                                  'KARVONEN, Veikko Leo',   'FIN');
SELECT ioc_insert(                  'Athletics',   'marathon Men',  1960,'Rome',   1,                                  'BIKILA, Abebe',   'ETH');

SELECT ioc_insert(        'Athletics',   'marathon Men',  1960,'Rome',   2,                                  'RHADI BEN ABDESSELEM, Abdesiem',   'MAR');
SELECT ioc_insert(        'Athletics',   'marathon Men',  1960,'Rome',   3,                                  'MAGEE, Arthur Barry',   'NZL');
SELECT ioc_insert(       'Athletics',   'marathon Men',  1964,'Tokyo',   1,                                  'BIKILA, Abebe',   'ETH');
SELECT ioc_insert(       'Athletics',   'marathon Men',  1964,'Tokyo',   2,                                  'HEATLEY, Benjamin Basil',   'GBR');
SELECT ioc_insert(       'Athletics',   'marathon Men',  1964,'Tokyo',   3,                                  'TSUBURAYA, Kokichi',   'JPN');
SELECT ioc_insert(      'Athletics',   'marathon Men',  1968,'Mexico',   1,                                  'WOLDE, Mamo',   'ETH');
SELECT ioc_insert(      'Athletics',   'marathon Men',  1968,'Mexico',   2,                                  'KIMIHARA, Kenji',   'JPN');
SELECT ioc_insert(      'Athletics',   'marathon Men',  1968,'Mexico',   3,                                  'RYAN, Michael Robert',   'NZL');
SELECT ioc_insert(      'Athletics',   'marathon Men',  1972,'Munich',   1,                                  'SHORTER, Frank Charles',   'USA');
SELECT ioc_insert(      'Athletics',   'marathon Men',  1972,'Munich',   2,                                  'LISMONT, Karel',   'BEL');
SELECT ioc_insert(      'Athletics',   'marathon Men',  1972,'Munich',   3,                                  'WOLDE, Mamo',   'ETH');
SELECT ioc_insert(    'Athletics',   'marathon Men',  1976,'Montreal',   1,   'CIERPINSKI, Waldemar',   'GDR');
SELECT ioc_insert(    'Athletics',   'marathon Men',  1976,'Montreal',   2,                                  'SHORTER, Frank Charles',   'USA');
SELECT ioc_insert(    'Athletics',   'marathon Men',  1976,'Montreal',   3,                                  'LISMONT, Karel',   'BEL');
SELECT ioc_insert(      'Athletics',   'marathon Men',  1980,'Moscow',   1,   'CIERPINSKI, Waldemar',   'GDR');
SELECT ioc_insert(      'Athletics',   'marathon Men',  1980,'Moscow',   2,                                  'NIJBOER, Gerard',   'NED');
SELECT ioc_insert(      'Athletics',   'marathon Men',  1980,'Moscow',   3,   'DZHUMANAZAROV, Setymkul',   'URS');
SELECT ioc_insert( 'Athletics',   'marathon Men',  1984,'Los Angeles',   1,                                  'LOPES, Carlos',   'POR');
SELECT ioc_insert( 'Athletics',   'marathon Men',  1984,'Los Angeles',   2,                                  'TREACY, John',   'IRL');
SELECT ioc_insert( 'Athletics',   'marathon Men',  1984,'Los Angeles',   3,                                  'SPEDDING, Charles',   'GBR');

SELECT ioc_insert( 'Athletics',   'marathon Women',  1996,'Atlanta',   3,   'ARIMORI, Yuko',   'JPN');
SELECT ioc_insert(  'Athletics',   'marathon Men',  2000,'Sydney',     1,   'ABERA, Gezahegne',   'ETH');
SELECT ioc_insert(  'Athletics',   'marathon Men',  2000,'Sydney',     2,   'WAINAINA, Erick',   'KEN');
SELECT ioc_insert(  'Athletics',   'marathon Men',  2000,'Sydney',     3,   'TOLA, Tesfaye',   'ETH');
SELECT ioc_insert(  'Athletics',   'marathon Women',  2000,'Sydney',   1,   'TAKAHASHI, Naoko',   'JPN');
SELECT ioc_insert(  'Athletics',   'marathon Women',  2000,'Sydney',   2,   'SIMON, Lidia Elena',   'ROU');
SELECT ioc_insert(  'Athletics',   'marathon Women',  2000,'Sydney',   3,   'CHEPCHUMBA, Joyce',   'KEN');
SELECT ioc_insert(  'Athletics',   'marathon Men',  2004,'Athens',     1,   'BALDINI, Stefano',   'ITA');
SELECT ioc_insert(  'Athletics',   'marathon Men',  2004,'Athens',     2,   'KEFLEZIGHI, Mebrahtom',   'USA');
SELECT ioc_insert(  'Athletics',   'marathon Men',  2004,'Athens',     3,   'DE LIMA, Vanderlei',   'BRA');
SELECT ioc_insert(  'Athletics',   'marathon Women',  2004,'Athens',   1,   'NOGUCHI, Mizuki',   'JPN');
SELECT ioc_insert(  'Athletics',   'marathon Women',  2004,'Athens',   2,   'NDEREBA, Catherine',   'KEN');
SELECT ioc_insert(  'Athletics',   'marathon Women',  2004,'Athens',   3,   'KASTOR, Deena',   'USA');
SELECT ioc_insert( 'Athletics',   'marathon Men',  2008,'Beijing',     1,   'WANSIRU, Samuel Kamau',   'KEN');
SELECT ioc_insert( 'Athletics',   'marathon Men',  2008,'Beijing',     2,   'GHARIB, Jaouad',   'MAR');
SELECT ioc_insert( 'Athletics',   'marathon Men',  2008,'Beijing',     3,   'KEBEDE, Tsegay',   'ETH');
SELECT ioc_insert( 'Athletics',   'marathon Women',  2008,'Beijing',   1,   'TOMESCU, Constantina',   'ROU');
SELECT ioc_insert( 'Athletics',   'marathon Women',  2008,'Beijing',   2,   'NDEREBA, Catherine',   'KEN');
SELECT ioc_insert( 'Athletics',   'marathon Women',  2008,'Beijing',   3,   'ZHOU, Chunxiu',   'CHN');

SELECT ioc_insert( 'Athletics',   'marathon Women',  1984,'Los Angeles',   1,                                  'BENOIT, Joan',   'USA');
SELECT ioc_insert( 'Athletics',   'marathon Women',  1984,'Los Angeles',   2,                                  'WAITZ-ANDERSEN, Grete',   'NOR');
SELECT ioc_insert( 'Athletics',   'marathon Women',  1984,'Los Angeles',   3,                                  'MOTA, Rosa',   'POR');
SELECT ioc_insert(       'Athletics',   'marathon Men',  1988,'Seoul',     1,                                  'BORDIN, Gelindo',   'ITA');
SELECT ioc_insert(       'Athletics',   'marathon Men',  1988,'Seoul',     2,                                  'WAKIIHURI, Douglas',   'KEN');
SELECT ioc_insert(       'Athletics',   'marathon Men',  1988,'Seoul',     3,                                  'AHMED SALAH, Hussein',   'DJI');
SELECT ioc_insert(       'Athletics',   'marathon Women',  1988,'Seoul',   1,                                  'MOTA, Rosa',   'POR');
SELECT ioc_insert(       'Athletics',   'marathon Women',  1988,'Seoul',   2,                                  'MARTIN-ONDIEKI-O''DEA, Lisa',   'AUS');
SELECT ioc_insert(       'Athletics',   'marathon Women',  1988,'Seoul',   3,   'DÖRRE, Katrin',   'GDR');
SELECT ioc_insert(   'Athletics',   'marathon Men',  1992,'Barcelona',     1,                                  'HWANG, Young-Cho',   'KOR');
SELECT ioc_insert(   'Athletics',   'marathon Men',  1992,'Barcelona',     2,                                  'MORISHITA, Koichi',   'JPN');
SELECT ioc_insert(   'Athletics',   'marathon Men',  1992,'Barcelona',     3,                                  'FREIGANG, Stephan Timo',   'GER');
SELECT ioc_insert(   'Athletics',   'marathon Women',  1992,'Barcelona',   1,   'YEGOROVA, Valentina',   'URS');
SELECT ioc_insert(   'Athletics',   'marathon Women',  1992,'Barcelona',   2,                                  'ARIMORI, Yuko',   'JPN');
SELECT ioc_insert(   'Athletics',   'marathon Women',  1992,'Barcelona',   3,                                  'MOLLER, Lorraine Mary',   'NZL');
SELECT ioc_insert(     'Athletics',   'marathon Men',  1996,'Atlanta',     1,                                  'THUGWANE, Josia',   'RSA');
SELECT ioc_insert(     'Athletics',   'marathon Men',  1996,'Atlanta',     2,                                  'LEE, Bong-Ju',   'KOR');
SELECT ioc_insert(     'Athletics',   'marathon Men',  1996,'Atlanta',     3,                                  'WAINAINA, Erick',   'KEN');
SELECT ioc_insert(     'Athletics',   'marathon Women',  1996,'Atlanta',   1,                                  'ROBA, Fatuma',   'ETH');
SELECT ioc_insert(     'Athletics',   'marathon Women',  1996,'Atlanta',   2,                                  'YEGOROVA, Valentina',   'RUS');

COMMIT;

