/*
   This file is public domain.

   The data is a matter of public record, and comes from

      http://www.olympics.org/
      http://en.wikipedia.org/wiki/Olympic_Games
*/


SET search_path TO sqltutor_data;

BEGIN;

SELECT ioc_insert( 'Artistic Gymnastics',   'horizontal bar Men',  1896,'Athens',         1,   'WEINGÄRTNER, Hermann',   'GER');
SELECT ioc_insert( 'Artistic Gymnastics',   'horizontal bar Men',  1896,'Athens',         2,   'FLATOW, Alfred',   'GER');
SELECT ioc_insert( 'Artistic Gymnastics',   'parallel bars Men',  1896,'Athens',          1,   'FLATOW, Alfred',   'GER');
SELECT ioc_insert( 'Artistic Gymnastics',   'parallel bars Men',  1896,'Athens',          2,   'ZUTTER, Louis',   'SUI');
SELECT ioc_insert( 'Artistic Gymnastics',   'pommel horse Men',  1896,'Athens',           1,   'ZUTTER, Louis',   'SUI');
SELECT ioc_insert( 'Artistic Gymnastics',   'pommel horse Men',  1896,'Athens',           2,   'WEINGÄRTNER, Hermann',   'GER');
SELECT ioc_insert( 'Artistic Gymnastics',   'rings Men',  1896,'Athens',                  1,   'MITROPOULOS, Ioannis',   'GRE');
SELECT ioc_insert( 'Artistic Gymnastics',   'rings Men',  1896,'Athens',                  2,   'WEINGÄRTNER, Hermann',   'GER');
SELECT ioc_insert( 'Artistic Gymnastics',   'rings Men',  1896,'Athens',                  3,   'PERSAKIS, Petros',   'GRE');
SELECT ioc_insert( 'Artistic Gymnastics',   'rope climbing Men',  1896,'Athens',          1,   'ANDRIAKOPOULOS, Nicolaos',   'GRE');
SELECT ioc_insert( 'Artistic Gymnastics',   'rope climbing Men',  1896,'Athens',          2,   'XENAKIS, Thomasios',   'GRE');
SELECT ioc_insert( 'Artistic Gymnastics',   'rope climbing Men',  1896,'Athens',          3,   'HOFMANN, Fritz',   'GER');
SELECT ioc_insert( 'Artistic Gymnastics',   'team, horizontal bar Men',  1896,'Athens',   1,   'HOFMANN, Fritz',   'GER');
SELECT ioc_insert( 'Artistic Gymnastics',   'team, horizontal bar Men',  1896,'Athens',   1,   'BÖCKER, Konrad',   'GER');
SELECT ioc_insert( 'Artistic Gymnastics',   'team, horizontal bar Men',  1896,'Athens',   1,   'FLATOW, Alfred',   'GER');
SELECT ioc_insert( 'Artistic Gymnastics',   'team, horizontal bar Men',  1896,'Athens',   1,   'FLATOW, Gustav Felix',   'GER');
SELECT ioc_insert( 'Artistic Gymnastics',   'team, horizontal bar Men',  1896,'Athens',   1,   'HILMAR, Georg',   'GER');
SELECT ioc_insert( 'Artistic Gymnastics',   'team, horizontal bar Men',  1896,'Athens',   1,   'MANTEUFFEL, Fritz',   'GER');
SELECT ioc_insert( 'Artistic Gymnastics',   'team, horizontal bar Men',  1896,'Athens',   1,   'NEUKIRCH, Karl',   'GER');
SELECT ioc_insert( 'Artistic Gymnastics',   'team, horizontal bar Men',  1896,'Athens',   1,   'RÖSTEL, Richard',   'GER');

SELECT ioc_insert( 'Artistic Gymnastics',   'team, horizontal bar Men',  1896,'Athens',   1,   'SCHUFT, Gustav',   'GER');
SELECT ioc_insert( 'Artistic Gymnastics',   'team, horizontal bar Men',  1896,'Athens',   1,   'SCHUMANN, Carl',   'GER');
SELECT ioc_insert( 'Artistic Gymnastics',   'team, horizontal bar Men',  1896,'Athens',   1,   'WEINGÄRTNER, Hermann',   'GER');
SELECT ioc_insert( 'Artistic Gymnastics',   'team, parallel bars Men',  1896,'Athens',    1,   'SCHUMANN, Carl',   'GER');
SELECT ioc_insert( 'Artistic Gymnastics',   'team, parallel bars Men',  1896,'Athens',    1,   'WEINGÄRTNER, Hermann',   'GER');
SELECT ioc_insert( 'Artistic Gymnastics',   'team, parallel bars Men',  1896,'Athens',    1,   'FLATOW, Gustav Felix',   'GER');
SELECT ioc_insert( 'Artistic Gymnastics',   'team, parallel bars Men',  1896,'Athens',    1,   'HILMAR, Georg',   'GER');
SELECT ioc_insert( 'Artistic Gymnastics',   'team, parallel bars Men',  1896,'Athens',    1,   'MANTEUFFEL, Fritz',   'GER');
SELECT ioc_insert( 'Artistic Gymnastics',   'team, parallel bars Men',  1896,'Athens',    1,   'NEUKIRCH, Karl',   'GER');
SELECT ioc_insert( 'Artistic Gymnastics',   'team, parallel bars Men',  1896,'Athens',    1,   'RÖSTEL, Richard',   'GER');
SELECT ioc_insert( 'Artistic Gymnastics',   'team, parallel bars Men',  1896,'Athens',    1,   'SCHUFT, Gustav',   'GER');
SELECT ioc_insert( 'Artistic Gymnastics',   'team, parallel bars Men',  1896,'Athens',    1,   'HOFMANN, Fritz',   'GER');
SELECT ioc_insert( 'Artistic Gymnastics',   'team, parallel bars Men',  1896,'Athens',    1,   'BÖCKER, Konrad',   'GER');
SELECT ioc_insert( 'Artistic Gymnastics',   'team, parallel bars Men',  1896,'Athens',    1,   'FLATOW, Alfred',   'GER');
SELECT ioc_insert( 'Artistic Gymnastics',   'team, parallel bars Men',  1896,'Athens',    2,   'ATHANASOPOULOS, Spyros',   'GRE');
SELECT ioc_insert( 'Artistic Gymnastics',   'team, parallel bars Men',  1896,'Athens',    2,   'ANDRIAKOPOULOS, Nicolaos',   'GRE');
SELECT ioc_insert( 'Artistic Gymnastics',   'team, parallel bars Men',  1896,'Athens',    2,   'PERSAKIS, Petros',   'GRE');
SELECT ioc_insert( 'Artistic Gymnastics',   'team, parallel bars Men',  1896,'Athens',    2,   'XENAKIS, Thomasios',   'GRE');
SELECT ioc_insert( 'Artistic Gymnastics',   'team, parallel bars Men',  1896,'Athens',    3,   'CHRYSAPHIS, Ioannis',   'GRE');
SELECT ioc_insert( 'Artistic Gymnastics',   'team, parallel bars Men',  1896,'Athens',    3,   'MITROPOULOS, Ioannis',   'GRE');

SELECT ioc_insert( 'Tennis',                  'singles Men',  1896,'Athens',                       3,   'TAPAVICZA, Momcsillo',   'HUN');
SELECT ioc_insert( 'Tennis',                  'singles Men',  1896,'Athens',                       3,   'PASPATIS, Konstantinos',   'GRE');
SELECT ioc_insert( 'Weightlifting',           'heavyweight - one hand lift Men',  1896,'Athens',   1,   'ELLIOTT, Launceston',   'GBR');
SELECT ioc_insert( 'Weightlifting',           'heavyweight - one hand lift Men',  1896,'Athens',   2,   'JENSEN, Viggo',   'DEN');
SELECT ioc_insert( 'Weightlifting',           'heavyweight - one hand lift Men',  1896,'Athens',   3,   'NIKOLOPOULOS, Alexandros',   'GRE');
SELECT ioc_insert( 'Weightlifting',           'heavyweight - two hand lift Men',  1896,'Athens',   1,   'JENSEN, Viggo',   'DEN');
SELECT ioc_insert( 'Weightlifting',           'heavyweight - two hand lift Men',  1896,'Athens',   2,   'ELLIOTT, Launceston',   'GBR');
SELECT ioc_insert( 'Weightlifting',           'heavyweight - two hand lift Men',  1896,'Athens',   3,   'VERSIS, Sotirios',   'GRE');
SELECT ioc_insert( 'Wrestling Greco-Roman',   'open event Men',  1896,'Athens',                    1,   'SCHUMANN, Carl',   'GER');
SELECT ioc_insert( 'Wrestling Greco-Roman',   'open event Men',  1896,'Athens',                    2,   'TSITAS, Georgios',   'GRE');
SELECT ioc_insert( 'Wrestling Greco-Roman',   'open event Men',  1896,'Athens',                    3,   'CHRISTOPOULOS, Stephanos',   'GRE');

SELECT ioc_insert( 'Artistic Gymnastics',   'team, parallel bars Men',  1896,'Athens',   3,   'LOUNDRAS, Dimitrios',   'GRE');
SELECT ioc_insert( 'Artistic Gymnastics',   'team, parallel bars Men',  1896,'Athens',   3,   'KARVELAS, Phillippos',   'GRE');
SELECT ioc_insert( 'Artistic Gymnastics',   'vault Men',  1896,'Athens',                 1,   'SCHUMANN, Carl',   'GER');
SELECT ioc_insert( 'Artistic Gymnastics',   'vault Men',  1896,'Athens',                 2,   'ZUTTER, Louis',   'SUI');
SELECT ioc_insert( 'Artistic Gymnastics',   'vault Men',  1896,'Athens',                 3,   'WEINGÄRTNER, Hermann',   'GER');
SELECT ioc_insert( 'Athletics',             '100m Men',  1896,'Athens',                  1,   'BURKE, Thomas',   'USA');
SELECT ioc_insert( 'Athletics',             '100m Men',  1896,'Athens',                  2,   'HOFMANN, Fritz',   'GER');
SELECT ioc_insert( 'Athletics',             '100m Men',  1896,'Athens',                  3,   'SZOKOLYI, Alajos',   'HUN');
SELECT ioc_insert( 'Athletics',             '100m Men',  1896,'Athens',                  3,   'LANE, Francis',   'USA');
SELECT ioc_insert( 'Athletics',             '110m hurdles Men',  1896,'Athens',          1,   'CURTIS, Thomas',   'USA');
SELECT ioc_insert( 'Athletics',             '110m hurdles Men',  1896,'Athens',          2,   'GOULDING, Grantley',   'GBR');
SELECT ioc_insert( 'Athletics',             '1500m Men',  1896,'Athens',                 1,   'FLACK, Edwin',   'AUS');
SELECT ioc_insert( 'Athletics',             '1500m Men',  1896,'Athens',                 2,   'BLAKE, Arthur',   'USA');
SELECT ioc_insert( 'Athletics',             '1500m Men',  1896,'Athens',                 3,   'LERMUSIAUX, Albin',   'FRA');
SELECT ioc_insert( 'Athletics',             '400m Men',  1896,'Athens',                  1,   'BURKE, Thomas',   'USA');
SELECT ioc_insert( 'Athletics',             '400m Men',  1896,'Athens',                  2,   'JAMISON, Herbert',   'USA');
SELECT ioc_insert( 'Athletics',             '400m Men',  1896,'Athens',                  3,   'GMELIN, Charles',   'GBR');
SELECT ioc_insert( 'Athletics',             '800m Men',  1896,'Athens',                  1,   'FLACK, Edwin',   'AUS');
SELECT ioc_insert( 'Athletics',             '800m Men',  1896,'Athens',                  2,   'DANI, Nandor',   'HUN');
SELECT ioc_insert( 'Athletics',             '800m Men',  1896,'Athens',                  3,   'GOLEMIS, Dimitrios',   'GRE');

SELECT ioc_insert( 'Shooting',   'army rifle, 300m Men',  1896,'Athens',             3,                        'JENSEN, Viggo',   'DEN');
SELECT ioc_insert( 'Swimming',   '100m freestyle for sailors Men',  1896,'Athens',   1,                        'MALOKINIS, Ioannis',   'GRE');
SELECT ioc_insert( 'Swimming',   '100m freestyle for sailors Men',  1896,'Athens',   2,                        'CHASAPIS, Spiridon',   'GRE');
SELECT ioc_insert( 'Swimming',   '100m freestyle for sailors Men',  1896,'Athens',   3,                        'DRIVAS, Dimitrios',   'GRE');
SELECT ioc_insert( 'Swimming',   '100m freestyle Men',  1896,'Athens',               1,                        'HAJOS, Alfred',   'HUN');
SELECT ioc_insert( 'Swimming',   '100m freestyle Men',  1896,'Athens',               2,                        'HERSCHMANN, Otto',   'AUT');
SELECT ioc_insert( 'Swimming',   '1200m freestyle Men',  1896,'Athens',              1,                        'HAJOS, Alfred',   'HUN');
SELECT ioc_insert( 'Swimming',   '1200m freestyle Men',  1896,'Athens',              2,                        'ANDREOU, Joannis',   'GRE');
SELECT ioc_insert( 'Swimming',   '1200m freestyle Men',  1896,'Athens',              3,                        'CHOROPHAS, Efstathios',   'GRE');
SELECT ioc_insert( 'Swimming',   '400m freestyle Men',  1896,'Athens',               1,                        'NEUMANN, Paul',   'AUT');
SELECT ioc_insert( 'Swimming',   '400m freestyle Men',  1896,'Athens',               2,                        'PEPANOS, Antonios',   'GRE');
SELECT ioc_insert( 'Swimming',   '400m freestyle Men',  1896,'Athens',               3,                        'CHOROPHAS, Efstathios',   'GRE');
SELECT ioc_insert( 'Tennis',     'doubles Men',  1896,'Athens',                      1,   'BOLAND, John',   NULL);
SELECT ioc_insert( 'Tennis',     'doubles Men',  1896,'Athens',                      1,   'TRAUN, Friedrich',   NULL);
SELECT ioc_insert( 'Tennis',     'doubles Men',  1896,'Athens',                      2,   'KASDAGLIS, Dionysios',   NULL);
SELECT ioc_insert( 'Tennis',     'doubles Men',  1896,'Athens',                      2,   'PETROKOKKINOS, Demetrios',   NULL);
SELECT ioc_insert( 'Tennis',     'doubles Men',  1896,'Athens',                      3,   'FLACK, Edwin',   NULL);
SELECT ioc_insert( 'Tennis',     'doubles Men',  1896,'Athens',                      3,   'ROBERTSON, George Stuart',   NULL);
SELECT ioc_insert( 'Tennis',     'singles Men',  1896,'Athens',                      1,                        'BOLAND, John',   'GBR');
SELECT ioc_insert( 'Tennis',     'singles Men',  1896,'Athens',                      2,                        'KASDAGLIS, Dionysios',   'GRE');

SELECT ioc_insert( 'Athletics',       'triple jump Men',  1896,'Athens',            2,   'TUFFERI, Alexandre',   'FRA');
SELECT ioc_insert( 'Athletics',       'triple jump Men',  1896,'Athens',            3,   'PERSAKIS, Ioannis',   'GRE');
SELECT ioc_insert( 'Cycling Road',    'individual road race Men',  1896,'Athens',   1,   'KONSTANTINIDIS, Aristidis',   'GRE');
SELECT ioc_insert( 'Cycling Road',    'individual road race Men',  1896,'Athens',   2,   'GOEDRICH, August',   'GER');
SELECT ioc_insert( 'Cycling Road',    'individual road race Men',  1896,'Athens',   3,   'BATTEL, Edward',   'GBR');
SELECT ioc_insert( 'Cycling Track',   '100km Men',  1896,'Athens',                  1,   'FLAMENG, Léon',   'FRA');
SELECT ioc_insert( 'Cycling Track',   '100km Men',  1896,'Athens',                  2,   'KOLETTIS, Georgios',   'GRE');
SELECT ioc_insert( 'Cycling Track',   '10km Men',  1896,'Athens',                   1,   'MASSON, Paul',   'FRA');
SELECT ioc_insert( 'Cycling Track',   '10km Men',  1896,'Athens',                   2,   'FLAMENG, Léon',   'FRA');
SELECT ioc_insert( 'Cycling Track',   '10km Men',  1896,'Athens',                   3,   'SCHMAL, Adolf',   'AUT');
SELECT ioc_insert( 'Cycling Track',   '12-hour race Men',  1896,'Athens',           1,   'SCHMAL, Adolf',   'AUT');
SELECT ioc_insert( 'Cycling Track',   '12-hour race Men',  1896,'Athens',           2,   'KEEPING, Frank',   'GBR');
SELECT ioc_insert( 'Cycling Track',   '1km time trial Men',  1896,'Athens',         1,   'MASSON, Paul',   'FRA');
SELECT ioc_insert( 'Cycling Track',   '1km time trial Men',  1896,'Athens',         2,   'NIKOLOPOULOS, Stamatios',   'GRE');
SELECT ioc_insert( 'Cycling Track',   '1km time trial Men',  1896,'Athens',         3,   'SCHMAL, Adolf',   'AUT');
SELECT ioc_insert( 'Cycling Track',   'Sprint indivual Men',  1896,'Athens',        1,   'MASSON, Paul',   'FRA');
SELECT ioc_insert( 'Cycling Track',   'Sprint indivual Men',  1896,'Athens',        2,   'NIKOLOPOULOS, Stamatios',   'GRE');
SELECT ioc_insert( 'Cycling Track',   'Sprint indivual Men',  1896,'Athens',        3,   'FLAMENG, Léon',   'FRA');
SELECT ioc_insert( 'Fencing',         'foil individual Men',  1896,'Athens',        1,   'GRAVELOTTE, Eugène-Henri',   'FRA');
SELECT ioc_insert( 'Fencing',         'foil individual Men',  1896,'Athens',        2,   'CALLOT, Henri',   'FRA');

SELECT ioc_insert( 'Fencing',    'foil individual Men',  1896,'Athens',                    3,   'PIERRAKOS-MAVROMICHALIS, Perikles',   'GRE');
SELECT ioc_insert( 'Fencing',    'foil, masters Men',  1896,'Athens',                      1,   'PYRGOS, Leonidas',   'GRE');
SELECT ioc_insert( 'Fencing',    'foil, masters Men',  1896,'Athens',                      2,   'PERRONNET, Maurice',   'FRA');
SELECT ioc_insert( 'Fencing',    'sabre individual Men',  1896,'Athens',                   1,   'GEORGIADIS, Ioannis',   'GRE');
SELECT ioc_insert( 'Fencing',    'sabre individual Men',  1896,'Athens',                   2,   'KARAKALOS, Telemachos',   'GRE');
SELECT ioc_insert( 'Fencing',    'sabre individual Men',  1896,'Athens',                   3,   'NIELSEN, Holger',   'DEN');
SELECT ioc_insert( 'Shooting',   '25m army pistol Men',  1896,'Athens',                    1,   'PAINE, John',   'USA');
SELECT ioc_insert( 'Shooting',   '25m army pistol Men',  1896,'Athens',                    2,   'PAINE, Sumner',   'USA');
SELECT ioc_insert( 'Shooting',   '25m army pistol Men',  1896,'Athens',                    3,   'MORAKIS, Nikolaos',   'GRE');
SELECT ioc_insert( 'Shooting',   '25m rapid fire pistol (60 shots) Men',  1896,'Athens',   1,   'PHRANGOUDIS, Joannis',   'GRE');
SELECT ioc_insert( 'Shooting',   '25m rapid fire pistol (60 shots) Men',  1896,'Athens',   2,   'ORPHANIDIS, Georgios',   'GRE');
SELECT ioc_insert( 'Shooting',   '25m rapid fire pistol (60 shots) Men',  1896,'Athens',   3,   'NIELSEN, Holger',   'DEN');
SELECT ioc_insert( 'Shooting',   '50m pistol (60 shots) Men',  1896,'Athens',              1,   'PAINE, Sumner',   'USA');
SELECT ioc_insert( 'Shooting',   '50m pistol (60 shots) Men',  1896,'Athens',              2,   'NIELSEN, Holger',   'DEN');
SELECT ioc_insert( 'Shooting',   '50m pistol (60 shots) Men',  1896,'Athens',              3,   'PHRANGOUDIS, Joannis',   'GRE');
SELECT ioc_insert( 'Shooting',   'army rifle, 200m Men',  1896,'Athens',                   1,   'KARASEVDAS, Pantelis',   'GRE');
SELECT ioc_insert( 'Shooting',   'army rifle, 200m Men',  1896,'Athens',                   2,   'PAVLIDIS, Pavlos',   'GRE');
SELECT ioc_insert( 'Shooting',   'army rifle, 200m Men',  1896,'Athens',                   3,   'TRIKUPIS, Nicolaos',   'GRE');
SELECT ioc_insert( 'Shooting',   'army rifle, 300m Men',  1896,'Athens',                   1,   'ORPHANIDIS, Georgios',   'GRE');
SELECT ioc_insert( 'Shooting',   'army rifle, 300m Men',  1896,'Athens',                   2,   'PHRANGOUDIS, Joannis',   'GRE');

SELECT ioc_insert( 'Athletics',   'discus throw Men',  1896,'Athens',   1,   'GARRETT, Robert',   'USA');
SELECT ioc_insert( 'Athletics',   'discus throw Men',  1896,'Athens',   2,   'PARASKEVOPOULOS, Panagiotis',   'GRE');
SELECT ioc_insert( 'Athletics',   'discus throw Men',  1896,'Athens',   3,   'VERSIS, Sotirios',   'GRE');
SELECT ioc_insert( 'Athletics',   'high jump Men',  1896,'Athens',      1,   'CLARK, Ellery',   'USA');
SELECT ioc_insert( 'Athletics',   'high jump Men',  1896,'Athens',      2,   'GARRETT, Robert',   'USA');
SELECT ioc_insert( 'Athletics',   'high jump Men',  1896,'Athens',      2,   'CONNOLLY, James',   'USA');
SELECT ioc_insert( 'Athletics',   'long jump Men',  1896,'Athens',      1,   'CLARK, Ellery',   'USA');
SELECT ioc_insert( 'Athletics',   'long jump Men',  1896,'Athens',      2,   'GARRETT, Robert',   'USA');
SELECT ioc_insert( 'Athletics',   'long jump Men',  1896,'Athens',      3,   'CONNOLLY, James',   'USA');
SELECT ioc_insert( 'Athletics',   'marathon Men',  1896,'Athens',       1,   'LOUIS, Spyridon',   'GRE');
SELECT ioc_insert( 'Athletics',   'marathon Men',  1896,'Athens',       2,   'VASILAKOS, Kharilaos',   'GRE');
SELECT ioc_insert( 'Athletics',   'marathon Men',  1896,'Athens',       3,   'KELLNER, Gyula',   'HUN');
SELECT ioc_insert( 'Athletics',   'pole vault Men',  1896,'Athens',     1,   'HOYT, William Welles',   'USA');
SELECT ioc_insert( 'Athletics',   'pole vault Men',  1896,'Athens',     2,   'TYLER, Albert',   'USA');
SELECT ioc_insert( 'Athletics',   'pole vault Men',  1896,'Athens',     3,   'DAMASKOS, Evangelos',   'GRE');
SELECT ioc_insert( 'Athletics',   'pole vault Men',  1896,'Athens',     3,   'THEODOROPOULOS, Ioannis',   'GRE');
SELECT ioc_insert( 'Athletics',   'shot put Men',  1896,'Athens',       1,   'GARRETT, Robert',   'USA');
SELECT ioc_insert( 'Athletics',   'shot put Men',  1896,'Athens',       2,   'GOUSKOS, Miltiadis',   'GRE');
SELECT ioc_insert( 'Athletics',   'shot put Men',  1896,'Athens',       3,   'PAPASIDERIS, Georgios',   'GRE');
SELECT ioc_insert( 'Athletics',   'triple jump Men',  1896,'Athens',    1,   'CONNOLLY, James',   'USA');

COMMIT;

