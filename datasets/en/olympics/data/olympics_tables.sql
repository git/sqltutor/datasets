/*
   This file is part of GNU Sqltutor
   Copyright (C) 2009  Free Software Foundation, Inc.
   Contributed by Ales Cepek <cepek@gnu.org>

   GNU Sqltutor is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   GNU Sqltutor is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GNU Sqltutor.  If not, see <http://www.gnu.org/licenses/>.
 */


SET search_path TO sqltutor_data;

DROP TABLE IF EXISTS ioc_codes CASCADE;
DROP TABLE IF EXISTS ioc_obsolete_codes CASCADE;
DROP TABLE IF EXISTS ioc_sports_status CASCADE;
DROP TABLE IF EXISTS ioc_sports CASCADE;
DROP TABLE IF EXISTS ioc_disciplines CASCADE;
DROP TABLE IF EXISTS ioc_events CASCADE;
DROP VIEW  IF EXISTS sports CASCADE;
DROP TABLE IF EXISTS olympics CASCADE;
DROP TABLE IF EXISTS athletes CASCADE;
DROP TABLE IF EXISTS medals CASCADE;

DROP FUNCTION IF EXISTS ioc_insert_discipline(text, text);
DROP FUNCTION IF EXISTS ioc_insert_event(text, text);
DROP FUNCTION IF EXISTS ioc_insert (text, text, int, text, int, text, text);


CREATE TABLE ioc_codes (
   country_code char(3) PRIMARY KEY,
   country      varchar(40)
);


CREATE TABLE ioc_obsolete_codes (
   country_code char(3) REFERENCES ioc_codes,
   obsolete     char(3),
   period       varchar(50) NOT NULL,
   PRIMARY KEY (obsolete, period)
);


CREATE TABLE ioc_sports_status (
   status_code int PRIMARY KEY DEFAULT 0,
   status      varchar(12) NOT NULL DEFAULT 'discontinued',
   CHECK (status_code=1 AND status='recognized' OR
          status_code=0 AND status='discontinued')               
);
INSERT INTO ioc_sports_status VALUES (1, 'recognized');
INSERT INTO ioc_sports_status VALUES (DEFAULT);


CREATE TABLE ioc_sports (
   sport_id    serial PRIMARY KEY,
   sport       varchar(30) UNIQUE NOT NULL,
   category    char (6) CHECK (category IN ('Summer', 'Winter')),
   status_code int REFERENCES ioc_sports_status NOT NULL DEFAULT 0
);


CREATE TABLE ioc_disciplines (
   discipline_id serial PRIMARY KEY,
   discipline    varchar(30) UNIQUE NOT NULL,
   sport_id      int REFERENCES ioc_sports NOT NULL,
   status_code   int REFERENCES ioc_sports_status NOT NULL DEFAULT 0
);

CREATE INDEX ioc_disciplines_lower 
    ON ioc_disciplines (lower(discipline));


CREATE TABLE ioc_events (
   event_id      serial PRIMARY KEY,
   event         varchar(50) NOT NULL,
   discipline_id int REFERENCES ioc_disciplines NOT NULL,
   status_code   int REFERENCES ioc_sports_status NOT NULL DEFAULT 0
);

CREATE INDEX ioc_events_lower 
    ON ioc_events (lower(event));


CREATE VIEW sports (sport, category,
                               discipline_id, discipline,
                               event_id, event, status)
AS
SELECT s.sport, s.category,
       d.discipline_id, d.discipline,
       e.event_id, e.event, 
       s.status_code+d.status_code+e.status_code
  FROM ioc_sports      AS s
       JOIN ioc_disciplines AS d USING (sport_id)
       JOIN ioc_events      AS e USING (discipline_id);


CREATE FUNCTION ioc_insert_discipline(text, text)
RETURNS text
AS $$
   INSERT INTO ioc_disciplines (discipline, sport_id)
      SELECT $2, (SELECT sport_id FROM ioc_sports WHERE sport=$1);
   SELECT $1 || ' - ' || $2;
$$ LANGUAGE SQL;


CREATE FUNCTION ioc_insert_event(text, text)
RETURNS text
AS $$
   INSERT INTO ioc_events (event, discipline_id)
      SELECT $2, 
         (SELECT discipline_id FROM ioc_disciplines WHERE discipline=$1);
   SELECT $1 || ' - ' || $2;
$$ LANGUAGE SQL;


CREATE TABLE olympics (
   year         int,
   city         varchar(30) NOT NULL,
   category     char(6) CHECK (category in ('Summer', 'Winter')) NOT NULL,
   country_code varchar(3) REFERENCES ioc_codes,
   opening      date,
   closing      date,
   nations      int,
   athletes     int,
   sports       int,
   events       int,
   PRIMARY KEY (year, category)
);


CREATE TABLE athletes (
   athlete_id   serial PRIMARY KEY,
   forename     varchar(50),
   surname      varchar(50) NOT NULL
);


-- country codes are not known for all athletes in first olympicss, can
-- be null
CREATE TABLE medals (
   year          int,
   discipline_id int     REFERENCES ioc_disciplines,
   event_id      int     REFERENCES ioc_events,
   athlete_id    int     REFERENCES athletes,
   medal         char(6) NOT NULL,
   country_code  char(3) REFERENCES ioc_codes,

   PRIMARY KEY (year, discipline_id, event_id, athlete_id)
);


-- Helper function for inserting olympics medal winners based on data
-- transformed from Wikipedia format as in the case of marathon race
-- winners. Possible multiple inserts of the record are filtered out.
-- 
-- Parameters:
--
--   1 discipline       5 medal (1/2/3)
--   2 event            6 athlete (SURNAME, forename)
--   3 year             7 country code
--   4 city
--
-- Examples:
--
-- SELECT ioc_insert('Athletics', 'marathon Men', 2008, 'Beijing',
--                        1, 'Samuel Wanjiru', 'KEN');
-- SELECT ioc_insert('Athletics', 'marathon Men', 2008, 'Beijing',
--                        2, 'Jaouad Gharib', 'MAR');
-- SELECT ioc_insert('Athletics', 'marathon Men', 2008, 'Beijing',
--                        3, 'Tsegay Kebede', 'ETH');
--

CREATE FUNCTION ioc_insert 
(
   IN discipline_   text, 
   IN event_        text, 
   IN year_         int, 
   IN city_         text, 
   IN medal_        int, 
   IN athlete_      text, 
   IN country_code_ text
)
RETURNS void
AS $$
DECLARE
  discid_ int = NULL;
  evenid_ int = NULL;
  athlid_ int = NULL;
  surname_  text = initcap(split_part(athlete_, ', ', 1)); 
  forename_ text =         split_part(athlete_, ', ', 2);
BEGIN
   INSERT INTO athletes (forename, surname) 
      SELECT forename_, surname_
      EXCEPT SELECT forename, surname
               FROM athletes
              WHERE forename = forename_
                AND surname  = surname_;

   SELECT INTO athlid_ athlete_id
     FROM athletes
    WHERE forename = forename_
      AND surname  = surname_;

   SELECT INTO discid_ discipline_id
     FROM ioc_disciplines
    WHERE lower(discipline)=lower(discipline_);

   SELECT INTO evenid_ event_id
     FROM ioc_events
    WHERE lower(event)=lower(event_) AND discipline_id=discid_;

   INSERT INTO medals 
          (year, discipline_id, event_id, medal, country_code, athlete_id)
      SELECT year_, discid_, evenid_, 
             case medal_ when 1 then 'gold' 
                         when 2 then 'silver' 
                         else 'bronz' 
             end, 
             country_code_, athlid_
      EXCEPT
      SELECT year, discipline_id, event_id, medal, country_code, athlete_id 
        FROM medals
       WHERE year=year_
         AND discipline_id=discid_
         AND event_id=evenid_
         AND athlete_id=athlid_
         AND (country_code=country_code_ OR country_code IS NULL);

END
$$ LANGUAGE plpgsql;

