/*
   This file is public domain.

   The data is a matter of public record, and comes from

      http://www.olympics.org/
      http://en.wikipedia.org/wiki/Olympic_Games
*/


SET search_path TO sqltutor_data;

BEGIN;

SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Men',  2008,'Beijing',     1,   'DALHAUSSER, Philip',   'USA');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Men',  2008,'Beijing',     1,   'ROGERS, Todd',   'USA');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Men',  2008,'Beijing',     2,   'ARAUJO, Marcio',   'BRA');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Men',  2008,'Beijing',     2,   'MAGALHAES, Fabio',   'BRA');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Men',  2008,'Beijing',     3,   'SANTOS, Ricardo',   'BRA');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Men',  2008,'Beijing',     3,   'REGO, Emanuel',   'BRA');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Women',  2008,'Beijing',   1,   'WALSH, Kerri',   'USA');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Women',  2008,'Beijing',   1,   'MAY-TREANOR, Misty',   'USA');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Women',  2008,'Beijing',   2,   'WANG, Jie',   'CHN');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Women',  2008,'Beijing',   2,   'TIAN, Jia',   'CHN');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Women',  2008,'Beijing',   3,   'XUE, Chen',   'CHN');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Women',  2008,'Beijing',   3,   'ZHANG, Xi',   'CHN');

SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Men',  1996,'Atlanta',     1,   'KIRALY, Charles',   'USA');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Men',  1996,'Atlanta',     1,   'STEFFES, Kent',   'USA');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Men',  1996,'Atlanta',     2,   'DODD, Michael',   'USA');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Men',  1996,'Atlanta',     2,   'WHITMARSH, Mike',   'USA');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Men',  1996,'Atlanta',     3,   'CHILD, John',   'CAN');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Men',  1996,'Atlanta',     3,   'HEESE, Mark',   'CAN');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Women',  1996,'Atlanta',   1,   'PIRES, Sandra',   'BRA');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Women',  1996,'Atlanta',   1,   'SILVA CRUZ, Jacqueline Cruz',   'BRA');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Women',  1996,'Atlanta',   2,   'RODRIGUES, Monica',   'BRA');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Women',  1996,'Atlanta',   2,   'SAMUEL RAMOS, Adriana',   'BRA');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Women',  1996,'Atlanta',   3,   'COOK, Natalie',   'AUS');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Women',  1996,'Atlanta',   3,   'POTTHARST, Kerri',   'AUS');

SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Men',  2004,'Athens',     1,   'SANTOS, Ricardo',   'BRA');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Men',  2004,'Athens',     1,   'REGO, Emanuel',   'BRA');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Men',  2004,'Athens',     2,   'BOSMA, Javier',   'ESP');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Men',  2004,'Athens',     2,   'HERRERA, Pablo',   'ESP');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Men',  2004,'Athens',     3,   'HEUSCHER, Patrick',   'SUI');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Men',  2004,'Athens',     3,   'KOBEL, Stefan',   'SUI');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Women',  2004,'Athens',   1,   'WALSH, Kerri',   'USA');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Women',  2004,'Athens',   1,   'MAY-TREANOR, Misty',   'USA');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Women',  2004,'Athens',   2,   'BEHAR, Adriana',   'BRA');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Women',  2004,'Athens',   2,   'BEDE, Shelda',   'BRA');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Women',  2004,'Athens',   3,   'MCPEAK, Holly',   'USA');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Women',  2004,'Athens',   3,   'YOUNGS, Elaine',   'USA');

SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Men',  2000,'Sydney',     1,   'BLANTON, Dain',   'USA');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Men',  2000,'Sydney',     1,   'FONOIMOANA, Eric',   'USA');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Men',  2000,'Sydney',     2,   'MELO, Jose Marco',   'BRA');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Men',  2000,'Sydney',     2,   'SANTOS, Ricardo',   'BRA');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Men',  2000,'Sydney',     3,   'AHMANN, Jörg',   'GER');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Men',  2000,'Sydney',     3,   'HAGER, Axel',   'GER');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Women',  2000,'Sydney',   1,   'COOK, Natalie',   'AUS');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Women',  2000,'Sydney',   1,   'POTTHARST, Kerri',   'AUS');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Women',  2000,'Sydney',   2,   'BEHAR, Adriana',   'BRA');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Women',  2000,'Sydney',   2,   'BEDE, Shelda',   'BRA');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Women',  2000,'Sydney',   3,   'PIRES, Sandra',   'BRA');
SELECT ioc_insert( 'Beach volleyball',   'beach volleyball Women',  2000,'Sydney',   3,   'SAMUEL RAMOS, Adriana',   'BRA');

COMMIT;

