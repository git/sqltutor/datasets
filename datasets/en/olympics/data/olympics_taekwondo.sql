/*
   This file is public domain.

   The data is a matter of public record, and comes from

      http://www.olympics.org/
      http://en.wikipedia.org/wiki/Olympic_Games
*/


SET search_path TO sqltutor_data;

BEGIN;


SELECT ioc_insert( 'Taekwondo',   '+ 67 kg Women',  2000,'Sydney',      1,   'CHEN, Zhong',   'CHN');
SELECT ioc_insert( 'Taekwondo',   '+ 67 kg Women',  2000,'Sydney',      2,   'IVANOVA, Natalia',   'RUS');
SELECT ioc_insert( 'Taekwondo',   '+ 67 kg Women',  2000,'Sydney',      3,   'BOSSHART, Dominique',   'CAN');
SELECT ioc_insert( 'Taekwondo',   '+ 80 kg Men',  2000,'Sydney',        1,   'KIM, Kyong-Hun',   'KOR');
SELECT ioc_insert( 'Taekwondo',   '+ 80 kg Men',  2000,'Sydney',        2,   'TRENTON, Daniel',   'AUS');
SELECT ioc_insert( 'Taekwondo',   '+ 80 kg Men',  2000,'Sydney',        3,   'GENTIL, Pascal',   'FRA');
SELECT ioc_insert( 'Taekwondo',   '- 49 kg Women',  2000,'Sydney',      1,   'BURNS, Lauren',   'AUS');
SELECT ioc_insert( 'Taekwondo',   '- 49 kg Women',  2000,'Sydney',      2,   'MELENDEZ RODRIGUEZ, Urbia',   'CUB');
SELECT ioc_insert( 'Taekwondo',   '- 49 kg Women',  2000,'Sydney',      3,   'CHI, Shu-Ju',   'TPE');
SELECT ioc_insert( 'Taekwondo',   '- 58 kg Men',  2000,'Sydney',        1,   'MOUROUTSOS, Michail',   'GRE');
SELECT ioc_insert( 'Taekwondo',   '- 58 kg Men',  2000,'Sydney',        2,   'ESPARZA, Gabriel',   'ESP');
SELECT ioc_insert( 'Taekwondo',   '- 58 kg Men',  2000,'Sydney',        3,   'HUANG, Chih Hsiung',   'TPE');
SELECT ioc_insert( 'Taekwondo',   '49 - 57 kg Women',  2000,'Sydney',   1,   'JUNG, Jae-Eun',   'KOR');
SELECT ioc_insert( 'Taekwondo',   '49 - 57 kg Women',  2000,'Sydney',   2,   'TRAN, Hieu Ngan',   'VIE');
SELECT ioc_insert( 'Taekwondo',   '49 - 57 kg Women',  2000,'Sydney',   3,   'BIKCIN, Hamide',   'TUR');
SELECT ioc_insert( 'Taekwondo',   '57 - 67 kg Women',  2000,'Sydney',   1,   'LEE, Sun-Hee',   'KOR');
SELECT ioc_insert( 'Taekwondo',   '57 - 67 kg Women',  2000,'Sydney',   2,   'GUNDERSEN, Trude',   'NOR');
SELECT ioc_insert( 'Taekwondo',   '57 - 67 kg Women',  2000,'Sydney',   3,   'OKAMOTO, Yoriko',   'JPN');
SELECT ioc_insert( 'Taekwondo',   '58 - 68 kg Men',  2000,'Sydney',     1,   'LOPEZ, Steven',   'USA');
SELECT ioc_insert( 'Taekwondo',   '58 - 68 kg Men',  2000,'Sydney',     2,   'SIN, Joon-Sik',   'KOR');

SELECT ioc_insert( 'Taekwondo',   '58 - 68 kg Men',  2000,'Sydney',     3,   'SAEIBONEHKOHAL, Hadi',   'IRI');
SELECT ioc_insert( 'Taekwondo',   '68 - 80 kg Men',  2000,'Sydney',     1,   'MATOS, Angel Valodia',   'CUB');
SELECT ioc_insert( 'Taekwondo',   '68 - 80 kg Men',  2000,'Sydney',     2,   'EBNOUTALIB, Faissal',   'GER');
SELECT ioc_insert( 'Taekwondo',   '68 - 80 kg Men',  2000,'Sydney',     3,   'ESTRADA GARIBAY, Victor Manuel',   'MEX');
SELECT ioc_insert( 'Taekwondo',   '+ 67 kg Women',  2004,'Athens',      1,   'CHEN, Zhong',   'CHN');
SELECT ioc_insert( 'Taekwondo',   '+ 67 kg Women',  2004,'Athens',      2,   'BAVEREL, Myriam',   'FRA');
SELECT ioc_insert( 'Taekwondo',   '+ 67 kg Women',  2004,'Athens',      3,   'CARMONA, Adriana',   'VEN');
SELECT ioc_insert( 'Taekwondo',   '+ 80 kg Men',  2004,'Athens',        1,   'MOON, Dae Sung',   'KOR');
SELECT ioc_insert( 'Taekwondo',   '+ 80 kg Men',  2004,'Athens',        2,   'NIKOLAIDIS, Alexandros',   'GRE');
SELECT ioc_insert( 'Taekwondo',   '+ 80 kg Men',  2004,'Athens',        3,   'GENTIL, Pascal',   'FRA');
SELECT ioc_insert( 'Taekwondo',   '- 49 kg Women',  2004,'Athens',      1,   'CHEN, Shih Hsin',   'TPE');
SELECT ioc_insert( 'Taekwondo',   '- 49 kg Women',  2004,'Athens',      2,   'LABRADA DIAZ, Yanelis Yuliet',   'CUB');
SELECT ioc_insert( 'Taekwondo',   '- 49 kg Women',  2004,'Athens',      3,   'BOORAPOLCHAI, Yaowapa',   'THA');
SELECT ioc_insert( 'Taekwondo',   '- 58 kg Men',  2004,'Athens',        1,   'CHU, Mu Yen',   'TPE');
SELECT ioc_insert( 'Taekwondo',   '- 58 kg Men',  2004,'Athens',        2,   'SALAZAR BLANCO, Oscar Francisco',   'MEX');
SELECT ioc_insert( 'Taekwondo',   '- 58 kg Men',  2004,'Athens',        3,   'BAYOUMI, Tamer',   'EGY');
SELECT ioc_insert( 'Taekwondo',   '49 - 57 kg Women',  2004,'Athens',   1,   'JANG, Ji Won',   'KOR');
SELECT ioc_insert( 'Taekwondo',   '49 - 57 kg Women',  2004,'Athens',   2,   'ABDALLAH, Nia',   'USA');
SELECT ioc_insert( 'Taekwondo',   '49 - 57 kg Women',  2004,'Athens',   3,   'SALAZAR BLANCO, Iridia',   'MEX');
SELECT ioc_insert( 'Taekwondo',   '57 - 67 kg Women',  2004,'Athens',   1,   'LUO, Wei',   'CHN');

SELECT ioc_insert(  'Taekwondo',   '57 - 67 kg Women',  2004,'Athens',   2,   'MYSTAKIDOU, Elisavet',   'GRE');
SELECT ioc_insert(  'Taekwondo',   '57 - 67 kg Women',  2004,'Athens',   3,   'HWANG, Kyung Sun',   'KOR');
SELECT ioc_insert(  'Taekwondo',   '58 - 68 kg Men',  2004,'Athens',     1,   'SAEIBONEHKOHAL, Hadi',   'IRI');
SELECT ioc_insert(  'Taekwondo',   '58 - 68 kg Men',  2004,'Athens',     2,   'HUANG, Chih Hsiung',   'TPE');
SELECT ioc_insert(  'Taekwondo',   '58 - 68 kg Men',  2004,'Athens',     3,   'SONG, Myeong Seob',   'KOR');
SELECT ioc_insert(  'Taekwondo',   '68 - 80 kg Men',  2004,'Athens',     1,   'LOPEZ, Steven',   'USA');
SELECT ioc_insert(  'Taekwondo',   '68 - 80 kg Men',  2004,'Athens',     2,   'TANRIKULU, Bahri',   'TUR');
SELECT ioc_insert(  'Taekwondo',   '68 - 80 kg Men',  2004,'Athens',     3,   'KARAMI, Yossef',   'IRI');
SELECT ioc_insert( 'Taekwondo',   '+ 67 kg Women',  2008,'Beijing',      1,   'ESPINOZA, Maria del Rosario',   'MEX');
SELECT ioc_insert( 'Taekwondo',   '+ 67 kg Women',  2008,'Beijing',      2,   'SOLHEIM, Nina',   'NOR');
SELECT ioc_insert( 'Taekwondo',   '+ 67 kg Women',  2008,'Beijing',      3,   'FALAVIGNA, Natalia',   'BRA');
SELECT ioc_insert( 'Taekwondo',   '+ 67 kg Women',  2008,'Beijing',      3,   'STEVENSON, Sarah',   'GBR');
SELECT ioc_insert( 'Taekwondo',   '+ 80 kg Men',  2008,'Beijing',        1,   'CHA, Dongmin',   'KOR');
SELECT ioc_insert( 'Taekwondo',   '+ 80 kg Men',  2008,'Beijing',        2,   'NIKOLAIDIS, Alexandros',   'GRE');
SELECT ioc_insert( 'Taekwondo',   '+ 80 kg Men',  2008,'Beijing',        3,   'CHILMANOV, Arman',   'KAZ');
SELECT ioc_insert( 'Taekwondo',   '+ 80 kg Men',  2008,'Beijing',        3,   'CHUKWUMERIJE, Chika Yagazie',   'NGR');
SELECT ioc_insert( 'Taekwondo',   '- 49 kg Women',  2008,'Beijing',      1,   'WU, Jingyu',   'CHN');
SELECT ioc_insert( 'Taekwondo',   '- 49 kg Women',  2008,'Beijing',      2,   'PUEDPONG, Buttree',   'THA');
SELECT ioc_insert( 'Taekwondo',   '- 49 kg Women',  2008,'Beijing',      3,   'CONTRERAS RIVERO, Dalia',   'VEN');
SELECT ioc_insert( 'Taekwondo',   '- 49 kg Women',  2008,'Beijing',      3,   'MONTEJO, Daynellis',   'CUB');

SELECT ioc_insert( 'Taekwondo',   '- 58 kg Men',  2008,'Beijing',        1,   'PEREZ, Guillermo',   'MEX');
SELECT ioc_insert( 'Taekwondo',   '- 58 kg Men',  2008,'Beijing',        2,   'MERCEDES, Yulis Gabriel',   'DOM');
SELECT ioc_insert( 'Taekwondo',   '- 58 kg Men',  2008,'Beijing',        3,   'CHU, Mu-Yen',   'TPE');
SELECT ioc_insert( 'Taekwondo',   '- 58 kg Men',  2008,'Beijing',        3,   'NIKPAI, Rohullah',   'AFG');
SELECT ioc_insert( 'Taekwondo',   '49 - 57 kg Women',  2008,'Beijing',   1,   'LIM, Sujeong',   'KOR');
SELECT ioc_insert( 'Taekwondo',   '49 - 57 kg Women',  2008,'Beijing',   2,   'TANRIKULU, Azize',   'TUR');
SELECT ioc_insert( 'Taekwondo',   '49 - 57 kg Women',  2008,'Beijing',   3,   'LOPEZ, Diana',   'USA');
SELECT ioc_insert( 'Taekwondo',   '49 - 57 kg Women',  2008,'Beijing',   3,   'ZUBCIC, Martina',   'CRO');
SELECT ioc_insert( 'Taekwondo',   '57 - 67 kg Women',  2008,'Beijing',   1,   'HWANG, Kyungseon',   'KOR');
SELECT ioc_insert( 'Taekwondo',   '57 - 67 kg Women',  2008,'Beijing',   2,   'SERGERIE, Karine',   'CAN');
SELECT ioc_insert( 'Taekwondo',   '57 - 67 kg Women',  2008,'Beijing',   3,   'EPANGUE, Gwladys Patience',   'FRA');
SELECT ioc_insert( 'Taekwondo',   '57 - 67 kg Women',  2008,'Beijing',   3,   'SARIC, Sandra',   'CRO');
SELECT ioc_insert( 'Taekwondo',   '58 - 68 kg Men',  2008,'Beijing',     1,   'SON, Taejin',   'KOR');
SELECT ioc_insert( 'Taekwondo',   '58 - 68 kg Men',  2008,'Beijing',     2,   'LOPEZ, Mark',   'USA');
SELECT ioc_insert( 'Taekwondo',   '58 - 68 kg Men',  2008,'Beijing',     3,   'SUNG, Yu-Chi',   'TPE');
SELECT ioc_insert( 'Taekwondo',   '58 - 68 kg Men',  2008,'Beijing',     3,   'TAZEGUL, Servet',   'TUR');
SELECT ioc_insert( 'Taekwondo',   '68 - 80 kg Men',  2008,'Beijing',     1,   'SAEI, Hadi',   'IRI');
SELECT ioc_insert( 'Taekwondo',   '68 - 80 kg Men',  2008,'Beijing',     2,   'SARMIENTO, Mauro',   'ITA');
SELECT ioc_insert( 'Taekwondo',   '68 - 80 kg Men',  2008,'Beijing',     3,   'LOPEZ, Steven',   'USA');
SELECT ioc_insert( 'Taekwondo',   '68 - 80 kg Men',  2008,'Beijing',     3,   'ZHU, Guo',   'CHN');

COMMIT;

