/*
   This file is public domain.

   The data is a matter of public record, and comes from

      http://www.olympics.org/
      http://en.wikipedia.org/wiki/Olympic_Games
*/


SET search_path TO sqltutor_data;

BEGIN;

SELECT ioc_insert(     'Table Tennis',   'doubles Men',  1988,'Seoul',     1,                        'CHEN, Long-Can',   'CHN');
SELECT ioc_insert(     'Table Tennis',   'doubles Men',  1988,'Seoul',     1,                        'WEI, Qing-Guang',   'CHN');
SELECT ioc_insert(     'Table Tennis',   'doubles Men',  1988,'Seoul',     2,   'LUPULESKU, Ilija',   NULL);
SELECT ioc_insert(     'Table Tennis',   'doubles Men',  1988,'Seoul',     2,   'PRIMORAC, Zoran',   NULL);
SELECT ioc_insert(     'Table Tennis',   'doubles Men',  1988,'Seoul',     3,                        'AN, Jae Hyung',   'KOR');
SELECT ioc_insert(     'Table Tennis',   'doubles Men',  1988,'Seoul',     3,                        'YOO, Nam-Kyu',   'KOR');
SELECT ioc_insert(     'Table Tennis',   'doubles Women',  1988,'Seoul',   1,                        'HYUN, Jung Hwa',   'KOR');
SELECT ioc_insert(     'Table Tennis',   'doubles Women',  1988,'Seoul',   1,                        'YANG, Young-Ja',   'KOR');
SELECT ioc_insert(     'Table Tennis',   'doubles Women',  1988,'Seoul',   2,                        'CHEN, Jing',   'CHN');
SELECT ioc_insert(     'Table Tennis',   'doubles Women',  1988,'Seoul',   2,                        'JIAO, Zhi-Min',   'CHN');
SELECT ioc_insert(     'Table Tennis',   'doubles Women',  1988,'Seoul',   3,   'REED, Jasna',   NULL);
SELECT ioc_insert(     'Table Tennis',   'doubles Women',  1988,'Seoul',   3,   'PERKUCIN, Gordana',   NULL);
SELECT ioc_insert(     'Table Tennis',   'singles Men',  1988,'Seoul',     1,                        'YOO, Nam-Kyu',   'KOR');
SELECT ioc_insert(     'Table Tennis',   'singles Men',  1988,'Seoul',     2,                        'KIM, Ki Taik',   'KOR');
SELECT ioc_insert(     'Table Tennis',   'singles Men',  1988,'Seoul',     3,                        'LINDH, Erik',   'SWE');
SELECT ioc_insert(     'Table Tennis',   'singles Women',  1988,'Seoul',   1,                        'CHEN, Jing',   'CHN');
SELECT ioc_insert(     'Table Tennis',   'singles Women',  1988,'Seoul',   2,                        'LI, Hui-Fen',   'CHN');
SELECT ioc_insert(     'Table Tennis',   'singles Women',  1988,'Seoul',   3,                        'JIAO, Zhi-Min',   'CHN');
SELECT ioc_insert( 'Table Tennis',   'doubles Men',  1992,'Barcelona',     1,                        'LU, Lin',   'CHN');
SELECT ioc_insert( 'Table Tennis',   'doubles Men',  1992,'Barcelona',     1,                        'WANG, Tao',   'CHN');

SELECT ioc_insert( 'Table Tennis',   'doubles Men',  1992,'Barcelona',     2,   'FETZNER, Steffen',   'GER');
SELECT ioc_insert( 'Table Tennis',   'doubles Men',  1992,'Barcelona',     2,   'ROSSKOPF, Jörg',   'GER');
SELECT ioc_insert( 'Table Tennis',   'doubles Men',  1992,'Barcelona',     3,   'KANG, Hee Chan',   'KOR');
SELECT ioc_insert( 'Table Tennis',   'doubles Men',  1992,'Barcelona',     3,   'LEE, Chul Seung',   'KOR');
SELECT ioc_insert( 'Table Tennis',   'doubles Men',  1992,'Barcelona',     3,   'KIM, Taek Soo',   'KOR');
SELECT ioc_insert( 'Table Tennis',   'doubles Men',  1992,'Barcelona',     3,   'YOO, Nam-Kyu',   'KOR');
SELECT ioc_insert( 'Table Tennis',   'doubles Women',  1992,'Barcelona',   1,   'DENG, Yaping',   'CHN');
SELECT ioc_insert( 'Table Tennis',   'doubles Women',  1992,'Barcelona',   1,   'QIAO, Hong',   'CHN');
SELECT ioc_insert( 'Table Tennis',   'doubles Women',  1992,'Barcelona',   2,   'CHEN, Zihe',   'CHN');
SELECT ioc_insert( 'Table Tennis',   'doubles Women',  1992,'Barcelona',   2,   'GAO, Jun',   'CHN');
SELECT ioc_insert( 'Table Tennis',   'doubles Women',  1992,'Barcelona',   3,   'HYUN, Jung Hwa',   'KOR');
SELECT ioc_insert( 'Table Tennis',   'doubles Women',  1992,'Barcelona',   3,   'HONG, Cha Ok',   'KOR');
SELECT ioc_insert( 'Table Tennis',   'doubles Women',  1992,'Barcelona',   3,   'LI, Bun Hui',   'PRK');
SELECT ioc_insert( 'Table Tennis',   'doubles Women',  1992,'Barcelona',   3,   'YU, Sun Bok',   'PRK');
SELECT ioc_insert( 'Table Tennis',   'singles Men',  1992,'Barcelona',     1,   'WALDNER, Jan-Ove',   'SWE');
SELECT ioc_insert( 'Table Tennis',   'singles Men',  1992,'Barcelona',     2,   'GATIEN, Jean-Philippe',   'FRA');
SELECT ioc_insert( 'Table Tennis',   'singles Men',  1992,'Barcelona',     3,   'KIM, Taek Soo',   'KOR');
SELECT ioc_insert( 'Table Tennis',   'singles Men',  1992,'Barcelona',     3,   'MA, Wenge',   'CHN');
SELECT ioc_insert( 'Table Tennis',   'singles Women',  1992,'Barcelona',   1,   'DENG, Yaping',   'CHN');
SELECT ioc_insert( 'Table Tennis',   'singles Women',  1992,'Barcelona',   2,   'QIAO, Hong',   'CHN');

SELECT ioc_insert( 'Table Tennis',   'singles Women',  1992,'Barcelona',   3,   'LI, Bun Hui',   'PRK');
SELECT ioc_insert( 'Table Tennis',   'singles Women',  1992,'Barcelona',   3,   'HYUN, Jung Hwa',   'KOR');
SELECT ioc_insert(   'Table Tennis',   'doubles Men',  1996,'Atlanta',     1,   'KONG, Linghui',   'CHN');
SELECT ioc_insert(   'Table Tennis',   'doubles Men',  1996,'Atlanta',     1,   'LIU, Guoliang',   'CHN');
SELECT ioc_insert(   'Table Tennis',   'doubles Men',  1996,'Atlanta',     2,   'LU, Lin',   'CHN');
SELECT ioc_insert(   'Table Tennis',   'doubles Men',  1996,'Atlanta',     2,   'WANG, Tao',   'CHN');
SELECT ioc_insert(   'Table Tennis',   'doubles Men',  1996,'Atlanta',     3,   'LEE, Chul Seung',   'KOR');
SELECT ioc_insert(   'Table Tennis',   'doubles Men',  1996,'Atlanta',     3,   'YOO, Nam-Kyu',   'KOR');
SELECT ioc_insert(   'Table Tennis',   'doubles Women',  1996,'Atlanta',   1,   'DENG, Yaping',   'CHN');
SELECT ioc_insert(   'Table Tennis',   'doubles Women',  1996,'Atlanta',   1,   'QIAO, Hong',   'CHN');
SELECT ioc_insert(   'Table Tennis',   'doubles Women',  1996,'Atlanta',   2,   'LIU, Wei',   'CHN');
SELECT ioc_insert(   'Table Tennis',   'doubles Women',  1996,'Atlanta',   2,   'QIAO, Yunping',   'CHN');
SELECT ioc_insert(   'Table Tennis',   'doubles Women',  1996,'Atlanta',   3,   'PARK, Hae-Jung',   'KOR');
SELECT ioc_insert(   'Table Tennis',   'doubles Women',  1996,'Atlanta',   3,   'RYU, Ji-Hye',   'KOR');
SELECT ioc_insert(   'Table Tennis',   'singles Men',  1996,'Atlanta',     1,   'LIU, Guoliang',   'CHN');
SELECT ioc_insert(   'Table Tennis',   'singles Men',  1996,'Atlanta',     2,   'WANG, Tao',   'CHN');
SELECT ioc_insert(   'Table Tennis',   'singles Men',  1996,'Atlanta',     3,   'ROSSKOPF, Jörg',   'GER');
SELECT ioc_insert(   'Table Tennis',   'singles Women',  1996,'Atlanta',   1,   'DENG, Yaping',   'CHN');
SELECT ioc_insert(   'Table Tennis',   'singles Women',  1996,'Atlanta',   2,   'CHEN, Jing',   'TPE');
SELECT ioc_insert(   'Table Tennis',   'singles Women',  1996,'Atlanta',   3,   'QIAO, Hong',   'CHN');

SELECT ioc_insert(  'Table Tennis',   'doubles Men',  2004,'Athens',     2,   'KO, Lai Chak',   'HKG');
SELECT ioc_insert(  'Table Tennis',   'doubles Men',  2004,'Athens',     2,   'LI, Ching',   'HKG');
SELECT ioc_insert(  'Table Tennis',   'doubles Men',  2004,'Athens',     3,   'MAZE, Michael John',   'DEN');
SELECT ioc_insert(  'Table Tennis',   'doubles Men',  2004,'Athens',     3,   'TUGWELL, Finn',   'DEN');
SELECT ioc_insert(  'Table Tennis',   'doubles Women',  2004,'Athens',   1,   'WANG, Nan',   'CHN');
SELECT ioc_insert(  'Table Tennis',   'doubles Women',  2004,'Athens',   1,   'ZHANG, Yining',   'CHN');
SELECT ioc_insert(  'Table Tennis',   'doubles Women',  2004,'Athens',   2,   'LEE, Eun Sil',   'KOR');
SELECT ioc_insert(  'Table Tennis',   'doubles Women',  2004,'Athens',   2,   'SEOK, Eun Mi',   'KOR');
SELECT ioc_insert(  'Table Tennis',   'doubles Women',  2004,'Athens',   3,   'GUO, Yue',   'CHN');
SELECT ioc_insert(  'Table Tennis',   'doubles Women',  2004,'Athens',   3,   'NIU, Jianfeng',   'CHN');
SELECT ioc_insert(  'Table Tennis',   'singles Men',  2004,'Athens',     1,   'RYU, Seungmin',   'KOR');
SELECT ioc_insert(  'Table Tennis',   'singles Men',  2004,'Athens',     2,   'WANG, Hao',   'CHN');
SELECT ioc_insert(  'Table Tennis',   'singles Men',  2004,'Athens',     3,   'WANG, Liqin',   'CHN');
SELECT ioc_insert(  'Table Tennis',   'singles Women',  2004,'Athens',   1,   'ZHANG, Yining',   'CHN');
SELECT ioc_insert(  'Table Tennis',   'singles Women',  2004,'Athens',   2,   'KIM, Hyang Mi',   'PRK');
SELECT ioc_insert(  'Table Tennis',   'singles Women',  2004,'Athens',   3,   'KIM, Kyungah',   'KOR');
SELECT ioc_insert( 'Table Tennis',   'singles Men',  2008,'Beijing',     1,   'MA, Lin',   'CHN');
SELECT ioc_insert( 'Table Tennis',   'singles Men',  2008,'Beijing',     2,   'WANG, Hao',   'CHN');
SELECT ioc_insert( 'Table Tennis',   'singles Men',  2008,'Beijing',     3,   'WANG, Liqin',   'CHN');
SELECT ioc_insert( 'Table Tennis',   'singles Women',  2008,'Beijing',   1,   'ZHANG, Yining',   'CHN');

SELECT ioc_insert( 'Table Tennis',   'singles Women',  2008,'Beijing',   2,   'WANG, Nan',   'CHN');
SELECT ioc_insert( 'Table Tennis',   'singles Women',  2008,'Beijing',   3,   'GUO, Yue',   'CHN');
SELECT ioc_insert( 'Table Tennis',   'team Men',  2008,'Beijing',        1,   'MA, Lin',   'CHN');
SELECT ioc_insert( 'Table Tennis',   'team Men',  2008,'Beijing',        1,   'WANG, Hao',   'CHN');
SELECT ioc_insert( 'Table Tennis',   'team Men',  2008,'Beijing',        1,   'WANG, Liqin',   'CHN');
SELECT ioc_insert( 'Table Tennis',   'team Men',  2008,'Beijing',        2,   'BOLL, Timo',   'GER');
SELECT ioc_insert( 'Table Tennis',   'team Men',  2008,'Beijing',        2,   'OVTCHAROV, Dimitrij',   'GER');
SELECT ioc_insert( 'Table Tennis',   'team Men',  2008,'Beijing',        2,   'SUSS, Christian',   'GER');
SELECT ioc_insert( 'Table Tennis',   'team Men',  2008,'Beijing',        3,   'OH, Sangeun',   'KOR');
SELECT ioc_insert( 'Table Tennis',   'team Men',  2008,'Beijing',        3,   'RYU, Seungmin',   'KOR');
SELECT ioc_insert( 'Table Tennis',   'team Men',  2008,'Beijing',        3,   'YOON, Jaeyoung',   'KOR');
SELECT ioc_insert( 'Table Tennis',   'team Women',  2008,'Beijing',      1,   'GUO, Yue',   'CHN');
SELECT ioc_insert( 'Table Tennis',   'team Women',  2008,'Beijing',      1,   'WANG, Nan',   'CHN');
SELECT ioc_insert( 'Table Tennis',   'team Women',  2008,'Beijing',      1,   'ZHANG, Yining',   'CHN');
SELECT ioc_insert( 'Table Tennis',   'team Women',  2008,'Beijing',      2,   'FENG, Tian Wei',   'SIN');
SELECT ioc_insert( 'Table Tennis',   'team Women',  2008,'Beijing',      2,   'LI, Jiawei',   'SIN');
SELECT ioc_insert( 'Table Tennis',   'team Women',  2008,'Beijing',      2,   'WANG, Yue Gu',   'SIN');
SELECT ioc_insert( 'Table Tennis',   'team Women',  2008,'Beijing',      3,   'DANG, Yeseo',   'KOR');
SELECT ioc_insert( 'Table Tennis',   'team Women',  2008,'Beijing',      3,   'KIM, Kyungah',   'KOR');
SELECT ioc_insert( 'Table Tennis',   'team Women',  2008,'Beijing',      3,   'PARK, Miyoung',   'KOR');

SELECT ioc_insert( 'Table Tennis',   'doubles Men',  2000,'Sydney',     1,   'WANG, Liqin',   'CHN');
SELECT ioc_insert( 'Table Tennis',   'doubles Men',  2000,'Sydney',     1,   'YAN, Sen',   'CHN');
SELECT ioc_insert( 'Table Tennis',   'doubles Men',  2000,'Sydney',     2,   'LIU, Guoliang',   'CHN');
SELECT ioc_insert( 'Table Tennis',   'doubles Men',  2000,'Sydney',     2,   'KONG, Linghui',   'CHN');
SELECT ioc_insert( 'Table Tennis',   'doubles Men',  2000,'Sydney',     3,   'GATIEN, Jean-Philippe',   'FRA');
SELECT ioc_insert( 'Table Tennis',   'doubles Men',  2000,'Sydney',     3,   'CHILA, Patrick Antoine Edouard',   'FRA');
SELECT ioc_insert( 'Table Tennis',   'doubles Women',  2000,'Sydney',   1,   'LI, Ju',   'CHN');
SELECT ioc_insert( 'Table Tennis',   'doubles Women',  2000,'Sydney',   1,   'WANG, Nan',   'CHN');
SELECT ioc_insert( 'Table Tennis',   'doubles Women',  2000,'Sydney',   2,   'SUN, Jin',   'CHN');
SELECT ioc_insert( 'Table Tennis',   'doubles Women',  2000,'Sydney',   2,   'YANG, Ying',   'CHN');
SELECT ioc_insert( 'Table Tennis',   'doubles Women',  2000,'Sydney',   3,   'KIM, Moo-Kyo',   'KOR');
SELECT ioc_insert( 'Table Tennis',   'doubles Women',  2000,'Sydney',   3,   'RYU, Ji-Hye',   'KOR');
SELECT ioc_insert( 'Table Tennis',   'singles Men',  2000,'Sydney',     1,   'KONG, Linghui',   'CHN');
SELECT ioc_insert( 'Table Tennis',   'singles Men',  2000,'Sydney',     2,   'WALDNER, Jan-Ove',   'SWE');
SELECT ioc_insert( 'Table Tennis',   'singles Men',  2000,'Sydney',     3,   'LIU, Guoliang',   'CHN');
SELECT ioc_insert( 'Table Tennis',   'singles Women',  2000,'Sydney',   1,   'WANG, Nan',   'CHN');
SELECT ioc_insert( 'Table Tennis',   'singles Women',  2000,'Sydney',   2,   'LI, Ju',   'CHN');
SELECT ioc_insert( 'Table Tennis',   'singles Women',  2000,'Sydney',   3,   'CHEN, Jing',   'TPE');
SELECT ioc_insert( 'Table Tennis',   'doubles Men',  2004,'Athens',     1,   'CHEN, Qi',   'CHN');
SELECT ioc_insert( 'Table Tennis',   'doubles Men',  2004,'Athens',     1,   'MA, Lin',   'CHN');

COMMIT;

