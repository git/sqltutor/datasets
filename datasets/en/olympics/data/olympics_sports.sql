/*
   This file is public domain.

   The data is a matter of public record, and comes from

      http://www.olympics.org/
      http://en.wikipedia.org/wiki/Olympic_Games
*/


SET search_path TO sqltutor_data;

BEGIN;

INSERT INTO ioc_sports (category, sport) VALUES

-- Summer sports:

('Summer', 'Aquatics'),
('Summer', 'Archery'),
('Summer', 'Athletics'),
('Summer', 'Badminton'),
('Summer', 'Basketball'),
('Summer', 'Boxing'),
('Summer', 'Canoe / Kayak'),
('Summer', 'Cycling'),
('Summer', 'Equestrian'),
('Summer', 'Fencing'),
('Summer', 'Football'),
('Summer', 'Gymnastics'),
('Summer', 'Handball'),
('Summer', 'Hockey'),
('Summer', 'Judo'),
('Summer', 'Modern Pentathlon'),
('Summer', 'Rowing'),
('Summer', 'Sailing'),
('Summer', 'Shooting'),
('Summer', 'Table Tennis'),
('Summer', 'Taekwondo'),
('Summer', 'Tennis'),
('Summer', 'Triathlon'),
('Summer', 'Volleyball'),
('Summer', 'Weightlifting'),
('Summer', 'Wrestling'),

-- Winter sports:

('Winter', 'Biathlon'),
('Winter', 'Bobsleigh'),
('Winter', 'Curling'),
('Winter', 'Ice Hockey'),
('Winter', 'Luge'),
('Winter', 'Skating'),
('Winter', 'Skiing');



-- Summer Disciplines:

SELECT ioc_insert_discipline('Aquatics',     'Diving');
SELECT ioc_insert_discipline('Aquatics',     'Swimming');
SELECT ioc_insert_discipline('Aquatics',     'Synchronized Swimming');
SELECT ioc_insert_discipline('Aquatics',     'Water polo');
SELECT ioc_insert_discipline('Archery',      'Archery');
SELECT ioc_insert_discipline('Athletics',    'Athletics');
SELECT ioc_insert_discipline('Badminton',    'Badminton');
SELECT ioc_insert_discipline('Basketball',   'Basketball');
SELECT ioc_insert_discipline('Boxing',       'Boxing');
SELECT ioc_insert_discipline('Canoe / Kayak','Canoe / Kayak Flatwater');
SELECT ioc_insert_discipline('Canoe / Kayak','Canoe / Kayak Slalom');
SELECT ioc_insert_discipline('Cycling',      'Cycling BMX');
SELECT ioc_insert_discipline('Cycling',      'Cycling Road');
SELECT ioc_insert_discipline('Cycling',      'Cycling Track');
SELECT ioc_insert_discipline('Cycling',      'Mountain Bike');
SELECT ioc_insert_discipline('Equestrian',   'Equestrian / Dressage');
SELECT ioc_insert_discipline('Equestrian',   'Equestrian / Eventing');
SELECT ioc_insert_discipline('Equestrian',   'Equestrian / Jumping');
SELECT ioc_insert_discipline('Fencing',      'Fencing');
SELECT ioc_insert_discipline('Football',     'Football');
SELECT ioc_insert_discipline('Gymnastics',   'Artistic Gymnastics');
SELECT ioc_insert_discipline('Gymnastics',   'Rhytmic Gymnastics');
SELECT ioc_insert_discipline('Gymnastics',   'Trampoline');
SELECT ioc_insert_discipline('Handball',     'Handball');
SELECT ioc_insert_discipline('Hockey',       'Hockey');
SELECT ioc_insert_discipline('Judo',         'Judo');
SELECT ioc_insert_discipline('Modern Pentathlon', 'Modern Pentathlon');
SELECT ioc_insert_discipline('Rowing',       'Rowing');
SELECT ioc_insert_discipline('Sailing',      'Sailing');
SELECT ioc_insert_discipline('Shooting',     'Shooting');
SELECT ioc_insert_discipline('Table Tennis', 'Table Tennis');
SELECT ioc_insert_discipline('Taekwondo',    'Taekwondo');
SELECT ioc_insert_discipline('Tennis',       'Tennis');
SELECT ioc_insert_discipline('Triathlon',    'Triathlon');
SELECT ioc_insert_discipline('Volleyball',   'Beach volleyball');
SELECT ioc_insert_discipline('Volleyball',   'Volleyball');
SELECT ioc_insert_discipline('Weightlifting','Weightlifting');
SELECT ioc_insert_discipline('Wrestling',    'Wrestling Freestyle');
SELECT ioc_insert_discipline('Wrestling',    'Wrestling Greco-Roman');

-- Winter Disciplines:

SELECT ioc_insert_discipline('Biathlon',     'Biathlon');
SELECT ioc_insert_discipline('Bobsleigh',    'Bobsleigh');
SELECT ioc_insert_discipline('Bobsleigh',    'Skeleton');
SELECT ioc_insert_discipline('Curling',      'Curling');
SELECT ioc_insert_discipline('Ice Hockey',   'Ice Hockey');
SELECT ioc_insert_discipline('Luge',         'Luge');
SELECT ioc_insert_discipline('Skating',      'Figure skating');
SELECT ioc_insert_discipline('Skating',       'Short Track Speed Skating');
SELECT ioc_insert_discipline('Skating',      'Speed skating');
SELECT ioc_insert_discipline('Skiing',       'Alpine Skiing');
SELECT ioc_insert_discipline('Skiing',       'Cross Country Skiing');
SELECT ioc_insert_discipline('Skiing',       'Freestyle Skiing');
SELECT ioc_insert_discipline('Skiing',       'Nordic Combined');
SELECT ioc_insert_discipline('Skiing',       'Ski Jumping');
SELECT ioc_insert_discipline('Skiing',       'Snowboard');

 -- SELECT *
 --   FROM ioc_sports S
 --        NATURAL JOIN
 --        ioc_disciplines D;



-- ***************************************************
-- *********   Summer Disciplines / Events   *********
-- ***************************************************

-- Discipline Diving, Events:

SELECT ioc_insert_event('Diving', '10m platform Men');
SELECT ioc_insert_event('Diving', '3m springboard Men');
SELECT ioc_insert_event('Diving', 'synchronized diving 10m platform Men');
SELECT ioc_insert_event('Diving', 'synchronized diving 3m springboard Men');
SELECT ioc_insert_event('Diving', '10m platform Women');
SELECT ioc_insert_event('Diving', '3m springboard Women');
SELECT ioc_insert_event('Diving', 'synchronized diving 10m platform Women');
SELECT ioc_insert_event('Diving', 'synchronized diving 3m springboard Women');

-- Discipline Swimming, Events:

SELECT ioc_insert_event('Swimming', '100m backstroke Men');
SELECT ioc_insert_event('Swimming', '100m breaststroke Men');
SELECT ioc_insert_event('Swimming', '100m butterfly Men');
SELECT ioc_insert_event('Swimming', '100m freestyle Men');
SELECT ioc_insert_event('Swimming', '1500m freestyle Men');
SELECT ioc_insert_event('Swimming', '200m backstroke Men');
SELECT ioc_insert_event('Swimming', '200m breaststroke Men');
SELECT ioc_insert_event('Swimming', '200m butterfly Men');
SELECT ioc_insert_event('Swimming', '200m freestyle Men');
SELECT ioc_insert_event('Swimming', '200m individual medley Men');
SELECT ioc_insert_event('Swimming', '400m freestyle Men');
SELECT ioc_insert_event('Swimming', '400m individual medley Men');
SELECT ioc_insert_event('Swimming', '4x100m freestyle relay Men');
SELECT ioc_insert_event('Swimming', '4x100m medley relay Men');
SELECT ioc_insert_event('Swimming', '4x200m freestyle relay Men');
SELECT ioc_insert_event('Swimming', '50m freestyle Men');
SELECT ioc_insert_event('Swimming', 'marathon 10km Men');
SELECT ioc_insert_event('Swimming', '100m backstroke Women');
SELECT ioc_insert_event('Swimming', '100m breaststroke Women');
SELECT ioc_insert_event('Swimming', '100m butterfly Women');
SELECT ioc_insert_event('Swimming', '100m freestyle Women');
SELECT ioc_insert_event('Swimming', '200m backstroke Women');
SELECT ioc_insert_event('Swimming', '200m breaststroke Women');
SELECT ioc_insert_event('Swimming', '200m butterfly Women');
SELECT ioc_insert_event('Swimming', '200m freestyle Women');
SELECT ioc_insert_event('Swimming', '200m individual medley Women');
SELECT ioc_insert_event('Swimming', '400m freestyle Women');
SELECT ioc_insert_event('Swimming', '400m individual medley Women');
SELECT ioc_insert_event('Swimming', '4x100m freestyle relay Women');
SELECT ioc_insert_event('Swimming', '4x100m medley relay Women');
SELECT ioc_insert_event('Swimming', '4x200m freestyle relay Women');
SELECT ioc_insert_event('Swimming', '50m freestyle Women');
SELECT ioc_insert_event('Swimming', '800m freestyle Women');
SELECT ioc_insert_event('Swimming', 'marathon 10km Women');

-- Discipline Synchronized Swimming, Events:

SELECT ioc_insert_event('Synchronized Swimming', 'duet Women');
SELECT ioc_insert_event('Synchronized Swimming', 'team Women');

-- Discipline Water polo, Events:

SELECT ioc_insert_event('Water polo', 'water polo Women');
SELECT ioc_insert_event('Water polo', 'water polo Men');

-- Discipline Archery, Events:

SELECT ioc_insert_event('Archery', 'individual (FITA Olympic round - 70m) Men');
SELECT ioc_insert_event('Archery', 'team (FITA Olympic round - 70m) Men');
SELECT ioc_insert_event('Archery', 'individual (FITA Olympic round - 70m) Women');
SELECT ioc_insert_event('Archery', 'team (FITA Olympic round - 70m) Women');

-- Discipline Athletics, Events:

SELECT ioc_insert_event('Athletics', '10000m Men');
SELECT ioc_insert_event('Athletics', '100m Men');
SELECT ioc_insert_event('Athletics', '110m hurdles Men');
SELECT ioc_insert_event('Athletics', '1500m Men');
SELECT ioc_insert_event('Athletics', '200m Men');
SELECT ioc_insert_event('Athletics', '20km walk Men');
SELECT ioc_insert_event('Athletics', '3000m steeplechase Men');
SELECT ioc_insert_event('Athletics', '400m Men');
SELECT ioc_insert_event('Athletics', '400m hurdles Men');
SELECT ioc_insert_event('Athletics', '4x100m relay Men');
SELECT ioc_insert_event('Athletics', '4x400m relay Men');
SELECT ioc_insert_event('Athletics', '5000m Men');
SELECT ioc_insert_event('Athletics', '50km walk Men');
SELECT ioc_insert_event('Athletics', '800m Men');
SELECT ioc_insert_event('Athletics', 'decathlon Men');
SELECT ioc_insert_event('Athletics', 'discus throw Men');
SELECT ioc_insert_event('Athletics', 'hammer throw Men');
SELECT ioc_insert_event('Athletics', 'high jump Men');
SELECT ioc_insert_event('Athletics', 'javelin throw Men');
SELECT ioc_insert_event('Athletics', 'long jump Men');
SELECT ioc_insert_event('Athletics', 'marathon Men');
SELECT ioc_insert_event('Athletics', 'pole vault Men');
SELECT ioc_insert_event('Athletics', 'shot put Men');
SELECT ioc_insert_event('Athletics', 'triple jump Men');
SELECT ioc_insert_event('Athletics', '10000m Women');
SELECT ioc_insert_event('Athletics', '100m Women');
SELECT ioc_insert_event('Athletics', '100m hurdles Women');
SELECT ioc_insert_event('Athletics', '1500m Women');
SELECT ioc_insert_event('Athletics', '200m Women');
SELECT ioc_insert_event('Athletics', '20km race walk Women');
SELECT ioc_insert_event('Athletics', '3000m steeplechase Women');
SELECT ioc_insert_event('Athletics', '400m Women');
SELECT ioc_insert_event('Athletics', '400m hurdles Women');
SELECT ioc_insert_event('Athletics', '4x100m relay Women');
SELECT ioc_insert_event('Athletics', '4x400m relay Women');
SELECT ioc_insert_event('Athletics', '5000m Women');
SELECT ioc_insert_event('Athletics', '800m Women');
SELECT ioc_insert_event('Athletics', 'discus throw Women');
SELECT ioc_insert_event('Athletics', 'hammer throw Women');
SELECT ioc_insert_event('Athletics', 'heptathlon Women');
SELECT ioc_insert_event('Athletics', 'high jump Women');
SELECT ioc_insert_event('Athletics', 'javelin throw Women');
SELECT ioc_insert_event('Athletics', 'long jump Women');
SELECT ioc_insert_event('Athletics', 'marathon Women');
SELECT ioc_insert_event('Athletics', 'pole vault Women');
SELECT ioc_insert_event('Athletics', 'shot put Women');
SELECT ioc_insert_event('Athletics', 'triple jump Women');

-- Discipline Badminton, Events:

SELECT ioc_insert_event('Badminton', 'doubles Men');
SELECT ioc_insert_event('Badminton', 'singles Men');
SELECT ioc_insert_event('Badminton', 'doubles Women');
SELECT ioc_insert_event('Badminton', 'singles Women');
SELECT ioc_insert_event('Badminton', 'doubles Mixed');

-- Discipline Basketball, Events:

SELECT ioc_insert_event('Basketball', 'basketball Men');
SELECT ioc_insert_event('Basketball', 'basketball Women');

-- Discipline Boxing, Events:

SELECT ioc_insert_event('Boxing', '+ 91kg (super heavyweight) Men');
SELECT ioc_insert_event('Boxing', '- 48kg (light-flyweight) Men');
SELECT ioc_insert_event('Boxing', '48 - 51kg (flyweight) Men');
SELECT ioc_insert_event('Boxing', '51 - 54kg (bantamweight) Men');
SELECT ioc_insert_event('Boxing', '54 - 57kg (featherweight) Men');
SELECT ioc_insert_event('Boxing', '57 - 60kg (lightweight) Men');
SELECT ioc_insert_event('Boxing', '60 - 64 kg Men');
SELECT ioc_insert_event('Boxing', '64 - 69 kg Men');
SELECT ioc_insert_event('Boxing', '69 - 75 kg Men');
SELECT ioc_insert_event('Boxing', '75 - 81kg (light-heavyweight) Men');
SELECT ioc_insert_event('Boxing', '81 - 91kg (heavyweight) Men');

-- Discipline Canoe / Kayak Flatwater, Events:

SELECT ioc_insert_event('Canoe / Kayak Flatwater', 'C-1 1000m (canoe single) Men');
SELECT ioc_insert_event('Canoe / Kayak Flatwater', 'C-1 500m (canoe single) Men');
SELECT ioc_insert_event('Canoe / Kayak Flatwater', 'C-2 1000m (canoe double) Men');
SELECT ioc_insert_event('Canoe / Kayak Flatwater', 'C-2 500m (canoe double) Men');
SELECT ioc_insert_event('Canoe / Kayak Flatwater', 'K-1 1000m (kayak single) Men');
SELECT ioc_insert_event('Canoe / Kayak Flatwater', 'K-1 500m (kayak single) Men');
SELECT ioc_insert_event('Canoe / Kayak Flatwater', 'K-2 1000m (kayak double) Men');
SELECT ioc_insert_event('Canoe / Kayak Flatwater', 'K-2 500m (kayak double) Men');
SELECT ioc_insert_event('Canoe / Kayak Flatwater', 'K-4 1000m (kayak four) Men');
SELECT ioc_insert_event('Canoe / Kayak Flatwater', 'K-1 500m (kayak single) Women');
SELECT ioc_insert_event('Canoe / Kayak Flatwater', 'K-2 500m (kayak double) Women');
SELECT ioc_insert_event('Canoe / Kayak Flatwater', 'K-4 500m (kayak four) Women');

-- Discipline Canoe / Kayak Slalom, Events:

SELECT ioc_insert_event('Canoe / Kayak Slalom', 'C-1 (canoe single) Men');
SELECT ioc_insert_event('Canoe / Kayak Slalom', 'C-2 (canoe double) Men');
SELECT ioc_insert_event('Canoe / Kayak Slalom', 'K-1 (kayak single) Men');
SELECT ioc_insert_event('Canoe / Kayak Slalom', 'K-1 (kayak single) Women');

-- Discipline Cycling BMX, Events:

SELECT ioc_insert_event('Cycling BMX', 'Individual Men');
SELECT ioc_insert_event('Cycling BMX', 'individual Women');

-- Discipline Cycling Road, Events:

SELECT ioc_insert_event('Cycling Road', 'individual road race Men');
SELECT ioc_insert_event('Cycling Road', 'individual time trial Men');
SELECT ioc_insert_event('Cycling Road', 'individual road race Women');
SELECT ioc_insert_event('Cycling Road', 'individual time trial Women');

-- Discipline Cycling Track, Events:

SELECT ioc_insert_event('Cycling Track', 'Individual Pursuit Men');
SELECT ioc_insert_event('Cycling Track', 'Keirin Men');
SELECT ioc_insert_event('Cycling Track', 'Madison Men');
SELECT ioc_insert_event('Cycling Track', 'Olympic Sprint Men');
SELECT ioc_insert_event('Cycling Track', 'Points Race Men');
SELECT ioc_insert_event('Cycling Track', 'Sprint indivual Men');
SELECT ioc_insert_event('Cycling Track', 'Team Pursuit (4000m) Men');
SELECT ioc_insert_event('Cycling Track', 'individual pursuit Women');
SELECT ioc_insert_event('Cycling Track', 'points race Women');
SELECT ioc_insert_event('Cycling Track', 'sprint Women');

-- Discipline Mountain Bike, Events:

SELECT ioc_insert_event('Mountain Bike', 'cross-country Men');
SELECT ioc_insert_event('Mountain Bike', 'cross-country Women');

-- Discipline Equestrian / Dressage, Events:

SELECT ioc_insert_event('Equestrian / Dressage', 'individual Mixed');
SELECT ioc_insert_event('Equestrian / Dressage', 'team Mixed');

-- Discipline Equestrian / Eventing, Events:

SELECT ioc_insert_event('Equestrian / Eventing', 'individual Mixed');
SELECT ioc_insert_event('Equestrian / Eventing', 'team Mixed');

-- Discipline Equestrian / Jumping, Events:

SELECT ioc_insert_event('Equestrian / Jumping', 'individual Mixed');
SELECT ioc_insert_event('Equestrian / Jumping', 'team Mixed');

-- Discipline Fencing, Events:

SELECT ioc_insert_event('Fencing', 'épée individual Men');
SELECT ioc_insert_event('Fencing', 'épée team Men');
SELECT ioc_insert_event('Fencing', 'foil individual Men');
SELECT ioc_insert_event('Fencing', 'sabre individual Men');
SELECT ioc_insert_event('Fencing', 'sabre team Men');
SELECT ioc_insert_event('Fencing', 'épée individual Women');
SELECT ioc_insert_event('Fencing', 'foil individual Women');
SELECT ioc_insert_event('Fencing', 'foil team Women');
SELECT ioc_insert_event('Fencing', 'sabre individual Women');
SELECT ioc_insert_event('Fencing', 'sabre team Women');

-- Discipline Football, Events:

SELECT ioc_insert_event('Football', 'football Men');
SELECT ioc_insert_event('Football', 'football Women');

-- Discipline Artistic Gymnsatics, Events:

SELECT ioc_insert_event('Artistic Gymnastics', 'floor exercises Men');
SELECT ioc_insert_event('Artistic Gymnastics', 'horizontal bar Men');
SELECT ioc_insert_event('Artistic Gymnastics', 'individual all-round Men');
SELECT ioc_insert_event('Artistic Gymnastics', 'parallel bars Men');
SELECT ioc_insert_event('Artistic Gymnastics', 'pommel horse Men');
SELECT ioc_insert_event('Artistic Gymnastics', 'rings Men');
SELECT ioc_insert_event('Artistic Gymnastics', 'team competition Men');
SELECT ioc_insert_event('Artistic Gymnastics', 'vault Men');
SELECT ioc_insert_event('Artistic Gymnastics', 'balance beam Women');
SELECT ioc_insert_event('Artistic Gymnastics', 'floor exercises Women');
SELECT ioc_insert_event('Artistic Gymnastics', 'individual all-round Women');
SELECT ioc_insert_event('Artistic Gymnastics', 'team competition Women');
SELECT ioc_insert_event('Artistic Gymnastics', 'uneven bars Women');
SELECT ioc_insert_event('Artistic Gymnastics', 'vault Women');

-- Discipline Rhytmic Gymnastics, Events:

SELECT ioc_insert_event('Rhytmic Gymnastics', 'group competition Women');
SELECT ioc_insert_event('Rhytmic Gymnastics', 'individual all-round Women');

-- Discipline Trampoline, Events:

SELECT ioc_insert_event('Trampoline', 'individual Men');
SELECT ioc_insert_event('Trampoline', 'individual Women');

-- Discipline Handball, Events:

SELECT ioc_insert_event('Handball', 'handball Men');
SELECT ioc_insert_event('Handball', 'handball Women');

-- Discipline Hockey, Events:

SELECT ioc_insert_event('Hockey', 'hockey Men');
SELECT ioc_insert_event('Hockey', 'hockey Women');

-- Discipline Judo, Events:

SELECT ioc_insert_event('Judo', '+ 100kg (heavyweight) Men');
SELECT ioc_insert_event('Judo', '- 60 kg Men');
SELECT ioc_insert_event('Judo', '60 - 66kg (half-lightweight) Men');
SELECT ioc_insert_event('Judo', '66 - 73kg (lightweight) Men');
SELECT ioc_insert_event('Judo', '73 - 81kg (half-middleweight) Men');
SELECT ioc_insert_event('Judo', '81 - 90kg (middleweight) Men');
SELECT ioc_insert_event('Judo', '90 - 100kg (half-heavyweight) Men');
SELECT ioc_insert_event('Judo', '+ 78kg (heavyweight) Women');
SELECT ioc_insert_event('Judo', '- 48kg (extra-lightweight) Women');
SELECT ioc_insert_event('Judo', '48 - 52kg (half-lightweight) Women');
SELECT ioc_insert_event('Judo', '52 - 57kg (lightweight) Women');
SELECT ioc_insert_event('Judo', '57 - 63kg (half-middleweight) Women');
SELECT ioc_insert_event('Judo', '63 - 70kg (middleweight) Women');
SELECT ioc_insert_event('Judo', '70 - 78kg (half-heavyweight) Women');

-- Discipline Modern Pentathlon, Events:

SELECT ioc_insert_event('Modern Pentathlon', 'Individual competition Men');
SELECT ioc_insert_event('Modern Pentathlon', 'Individual competition Women');

-- Discipline Rowing, Events:

SELECT ioc_insert_event('Rowing', 'coxless pair (2-) Men');
SELECT ioc_insert_event('Rowing', 'double sculls (2x) Men');
SELECT ioc_insert_event('Rowing', 'eight with coxswain (8+) Men');
SELECT ioc_insert_event('Rowing', 'four without coxswain (4-) Men');
SELECT ioc_insert_event('Rowing', 'lightweight coxless four (4-) Men');
SELECT ioc_insert_event('Rowing', 'lightweight double sculls (2x) Men');
SELECT ioc_insert_event('Rowing', 'quadruple sculls without coxsw Men');
SELECT ioc_insert_event('Rowing', 'single sculls (1x) Men');
SELECT ioc_insert_event('Rowing', 'double sculls (2x) Women');
SELECT ioc_insert_event('Rowing', 'eight with coxswain (8+) Women');
SELECT ioc_insert_event('Rowing', 'lightweight double sculls (2x) Women');
SELECT ioc_insert_event('Rowing', 'pair without coxswain (2-) Women');
SELECT ioc_insert_event('Rowing', 'quadruple sculls without coxsw Women');
SELECT ioc_insert_event('Rowing', 'single sculls (1x) Women');

-- Discipline Sailing, Events:

SELECT ioc_insert_event('Sailing', '470 - Two Person Dinghy Men');
SELECT ioc_insert_event('Sailing', 'Laser - One Person Dinghy Men');
SELECT ioc_insert_event('Sailing', 'RS:X - Windsurfer Men');
SELECT ioc_insert_event('Sailing', 'Star - Keelboat Men');
SELECT ioc_insert_event('Sailing', '470 - Two Person Dinghy Women');
SELECT ioc_insert_event('Sailing', 'Laser Radial - One Person Ding Women');
SELECT ioc_insert_event('Sailing', 'RS:X - Windsurfer Women');
SELECT ioc_insert_event('Sailing', 'Yngling - Keelboat Women');
SELECT ioc_insert_event('Sailing', '49er - Skiff Mixed');
SELECT ioc_insert_event('Sailing', 'Finn - Heavyweight Dinghy Mixed');
SELECT ioc_insert_event('Sailing', 'Tornado - Multihull Mixed');

-- Discipline Shooting, Events:

SELECT ioc_insert_event('Shooting', '10m air pistol (60 shots) Men');
SELECT ioc_insert_event('Shooting', '10m air rifle (60 shots) Men');
SELECT ioc_insert_event('Shooting', '25m rapid fire pistol (60 shots) Men');
SELECT ioc_insert_event('Shooting', '50m pistol (60 shots) Men');
SELECT ioc_insert_event('Shooting', '50m rifle 3 positions (3x40 shots) Men');
SELECT ioc_insert_event('Shooting', '50m rifle prone (60 shots) Men');
SELECT ioc_insert_event('Shooting', 'double trap (150 targets) Men');
SELECT ioc_insert_event('Shooting', 'skeet (125 targets) Men');
SELECT ioc_insert_event('Shooting', 'trap (125 targets) Men');
SELECT ioc_insert_event('Shooting', '10m air pistol (40 shots) Women');
SELECT ioc_insert_event('Shooting', '10m air rifle (40 shots) Women');
SELECT ioc_insert_event('Shooting', '25m pistol (30+30 shots) Women');
SELECT ioc_insert_event('Shooting', '50m rifle 3 positions (3x20 shots) Women');
SELECT ioc_insert_event('Shooting', 'skeet (75 targets) Women');
SELECT ioc_insert_event('Shooting', 'trap (75 targets) Women');

-- Discipline Table Tennis, Events:

SELECT ioc_insert_event('Table Tennis', 'singles Men');
SELECT ioc_insert_event('Table Tennis', 'doubles Men');
SELECT ioc_insert_event('Table Tennis', 'team Men');
SELECT ioc_insert_event('Table Tennis', 'singles Women');
SELECT ioc_insert_event('Table Tennis', 'doubles Women');
SELECT ioc_insert_event('Table Tennis', 'team Women');

-- Discipline Taekwondo, Events:

SELECT ioc_insert_event('Taekwondo', '+ 80 kg Men');
SELECT ioc_insert_event('Taekwondo', '- 58 kg Men');
SELECT ioc_insert_event('Taekwondo', '58 - 68 kg Men');
SELECT ioc_insert_event('Taekwondo', '68 - 80 kg Men');
SELECT ioc_insert_event('Taekwondo', '+ 67 kg Women');
SELECT ioc_insert_event('Taekwondo', '- 49 kg Women');
SELECT ioc_insert_event('Taekwondo', '49 - 57 kg Women');
SELECT ioc_insert_event('Taekwondo', '57 - 67 kg Women');

-- Discipline Tennis, Events:

SELECT ioc_insert_event('Tennis', 'doubles Men');
SELECT ioc_insert_event('Tennis', 'singles Men');
SELECT ioc_insert_event('Tennis', 'doubles Women');
SELECT ioc_insert_event('Tennis', 'singles Women');

-- Discipline Triathlon, Events:

SELECT ioc_insert_event('Triathlon', 'individual Men');
SELECT ioc_insert_event('Triathlon', 'individual Women');

-- Discipline Beach volleyball, Events:

SELECT ioc_insert_event('Beach volleyball', 'beach volleyball Women');
SELECT ioc_insert_event('Beach volleyball', 'beach volleyball Men');

-- Discipline Volleyball, Events:

SELECT ioc_insert_event('Volleyball', 'volleyball Men');
SELECT ioc_insert_event('Volleyball', 'volleyball Women');

-- Discipline Weightlifting, Events:

SELECT ioc_insert_event('Weightlifting', '+ 105kg Men');
SELECT ioc_insert_event('Weightlifting', '105kg Men');
SELECT ioc_insert_event('Weightlifting', '56kg Men');
SELECT ioc_insert_event('Weightlifting', '62kg Men');
SELECT ioc_insert_event('Weightlifting', '69kg Men');
SELECT ioc_insert_event('Weightlifting', '77kg Men');
SELECT ioc_insert_event('Weightlifting', '85kg Men');
SELECT ioc_insert_event('Weightlifting', '94kg Men');
SELECT ioc_insert_event('Weightlifting', '+ 75kg Women');
SELECT ioc_insert_event('Weightlifting', '48kg Women');
SELECT ioc_insert_event('Weightlifting', '53kg Women');
SELECT ioc_insert_event('Weightlifting', '58kg Women');
SELECT ioc_insert_event('Weightlifting', '63kg Women');
SELECT ioc_insert_event('Weightlifting', '69kg Women');
SELECT ioc_insert_event('Weightlifting', '75kg Women');

-- Discipline Wrestling Freestyle, Events:

SELECT ioc_insert_event('Wrestling Freestyle', '- 55kg Men');
SELECT ioc_insert_event('Wrestling Freestyle', '55 - 60kg Men');
SELECT ioc_insert_event('Wrestling Freestyle', '60 - 66kg Men');
SELECT ioc_insert_event('Wrestling Freestyle', '66 - 74kg Men');
SELECT ioc_insert_event('Wrestling Freestyle', '74 - 84kg Men');
SELECT ioc_insert_event('Wrestling Freestyle', '84 - 96kg Men');
SELECT ioc_insert_event('Wrestling Freestyle', '96 - 120kg Men');
SELECT ioc_insert_event('Wrestling Freestyle', '- 48kg Women');
SELECT ioc_insert_event('Wrestling Freestyle', '48 - 55kg Women');
SELECT ioc_insert_event('Wrestling Freestyle', '55 - 63kg Women');
SELECT ioc_insert_event('Wrestling Freestyle', '63 - 72kg Women');

-- Discipline Wrestling Greco-Roman, Events:

SELECT ioc_insert_event('Wrestling Greco-Roman', '- 55kg Men');
SELECT ioc_insert_event('Wrestling Greco-Roman', '55 - 60kg Men');
SELECT ioc_insert_event('Wrestling Greco-Roman', '60 - 66kg Men');
SELECT ioc_insert_event('Wrestling Greco-Roman', '66 - 74kg Men');
SELECT ioc_insert_event('Wrestling Greco-Roman', '74 - 84kg Men');
SELECT ioc_insert_event('Wrestling Greco-Roman', '84 - 96kg Men');
SELECT ioc_insert_event('Wrestling Greco-Roman', '96 - 120kg Men');

-- **************************************************
-- *********   Winter Disciplines / Events  *********
-- **************************************************

-- Discipline Biathlon, Events:

SELECT ioc_insert_event('Biathlon', '10km Men');
SELECT ioc_insert_event('Biathlon', '12.5km pursuit Men');
SELECT ioc_insert_event('Biathlon', '15km mass start Men');
SELECT ioc_insert_event('Biathlon', '20km Men');
SELECT ioc_insert_event('Biathlon', '4x7.5km relay Men');
SELECT ioc_insert_event('Biathlon', '10km pursuit Women');
SELECT ioc_insert_event('Biathlon', '12,5km mass start Women');
SELECT ioc_insert_event('Biathlon', '15km Women');
SELECT ioc_insert_event('Biathlon', '4x6km relay Women');
SELECT ioc_insert_event('Biathlon', '7.5km Women');

-- Discipline Bobsleigh, Events:

SELECT ioc_insert_event('Bobsleigh', 'four-man Men');
SELECT ioc_insert_event('Bobsleigh', 'two-man Men');
SELECT ioc_insert_event('Bobsleigh', 'two-man Women');

-- Discipline Skeleton, Events:

SELECT ioc_insert_event('Skeleton', 'individual Men');
SELECT ioc_insert_event('Skeleton', 'individual Women');

-- Discipline Curling, Events:

SELECT ioc_insert_event('Curling', 'curling Men');
SELECT ioc_insert_event('Curling', 'curling Women');

-- Discipline Ice Hockey, Events:

SELECT ioc_insert_event('Ice Hockey', 'ice hockey Men');
SELECT ioc_insert_event('Ice Hockey', 'ice hockey Women');

-- Discipline Luge, Events:

SELECT ioc_insert_event('Luge', 'singles Men');
SELECT ioc_insert_event('Luge', 'singles Women');
SELECT ioc_insert_event('Luge', 'doubles Mixed');

-- Discipline Figure skating, Events:

SELECT ioc_insert_event('Figure skating', 'individual Men');
SELECT ioc_insert_event('Figure skating', 'individual Women');
SELECT ioc_insert_event('Figure skating', 'ice dancing Mixed');
SELECT ioc_insert_event('Figure skating', 'pairs Mixed');

-- Discipline Short Track Speed Skating, Events:

SELECT ioc_insert_event('Short Track Speed Skating', '1000m Men');
SELECT ioc_insert_event('Short Track Speed Skating', '1500m Men');
SELECT ioc_insert_event('Short Track Speed Skating', '5000m relay Men');
SELECT ioc_insert_event('Short Track Speed Skating', '500m Men');
SELECT ioc_insert_event('Short Track Speed Skating', '1000m Women');
SELECT ioc_insert_event('Short Track Speed Skating', '1500m Women');
SELECT ioc_insert_event('Short Track Speed Skating', '3000m relay Women');
SELECT ioc_insert_event('Short Track Speed Skating', '500m Women');

-- Discipline Speed skating, Events:

SELECT ioc_insert_event('Speed skating', '10000m Men');
SELECT ioc_insert_event('Speed skating', '1000m Men');
SELECT ioc_insert_event('Speed skating', '1500m Men');
SELECT ioc_insert_event('Speed skating', '5000m Men');
SELECT ioc_insert_event('Speed skating', '500m Men');
SELECT ioc_insert_event('Speed skating', 'Team pursuit Men');
SELECT ioc_insert_event('Speed skating', '1000m Women');
SELECT ioc_insert_event('Speed skating', '1500m Women');
SELECT ioc_insert_event('Speed skating', '3000m Women');
SELECT ioc_insert_event('Speed skating', '5000m Women');
SELECT ioc_insert_event('Speed skating', '500m Women');
SELECT ioc_insert_event('Speed skating', 'Team pursuit Women');
SELECT ioc_insert_event('Speed skating', '2x500m Men');
SELECT ioc_insert_event('Speed skating', '2x500m Women');

-- Discipline Alpine Skiing, Events:

SELECT ioc_insert_event('Alpine Skiing', 'alpine combined Men');
SELECT ioc_insert_event('Alpine Skiing', 'downhill Men');
SELECT ioc_insert_event('Alpine Skiing', 'giant slalom Men');
SELECT ioc_insert_event('Alpine Skiing', 'slalom Men');
SELECT ioc_insert_event('Alpine Skiing', 'super-G Men');
SELECT ioc_insert_event('Alpine Skiing', 'alpine combined Women');
SELECT ioc_insert_event('Alpine Skiing', 'downhill Women');
SELECT ioc_insert_event('Alpine Skiing', 'giant slalom Women');
SELECT ioc_insert_event('Alpine Skiing', 'slalom Women');
SELECT ioc_insert_event('Alpine Skiing', 'super-G Women');

-- Discipline Cross Country Skiing, Events:

SELECT ioc_insert_event('Cross Country Skiing', '10km pursuit Men');
SELECT ioc_insert_event('Cross Country Skiing', '15km Men');
SELECT ioc_insert_event('Cross Country Skiing', '30km mass start Men');
SELECT ioc_insert_event('Cross Country Skiing', '4x10km relay Men');
SELECT ioc_insert_event('Cross Country Skiing', '50km Men');
SELECT ioc_insert_event('Cross Country Skiing', 'Sprint 1,5km Men');
SELECT ioc_insert_event('Cross Country Skiing', '5km Women');
SELECT ioc_insert_event('Cross Country Skiing', '10km Women');
SELECT ioc_insert_event('Cross Country Skiing', '15km mass start Women');
SELECT ioc_insert_event('Cross Country Skiing', '30km Women');
SELECT ioc_insert_event('Cross Country Skiing', '4x5km relay Women');
SELECT ioc_insert_event('Cross Country Skiing', '5km pursuit Women');
SELECT ioc_insert_event('Cross Country Skiing', 'sprint 1.5km Women');
SELECT ioc_insert_event('Cross Country Skiing', 'Combined 10km + 15km pursuit Men');
SELECT ioc_insert_event('Cross Country Skiing', 'Combined 5km + 10km pursuit Women');
SELECT ioc_insert_event('Cross Country Skiing', '3x5km relay Women');
SELECT ioc_insert_event('Cross Country Skiing', 'Combined 7.5 + 7.5km mass star Women');
SELECT ioc_insert_event('Cross Country Skiing', '20km Women');

-- added for 2006 Turin games
SELECT ioc_insert_event('Cross Country Skiing',
                             'combined 15 + 15km mass start Men');
SELECT ioc_insert_event('Cross Country Skiing',
                             'combined 15 + 15km mass start Women');
SELECT ioc_insert_event('Cross Country Skiing',
                             'combined 7.5 + 7.5km mass start Women');
SELECT ioc_insert_event('Cross Country Skiing',
                             'team sprint Men');
SELECT ioc_insert_event('Cross Country Skiing',
                             'team sprint Women');
SELECT ioc_insert_event('Nordic Combined', 'individual sprint Men');

-- Discipline Freestyle Skiing, Events:

SELECT ioc_insert_event('Freestyle Skiing', 'aerials Men');
SELECT ioc_insert_event('Freestyle Skiing', 'moguls Men');
SELECT ioc_insert_event('Freestyle Skiing', 'aerials Women');
SELECT ioc_insert_event('Freestyle Skiing', 'moguls Women');

-- Discipline Nordic Combined, Events:

SELECT ioc_insert_event('Nordic Combined', 'individual Men');
SELECT ioc_insert_event('Nordic Combined', 'sprint Men');
SELECT ioc_insert_event('Nordic Combined', 'team Men');

-- Discipline Ski Jumping, Events:

SELECT ioc_insert_event('Ski Jumping', 'K120 individual (90m) Men');
SELECT ioc_insert_event('Ski Jumping', 'K120 team (90m) Men');
SELECT ioc_insert_event('Ski Jumping', 'K90 individual (70m) Men');

-- Discipline Snowboard, Events:

SELECT ioc_insert_event('Snowboard', 'Giant parallel slalom Men');
SELECT ioc_insert_event('Snowboard', 'Half-pipe Men');
SELECT ioc_insert_event('Snowboard', 'Snowboard Cross Men');
SELECT ioc_insert_event('Snowboard', 'Giant parallel slalom Women');
SELECT ioc_insert_event('Snowboard', 'Half-pipe Women');
SELECT ioc_insert_event('Snowboard', 'Snowboard Cross Women');
SELECT ioc_insert_event('Snowboard', 'giant-slalom Men');
SELECT ioc_insert_event('Snowboard', 'giant-slalom Women');

-- implicit status code is 0 (discontinued) for past olympics sports
-- setting status code 1 (recognized) for all previous objects

UPDATE ioc_sports      SET status_code=1;
UPDATE ioc_disciplines SET status_code=1;
UPDATE ioc_events      SET status_code=1;




-- *****************************************************************
-- *********   Discontinued Sports / Disciplines / Events  *********
-- *****************************************************************

INSERT INTO ioc_sports (category, sport) VALUES ('Summer', 'Cricket');
SELECT ioc_insert_discipline('Cricket', 'Cricket');

INSERT INTO ioc_sports (category, sport) VALUES ('Summer', 'Croquet');
SELECT ioc_insert_discipline('Croquet', 'Croquet');

INSERT INTO ioc_sports (category, sport) VALUES ('Summer', 'Golf');
SELECT ioc_insert_discipline('Golf', 'Golf');

INSERT INTO ioc_sports (category, sport) VALUES ('Summer', 'Jeu de paunme');
SELECT ioc_insert_discipline('Jeu de paunme', 'Jeu de paunme');

INSERT INTO ioc_sports (category, sport) VALUES ('Summer', 'Lacrosse');
SELECT ioc_insert_discipline('Lacrosse', 'Lacrosse');

INSERT INTO ioc_sports (category, sport) VALUES ('Summer', 'Basque Pelota');
SELECT ioc_insert_discipline('Basque Pelota', 'Basque Pelota');

INSERT INTO ioc_sports (category, sport) VALUES ('Summer', 'Polo');
SELECT ioc_insert_discipline('Polo', 'Polo');

INSERT INTO ioc_sports (category, sport) VALUES ('Summer', 'Power boating');
SELECT ioc_insert_discipline('Power boating', 'Power boating');

INSERT INTO ioc_sports (category, sport) VALUES ('Summer', 'Rackets');
SELECT ioc_insert_discipline('Rackets', 'Rackets');

INSERT INTO ioc_sports (category, sport) VALUES ('Summer', 'Rink-hockey');
SELECT ioc_insert_discipline('Rink-hockey', 'Rink-hockey');

INSERT INTO ioc_sports (category, sport) VALUES ('Summer', 'Roque');
SELECT ioc_insert_discipline('Roque', 'Roque');

INSERT INTO ioc_sports (category, sport) VALUES ('Summer', 'Rugby');
SELECT ioc_insert_discipline('Rugby', 'Rugby');

INSERT INTO ioc_sports (category, sport) VALUES ('Summer', 'Tug of War');
SELECT ioc_insert_discipline('Tug of War', 'Tug of War');

INSERT INTO ioc_sports (category, sport) VALUES ('Summer', 'Water Skiing');
SELECT ioc_insert_discipline('Water Skiing', 'Water Skiing');




-- Athens 1896

SELECT ioc_insert_event('Artistic Gymnastics', 'rope climbing Men');
SELECT ioc_insert_event('Artistic Gymnastics', 'team, horizontal bar Men');
SELECT ioc_insert_event('Artistic Gymnastics', 'team, parallel bars Men');
SELECT ioc_insert_event('Cycling Track',       '10km Men');
SELECT ioc_insert_event('Cycling Track',       '100km Men');
SELECT ioc_insert_event('Cycling Track',       '12-hour race Men');
SELECT ioc_insert_event('Cycling Track',       '1km time trial Men');
SELECT ioc_insert_event('Fencing',             'foil, masters Men');
SELECT ioc_insert_event('Shooting',            '25m army pistol Men');
SELECT ioc_insert_event('Shooting','25m rapid fire pistol (60 shots) Men');
SELECT ioc_insert_event('Shooting',            'army rifle, 200m Men');
SELECT ioc_insert_event('Shooting',            'army rifle, 300m Men');
SELECT ioc_insert_event('Swimming', '100m freestyle for sailors Men');
SELECT ioc_insert_event('Swimming',            '1200m freestyle Men');
SELECT ioc_insert_event('Weightlifting','heavyweight - one hand lift Men');
SELECT ioc_insert_event('Weightlifting','heavyweight - two hand lift Men');
SELECT ioc_insert_event('Wrestling Greco-Roman', 'open event Men');

-- Paris 1900

SELECT ioc_insert_event('Athletics', '200m hurdles Men');
SELECT ioc_insert_event('Athletics', '4000m steeplechase Men');
SELECT ioc_insert_event('Athletics', 'long jump, standing Men');
SELECT ioc_insert_event('Athletics', 'triple jump, standing Men');
SELECT ioc_insert_event('Basque Pelota',   'cesta punta Men');
SELECT ioc_insert_event('Rowing',   'four-oared shell with coxswain Men');
SELECT ioc_insert_event('Equestrian / Jumping',   'high jump Mixed');
SELECT ioc_insert_event('Equestrian / Jumping',   'individual Mixed');
SELECT ioc_insert_event('Equestrian / Jumping',   'long jump individual Mixed');
SELECT ioc_insert_event('Fencing',                'épée individual Men');
SELECT ioc_insert_event('Cricket',                'cricket Men');
SELECT ioc_insert_event( 'Croquet',                'double Men');
SELECT ioc_insert_event('Croquet',                'individual 1 ball Men');
SELECT ioc_insert_event('Croquet',                'individual 2 balls Men');
SELECT ioc_insert_event('Cycling Track',          '25 kilometres Men');
SELECT ioc_insert_event('Rowing',   'pair-oared shell with coxswain Men');
SELECT ioc_insert_event('Rowing', 'four-oared shell with coxswain Men');
SELECT ioc_insert_event('Rowing',   'single sculls (1x) Men');
SELECT ioc_insert_event( 'Rugby',    'rugby Men');
SELECT ioc_insert_event('Fencing',    'épée, amateurs and masters Men');
SELECT ioc_insert_event('Fencing',    'épée, masters Men');
SELECT ioc_insert_event('Fencing',    'foil individual Men');
SELECT ioc_insert_event('Fencing',    'foil, masters Men');
SELECT ioc_insert_event('Fencing',    'sabre individual Men');
SELECT ioc_insert_event('Fencing',    'sabre, masters Men');
SELECT ioc_insert_event('Football',   'football Men');
SELECT ioc_insert_event('Athletics',   '60m Men');
SELECT ioc_insert_event('Athletics',   '800m Men');
SELECT ioc_insert_event( 'Athletics',   'hammer throw Men');
SELECT ioc_insert_event( 'Athletics',   'high jump, standing Men');
SELECT ioc_insert_event( 'Athletics',   'long jump, standing Men');
SELECT ioc_insert_event('Golf',       'individual Men');
SELECT ioc_insert_event('Golf',       'individual Women');
SELECT ioc_insert_event('Polo',       'polo Men');
SELECT ioc_insert_event('Shooting',   '50m army pistol, team Men');
SELECT ioc_insert_event('Shooting',   'army rifle, 300m, 3 positions Men');
SELECT ioc_insert_event('Shooting',   'army rifle, 300m, kneeling Men');
SELECT ioc_insert_event('Tennis',   'mixed doubles Mixed');
SELECT ioc_insert_event('Swimming',   '200m team swimming Men');
SELECT ioc_insert_event('Swimming',   '4000m freestyle Men');
SELECT ioc_insert_event('Swimming',   'underwater swimming Men');
SELECT ioc_insert_event('Shooting',   'army rifle, 300m, prone Men');
SELECT ioc_insert_event('Shooting',   'army rifle, 300m, standing Men');
SELECT ioc_insert_event('Shooting',   'free rifle, team Men');
SELECT ioc_insert_event('Rowing',   'eight with coxswain (8+) Men');
SELECT ioc_insert_event('Rowing',   'four-oared shell with coxswain Men');
SELECT ioc_insert_event('Sailing',   '0.5-1t Mixed');
SELECT ioc_insert_event('Sailing',   '10-20t Mixed');
SELECT ioc_insert_event('Archery',               'au chapelet, 50m Men');
SELECT ioc_insert_event('Archery',               'au chapelet, 33m Men');
SELECT ioc_insert_event('Archery',               'au cordon doré, 33m Men');
SELECT ioc_insert_event('Archery',               'au cordon doré, 50m Men');
SELECT ioc_insert_event('Archery',               'sur la perche à la herse Men');
SELECT ioc_insert_event('Archery',               'sur la perche à la pyramide Men');
SELECT ioc_insert_event('Artistic Gymnastics',   'individual all-round Men');
SELECT ioc_insert_event('Athletics',   '5000m team Men');
SELECT ioc_insert_event('Sailing',    '3-10t Mixed');
SELECT ioc_insert_event('Sailing',    'open class Mixed');
SELECT ioc_insert_event('Shooting',   '25m rapid fire pistol (60 shots) Men');
SELECT ioc_insert_event('Shooting',   '50m army pistol, team Men');
SELECT ioc_insert_event('Tug of War',  'tug of war Men');
SELECT ioc_insert_event('Swimming',   '200m obstacle event Men');
SELECT ioc_insert_event('Swimming',   '200m team swimming Men');

-- St. Luis 1904

SELECT ioc_insert_event('Diving',   'plunge for distance Men');
SELECT ioc_insert_event('Swimming', '400m breaststroke Men');
SELECT ioc_insert_event('Swimming', '4x50y freestyle relay Men');
SELECT ioc_insert_event('Swimming', '50y freestyle (45.72m) Men');
SELECT ioc_insert_event('Swimming', '880y freestyle (804.66m) Men');
SELECT ioc_insert_event('Diving',   '10m platform Men');
SELECT ioc_insert_event('Diving',   '3m springboard Men');

-- St. Luis 1912

SELECT ioc_insert_event('Diving',   'plain high diving Men');

-- Los Angeles 1984

SELECT ioc_insert_event('Synchronized Swimming', 'solo Women');

-- Skiing

SELECT ioc_insert_event('Cross Country Skiing', '18km Men');


-- ********************************************************************
-- ******  update disciplines and events of discontinued sports  ******
-- ********************************************************************

UPDATE ioc_disciplines SET status_code = 0
 WHERE sport_id IN (SELECT sport_id
                      FROM ioc_sports
                     WHERE status_code = 0);

UPDATE ioc_events SET status_code = 0
 WHERE discipline_id IN (SELECT discipline_id
                           FROM ioc_disciplines
                          WHERE status_code = 0);

COMMIT;
