/*
   This file is public domain.

   The data is a matter of public record, and the views mimic base
   tables ttmd, team, ttms, ttws and games from http://sqlzoo.net/
 */


DROP   VIEW IF EXISTS sqltutor_data.ttmd;
CREATE VIEW sqltutor_data.ttmd (games, color, team, country)
AS
SELECT DISTINCT year, medal,
       1000*((year-1873)%10)
       +CASE medal WHEN 'gold'   THEN 500
                   WHEN 'silver' THEN 100
                   WHEN 'bronz'  THEN 700 
        END
       +(year-1873)/10, 
       country_code
  FROM sqltutor_data.medals
       NATURAL JOIN sqltutor_data.athletes
       NATURAL JOIN sqltutor_data.sports
 WHERE discipline='Table Tennis'
   AND event='doubles Men';


DROP   VIEW IF EXISTS sqltutor_data.team;
CREATE VIEW sqltutor_data.team(id, name)
AS
SELECT 1000*((year-1873)%10)
       +CASE medal WHEN 'gold'   THEN 500
                   WHEN 'silver' THEN 100
                   WHEN 'bronz'  THEN 700 
        END
       +(year-1873)/10, 
       upper(surname) || ', ' || forename
  FROM sqltutor_data.medals
       NATURAL JOIN sqltutor_data.athletes
       NATURAL JOIN sqltutor_data.sports
 WHERE discipline='Table Tennis'
   AND event='doubles Men';


DROP   VIEW IF EXISTS sqltutor_data.ttms;
CREATE VIEW sqltutor_data.ttms(games, color, who, country)
AS
SELECT year, medal, upper(surname) || ', ' || forename, country_code
  FROM sqltutor_data.medals
       NATURAL JOIN sqltutor_data.athletes
       NATURAL JOIN sqltutor_data.sports
 WHERE discipline='Table Tennis'
   AND event='singles Men';


DROP   VIEW IF EXISTS sqltutor_data.ttws;
CREATE VIEW sqltutor_data.ttws(games, color, who, country)
AS
SELECT year, medal, upper(surname) || ', ' || forename, country_code
  FROM sqltutor_data.medals
       NATURAL JOIN sqltutor_data.athletes
       NATURAL JOIN sqltutor_data.sports
 WHERE discipline='Table Tennis'
   AND event='singles Women';


DROP   VIEW IF EXISTS sqltutor_data.games;
CREATE VIEW sqltutor_data.games(year, city, country)
AS
SELECT year, city, country_code
  FROM sqltutor_data.olympics;


DROP   VIEW IF EXISTS sqltutor_data.country;
CREATE VIEW sqltutor_data.country(id, name)
AS
SELECT country_code, country
  FROM sqltutor_data.ioc_codes;



SELECT sqltutor.init_dataset ('ttms');
SELECT sqltutor.add_ds_source('ttms', 2008, 'http://sqlzoo.net');
SELECT sqltutor.add_ds_table ('ttms', 1, 'ttms', 'games, color, who, country');
SELECT sqltutor.add_ds_table ('ttms', 2, 'country', 'id, name');


SELECT sqltutor.init_dataset ('ttws');
SELECT sqltutor.add_ds_source('ttws', 2008, 'http://sqlzoo.net');
SELECT sqltutor.add_ds_table ('ttws', 1, 'ttws', 'games, color, who, country');
SELECT sqltutor.add_ds_table ('ttws', 2, 'games', 'year, city, country');


SELECT sqltutor.init_dataset ('ttmd');
SELECT sqltutor.add_ds_source('ttmd', 2008, 'http://sqlzoo.net');
SELECT sqltutor.add_ds_table ('ttmd', 1, 'ttmd', 'games, color, team, country');
SELECT sqltutor.add_ds_table ('ttmd', 2, 'team', 'id, name');


BEGIN;

SET search_path TO sqltutor;

SELECT insert_problem ('ttms', 1201, 3, 'join');
SELECT insert_question('ttms', 1201, 1, 'en',
'Men''s Singles Table Tennis Olympics Database:
Show the athelete (who) and the country name for medal winners in
2000.
');
SELECT insert_answer  ('ttms', 1201, 1,
'SELECT who, country.name
  FROM ttms JOIN country
         ON (ttms.country=country.id)
 WHERE games = 2000;
');

SELECT insert_problem ('ttms', 1202, 3, 'join');
SELECT insert_question('ttms', 1202, 1, 'en',
'Men''s Singles Table Tennis Olympics Database:
Show the who and the color of the medal for the medal winners from
''Sweden''.
');
SELECT insert_answer  ('ttms', 1202, 1,
'SELECT who, color
  FROM ttms JOIN country
         ON (ttms.country=country.id)
 WHERE name = ''Sweden'';
');

SELECT insert_problem ('ttms', 1203, 3, 'join');
SELECT insert_question('ttms', 1203, 1, 'en',
'Men''s Singles Table Tennis Olympics Database:
Show the years in which ''China'' won a ''gold'' medal.
');
SELECT insert_answer  ('ttms', 1203, 1,
'SELECT games
 FROM ttms JOIN country
         ON (ttms.country=country.id)
WHERE name=''China'' AND color=''gold'';
');

SELECT insert_problem ('ttws', 1204, 3, 'join');
SELECT insert_question('ttws', 1204, 1, 'en',
'Women''s Singles Table Tennis Olympics Database:
Show who won medals in the ''Barcelona'' games.
');
SELECT insert_answer  ('ttws', 1204, 1,
'SELECT who
  FROM ttws JOIN games
            ON (ttws.games=games.year)
  WHERE city = ''Barcelona'';
');

-- This QA is buggy, because it ignores category summer/winter
-- and gives a false result of 4 records including winter olympics in Calgary.
--
-- reported by Josef Pudil
-- removed  by Aleš Čepek 2017-12-16
--
-- SELECT insert_problem ('ttws', 1205, 3, 'join');
-- SELECT insert_question('ttws', 1205, 1, 'en',
-- 'Women''s Singles Table Tennis Olympics Database:
-- Show which city ''CHEN, Jing'' won medals. Show the city and the medal
-- color.
-- ');
-- SELECT insert_answer  ('ttws', 1205, 1,
-- 'SELECT city, color
--   FROM ttws JOIN games
--             ON (ttws.games=games.year)
--   WHERE who = ''CHEN, Jing'';
-- ');

SELECT insert_problem ('ttws', 1206, 3, 'join');
SELECT insert_question('ttws', 1206, 1, 'en',
'Women''s Singles Table Tennis Olympics Database:
Show who won the gold medal and the city.
');
SELECT insert_answer  ('ttws', 1206, 1,
'SELECT who, city
  FROM ttws JOIN games
            ON (ttws.games=games.year)
  WHERE color = ''gold'';
');

SELECT insert_problem ('ttmd', 1207, 3, 'join');
SELECT insert_question('ttmd', 1207, 1, 'en',
'Table Tennis Men''s Doubles:
Show the games and color of the medal won by the team that includes
''YAN, Sen''.
');
SELECT insert_answer  ('ttmd', 1207, 1,
'SELECT games, color
  FROM ttmd JOIN team ON team=team.id
 WHERE name = ''YAN, Sen'';
');

SELECT insert_problem ('ttmd', 1208, 3, 'join');
SELECT insert_question('ttmd', 1208, 1, 'en',
'Table Tennis Men''s Doubles:
Show the ''gold'' medal winners in 2004.
');
SELECT insert_answer  ('ttmd', 1208, 1,
'SELECT name
  FROM ttmd JOIN team ON team=team.id
 WHERE color=''gold'' AND games=2004;
');

SELECT insert_problem ('ttmd', 1209, 3, 'join');
SELECT insert_question('ttmd', 1209, 1, 'en',
'Table Tennis Men''s Doubles:
Show the name of each medal winner country ''FRA''.
');
SELECT insert_answer  ('ttmd', 1209, 1,
'SELECT name
  FROM ttmd JOIN team ON team=team.id
 WHERE country = ''FRA'';
');

COMMIT;

