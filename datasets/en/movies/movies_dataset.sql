SELECT sqltutor.init_dataset ('movies');
SELECT sqltutor.add_ds_source('movies', 2008, 'http://sqlzoo.net/');
SELECT sqltutor.add_ds_table ('movies', 1, 'actor',   'id, name');
SELECT sqltutor.add_ds_table ('movies', 2, 'movie',
                              'id, title, year, score, votes, director');
SELECT sqltutor.add_ds_table ('movies', 3, 'casting', 'movieid, actorid, ord');
