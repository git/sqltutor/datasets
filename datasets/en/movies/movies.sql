BEGIN;

SET search_path TO sqltutor;

SELECT insert_problem ('movies', 1301, 1, 'select');
SELECT insert_question('movies', 1301, 1, 'en', 
'List the films where the year is 1962 [Show id, title]
');
SELECT insert_answer  ('movies', 1301, 1,
'SELECT id, title
 FROM movie
 WHERE year=1962;
');

SELECT insert_problem ('movies', 1303, 1, 'select');
SELECT insert_question('movies', 1303, 1, 'en', 
'List all of the Star Trek movies, include the id  title and year.
');
SELECT insert_answer  ('movies', 1303, 1,
'SELECT id, title, year
 FROM movie
 WHERE title LIKE ''Star Trek%'';
');

SELECT insert_problem ('movies', 1304, 1, 'select');
SELECT insert_question('movies', 1304, 1, 'en', 
'What are the titles of the films with id 1, 2, 3 ?
');
SELECT insert_answer  ('movies', 1304, 1,
'SELECT title FROM movie WHERE id IN (1,2,3);
');

SELECT insert_problem ('movies', 1305, 1, 'select');
SELECT insert_question('movies', 1305, 1, 'en', 
'What id number does the actor ''Glenn Close'' have?
');
SELECT insert_answer  ('movies', 1305, 1,
'SELECT id FROM actor
  WHERE name= ''Glenn Close'';
');

SELECT insert_problem ('movies', 1306, 1, 'select');
SELECT insert_question('movies', 1306, 1, 'en', 
'What is the id of the film ''Casablanca''?
');
SELECT insert_answer  ('movies', 1306, 1,
'SELECT id FROM movie WHERE title=''Casablanca'';
');

SELECT insert_problem ('movies', 1307, 3, 'subselect');
SELECT insert_question('movies', 1307, 1, 'en', 
'Obtain the cast list for ''Casablanca''. 
');
SELECT insert_answer  ('movies', 1307, 1,
'SELECT name
  FROM casting, actor
  WHERE movieid=(SELECT id FROM movie WHERE title=''Casablanca'')
    AND actorid=actor.id;
');

SELECT insert_problem ('movies', 1308, 4, 'join');
SELECT insert_question('movies', 1308, 1, 'en', 
'Obtain the cast list for the film ''Alien''
');
SELECT insert_answer  ('movies', 1308, 1,
'SELECT name
  FROM movie, casting, actor
  WHERE title=''Alien''
    AND movieid=movie.id
    AND actorid=actor.id;
');

SELECT insert_problem ('movies', 1309, 4, 'join');
SELECT insert_question('movies', 1309, 1, 'en', 
'List the films in which ''Harrison Ford'' has appeared
');
SELECT insert_answer  ('movies', 1309, 1,
'SELECT title
  FROM movie, casting, actor
 WHERE name=''Harrison Ford''
    AND movieid=movie.id
    AND actorid=actor.id
  AND ord>1;
');

SELECT insert_problem ('movies', 1310, 4, 'join');
SELECT insert_question('movies', 1310, 1, 'en', 
'List the films together with the leading star for all 1962 films.
');
SELECT insert_answer  ('movies', 1310, 1,
'SELECT title, name
  FROM movie, casting, actor
 WHERE year=1962
    AND movieid=movie.id
    AND actorid=actor.id
    AND ord=1;
');

SELECT insert_problem ('movies', 1311, 10, 'aggregate');
SELECT insert_question('movies', 1311, 1, 'en', 
'Which were the busiest years for ''John Travolta''. Show the number
of movies he made for each year.
');
SELECT insert_answer  ('movies', 1311, 1,
'SELECT year,COUNT(title) FROM
  movie JOIN casting ON movie.id=movieid
        JOIN actor   ON actorid=actor.id
where name=''John Travolta''
GROUP BY year
HAVING COUNT(title)=(SELECT MAX(c) FROM
(SELECT year,COUNT(title) AS c FROM
   movie JOIN casting ON movie.id=movieid
         JOIN actor   ON actorid=actor.id
 where name=''John Travolta''
 GROUP BY year) AS t);
');

SELECT insert_problem ('movies', 1312, 10, 'join|subselect');
SELECT insert_question('movies', 1312, 1, 'en', 
'List the film title and the leading actor for all of ''Julie Andrews'' films.
');
SELECT insert_answer  ('movies', 1312, 1,
'SELECT title, name
  FROM movie, casting, actor
  WHERE movieid=movie.id
    AND actorid=actor.id
    AND ord=1
    AND movieid IN
    (SELECT movieid FROM casting, actor
     WHERE actorid=actor.id
     AND name=''Julie Andrews'');
');

SELECT insert_problem ('movies', 1313, 10, 'join|aggregate');
SELECT insert_question('movies', 1313, 1, 'en', 
'Obtain a list of actors in who have had at least 10 starring roles.
');
SELECT insert_answer  ('movies', 1313, 1,
'SELECT name
    FROM casting JOIN actor
      ON  actorid = actor.id
    WHERE ord=1
    GROUP BY name
    HAVING COUNT(movieid)>=10;
');

SELECT insert_problem ('movies', 1314, 10, 'join|aggregate');
SELECT insert_question('movies', 1314, 1, 'en', 
'List the 1978 films by order of cast list size.
');
SELECT insert_answer  ('movies', 1314, 1,
'SELECT title, COUNT(actorid)
  FROM casting, movie
  WHERE year=1978
    AND movieid=movie.id
  GROUP BY title
  ORDER BY 2 DESC;
');

SELECT insert_problem ('movies', 1315, 10, 'join|aggregate');
SELECT insert_question('movies', 1315, 1, 'en', 
'List all the people who have worked with ''Art Garfunkel''.
');
SELECT insert_answer  ('movies', 1315, 1,
'SELECT DISTINCT name
  FROM actor, casting
  WHERE actorid=actor.id
    AND movieid IN (
    SELECT movieid FROM casting, actor
      WHERE actorid=actor.id
        AND name=''Art Garfunkel''
    )
    AND NOT name=''Art Garfunkel'';
');

COMMIT;

