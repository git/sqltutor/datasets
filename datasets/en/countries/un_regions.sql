/*
   This file is public domain.

   The data is a matter of public record, and comes from
   http://unstats.un.org/unsd/methods/m49/m49regin.htm

   2008-10-04

   Composition of macro geographical (continental) regions, geographical
   sub-regions, and selected economic and other groupings
 */


/* Geographical region and composition */

SET search_path TO sqltutor_data;

BEGIN;

INSERT INTO un_regions (region, name) VALUES
   (001, 'World'),
   (002, 'Africa'),
   (014, 'Eastern Africa'),
   (017, 'Middle Africa'),
   (015, 'Northern Africa'),
   (018, 'Southern Africa'),
   (011, 'Western Africa'),
   (019, 'Americas'),
   (419, 'Latin America and the Caribbean'),
   (029, 'Caribbean'),
   (013, 'Central America'),
   (005, 'South America'),
   (021, 'Northern America'),
   (142, 'Asia'),
   (143, 'Central Asia'),
   (030, 'Eastern Asia'),
   (034, 'Southern Asia'),
   (035, 'South-Eastern Asia'),
   (145, 'Western Asia'),
   (150, 'Europe'),
   (151, 'Eastern Europe'),
   (154, 'Northern Europe'),
   (039, 'Southern Europe'),
   (155, 'Western Europe'),
   (009, 'Oceania'),
   (053, 'Australia and New Zealand'),
   (054, 'Melanesia'),
   (057, 'Micronesia'),
   (061, 'Polynesia');

-- 002 Africa
-- 014 Eastern Africa

INSERT INTO un_regions_countries (region, country) VALUES
(002, 108), -- Burundi
(002, 174), -- Comoros
(002, 262), -- Djibouti
(002, 232), -- Eritrea
(002, 231), -- Ethiopia
(002, 404), -- Kenya
(002, 450), -- Madagascar
(002, 454), -- Malawi
(002, 480), -- Mauritius
(002, 175), -- Mayotte
(002, 508), -- Mozambique
(002, 638), -- Réunion
(002, 646), -- Rwanda
(002, 690), -- Seychelles
(002, 706), -- Somalia
(002, 800), -- Uganda
(002, 834), -- United Republic of Tanzania
(002, 894), -- Zambia
(002, 716), -- Zimbabwe
(014, 108), -- Burundi
(014, 174), -- Comoros
(014, 262), -- Djibouti
(014, 232), -- Eritrea
(014, 231), -- Ethiopia
(014, 404), -- Kenya
(014, 450), -- Madagascar
(014, 454), -- Malawi
(014, 480), -- Mauritius
(014, 175), -- Mayotte
(014, 508), -- Mozambique
(014, 638), -- Réunion
(014, 646), -- Rwanda
(014, 690), -- Seychelles
(014, 706), -- Somalia
(014, 800), -- Uganda
(014, 834), -- United Republic of Tanzania
(014, 894), -- Zambia
(014, 716), -- Zimbabwe

-- 002 Africa
-- 017 Middle Africa

(002, 024), -- Angola
(002, 120), -- Cameroon
(002, 140), -- Central African Republic
(002, 148), -- Chad
(002, 178), -- Congo
(002, 180), -- Democratic Republic of the Congo
(002, 226), -- Equatorial Guinea
(002, 266), -- Gabon
(002, 678), -- Sao Tome and Principe

(017, 024), -- Angola
(017, 120), -- Cameroon
(017, 140), -- Central African Republic
(017, 148), -- Chad
(017, 178), -- Congo
(017, 180), -- Democratic Republic of the Congo
(017, 226), -- Equatorial Guinea
(017, 266), -- Gabon
(017, 678), -- Sao Tome and Principe


-- 002 Africa
-- 015 Northern Africa

(002, 012), -- Algeria
(002, 818), -- Egypt
(002, 434), -- Libyan Arab Jamahiriya
(002, 504), -- Morocco
(002, 736), -- Sudan
(002, 788), -- Tunisia
(002, 732), -- Western Sahara

(015, 012), -- Algeria
(015, 818), -- Egypt
(015, 434), -- Libyan Arab Jamahiriya
(015, 504), -- Morocco
(015, 736), -- Sudan
(015, 788), -- Tunisia
(015, 732), -- Western Sahara


-- 002 Africa
-- 018 Southern Africa

(002, 072), -- Botswana
(002, 426), -- Lesotho
(002, 516), -- Namibia
(002, 710), -- South Africa
(002, 748), -- Swaziland

(018, 072), -- Botswana
(018, 426), -- Lesotho
(018, 516), -- Namibia
(018, 710), -- South Africa
(018, 748), -- Swaziland


-- 002 Africa
-- 011 Western Africa

(002, 204), -- Benin
(002, 854), -- Burkina Faso
(002, 132), -- Cape Verde
(002, 384), -- Cote d'Ivoire
(002, 270), -- Gambia
(002, 288), -- Ghana
(002, 324), -- Guinea
(002, 624), -- Guinea-Bissau
(002, 430), -- Liberia
(002, 466), -- Mali
(002, 478), -- Mauritania
(002, 562), -- Niger
(002, 566), -- Nigeria
(002, 654), -- Saint Helena
(002, 686), -- Senegal
(002, 694), -- Sierra Leone
(002, 768), -- Togo

(011, 204), -- Benin
(011, 854), -- Burkina Faso
(011, 132), -- Cape Verde
(011, 384), -- Cote d'Ivoire
(011, 270), -- Gambia
(011, 288), -- Ghana
(011, 324), -- Guinea
(011, 624), -- Guinea-Bissau
(011, 430), -- Liberia
(011, 466), -- Mali
(011, 478), -- Mauritania
(011, 562), -- Niger
(011, 566), -- Nigeria
(011, 654), -- Saint Helena
(011, 686), -- Senegal
(011, 694), -- Sierra Leone
(011, 768), -- Togo


-- 019 Americas
-- 419 Latin America and the Caribbean
-- 029 Caribbean

(019, 660), -- Anguilla
(019, 028), -- Antigua and Barbuda
(019, 533), -- Aruba
(019, 044), -- Bahamas
(019, 052), -- Barbados
(019, 092), -- British Virgin Islands
(019, 136), -- Cayman Islands
(019, 192), -- Cuba
(019, 212), -- Dominica
(019, 214), -- Dominican Republic
(019, 308), -- Grenada
(019, 312), -- Guadeloupe
(019, 332), -- Haiti
(019, 388), -- Jamaica
(019, 474), -- Martinique
(019, 500), -- Montserrat
(019, 530), -- Netherlands Antilles
(019, 630), -- Puerto Rico
(019, 652), -- Saint-Barthélemy
(019, 659), -- Saint Kitts and Nevis
(019, 662), -- Saint Lucia
(019, 663), -- Saint Martin (French part)
(019, 670), -- Saint Vincent and the Grenadines
(019, 780), -- Trinidad and Tobago
(019, 796), -- Turks and Caicos Islands
(019, 850), -- United States Virgin Islands

(419, 660), -- Anguilla
(419, 028), -- Antigua and Barbuda
(419, 533), -- Aruba
(419, 044), -- Bahamas
(419, 052), -- Barbados
(419, 092), -- British Virgin Islands
(419, 136), -- Cayman Islands
(419, 192), -- Cuba
(419, 212), -- Dominica
(419, 214), -- Dominican Republic
(419, 308), -- Grenada
(419, 312), -- Guadeloupe
(419, 332), -- Haiti
(419, 388), -- Jamaica
(419, 474), -- Martinique
(419, 500), -- Montserrat
(419, 530), -- Netherlands Antilles
(419, 630), -- Puerto Rico
(419, 652), -- Saint-Barthélemy
(419, 659), -- Saint Kitts and Nevis
(419, 662), -- Saint Lucia
(419, 663), -- Saint Martin (French part)
(419, 670), -- Saint Vincent and the Grenadines
(419, 780), -- Trinidad and Tobago
(419, 796), -- Turks and Caicos Islands
(419, 850), -- United States Virgin Islands

(029, 660), -- Anguilla
(029, 028), -- Antigua and Barbuda
(029, 533), -- Aruba
(029, 044), -- Bahamas
(029, 052), -- Barbados
(029, 092), -- British Virgin Islands
(029, 136), -- Cayman Islands
(029, 192), -- Cuba
(029, 212), -- Dominica
(029, 214), -- Dominican Republic
(029, 308), -- Grenada
(029, 312), -- Guadeloupe
(029, 332), -- Haiti
(029, 388), -- Jamaica
(029, 474), -- Martinique
(029, 500), -- Montserrat
(029, 530), -- Netherlands Antilles
(029, 630), -- Puerto Rico
(029, 652), -- Saint-Barthélemy
(029, 659), -- Saint Kitts and Nevis
(029, 662), -- Saint Lucia
(029, 663), -- Saint Martin (French part)
(029, 670), -- Saint Vincent and the Grenadines
(029, 780), -- Trinidad and Tobago
(029, 796), -- Turks and Caicos Islands
(029, 850), -- United States Virgin Islands


-- 019 Americas
-- 419 Latin America and the Caribbean
-- 013 Central America

(019, 084), -- Belize
(019, 188), -- Costa Rica
(019, 222), -- El Salvador
(019, 320), -- Guatemala
(019, 340), -- Honduras
(019, 484), -- Mexico
(019, 558), -- Nicaragua
(019, 591), -- Panama

(419, 084), -- Belize
(419, 188), -- Costa Rica
(419, 222), -- El Salvador
(419, 320), -- Guatemala
(419, 340), -- Honduras
(419, 484), -- Mexico
(419, 558), -- Nicaragua
(419, 591), -- Panama

(013, 084), -- Belize
(013, 188), -- Costa Rica
(013, 222), -- El Salvador
(013, 320), -- Guatemala
(013, 340), -- Honduras
(013, 484), -- Mexico
(013, 558), -- Nicaragua
(013, 591), -- Panama


-- 019 Americas
-- 419 Latin America and the Caribbean
-- 005 South America

(019, 032), -- Argentina
(019, 068), -- Bolivia
(019, 076), -- Brazil
(019, 152), -- Chile
(019, 170), -- Colombia
(019, 218), -- Ecuador
(019, 238), -- Falkland Islands (Malvinas)
(019, 254), -- French Guiana
(019, 328), -- Guyana
(019, 600), -- Paraguay
(019, 604), -- Peru
(019, 740), -- Suriname
(019, 858), -- Uruguay
(019, 862), -- Venezuela (Bolivarian Republic of)

(419, 032), -- Argentina
(419, 068), -- Bolivia
(419, 076), -- Brazil
(419, 152), -- Chile
(419, 170), -- Colombia
(419, 218), -- Ecuador
(419, 238), -- Falkland Islands (Malvinas)
(419, 254), -- French Guiana
(419, 328), -- Guyana
(419, 600), -- Paraguay
(419, 604), -- Peru
(419, 740), -- Suriname
(419, 858), -- Uruguay
(419, 862), -- Venezuela (Bolivarian Republic of)

(005, 032), -- Argentina
(005, 068), -- Bolivia
(005, 076), -- Brazil
(005, 152), -- Chile
(005, 170), -- Colombia
(005, 218), -- Ecuador
(005, 238), -- Falkland Islands (Malvinas)
(005, 254), -- French Guiana
(005, 328), -- Guyana
(005, 600), -- Paraguay
(005, 604), -- Peru
(005, 740), -- Suriname
(005, 858), -- Uruguay
(005, 862), -- Venezuela (Bolivarian Republic of)


-- 019 Americas
-- 419 Latin America and the Caribbean
-- 021 Northern America

(019, 060), -- Bermuda
(019, 124), -- Canada
(019, 304), -- Greenland
(019, 666), -- Saint Pierre and Miquelon
(019, 840), -- United States of America

(419, 060), -- Bermuda
(419, 124), -- Canada
(419, 304), -- Greenland
(419, 666), -- Saint Pierre and Miquelon
(419, 840), -- United States of America

(021, 060), -- Bermuda
(021, 124), -- Canada
(021, 304), -- Greenland
(021, 666), -- Saint Pierre and Miquelon
(021, 840), -- United States of America


-- 142 Asia
-- 143 Central Asia

(142, 398), -- Kazakhstan
(142, 417), -- Kyrgyzstan
(142, 762), -- Tajikistan
(142, 795), -- Turkmenistan
(142, 860), -- Uzbekistan

(143, 398), -- Kazakhstan
(143, 417), -- Kyrgyzstan
(143, 762), -- Tajikistan
(143, 795), -- Turkmenistan
(143, 860), -- Uzbekistan


-- 142 Asia
-- 030 Eastern Asia

(142, 156), -- China
(142, 344), -- Hong Kong
(142, 446), -- Macao
(142, 408), -- Dem. People's Republic of Korea
(142, 392), -- Japan
(142, 496), -- Mongolia
(142, 410), -- Republic of Korea

(030, 156), -- China
(030, 344), -- Hong Kong
(030, 446), -- Macao
(030, 408), -- Dem. People's Republic of Korea
(030, 392), -- Japan
(030, 496), -- Mongolia
(030, 410), -- Republic of Korea


-- 142 Asia
-- 034 Southern Asia

(142, 004), -- Afghanistan
(142, 050), -- Bangladesh
(142, 064), -- Bhutan
(142, 356), -- India
(142, 364), -- Iran, Islamic Republic of
(142, 462), -- Maldives
(142, 524), -- Nepal
(142, 586), -- Pakistan
(142, 144), -- Sri Lanka

(034, 004), -- Afghanistan
(034, 050), -- Bangladesh
(034, 064), -- Bhutan
(034, 356), -- India
(034, 364), -- Iran, Islamic Republic of
(034, 462), -- Maldives
(034, 524), -- Nepal
(034, 586), -- Pakistan
(034, 144), -- Sri Lanka


-- 142 Asia
-- 035 South-Eastern Asia

(142, 096), -- Brunei Darussalam
(142, 116), -- Cambodia
(142, 360), -- Indonesia
(142, 418), -- Laos
(142, 458), -- Malaysia
(142, 104), -- Myanmar
(142, 608), -- Philippines
(142, 702), -- Singapore
(142, 764), -- Thailand
(142, 626), -- Timor-Leste
(142, 704), -- Viet Nam

(035, 096), -- Brunei Darussalam
(035, 116), -- Cambodia
(035, 360), -- Indonesia
(035, 418), -- Laos
(035, 458), -- Malaysia
(035, 104), -- Myanmar
(035, 608), -- Philippines
(035, 702), -- Singapore
(035, 764), -- Thailand
(035, 626), -- Timor-Leste
(035, 704), -- Viet Nam


-- 142 Asia
-- 145 Western Asia

(142, 051), -- Armenia
(142, 031), -- Azerbaijan
(142, 048), -- Bahrain
(142, 196), -- Cyprus
(142, 268), -- Georgia
(142, 368), -- Iraq
(142, 376), -- Israel
(142, 400), -- Jordan
(142, 414), -- Kuwait
(142, 422), -- Lebanon
(142, 275), -- Palestinian Territory
(142, 512), -- Oman
(142, 634), -- Qatar
(142, 682), -- Saudi Arabia
(142, 760), -- Syrian Arab Republic
(142, 792), -- Turkey
(142, 784), -- United Arab Emirates
(142, 887), -- Yemen

(145, 051), -- Armenia
(145, 031), -- Azerbaijan
(145, 048), -- Bahrain
(145, 196), -- Cyprus
(145, 268), -- Georgia
(145, 368), -- Iraq
(145, 376), -- Israel
(145, 400), -- Jordan
(145, 414), -- Kuwait
(145, 422), -- Lebanon
(145, 275), -- Palestinian Territory
(145, 512), -- Oman
(145, 634), -- Qatar
(145, 682), -- Saudi Arabia
(145, 760), -- Syrian Arab Republic
(145, 792), -- Turkey
(145, 784), -- United Arab Emirates
(145, 887), -- Yemen


-- 150 Europe
-- 151 Eastern Europe

(150, 112), -- Belarus
(150, 100), -- Bulgaria
(150, 203), -- Czech Republic
(150, 348), -- Hungary
(150, 498), -- Moldova
(150, 616), -- Poland
(150, 642), -- Romania
(150, 643), -- Russian Federation
(150, 703), -- Slovakia
(150, 804), -- Ukraine

(151, 112), -- Belarus
(151, 100), -- Bulgaria
(151, 203), -- Czech Republic
(151, 348), -- Hungary
(151, 498), -- Moldova
(151, 616), -- Poland
(151, 642), -- Romania
(151, 643), -- Russian Federation
(151, 703), -- Slovakia
(151, 804), -- Ukraine


-- 150 Europe
-- 154 Northern Europe

(150, 248), -- Åland Islands
-- (150, 830), -- Channel slands
(150, 208), -- Denmark
(150, 233), -- Estonia
(150, 234), -- Faeroe Islands
(150, 246), -- Finland
(150, 831), -- Gurnsey
(150, 352), -- Iceland
(150, 372), -- Ireland
(150, 833), -- Isle of Man
(150, 832), -- Jerse
(150, 428), -- Latvia
(150, 440), -- Lithuania
(150, 578), -- Norway
(150, 744), -- Svalbard and Jan Mayen Islands
(150, 752), -- Sweden
(150, 826), -- UK

(154, 248), -- Åland Islands
-- (154, 830), -- Channel slands
(154, 208), -- Denmark
(154, 233), -- Estonia
(154, 234), -- Faeroe Islands
(154, 246), -- Finland
(154, 831), -- Gurnsey
(154, 352), -- Iceland
(154, 372), -- Ireland
(154, 833), -- Isle of Man
(154, 832), -- Jerse
(154, 428), -- Latvia
(154, 440), -- Lithuania
(154, 578), -- Norway
(154, 744), -- Svalbard and Jan Mayen Islands
(154, 752), -- Sweden
(154, 826), -- UK


-- 150 Europe
-- 039 Southern Europe

(150, 008), -- Albania
(150, 020), -- Andorra
(150, 070), -- Bosnia and Herzegovina
(150, 191), -- Croatia
(150, 292), -- Gibraltar
(150, 300), -- Greece
(150, 336), -- Holy See
(150, 380), -- Italy
(150, 470), -- Malta
(150, 499), -- Montenegro
(150, 620), -- Portugal
(150, 674), -- San Marino
(150, 688), -- Serbia
(150, 705), -- Slovenia
(150, 724), -- Spain
(150, 807), -- Macedonia

(039, 008), -- Albania
(039, 020), -- Andorra
(039, 070), -- Bosnia and Herzegovina
(039, 191), -- Croatia
(039, 292), -- Gibraltar
(039, 300), -- Greece
(039, 336), -- Holy See
(039, 380), -- Italy
(039, 470), -- Malta
(039, 499), -- Montenegro
(039, 620), -- Portugal
(039, 674), -- San Marino
(039, 688), -- Serbia
(039, 705), -- Slovenia
(039, 724), -- Spain
(039, 807), -- Macedonia


-- 150 Europe
-- 155 Western Europe

(150, 040), -- Austria
(150, 056), -- Belgium
(150, 250), -- France
(150, 276), -- Germany
(150, 438), -- Liechtenstein
(150, 442), -- Luxembourg
(150, 492), -- Monaco
(150, 528), -- Netherlands
(150, 756), -- Switzerland

(155, 040), -- Austria
(155, 056), -- Belgium
(155, 250), -- France
(155, 276), -- Germany
(155, 438), -- Liechtenstein
(155, 442), -- Luxembourg
(155, 492), -- Monaco
(155, 528), -- Netherlands
(155, 756), -- Switzerland


-- 009 Oceania
-- 053 Australia and New Zealand

(009, 036), -- Australia
(009, 554), -- New Zealand
(009, 574), -- Norfolk Island

(053, 036), -- Australia
(053, 554), -- New Zealand
(053, 574), -- Norfolk Island


-- 009 Oceania
-- 054 Melanesia

(009, 242), -- Fiji
(009, 540), -- New Caledonia
(009, 598), -- Papua New Guinea
(009, 090), -- Solomon Islands
(009, 548), -- Vanuatu

(054, 242), -- Fiji
(054, 540), -- New Caledonia
(054, 598), -- Papua New Guinea
(054, 090), -- Solomon Islands
(054, 548), -- Vanuatu


-- 009 Oceania
-- 057 Micronesia

(009, 316), -- Guam
(009, 296), -- Kiribati
(009, 584), -- Marshall Islands
(009, 583), -- Micronesia
(009, 520), -- Nauru
(009, 580), -- Northern Mariana Islands
(009, 585), -- Palau

(057, 316), -- Guam
(057, 296), -- Kiribati
(057, 584), -- Marshall Islands
(057, 583), -- Micronesia
(057, 520), -- Nauru
(057, 580), -- Northern Mariana Islands
(057, 585), -- Palau


-- 009 Oceania
-- 061 Polynesia

(009, 016), -- American Samoa
(009, 184), -- Cook Islands
(009, 258), -- French Polynesia
(009, 570), -- Niue
(009, 612), -- Pitcairn
(009, 882), -- Samoa
(009, 772), -- Tokelau
(009, 776), -- Tonga
(009, 798), -- Tuvalu
(009, 876), -- Wallis and Futuna Islands

(061, 016), -- American Samoa
(061, 184), -- Cook Islands
(061, 258), -- French Polynesia
(061, 570), -- Niue
(061, 612), -- Pitcairn
(061, 882), -- Samoa
(061, 772), -- Tokelau
(061, 776), -- Tonga
(061, 798), -- Tuvalu
(061, 876); -- Wallis and Futuna Islands

COMMIT;
