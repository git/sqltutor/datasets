BEGIN;

SELECT sqltutor.init_dataset ('countries');
SELECT sqltutor.add_ds_source('countries', 2008, 'http://wikipedia.org/');
SELECT sqltutor.add_ds_table ('countries', 1, 
                              'country_codes', 'a2, a3, num, name');
SELECT sqltutor.add_ds_table ('countries', 2, 'un_regions', 'region, name');
SELECT sqltutor.add_ds_table ('countries', 3, 
                              'un_regions_countries', 'region, country');
SELECT sqltutor.add_ds_table ('countries', 4, 
                 'national_capitals', 'country, city, population, area');

COMMIT;
