DROP VIEW IF EXISTS sqltutor_data.laureati;
CREATE VIEW sqltutor_data.laureati (rok, obor, laureat)
AS
SELECT year, CASE subject
               WHEN 'Chemistry'  THEN 'Chemie'
               WHEN 'Economics'  THEN 'Ekomonie'
               WHEN 'Literature' THEN 'Literatura'
               WHEN 'Medicine'   THEN 'Medicína'
               WHEN 'Peace'      THEN 'Mír'
               WHEN 'Physics'    THEN 'Fyzika'
               ELSE subject
             END,
       winner
  FROM sqltutor_data.nobel;

BEGIN;

SET search_path TO sqltutor;

SELECT sqltutor.init_dataset ('nobelova_cena');
SELECT sqltutor.add_ds_source('nobelova_cena', 2008, 'http://sqlzoo.net/');
SELECT sqltutor.add_ds_table ('nobelova_cena', 1, 'laureati', 
                              'rok, obor, laureat');


SELECT insert_problem ('nobelova_cena', 151, 1, 'select');
SELECT insert_question('nobelova_cena', 151, 1, 'cs', 
'Uveďte všechny údaje o Nobelových cenách za rok 1950.
');

SELECT insert_problem ('nobelova_cena', 152, 2, 'select');
SELECT insert_question('nobelova_cena', 152, 1, 'cs', 
'Kdo získal Nobelovu cenu za literaturu v roce 1962?
');

SELECT insert_problem ('nobelova_cena', 153, 2, 'select');
SELECT insert_question('nobelova_cena', 153, 1, 'cs', 
'Uveďte rok a obor ve kterém získal Nobelovu cenu ''Albert Einstein''.
');

SELECT insert_problem ('nobelova_cena', 154, 2, 'select');
SELECT insert_question('nobelova_cena', 154, 1, 'cs', 
'Uveďte jména nositelů Nobelovy ceny míru od roku 2000 (včetně).
');

SELECT insert_problem ('nobelova_cena', 155, 2, 'select');
SELECT insert_question('nobelova_cena', 155, 1, 'cs', 
'Uveďte všechny údaje (rok, obor, laureat) o laureátech Nobelovy
ceny za literaturu z let 1980 až 1989.
');

SELECT insert_problem ('nobelova_cena', 156, 3, 'select');
SELECT insert_question('nobelova_cena', 156, 1, 'cs', 
'Uveďte všechny údaje (rok, obor, laureat) o "prezidentských"
laureátech (''Theodore Roosevelt'', ''Woodrow Wilson'', ''Jed Bartlet'',
''Jimmy Carter'', ''Barack H. Obama'')
');

SELECT insert_problem ('nobelova_cena', 159, 6, 'aggregate|subselect');
SELECT insert_question('nobelova_cena', 159, 1, 'cs', 
'Kdo získal Nobelovu cenu více než jedenkrát? Uveďte rok, obor a
jméno laureáta.
');

SELECT insert_problem ('nobelova_cena', 160, 13, 'aggregate|subselect|derivedtable');
SELECT insert_question('nobelova_cena', 160, 1, 'cs', 
'Kdo získal Nobelovu cenu ve více oborech? Uveďte rok, obor a
jméno laureáta.
');

COMMIT;

