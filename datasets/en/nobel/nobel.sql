BEGIN;

SET search_path TO sqltutor;

SELECT insert_problem ('nobel', 151, 1, 'select');
SELECT insert_question('nobel', 151, 1, 'en',
'Show Nobel prizes for 1950.
');
SELECT insert_answer  ('nobel', 151, 1,
'SELECT year, subject, winner
  FROM nobel
 WHERE year = 1950
');

SELECT insert_problem ('nobel', 152, 2, 'select');
SELECT insert_question('nobel', 152, 1, 'en',
'Show who won the 1962 prize for Literature.
');
SELECT insert_answer  ('nobel', 152, 1,
'SELECT winner
  FROM nobel
 WHERE year = 1962
   AND subject = ''Literature''
');

SELECT insert_problem ('nobel', 153, 2, 'select');
SELECT insert_question('nobel', 153, 1, 'en',
'Show the year and subject that won ''Albert Einstein'' his prize.
');
SELECT insert_answer  ('nobel', 153, 1,
'SELECT year, subject
  FROM nobel
 WHERE winner=''Albert Einstein''
');

SELECT insert_problem ('nobel', 154, 2, 'select');
SELECT insert_question('nobel', 154, 1, 'en',
'Give the name of the ''Peace'' winners since the year 2000, including 2000.
');
SELECT insert_answer  ('nobel', 154, 1,
'SELECT winner
  FROM nobel
 WHERE subject=''Peace''
   AND year >= 2000
');

SELECT insert_problem ('nobel', 155, 2, 'select');
SELECT insert_question('nobel', 155, 1, 'en',
'Show all details (year, subject, winner) of the Literature prize
winners for 1980 to 1989 inclusive.
');
SELECT insert_answer  ('nobel', 155, 1,
'SELECT year, subject, winner
  FROM nobel
 WHERE subject = ''Literature''
   AND year BETWEEN 1980 AND 1989
');

SELECT insert_problem ('nobel', 156, 2, 'select');
SELECT insert_question('nobel', 156, 1, 'en',
'Show all details of the presidential winners: 
(''Theodore Roosevelt'', ''Woodrow Wilson'', ''Jed Bartlet'', ''Jimmy Carter'',
''Barack H. Obama'')
');
SELECT insert_answer  ('nobel', 156, 1,
'SELECT * FROM nobel
 WHERE winner IN (''Theodore Roosevelt'', ''Woodrow Wilson'', 
                  ''Jed Bartlet'', ''Jimmy Carter'', ''Barack H. Obama'')
');

SELECT insert_problem ('nobel', 159, 6, 'aggregate|subselect');
SELECT insert_question('nobel', 159, 1, 'en', 
'Who won Nobel prize more than once? Select year, subject and winner.
');
SELECT insert_answer  ('nobel', 159, 1,
'SELECT year, subject, winner
  FROM nobel
 WHERE winner IN (SELECT winner
                     FROM nobel
                    GROUP BY winner
                  HAVING count(*) > 1);
');

SELECT insert_problem ('nobel', 160, 13, 'aggregate|subselect|derivedtable');
SELECT insert_question('nobel', 160, 1, 'en', 
'Who won Nobel prize in more subjects? Select year, subject and winner.
');
SELECT insert_answer  ('nobel', 160, 2,
'SELECT A.year, A.subject, A.winner
  FROM nobel AS A
       JOIN 
       nobel AS B
       ON A.winner=B.winner
 GROUP BY A.year, A.subject, A.winner
HAVING count(distinct B.subject) > 1
 ORDER BY A.year;
');
SELECT insert_answer  ('nobel', 160, 1,
'SELECT DISTINCT A.year, A.subject, A.winner
  FROM nobel AS A
       JOIN
       nobel AS B
       ON A.winner=B.winner AND A.subject<>B.subject
');

COMMIT;
