/* 
   This file is public domain.

   The data is a matter of public record, and comes from
   http://www.gnu.org/software/sqltutor   
 */


delete from  sqltutor.answers  
       where dataset_id = (select dataset_id 
                             from sqltutor.datasets 
                            where dataset='nobelova_cena');

insert into  sqltutor.answers 
     select (select dataset_id
               from sqltutor.datasets
              where dataset='nobelova_cena'),
             problem_id, priority, answer
       from  sqltutor.answers 
      where  dataset_id = (select dataset_id 
                             from sqltutor.datasets 
                            where dataset='nobel');

update sqltutor.answers set answer = 
       replace(
       replace(
       replace(
       replace(
       replace(
       replace(
       replace(
       replace(
       replace(
       replace(answer,
       'Chemistry',  'Chemie'),
       'Economics',  'Ekomonie'),
       'Literature', 'Literatura'),
       'Medicine',   'Medicína'),
       'Peace',      'Mír'),
       'Physics',    'Fyzika'),
       'year',       'rok'),
       'subject',    'obor'),
       'winner',     'laureat'),
       'nobel',      'laureati')
       where dataset_id  = (select dataset_id 
                             from sqltutor.datasets 
                            where dataset='nobelova_cena');
