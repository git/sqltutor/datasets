/* This file is public domain.
 
   The data is a matter of public record, and comes from
    http://en.wikibooks.org/wiki/SQL_Exercises/The_computer_store
 */


/* Sqltutor data belong to a schema "sqltutor_data" */

BEGIN;

SET search_path TO sqltutor_data;

DROP TABLE IF EXISTS products CASCADE;
DROP TABLE IF EXISTS manufacturers CASCADE;


CREATE TABLE manufacturers (
   code integer PRIMARY KEY NOT NULL,
   name text NOT NULL 
);

CREATE TABLE products (
   code integer PRIMARY KEY NOT NULL,
   name text NOT NULL ,
   price real NOT NULL ,
   manufacturer integer NOT NULL
      CONSTRAINT fk_manufacturers_code REFERENCES manufacturers(code)
);


INSERT INTO manufacturers(code,name) VALUES
       (1,'Sony'),
       (2,'Creative Labs'),
       (3,'Hewlett-Packard'),
       (4,'Iomega'),
       (5,'Fujitsu'),
       (6,'Winchester');

INSERT INTO products(code,name,price,manufacturer) VALUES
       (1,'Hard drive',240,5),
       (2,'Memory',120,6),
       (3,'ZIP drive',150,4),
       (4,'Floppy disk',5,6),
       (5,'Monitor',240,1),
       (6,'DVD drive',180,2),
       (7,'CD drive',90,2),
       (8,'Printer',270,3),
       (9,'Toner cartridge',66,3),
       (10,'DVD burner',180,2);



/* Tutorials, datasets, problems, questions and answers are defined in
   a schema "sqltutor" */

SET search_path TO sqltutor;


/* Dataset definition */

SELECT init_dataset ('computer_store');
SELECT add_ds_source('computer_store', 2010,
          'http://en.wikibooks.org/wiki/SQL_Exercises/The_computer_store');
SELECT add_ds_table ('computer_store', 1, 'manufacturers', 'code, name');
SELECT add_ds_table ('computer_store', 2, 'products',
                                     'code, name, price, manufacturer');


/* For each "problem" we can formulate one or more quesitions and
   one or more answers (sql queries) */

-- dataset name, internal problem id, points, category (currently unused)
SELECT insert_problem ('computer_store', 10, 1, 'select');

-- dataset name, internal problem id, order, language, question text
SELECT insert_question('computer_store', 10, 1, 'en',
                       'Select the names and the prices of all the products 
                        in the store.');

--  dataset name, internal dataset id, order, language, sql answer
SELECT insert_answer  ('computer_store', 10, 1,
                       'SELECT name FROM products;');

-- ..........................................................................

-- Problem defines id, point rating and category
SELECT insert_problem ('computer_store', 30, 2, 'select');
SELECT insert_question('computer_store', 30, 1, 'en',
                       'Select the names of the products with a price less
                        than or equal to $200.');
SELECT insert_answer  ('computer_store', 30, 1,
                       'SELECT name FROM products WHERE price <= 200;');

-- Czech translation of problem id 30.  Answers (SQL code) are shared
-- among all translations
SELECT insert_question('computer_store', 30, 1, 'cs',
                       'Vypište jména všech produktů, jejichž cena
                        je menší nebo rovna 200 dolarů.');

-- ..........................................................................

-- When multiple questions and/or answers are defined for a given
-- problem, they are chosen at random. Order of columns in SQL query
-- is irelevant.
SELECT insert_problem ('computer_store', 40, 2, 'select');
SELECT insert_question('computer_store', 40, 1, 'en',
                       'Select all the products with a price between
                        $60 and $120.');
SELECT insert_question('computer_store', 40, 2, 'en',
                       'Select all the products with a price in the interwal
                        $60 and $120.');
SELECT insert_answer  ('computer_store', 40, 1,
'SELECT * FROM products
  WHERE price >= 60 AND price <= 120;');
SELECT insert_answer  ('computer_store', 40, 2,
'SELECT * FROM products
 WHERE price BETWEEN 60 AND 120;');

SELECT insert_question('computer_store', 40, 1, 'cs',
                       'Vypište všechny informace o produktech
                        jejichž ceny jsou v rozmezi
                        od 60 do 120 dolarů.');

-- ..........................................................................

SELECT insert_problem ('computer_store', 50, 2, 'select');
SELECT insert_question('computer_store', 50, 1, 'en',
                       'Select the name and price in cents
                       (i.e., the price must be multiplied by 100).');
SELECT insert_answer  ('computer_store', 50, 1,
                       'SELECT name, price * 100 FROM products;');

SELECT insert_question('computer_store', 50, 1, 'cs',
                       'Vypište jména produktů a ceny v centech 
                        (tj. cena v dolarech se musí vynásobit 100).');

-- ..........................................................................

SELECT insert_problem ('computer_store', 60, 2, 'select');
SELECT insert_question('computer_store', 60, 1, 'en',
                       'Compute the average price of all the products.');
SELECT insert_answer  ('computer_store', 60, 1,
                       'SELECT AVG(price) FROM products;');

SELECT insert_question('computer_store', 60, 1, 'cs',
                       'Jaká je průměrná cena všech produktů?.');

-- ..........................................................................

SELECT insert_problem ('computer_store', 70, 2, 'select');
SELECT insert_question('computer_store', 70, 1, 'en',
                       'Compute the average price of all products
                       with manufacturer code equal to 2.');
SELECT insert_answer  ('computer_store', 70, 1,
                       'SELECT AVG(price) FROM products WHERE manufacturer=2;');

SELECT insert_question('computer_store', 70, 1, 'cs',
                       'Vypočtěte průměrnou cenu produktů výrobce (manufacturer)
                        s kódem 2.');

-- ..........................................................................

SELECT insert_problem ('computer_store', 72, 3, 'select');
SELECT insert_question('computer_store', 72, 1, 'en',
                       'Compute the number of products with a price larger
                        than or equal to $180.');
SELECT insert_answer  ('computer_store', 72, 1,
                       'SELECT COUNT(*) FROM products WHERE price >= 180;');

SELECT insert_question('computer_store', 72, 1, 'cs',
                       'Kolik je produktů s cenou vyšší nebo rovnou 180 dolarů?');

-- ..........................................................................

SELECT insert_problem ('computer_store', 74, 3, 'select');
SELECT insert_question('computer_store', 74, 1, 'en',
                       'Select all the data from the products, including
                        all the data for each product''s manufacturer.');
SELECT insert_answer  ('computer_store', 74, 1,
'SELECT *
  FROM products INNER JOIN manufacturers
       ON products.manufacturer = manufacturers.code;');
-- SELECT insert_answer  ('computer_store', 74, 2,
-- 'SELECT *
--   FROM products JOIN manufacturers
--        ON products.manufacturer = manufacturers.code;');

SELECT insert_question('computer_store', 74, 1, 'cs',
                       'Vypište všechny údaje o všech produktech, včetně
                        všech informací o výrobci každého produktu.');

-- ..........................................................................

SELECT insert_problem ('computer_store', 76, 4, 'select');
SELECT insert_question('computer_store', 76, 1, 'en',
                       'Select the product name, price, and manufacturer
                        name of all the products.');
SELECT insert_answer  ('computer_store', 76, 1,
'SELECT products.name, price, manufacturers.name
  FROM products INNER JOIN manufacturers
       ON products.manufacturer = manufacturers.code;');

-- ..........................................................................

SELECT insert_problem ('computer_store', 78, 4, 'select');
SELECT insert_question('computer_store', 78, 1, 'en',
                       'Select the average price of each manufacturer''s
                        products, showing the manufacturer''s name.');
SELECT insert_answer  ('computer_store', 78, 1,
'SELECT AVG(price), manufacturers.name
  FROM products INNER JOIN manufacturers
       ON products.manufacturer = manufacturers.code
 GROUP BY manufacturers.name;');

SELECT insert_question('computer_store', 78, 1, 'cs',
                       'Pro každého výrobce vypište jeho jméno a průměrnou
                        cenu jeho produktů.');

-- ..........................................................................

SELECT insert_problem ('computer_store', 80, 6, 'select');
SELECT insert_question('computer_store', 80, 1, 'en',
                       'Select the names and average prices of manufacturer
                        whose products
                        have an average price larger than or equal to $150.');
SELECT insert_answer  ('computer_store', 80, 1,
'SELECT AVG(price), manufacturers.name
  FROM products INNER JOIN manufacturers
       ON products.manufacturer = manufacturers.code
 GROUP BY manufacturers.name
HAVING AVG(price) >= 150;');

SELECT insert_question('computer_store', 80, 1, 'cs',
                       'Vypište průměrné ceny a jména výrobců,
                        jejichž produkty mají průměrnou
                        cenu vyšší nebo rovnu 150 dolarů.');

-- ..........................................................................

SELECT insert_problem ('computer_store', 82, 5, 'select');
SELECT insert_question('computer_store', 82, 1, 'en',
                       'Select the name and price of the cheapest product.');
SELECT insert_answer  ('computer_store', 82, 1,
'SELECT name, price
  FROM products
 WHERE price = (SELECT MIN(price) FROM products);');

SELECT insert_question('computer_store', 82, 1, 'cs',
                       'Jaké je jméno a cena nejlevnějšího produktu?');

-- ..........................................................................

SELECT insert_problem ('computer_store', 84, 3, 'select');
SELECT insert_question('computer_store', 84, 1, 'en',
                       'Select the name of each manufacturer along with the name
                        and price of its most expensive product.');
SELECT insert_answer  ('computer_store', 84, 1, '
SELECT A.name, A.price, F.name
   FROM products A INNER JOIN manufacturers F
   ON A.manufacturer = F.code
     AND A.price =
     (
       SELECT MAX(A.price)
         FROM products A
         WHERE A.manufacturer = F.code
     );');

SELECT insert_question('computer_store', 84, 1, 'cs',
                       'Vypište jméno každého výrobce spolu se jménem a cenou
                        jeho nejdražšího produktu.');

-- ..........................................................................



/* Tutorial definition */

/*
SELECT init_tutorial ('en', 'Demo');
SELECT insert_dataset('Demo', 'en', 'computer_store');

SELECT init_tutorial ('cs', 'Demo');
SELECT insert_dataset('Demo', 'cs', 'computer_store');
*/

/* Tutorials can be easily deleted. Datasets are independent on
   tutorials and they are not affected in any way when a tutorial
   referencing to them is deleted. */

-- SELECT delete_tutorial('cs', 'Demo');

COMMIT;
