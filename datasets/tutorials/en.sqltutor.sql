SELECT sqltutor.init_tutorial('en', 'SQLtutor');

SELECT sqltutor.insert_dataset('SQLtutor', 'en', 'nobel' );
SELECT sqltutor.insert_dataset('SQLtutor', 'en', 'buses' );
SELECT sqltutor.insert_dataset('SQLtutor', 'en', 'movies');
SELECT sqltutor.insert_dataset('SQLtutor', 'en', 'bbc'   );
SELECT sqltutor.insert_dataset('SQLtutor', 'en', 'ttms'  );
SELECT sqltutor.insert_dataset('SQLtutor', 'en', 'ttws'  );
SELECT sqltutor.insert_dataset('SQLtutor', 'en', 'ttmd'  );

SELECT sqltutor.insert_dataset('SQLtutor', 'en', 'suppliers');
SELECT sqltutor.insert_dataset('SQLtutor', 'en', 'olympics' );
SELECT sqltutor.insert_dataset('SQLtutor', 'en', 'twitter'  );
